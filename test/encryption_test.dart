import 'package:flutter_test/flutter_test.dart';
import 'package:user_ui/utils/bk_encryption_helper.dart';

void main() {
  final encryptionHelper = BKEncryptionHelper();
  const publicKeyPem = """-----BEGIN PUBLIC KEY-----
MIICCgKCAgEAt2Aa+/XFJMmAVu7dG5GoOvms9ADziMRgtnGN3Rbejk23sxS4nyOI
ngwTtP1tp5pRH8pfOmYNh2YuqK5seYlVbPJOvjrsk+tPirEMGIVsMF6Qg4J44ubs
H00zAFAQ45Sjg6v2RczFIyU0N+tY20IE5+AHSxmydC+b3mvsBM2fYWOO4FbxGwov
+SLp2M+oNdjvD2SKn+fufVbSSqVvZtBWjfUEcsDNfrDy1Ue8G8+gcUw+VvDmW6eP
qFym2cod6DlqSOEsE1D8eUv+mPzKbnutp4n6/n4dnKQwByusH2+IQknLSEimadrD
OtpLVB0KmmScAD7BclPVlC+F2V9555UIIh026HBmRHD3316TidfQnrd4ZXhAnsiC
I259Dz+WPTq2wtYrjbCkbcyvgQ3Xztq9dfQZGbAx/2lKwTO3DBbvnaK9ZMIvulHr
X9fKwyg5wtXtYjfXok85Zeanzj2GTZL/mpf+xXMrPRwwhA40FL1VK2+hMwhLRh92
D3u+qGNr5pUHYGvAxnEuLppbSL7ehnkWiKou6IjQWKZARyHeuvZQYUie9wN9EzJY
9Ex/udwSTSFiOIgriB7zimg8aLnmeSqRhqhbyPsIvGzigJV3f+wXTiXmQsWUYadK
fhOihgwuMqqIkzxgqPmd75xqFcFf+avWCzKtW5Nc/UNsK9g+YAvRIT8CAwEAAQ==
-----END PUBLIC KEY-----""";

  const privateKeyPem = """-----BEGIN RSA PRIVATE KEY-----
MIIJKAIBAAKCAgEAt2Aa+/XFJMmAVu7dG5GoOvms9ADziMRgtnGN3Rbejk23sxS4
nyOIngwTtP1tp5pRH8pfOmYNh2YuqK5seYlVbPJOvjrsk+tPirEMGIVsMF6Qg4J4
4ubsH00zAFAQ45Sjg6v2RczFIyU0N+tY20IE5+AHSxmydC+b3mvsBM2fYWOO4Fbx
Gwov+SLp2M+oNdjvD2SKn+fufVbSSqVvZtBWjfUEcsDNfrDy1Ue8G8+gcUw+VvDm
W6ePqFym2cod6DlqSOEsE1D8eUv+mPzKbnutp4n6/n4dnKQwByusH2+IQknLSEim
adrDOtpLVB0KmmScAD7BclPVlC+F2V9555UIIh026HBmRHD3316TidfQnrd4ZXhA
nsiCI259Dz+WPTq2wtYrjbCkbcyvgQ3Xztq9dfQZGbAx/2lKwTO3DBbvnaK9ZMIv
ulHrX9fKwyg5wtXtYjfXok85Zeanzj2GTZL/mpf+xXMrPRwwhA40FL1VK2+hMwhL
Rh92D3u+qGNr5pUHYGvAxnEuLppbSL7ehnkWiKou6IjQWKZARyHeuvZQYUie9wN9
EzJY9Ex/udwSTSFiOIgriB7zimg8aLnmeSqRhqhbyPsIvGzigJV3f+wXTiXmQsWU
YadKfhOihgwuMqqIkzxgqPmd75xqFcFf+avWCzKtW5Nc/UNsK9g+YAvRIT8CAwEA
AQKCAgArY6ce5SlvqeofJ8fTpSRsR/WfirYVL3o+0SGjJa0leMg1rHp+1TaXRv5G
vgx7Mu1tG0JrHAipeAkkSplKLK+05qSxKFogKfaZN4lIKBHQZB/HrlCSR9epFGgz
8737S4lhN4g/PdOLnFr9vEc7IiTtBLpVD9CE41r7RwgCnvDOZ3NAK/JC1qdBSPyS
G5iOnTT7rGuMqKFqsOdzWC/C4RsJ3ebejDZTeuUKiD2/SuKIzGSXx8qJ91zrlni4
YbWv5B38/qKiM3B59vxYEMCJYeRWFzT3kLnK/aKLn87LZWWVYcai5OXTeDrnqw1V
6sU+gP3UpQS625FWzePa6ld6722L1liQXflwQUGOd1/Szhp9V7gHi8m/5aA1BI5k
rCFvD2kRTKsZZ0zUROUSYi2xHUhJKVv9S7YvQj4OzbZUeMfY1MmmevnyQ7EvSp6v
3FNSGpXCIArpQuBhs8GyAEs3+qfDN7vkBKZNW/PMuatESaRZ5ozqhhgsTEGE1baa
VceifSjwrDEI+KsDRWPHjBrkrbToViddDlomRFtOE1+c647CkBwAHfTG04WT+uqI
1OlTDznlx7UCe3gs1HqN8YTqWp58DHGKrrbdx+4DWfyUmJGtMIvyTqxiifIi9puo
Ia/WDP5iVXT3PpCbS3M5n+A5CEujZaomiYVHlRUpYv3HI5yi6QKCAQEA267Ym9RG
1KALgj3LqvdGVhTyhHrB/1PwAR92oSWJXCB8ElOErJgWinl8ebkQwlDgsVmdak6K
8LkZOCDFwK6NPageaUlMhiovXRBeY6Gxe780NACCpXBQQJ0vqQqKUZwqfxktnfuy
+DhpyNLL973Ikx5SV/tXozyy5qRkVQ9c6UZF1G8VUi/a1ig8PR1sxgK2HhpdCZ3E
MumRBhR+91NFj+yjAbYkQrQ+Ht8Jh9h27/4czNst9HS7f+Y40SuKW+VD54jyEgfw
DLlQpv/TLmrJ4si0by2bVoC7Ni1ZIbg18o/Ahm8+84xi4yGzLcbSN3sryRfn+XA/
gT9aaUk0hZBKmwKCAQEA1bCxxYR/0dWUJ6SJKsOYkA1mxZff3eWq67/NcCG0mTLu
mLd2YRIcB7LaFMVcGbBPkSUrBbHhQY6NQk7WtBWNt928NXcoDYBTuojTn5yRS8l/
hYgoXeaolwuIC70MzbuhGnJmUHeZji7u6VgjoPsn2DBh85LVstqzDRBcHpLQWRJg
+13StGF2ej1eKSDjJRwTeYvEX7qI/DPjLrsGZ/TE+HXWLbVa7lDnwxrwjpCjyGnB
EpUKqzgYvXURYiMneQdPw2OijpN4QDQYRkt0ddtOZAPWgNE8UGnuChp7KUseVUaj
usqoHt32YNqNN3+3PItpzLWXMwu1tnmCeEyD58pMLQKCAQASm3GvaUCCm/e9lVxd
48nqWqXcAMXTyZlHjxGuPo6u5fV8W+Sd9dfa7MVVTg6UVuNhQjTqHzL3hsYTEfuO
AXrnIQlKY7H+ny4Z1NwZ1kVBNQXH7c8jEitJ/cZerAzhMrgKwegyPHKBQc37+5bZ
KhMGGwhgeWKH6glBLeVtqvp0q8YYYzxMFM+VWh0YFBj1gJ9KV3NP8DQBF/V3rV6/
tibrNODtsS7LE5c7aCrXfcc9Nqnb1CjFTunewHJJjUWP2RByWRAf5No5Sa0CKCMM
CHGHKvbVf+hrYEX7JcYp6/9txy1Idb3ARUDO+jjCBNgjaORhiQvV/eLzIJmY12GC
eqFHAoIBAA+8VTrwSOFQ0vogWaF2idOBySGfz3JtqSp3E0/Ai6YEZCGG0QbQ6JOA
jiKdbezOWO2dSQ/AS5AiSTCq0ZCtTaROhb+CKMblvkSsMrk8NE7aZbOVlTNk+uE0
ji4fG8RCnthtuC8Qv5QCzMEOJoGCPSrkVTI0i9wB0tGRdNcjhIgqnE2mWQ/DZZAW
2Mo6i79908lNi4ZpHBFGWOJmD1C0a5TISJ9RDYMjHg31++TjrcviTb9qjkCRfvDk
oAUUBaIZ8bu3qI7LOT2xGGCEyeyr25ft2Gvf/IsHYeoIjS07RN6OtxvYNI0hVzVG
osOFeh7RPVc/wASYRidLx1nIeKYm9XkCggEBAMaUmsdhUCMcCpvLpuAqR8Xz+MKT
t/33A2fiZeKQ80r+nAs/2TNwmrqE3lZyApB/16kpVbudHdr/XUX9qxEDkOrpdG85
8ORzPknvb1C3b+lqv3YqP8ECg5NBcnng+0bZFJRyabIaFJZt0Jpo/GrCBlJfaoio
MLVPtWv1rkTdDLIIKoq3nYKhvFy5nn3Lf+1i3Mw33X0Ptp1hude8JIJ8lbj8MfpO
60YyIYlhjcm7QxIMSAULgoMvKEgo9ZyM5fhtE6kU28oewPYyOq1FNKDKHFZ1lzcG
CzXH4uwq5/nnDoZiuIwFUKHd76N1n4Yw0eAuhGKKZ9Xx8UWc59/JAz/j654=
-----END RSA PRIVATE KEY-----""";

  const plaintextMessage = "Data to encrypt";
  const messageEncryptedInGolang = 'Qskq7rPYPjM6k6Kh+v8ak/ofb5gz'
      'i2HUbtUAH8xiVwfdQesekrMTsiS9sVKY68cQ5f9XB+06Ev18eF+DtF+je'
      'fEjmQjSkpDhNhrkbNQFAB0O/K6uGZgv+APi5+mDs7aAWalTu0y/NktbnI'
      'cskUctgCo3VIUclM3Itaxb/oWy9rv59SnrZA4yQDFamzHCJNKtp+1MFn8'
      'hKoQFRCDJ1OQy+BcDfd4GFP0YMA+bEamPdtkG6b0bSs6htqGtcyVTJvLd'
      'UqpLQbCi8GeCnlIqYRd72EDw4hDBkp4m1I8jzB5TRi2gWhjkuoSNqryIw'
      'XXcHD2ba32GI6RAVYd7AritWB+/fmgLLNDKYV+W+FMiPKugHpStoZIZWH'
      '/AS/838LHm2vbvXDWrxuH0roqKjiGut46VbwZtVJQPrXNB7bQJ1zaul6J'
      'Lt39w/Qp2TVUj0A/mV6iTaot6Sl6Qtu4zX8FmSnSnXS7gyM9FFk0T9nh8'
      'bC1p+8QAkmQOljnAY9FfuX+1ztYpVaABssQGVVyEZwzPn3dbVkZlgFVmm'
      'XQznWPLSJWQIFXoNJ6GrtCQSN9AzLNVw5wlbGAAWCYGUfdSlxbbCtwHp9'
      'Zn/m4P/70EHmfIqCanHyWhQeFk11q+uEX7gngi7lboql4uBN6MYqGKm5q'
      'K9/Fz0MCAxprlPLojZ0Nx9EuB2ZM=';
  const pssSignatureGolang = 'YHX8s2sSlq6MG/OumQENMqqdOmO+u3vhP5'
      '3P3WT7ltbWRnSIQ6FE8pAg0YTuccCQAB2qsRDTh+oEPdIe0qgYf5tHVTd'
      'hdi3JoT1hrznlNwLSoi0mbq0V7iK5hcPe+75FxUrCnHwBeVQbqNZke//I'
      'GhRDXJl0FxtP2GFtWrEW4wE6AeC5Ll5ECixc7DFyaCZZe0N8dpc/S5Xa5'
      'nz//ZOWkeMQ9GFS3NLlCfLs9Ma+/9TIi7CKhTGq04UAuT5CvfsKL5FHTS'
      'DvsobwYWdJlWUeVJn89zCBlLKmQ8wIcLj8XM0lyF4b+mbnZ5HxW85Jz4w'
      'Q1zc3MuHNYEj2fVFZRQGbcdspNQ1npcW963uRpgMQ710xs6anPh1TS1AA'
      '26rcJw+Hvd4nFksOymdgg1Fk3H8wTJsU2aBaRIDAbUgbZAkYyeLvHI2lg'
      'mLZfsFV1CdMJYe5dINpc12YBba2nJUVbMoaNfXkRRN+nTnHlSyReb4mPb'
      'FBzu/3vGIk5srOXP93NGfwAmOL53Bg7rUe57ud0bt0t2188RNDx0bDFoK'
      'she3Avt4p9w2T6kkIUKU7Mp61Pkvl8T1qAPFl+0JJann8boxzctXe7ZiW'
      'e1WYX7kc6/ABGnexmF/X5x00Nl6VtPxxNh37BrDw+lBJWFX3ZOk7DlPop'
      'MKz/s8q58GvWcLcju0ugQI=';

  const longPlaintextMessage =
      """Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Pellentesque aliquam sapien at congue lobortis. Fusce dolor nibh,
porta eu lectus ut, scelerisque sagittis tortor. Pellentesque
iaculis velit ut nisi vulputate congue. Vestibulum dui erat,
venenatis non lacus nec, sollicitudin ornare turpis. Suspendisse
potenti. Etiam dictum placerat tellus in sollicitudin. In hac
habitasse platea dictumst. Fusce maximus lacus ac congue ullamcorper.
Curabitur vulputate mi tortor, quis tristique nunc hendrerit non.
Nulla ultrices quis lectus eu lobortis. Fusce varius lacinia
lacus, vel scelerisque justo fermentum in. Quisque auctor
dignissim turpis sit amet egestas. In hac habitasse platea dictumst.

Vivamus at justo pulvinar, elementum diam ut, dapibus elit.
Nunc orci nisl, semper in justo eu, sagittis mollis leo.
Maecenas accumsan velit quis lacinia ultricies. Donec feugiat
interdum odio, sed sodales massa facilisis non. Integer massa
purus, sodales sit amet diam quis, luctus congue nunc. Aliquam
a fermentum tortor. Sed sed consequat ipsum, quis mollis felis.

Aliquam in nulla vel dolor hendrerit finibus ac semper justo.
Sed rhoncus dolor ut nisi varius, placerat suscipit lorem
accumsan. Nam orci sem, convallis id rutrum vel, faucibus nec
lorem. Etiam purus tellus, ultricies non enim at, placerat
tempor dolor. In sit amet dictum leo, at vehicula felis.
Phasellus eget turpis ut sapien lacinia bibendum. Maecenas
vulputate mattis fermentum. Duis et tortor tellus.

Nunc dapibus et purus eget facilisis. Aenean blandit leo nunc,
non cursus turpis scelerisque in. Integer pellentesque purus
nec nulla laoreet suscipit. Etiam sit amet dapibus arcu. Nulla
id eros nisi. Quisque dignissim nibh sed massa tempus, ornare
commodo neque venenatis. Sed quis auctor lacus. Sed eleifend
dui non nulla dapibus, ut imperdiet nibh dictum.

Vestibulum vel eros euismod, imperdiet nisi a, imperdiet orci.
Sed aliquam mi sed urna venenatis, sed feugiat libero luctus.
Aliquam non nunc nec dui finibus interdum at eget metus.
Suspendisse lobortis pretium purus, ut volutpat nisi elementum
ac. Integer leo augue, facilisis ac neque ac, vehicula facilisis
nisl. Morbi ullamcorper nulla vel nulla iaculis vehicula.
Mauris eget lacinia eros, id vestibulum massa. Fusce eget
blandit odio, vitae condimentum metus. Aliquam vitae nunc ex.
Curabitur dictum dui id diam dignissim, eu vulputate arcu
euismod. Cras ultrices tempus accumsan. Morbi laoreet, quam
quis luctus ultrices, nulla nulla sagittis mi, vitae pharetra
arcu tellus sit amet elit. Vivamus vestibulum ac mi sed accumsan
Mauris fermentum aliquam lorem, sed rutrum nulla suscipit nec.
Quisque porttitor tincidunt risus, nec posuere nibh aliquet
sollicitudin. Pellentesque habitant morbi tristique senectus
et netus et malesuada fames ac turpis egestas.

In dapibus nunc sit amet nunc feugiat consequat. Mauris cursus
metus id urna venenatis, eu consequat velit porta. In hac
habitasse platea dictumst. Quisque cursus, est id aliquet
cursus, est risus molestie risus, id iaculis velit velit ac
urna. Aliquam a mi at dolor pharetra sollicitudin. Sed id
imperdiet magna. Integer sagittis felis ex, sed posuere turpis
facilisis quis. Vivamus sed sollicitudin urna. Donec a mauri
quis erat varius placerat a eu nisl. Nam nulla enim,
pellentesque gravida purus quis, imperdiet consequat
lectus. Mauris pharetra mollis mollis. Nulla facilisi. """;
  const longMessageEncryptedInGolang =
      "NZka9UkMy/D2wqFIaHgasjjepZ/4H0ItiZ3S/h8C91eL3wDTGmrdak93ZxEnTHRLmnRbWnUq8H5zQ0KBAKKxfeLmcD4c7t7MzutPflvCvr5k3O0ckhKuIa5E22DxjDlQmXOd9Tqqq0u8mmpxjpBhfbuFgEF1NGZK9eGtfoEiSoLUYZ0HBWXMY37x0B1ALyjY0zrDYv2p+kEwsnJI+mo4jpo/+0zUUWQcDikjVs5KHAdMRGg9DCSNpqWdtvFtA5g5LTCk6xgvBZ/Bz13cJfdKAd4KUwgLHcE94zM00n+JU+PP9JhiL8BC9Y6G3d0zOg/VAb7r9rb7Yu3d3zI353wiVJbbsilJDshMiYh7/AsbNwDDAjJZx4ESPKHZWz9epmoCymOXiAXx5JFBPbuTLWjZeaN8Rieaot1ZT4uy/H7MD6859NKTnCVsQZPR/i1wnP78CB67aIreLbAW1wHCXk2l0eXGp1jGPi1zWa3y+sIir+ifiAKU39D56Vmce8cYP/wsbEqhY//q7hxBQAHSZ0dqBRqtFz7VeavP38AdBLq2ffA8h/j9cKYEAjcQN8Y1wvlRN9dmOP0qZqI38dUbzyB27FVmVJw4UlHkAr3bVv1rXL7Y42mumwEku28reR/rk/f06MZ0N/1ARdsNdGzNB7Y0tC/zPnmwT3zVT9e/uaCozxJKrZzmBWmtMIqQwBRfaeIuyWzxBE8LJNXxyO5gMLAMYoQNiHhQL8ZQEVlNj9YsniLnzprs/5TSxIJLstL2+ZwRY+CwyI5nxeHq/hMuCVFdCdzI5jqXRuBhr6I6w5maXVF+RrVPgOVwGZs6Z7a6efXtir2W5N2URxwIET/+IbZUiO+wrOK+cJ+QvuXZ8sSNht/DpKMwMriUX2HcSQ8nOIRzls+Lz4wVGVUJfCmNY42YwdZ2uwJw2zH42pNDwRn5j9owZK/WsqNghBnH6q3sflGUe9kG6n+aoFuBvMQaDnjbRu7hnvsaz0t27Hh7Pa1HjJ2q9p+2VjDJ58Vs0NDPxN+nUsdm4yXeoBlqUtVR4/hardEWBPQhZ3KOlHVsxEGW1b9KZnUWCFl3WWd9H8g61CDmXQ5SMi6JjgRVXtEgUaJ4i1jkXCV3m3zcxWWa7YkexZu+B1zvfJ3xchAh2jGqKAAOXEG6a1uTVPl6nJytjGHkFbqscN5Xge6SskADXFpKbfM2Rg/D7xobJfjZkfnB5QdCweBdeD3uvnllp+Lh2a8EH2cPVieIS7gK1Bvx48opCPejWjBflAoNdcGM9UaLSw7J9Lhc5zXPYIpSfHBjapNg/upNGTdSg0RC6Wld0Mwq9QyFCwuPdILnwfFKAFp65ZQV4vs0kiRQeZsYNAxgW8YpKyoyFNrSKUAmCR7GfIJjjeKdmdJ0v7vNxKcTGnbflnGCHj4WoGYxbxHuCIpGtIj4Aga7sLJfJvO1PHiSWBAkk+f+/kWctO2ni+15uyUL5XmYo3HDghdW5533YQEEJLrLpgDvmoPyu+Mr4GXVh9LxCpv+ggclS16dCLthklhprzkOHg7nDFfhVSKbE6NwYx8n8311zy0JK4uh2Bi/qoqRRPcqaau+EQX1OsNmpI+f866t+EzQYx1f5Ieut5FQqwMXTcI9KTcn6dVoVodtLAbOnh81y9UEUmNSbIkdai3tCveh+MFosqXSyI3GkJLJqWNKrVYFTOslBj7j9qFWpLsYl4kHuvH+4SwYlq7laRgAF9HUQBs/1stzBeiI3K0eMmEZp6dWY33j68++5LuEyxo86AUV095cVEoITCmU23mas1FvAg3zt1n1ZD98duy9TAN4q7xwrYvWIIfjbcuWRc20V+4rLlw58JqG+9Ojj+GjA6A9SB9JVaPQmr27bmKYwdaLkXiXCgUbk5GWBKDsGdYQcSBErdoiRNfVGK88NDtY58zAXqSFmtJLjVdj1cpa9ChGzg/gM+6r9hFUcNqNYEc09PzlzJUA7ceuBvSiKm3ewghaWB/Sxu2Z5Q3oig6100hYNnaFqaQZoRqo0fbhTiuvTNDRu12OOsM5XXT/gaJSJB+IiXL+x0Lms06BkzlfbJ/8XML1HykQ3A/nTKWIrPbzOou/NHI+KmOBERxVIRulB28SrOkPocLYuPk9rgvO6u1IOIS7yZUShWMmiXRFSLJOv8/f5YTU0l7eXzQ7z4FFtUyRJ0jn9C/s2WZ3Q8TwqepyCkHi5/7yNGarYQzHwpd5Kd6QcVJSMgciacYefa0bh+6nsD4IjaQcLgeVixerprZ3K1nhe7WUbbZfPKtbg/6r0g48/tV9wCh4ad1CUZS/P1euFBzPs4ANM6YOjFZFX8Cfamrfph6W9DMdnxNHY28cQS/M49YQAHDi51+UDqFBdhVc88YDcUV4g2kmYZXA6wdv9bV5RC35/M1g1chaLEHO44djAqOxMYOSUXqxRa73yDkyRK7X5wu/tehCtBpa56iqIA+H4V0NvU6OLvkT9+lML21iiBgJ6lFk6MOg5OtL/IMPao84BUGnCulaT05rto8CIFa1Ag8bbNP/RT7G9PzDkh2ckEeaSYNjCVFw96r/wKxiA0T8SjXGIgsnvptcs5NNf0iGJQV6OnfB/rHEa17Y0BAU4BPARZqSBtuEWc+gT/7TSRRlVpa+PoDFx4x8gTFZnySuvFda4lSc21Re2kHBC140C4VGCCQEjaZRaa9CM9ftIc5KSrcNET4+GkE78PC4CZCNzwqlSEn2D/6eTj5jcgiSwzxRxodXqv0b/CaRUJaPUB7KOB8+gR4LakwBx2bSDQWzPp2WPtm7agHXCALbLyNUM5EkpXAnRmJ2FeBQmQggAJjYazjT4kNLi0gaFtUULe59bHYcn+uHQXGDTl+6jwPNktIyn4LF6moH3ObRVTtkgwv4150nW5fvwLGo8CSf4wsClybAeIi4TeRSTdmxYgFea2N6172wPfvYMrMygHnXxUxmbNwGcuGnSA2ofTrXopcje0dmOrDxfyCA8rQBwukYwXFsYceM91x4hC7u7UStN/BTuHu0TQCp0zzvq4i23gOnh0EfMrTzqlyV7GdOMiQJnZ54TJky6xelShZyQzZ8XgH76u+rZ+XYs6i6uIwH+7BqRu8lbmZXV8yV/6JSzjBEWqnvufIi5/oVnz9JT6TpE4cWb836zbWeqCpKIXRM0uE9+XgMBSrwxdGJiTpJeu00V1B0LK53keFpJMe7SxyKm0GJT+wFGkU+kpXwVr75ei4vOlL4e2fKHMRmccrKsmcJmNJYyH1EbX4PorD91rYMTL549/1jzBiiRbM+Ka3tQAto8wUPQmKg9mV+Ory8V5aHk+TaCc28izVpYsYeWxLZi9lHZf4XGP+LU1pzj7mY4WzDcZZdRiDTEu1KTYqSHgXCVagd+BBHUN85Z7kyiqQcE3/Dgflpo9HHqAxPNIfdpnr06VARe79F1QhdEWcQtIhs2crsOIpZOX1ZXPUZgrw9i/nlx3zwRXQq6bkN1XfpDzITBXOwXN8oPy+3xGU5MaevPCJoMKckHaKDsFfwmb948H/LfaMu7515XH4PVARKU3JygKOEQDyRLNmWGdAgLZJrHfG33KmF7Dn70jZP7g+CTjuylmFmYWG+fZf154gqHeZAd+F9A1CblaK/EcGMkPpAJn5jrbUo3eaRUYgGwHiGspz4yWkHAo8L/UQfo/ZI/iOwYzxdwdmO22Cr4C4nnnSaizavmu2sQOI+TaOWH9obfxWoXqP9XlRTI9vms/0BcxLCgtSCVkEzgbUKQSenxc8iMlDpWyblIVyBmeP8W5HzJFQUIb4c0iRJDUfGt0k+H08JOt4tng5RRZQI+bFPaGqejM/31uU8LCe6dTE0LYAE89v6VewpFPyzOgxtbbvhHVmTQK5dn8KlBAsz8lDa2r12L+r4dRGmqzfiO7k1LNMd3g/Du5FMRxGEcxSW0dzxDPgyn+tOWcRD5uJNAi1LNI1uxi6WDpnmv156x/kH85dauTxVM2WrbIbW+LP0Qff9mNFdu+Nwuyv70sGqGSZWoG+sTIT3yHrjDCWDbm7Jzm7q8ncghKQGDasRraOJELBC4nFl1wtYnOpBTizD8vMXdWwSwrWZ2RVrLIYebbALfBYzRVFn1PLYdteYNJEDDOrUAfBRve7DhwZpSe1Dft3dmtsZV+LljUVLRvsGkNIXC1HCDb82xF0zkUl6VIwgQtOf97vsmGPihDA2b2Sl4/h09CIQEJTiFEiR+ybSOluUGc8EJseYZFMn6Gip1CMxtANdLaynzwaw8cIsjFcKIzKDq16tThzZ9/XcVWvL4SGwOIluSRl2cVIuwgcRfbAZ0UsaYixLxbvUNKVn37Mj5LXX7F3/nguEisU0HqCUbjlURMbkDCIaAYZvNAMlgtbcjZo02c8oy4SsI3dOtko7rl/37RYQHMhJc1swdnJmebxZnrZgbiDoTz11k+14vx1Z1SXrwmqYX9z+rdGh0oLxONnPVKzqdeqQxXy+Dn9EBUADY6AJfugvTUueW0YkrbwyNwh0hYLOZqWuqbRyikSnXh7po2YGxmvM88Sjq2XxRFh+U6VmIsVe0ADD9LBUnsGqOWHga1/8r9jS6o0K/uXIbcSXFvMRTl16/Mp81AqGKmC0sK5eEjtzdgvY1vGivXfYFAlIRjq+UHpD1yqxh0zeutsssOp3sGh0q7NCM85dVv4uxqk9jq+PKHtgkD4yTXg/fzTYezFlGeg/M9uamceIGy0ymtNHongp7Tq5RRHB0PHL8NT/sbKbja8M5RCrFRcluNU5pgk90SdAUhxV1xlYJLc2gHWamaPHKg3f/9vNClG9Cds+9FOswm3mwxi5kVuvtvsBw544E/GWGzmAz8uaVPVYuXDElbMYWaDj3G46/T0s1y2AdvSfjLdat7dCHQa4k8+Y0IAv+uU7K4MmGZxnxWSOkwpPhkWlaxz35u+Eskyd4deiscYCgODnes5+mWkLvHZXX9JRh9GJxPGElSLUQOrMJ/y2dPGM8m3nKrZI7MABMOr3lTdC84Rm9MCmCvUHd+cWWMCZnfDpdEhE0CGXCT3FJvzcK4PDl1yYs5UKCIi2rWIpWUULF/o7E0eE+6vwhHfDAiPhj1oD3R0vq2MRj1Cun97a35pAOD2c3vFN+6iGEv0Tw8v9I6Io79IGzitY2K7NCOgHUmCyfXmnDW++LU19MmdLgyMB9sI5BymxHYzOBrwraXt4aV4C+PfVjKrFiPLq2eb7y6xh1UxhJgx6UaUq48lr/SdbpLkp/URZcfskX/wgHEwNy2OXl6uo3DEnaPooqqP9hTrrrfre/TCic9efFUiQZAvXpLraCEZ/t94A6PGxQAqWz8k/azNYj/W2uGNtLnUxNFQRqdqaXfbIiw2oe0zfUeP8HHXiXZ4zBD9kQefELmRksnwr89Yd6zwRKabdlXb44w7cLx7THHYrOhcc/xlQPW/ZY/6w/mAWHE34wFKKBadB4qI9+0/IF9SvUO76OQ==";

  test('OAEP - encrypt and decrypt', () async {
    var encryptedMessage = await encryptionHelper.rsaEncrypt(
      plaintextMessage,
      "",
      publicKeyPem,
    );

    var decryptedMessage = await encryptionHelper.rsaDecrypt(
      encryptedMessage,
      "",
      privateKeyPem,
    );

    expect(plaintextMessage, decryptedMessage);
  });

  test('OAEP - decrypt a message encrypted in Golang', () async {
    var decryptedMessage = await encryptionHelper.rsaDecrypt(
      messageEncryptedInGolang,
      "",
      privateKeyPem,
    );

    expect(plaintextMessage, decryptedMessage);
  });

  test('OAEP - encrypt and decrypt a long message', () async {
    var encryptedMessage = await encryptionHelper.rsaEncrypt(
      longPlaintextMessage,
      "",
      publicKeyPem,
    );

    var decryptedMessage = await encryptionHelper.rsaDecrypt(
      encryptedMessage,
      "",
      privateKeyPem,
    );

    expect(longPlaintextMessage, decryptedMessage);
  });

  test('OAEP - decrypt a message encrypted in Golang', () async {
    var decryptedMessage = await encryptionHelper.rsaDecrypt(
      longMessageEncryptedInGolang,
      "",
      privateKeyPem,
    );

    expect(longPlaintextMessage, decryptedMessage);
  });

  test('PSS - sign and verify', () async {
    var pssSignature = await encryptionHelper.sign(
      plaintextMessage,
      privateKeyPem,
    );

    var valid = await encryptionHelper.verify(
      pssSignature,
      plaintextMessage,
      publicKeyPem,
    );

    expect(valid, true);
  });

  test('PSS - verify a signature created in Golang', () async {
    var valid = await encryptionHelper.verify(
      pssSignatureGolang,
      plaintextMessage,
      publicKeyPem,
    );

    expect(valid, true);
  });
}
