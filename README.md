# user_ui

The UI for users to see there financial claims.

## Getting Started

To get started with this project you need:
[FVM](https://fvm.app/docs/getting_started/installation), as [Flutter](https://docs.flutter.dev/get-started/install) Version Management.

## Running App

### Production

```sh
flutter run
```

### Development

#### Mock

Run and debug mobile-app `mock` from Visual Code Studio. (see launch.json)

Or via the terminal

```sh
flutter run -t lib/main_mock.dart
```

#### IOS with local API's

Run and debug mobile-app `local ios` from Visual Code Studio. (see launch.json)

Or via the terminal

```sh
flutter run -t lib/main_local.dart
```

\*_And also the backend services, see readme of developer-tools\*

### Web version

```sh
skaffold dev -f=skaffold.yaml -p dev
```

## Test

```sh
flutter test
```

### Mock generation

[Mockito Null Safety](https://github.com/dart-lang/mockito/blob/master/NULL_SAFETY_README.md)

```sh
flutter pub run build_runner build --delete-conflicting-outputs
```

### Launch Screen generation

To generate the Launch Screens, change settings in pubspec.yaml and run

```sh
dart run flutter_native_splash:create
```

### Coverage

Install lcov (Mac):

```sh
brew install lcov
```

Run tests, generate coverage files and convert to HTML:

```sh
flutter test --coverage
genhtml coverage/lcov.info -o coverage/html
```

Open `coverage/html/index.html` in your browser.

### Pushing a build to Testflight (manual)

Make sure bundler and Fastlane is installed and you have access to the certificates.
If you really want to push the build to Testflight, make sure to uncomment
`upload_to_testflight` in `fastlane/Fastfile`.

```sh
flutter build ipa
cd ios
bundle exec fastlane beta
```

#### Android APK build

To build an APK-file run the following command:

```sh
flutter build apk --release
```

#### Database build steps

Build the database:

```sh
dart run build_runner build --delete-conflicting-outputs
```

### Suggested reading

#### Fastlane

- <https://docs.flutter.dev/deployment/cd>
- <https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html>
- <https://www.runway.team/blog/how-to-build-the-perfect-fastlane-pipeline-for-ios>
- <https://docs.fastlane.tools/getting-started/ios/authentication/>
- <https://docs.fastlane.tools/getting-started/ios/beta-deployment/>
- <https://docs.fastlane.tools/best-practices/continuous-integration/gitlab/>

#### Flutter

- <https://github.com/abuanwar072/Flutter-Responsive-Admin-Panel-or-Dashboard>
- <https://www.slideshare.net/BartoszKosarzycki/provider-vs-bloc-vs-redux>
- <https://github.com/rrousselGit/flutter_hooks>
