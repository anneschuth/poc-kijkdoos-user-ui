import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:stream_transform/stream_transform.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/usecases/request_financial_claims_information/request_financial_claims_information_usecase.dart';

abstract class IWorker {}

class Worker implements IWorker {
  final FinancialClaimsInformationRequestRepository
      financialClaimsInformationRequestRepository;
  final IRequestFinancialClaimsInformationUsecase
      requestFinancialClaimsInformationUsecase;

  Worker(
    this.financialClaimsInformationRequestRepository,
    this.requestFinancialClaimsInformationUsecase,
  ) {
    var watch = financialClaimsInformationRequestRepository.watchProcessable();

    financialClaimsInformationRequestRepository
        .releaseLockForUncompletedRequests()
        .onError(
          // TODO: error handling
          (error, _) => debugPrint("    ERROR: $error"),
        )
        .whenComplete(() {
      watch.debounce(const Duration(milliseconds: 100)).listen((_) async {
        debugPrint("#### TRIGGER WORK");
        await doWork();
        // TODO: error handling
      });
    });
  }

  Future<void> doWork() async {
    var financialClaimsInformationRequest =
        await financialClaimsInformationRequestRepository
            .getAndLockNextProcessable();
    if (financialClaimsInformationRequest != null) {
      debugPrint("doWork: ${financialClaimsInformationRequest.id}");

      try {
        await requestFinancialClaimsInformationUsecase
            .process(financialClaimsInformationRequest);
      } catch (e) {
        debugPrint("error: ${e.toString()}");
        // TODO: error handling
        // TODO expire financial claims configuration and request. Start a new financial claims request
      }

      await financialClaimsInformationRequestRepository
          .releaseLock(financialClaimsInformationRequest.id);
    }
  }
}

final workerProvider = Provider<IWorker>((ref) {
  final financialClaimsInformationRequestRepository =
      ref.watch(financialClaimsInformationRequestRepositoryProvider.notifier);
  final requestFinancialClaimsInformation =
      ref.watch(requestFinancialClaimsInformationUsecaseProvider);
  return Worker(
    financialClaimsInformationRequestRepository,
    requestFinancialClaimsInformation,
  );
});
