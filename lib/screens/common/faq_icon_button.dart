// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';

class FaqIconButton extends StatelessWidget {
  const FaqIconButton({super.key});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(
        Icons.question_mark_rounded,
        size: 24,
      ),
      onPressed: () => context.goNamed(HomeScreen.routeName),
    );
  }
}
