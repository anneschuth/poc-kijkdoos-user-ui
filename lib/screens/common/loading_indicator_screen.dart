// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/screens/features/home/components/deep_link.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_screen.dart';

class LoadingIndicatorScreen extends StatelessWidget {
  const LoadingIndicatorScreen({
    required this.text,
    this.onBackbuttonPress,
    this.showDrawer = false,
    this.addDeepLink = false,
    super.key,
  });

  final void Function()? onBackbuttonPress;
  final bool showDrawer;
  final String text;
  final bool addDeepLink;

  @override
  Widget build(BuildContext context) {
    return ThemedScreen(
      onBackbuttonPress: onBackbuttonPress,
      showDrawer: showDrawer,
      childrenPadding: true,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Text(
              text,
              style: Theme.of(context).textTheme.displayMedium,
              textAlign: TextAlign.center,
            ),
            addVerticalSpace(defaultPadding * 2),
            const SizedBox(
              height: 32,
              width: 32,
              child: CircularProgressIndicator(),
            ),
          ],
        ),
        if (addDeepLink) const DeepLink()
      ],
    );
  }
}
