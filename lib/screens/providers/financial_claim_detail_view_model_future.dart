// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/usecases/financial_claims_information/financial_claims_information_usecase.dart';
import 'package:user_ui/usecases/financial_claims_information/models/financial_claim_detail_view_model.dart';

class FinancialClaimDetailViewModelQuery {
  final int financialClaimsInformationStorageId;
  final String zaakkenmerk;

  FinancialClaimDetailViewModelQuery({
    required this.financialClaimsInformationStorageId,
    required this.zaakkenmerk,
  });

  @override
  bool operator ==(other) =>
      other is FinancialClaimDetailViewModelQuery &&
      financialClaimsInformationStorageId ==
          other.financialClaimsInformationStorageId &&
      zaakkenmerk == other.zaakkenmerk;

  @override
  int get hashCode => Object.hash(
        financialClaimsInformationStorageId.hashCode,
        zaakkenmerk.hashCode,
      );
}

final financialClaimDetailViewModelProvider = FutureProvider.autoDispose
    .family<FinancialClaimDetailViewModel, FinancialClaimDetailViewModelQuery>(
        (ref, financialClaimDetailViewModelQuery) async {
  final financialClaimsInformationUsecase =
      ref.watch(financialClaimsInformationUsecaseProvider);

  return financialClaimsInformationUsecase
      .generateFinancialClaimDetailViewModelFromStorage(
    financialClaimDetailViewModelQuery.financialClaimsInformationStorageId,
    financialClaimDetailViewModelQuery.zaakkenmerk,
  );
});
