// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/entities/financial_claims_information_storage.dart';
import 'package:user_ui/entities/scheme_organization.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_document_type_repository.dart';
import 'package:user_ui/screens/providers/models/financial_claim.dart';
import 'package:user_ui/screens/providers/models/financial_claims_overview_view_model.dart';
import 'package:user_ui/usecases/request_financial_claims_information/models/financial_claims_information_document_v3.dart';

abstract class IFinancialClaimsOverviewViewModelStateNotifier {
  FinancialClaimsOverviewViewModel get financialClaimsOverviewViewModel;
}

class FinancialClaimsOverviewViewModelStateNotifier
    extends StateNotifier<FinancialClaimsOverviewViewModel>
    implements IFinancialClaimsOverviewViewModelStateNotifier {
  final List<FinancialClaimsInformationStorage>
      financialClaimsInformationStorageList;
  final List<SchemeOrganization> organizations;
  final LoggingHelper loggingHelper;

  FinancialClaimsOverviewViewModelStateNotifier(
      this.financialClaimsInformationStorageList,
      this.organizations,
      this.loggingHelper)
      : super(FinancialClaimsOverviewViewModel(
          directBetalen: <String, FinancialClaim>{},
          achterstand: <String, FinancialClaim>{},
          betaald: [],
          betalingskenmerkGebeurtenissen: <String, String>{},
        )) {
    state = financialClaimsOverviewViewModel;
  }

  @override
  FinancialClaimsOverviewViewModel get financialClaimsOverviewViewModel {
    var financialClaimsOverviewViewModel = FinancialClaimsOverviewViewModel(
      directBetalen: <String, FinancialClaim>{},
      achterstand: <String, FinancialClaim>{},
      betaald: [],
      betalingskenmerkGebeurtenissen: <String, String>{},
    );

    Set<String> organizationOins = {};

    for (var financialClaimsInformationStorage
        in financialClaimsInformationStorageList) {
      organizationOins.add(financialClaimsInformationStorage.oin);
    }

    for (var organizationOin in organizationOins) {
      var financialClaim = financialClaimsInformationStorageList
          .where((financialClaimsInformationStorage) =>
              financialClaimsInformationStorage.oin == organizationOin)
          .last;

      var financialClaimsInformationDocumentJson =
          json.decode(financialClaim.financialClaimsInformationDocument);
      FinancialClaimsInformationDocument financialClaimsInformationDocument;
      try {
        financialClaimsInformationDocument =
            FinancialClaimsInformationDocument.fromJson(
                financialClaimsInformationDocumentJson);
      } catch (e) {
        loggingHelper.addLog(DeviceEvent.uu_14, e.toString());
        rethrow;
      }
      for (var financieleZaak
          in financialClaimsInformationDocument.body.financieleZaken) {
        var primaireFinancieleVerplichting = financieleZaak.gebeurtenissen
                .firstWhere((f) =>
                    f.runtimeType == FinancieleVerplichtingOpgelegd &&
                    (f as FinancieleVerplichtingOpgelegd).primaireVerplichting)
            as FinancieleVerplichtingOpgelegd;

        var organizationsByOin = organizations
            .where((o) => o.oin == primaireFinancieleVerplichting.opgelegdDoor);

        String organizationName;
        if (organizationsByOin.isEmpty) {
          organizationName =
              "Onbekend"; // TODO: organization not in scheme error handling
        } else {
          organizationName = organizationsByOin.first.name;
        }

        if (financieleZaak.saldo == 0) {
          financialClaimsOverviewViewModel.betaald.add(
            FinancialClaim(
              title: getHumanReadableName(primaireFinancieleVerplichting.type),
              saldoDateTime: financieleZaak.saldoDatumtijd,
              organizationName: organizationName,
              saldo: financieleZaak.saldo,
              financialClaimsInformationStorageId: financialClaim.id,
              zaakkenmerk: financieleZaak.zaakkenmerk,
            ),
          );
          continue;
        }

        for (var gebeurtenis in financieleZaak.gebeurtenissen) {
          switch (gebeurtenis.runtimeType) {
            case FinancieleVerplichtingOpgelegd:
              break;
            case BetalingsverplichtingOpgelegd:
              gebeurtenis as BetalingsverplichtingOpgelegd;

              if (financieleZaak.achterstanden
                  .contains(gebeurtenis.gebeurtenisKenmerk)) {
                financialClaimsOverviewViewModel
                        .achterstand[gebeurtenis.gebeurtenisKenmerk] =
                    FinancialClaim(
                  title:
                      getHumanReadableName(primaireFinancieleVerplichting.type),
                  saldoDateTime: financieleZaak.saldoDatumtijd,
                  organizationName: organizationName,
                  saldo: financieleZaak.saldo,
                  financialClaimsInformationStorageId: financialClaim.id,
                  zaakkenmerk: financieleZaak.zaakkenmerk,
                );
              } else {
                financialClaimsOverviewViewModel
                        .directBetalen[gebeurtenis.gebeurtenisKenmerk] =
                    FinancialClaim(
                  title:
                      getHumanReadableName(primaireFinancieleVerplichting.type),
                  saldoDateTime: financieleZaak.saldoDatumtijd,
                  organizationName: organizationName,
                  saldo: financieleZaak.saldo,
                  financialClaimsInformationStorageId: financialClaim.id,
                  zaakkenmerk: financieleZaak.zaakkenmerk,
                );
              }

              financialClaimsOverviewViewModel.betalingskenmerkGebeurtenissen[
                      gebeurtenis.betalingskenmerk] =
                  gebeurtenis.gebeurtenisKenmerk;

              break;
            case BetalingsverplichtingIngetrokken:
              gebeurtenis as BetalingsverplichtingIngetrokken;

              financialClaimsOverviewViewModel.achterstand
                  .remove(gebeurtenis.ingetrokkenGebeurtenisKenmerk);

              financialClaimsOverviewViewModel.directBetalen
                  .remove(gebeurtenis.ingetrokkenGebeurtenisKenmerk);
              break;
            case BetalingVerwerkt:
              gebeurtenis as BetalingVerwerkt;

              financialClaimsOverviewViewModel.achterstand.remove(
                  financialClaimsOverviewViewModel
                          .betalingskenmerkGebeurtenissen[
                      gebeurtenis.betalingskenmerk]);

              financialClaimsOverviewViewModel.directBetalen.remove(
                  financialClaimsOverviewViewModel
                          .betalingskenmerkGebeurtenissen[
                      gebeurtenis.betalingskenmerk]);
              break;
            default:
              loggingHelper.addLog(DeviceEvent.uu_13,
                  "unknown gebeurtenis.runtimeType: ${gebeurtenis.runtimeType}");
              throw Exception(
                  "unknown gebeurtenis.runtimeType: ${gebeurtenis.runtimeType}");
          }
        }
      }
    }

    return financialClaimsOverviewViewModel;
  }
}

final financialClaimsOverviewViewModelStateNotifierProvider =
    StateNotifierProvider<FinancialClaimsOverviewViewModelStateNotifier,
        FinancialClaimsOverviewViewModel>((ref) {
  final financialClaimsInformationStorageList =
      ref.watch(financialClaimsInformationStorageRepositoryProvider);

  final organizations = ref.watch(schemeOrganizationRepositoryProvider);

  final logging = ref.watch(loggingProvider);

  return FinancialClaimsOverviewViewModelStateNotifier(
    financialClaimsInformationStorageList,
    organizations,
    logging,
  );
});

String getHumanReadableName(String financialClaimType) {
  switch (financialClaimType) {
    case "DUO_LLSF":
      return "Studieschuld";
    case "CJIB_WAHV":
      return "Verkeersboete";
    case "CJIB_ADMINISTRATIEKOSTEN":
      return "Administratiekosten";
    case "CJIB_KOSTEN_EERSTE_AANMANING":
      return "Kosten eerste aanmaning";
    case "BD_Inkomensheffing":
      return "Heffing inkomstenbelasting";
    default:
      return financialClaimType;
  }
}
