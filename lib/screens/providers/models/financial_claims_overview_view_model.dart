// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:user_ui/screens/providers/models/financial_claim.dart';

class FinancialClaimsOverviewViewModel {
  Map<String, FinancialClaim> directBetalen = <String, FinancialClaim>{};
  Map<String, FinancialClaim> achterstand = <String, FinancialClaim>{};
  List<FinancialClaim> betaald = [];

  Map<String, String> betalingskenmerkGebeurtenissen = <String, String>{};

  FinancialClaimsOverviewViewModel({
    required this.directBetalen,
    required this.achterstand,
    required this.betaald,
    required this.betalingskenmerkGebeurtenissen,
  });
}
