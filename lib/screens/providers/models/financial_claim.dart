// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

class FinancialClaim {
  String title;
  String organizationName;
  int saldo;
  DateTime saldoDateTime;
  int financialClaimsInformationStorageId;
  String zaakkenmerk;

  FinancialClaim({
    required this.title,
    required this.organizationName,
    required this.saldo,
    required this.saldoDateTime,
    required this.financialClaimsInformationStorageId,
    required this.zaakkenmerk,
  });
}
