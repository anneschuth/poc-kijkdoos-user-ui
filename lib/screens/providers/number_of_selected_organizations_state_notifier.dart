// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/entities/organization_selection.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';

abstract class INumberOfSelectedOrganizationsStateNotifier {
  int get numberOfSelectedOrganizations;
}

class NumberOfSelectedOrganizationsStateNotifier extends StateNotifier<int>
    implements INumberOfSelectedOrganizationsStateNotifier {
  final List<OrganizationSelection> organizationSelections;

  NumberOfSelectedOrganizationsStateNotifier(this.organizationSelections)
      : super(0) {
    state = numberOfSelectedOrganizations;
  }

  @override
  int get numberOfSelectedOrganizations {
    return organizationSelections
        .where((organizationSelection) => organizationSelection.selected)
        .length;
  }
}

final numberOfSelectedOrganizationsProvider =
    StateNotifierProvider<NumberOfSelectedOrganizationsStateNotifier, int>(
        (ref) {
  final organizationSelections =
      ref.watch(organizationSelectionRepositoryProvider);

  return NumberOfSelectedOrganizationsStateNotifier(organizationSelections);
});
