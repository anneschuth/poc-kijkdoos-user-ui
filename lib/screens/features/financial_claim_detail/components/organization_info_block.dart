import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';

class OrganizationInfoBlock extends StatelessWidget {
  const OrganizationInfoBlock({
    super.key,
    required this.phoneNumber,
    required this.emailAddress,
    required this.websiteUrl,
  });

  final String phoneNumber;
  final String emailAddress;
  final String websiteUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      constraints: BoxConstraints(maxWidth: maxWidthConstraints(context)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Vragen over deze vordering?',
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          addVerticalSpace(Spacing.medium.value),
          Text(
            'Neem dan contact op met Centraal Justitieel Incassobureau (CJIB):',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          addVerticalSpace(Spacing.medium.value),
          _OrganizationInfoButton(
            text: phoneNumber,
            icon: Icons.phone,
            onPressed: () async {
              //TODO: Test on a real device
              final call = Uri(
                scheme: 'tel',
                path: phoneNumber,
              );
              if (await canLaunchUrl(call)) {
                launchUrl(call);
              } else {
                throw 'Could not launch $call';
              }
            },
          ),
          _OrganizationInfoButton(
            text: websiteUrl,
            icon: Icons.public,
            onPressed: () async {
              final web = Uri(
                scheme: 'https',
                path: websiteUrl,
              );
              if (await canLaunchUrl(web)) {
                launchUrl(web);
              } else {
                throw 'Could not launch $web';
              }
            },
          ),
          _OrganizationInfoButton(
            text: 'Contactformulier',
            icon: Icons.article_outlined,
            onPressed: () async {
              //TODO: Test on a real device
              final email = Uri(
                scheme: 'mailto',
                path: emailAddress,
                query: 'subject=Hello&body=Test',
              );
              if (await canLaunchUrl(email)) {
                launchUrl(email);
              } else {
                throw 'Could not launch $email';
              }
            },
          ),
        ],
      ),
    );
  }
}

class _OrganizationInfoButton extends StatelessWidget {
  const _OrganizationInfoButton({
    required this.text,
    required this.icon,
    this.onPressed,
  });

  final String text;
  final IconData icon;
  final void Function()? onPressed;

  final double iconSize = 24;

  @override
  Widget build(BuildContext context) {
    final Color iconColor = Theme.of(context).colorScheme.primary;

    return TextButton.icon(
      onPressed: onPressed,
      style: TextButton.styleFrom(
        padding: EdgeInsets.zero,
        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
        alignment: Alignment.centerLeft,
      ),
      icon: Icon(
        icon,
        size: iconSize,
        color: iconColor,
      ),
      label: Text(
        text,
        style: Theme.of(context).textTheme.bodyLarge?.copyWith(
              color: iconColor,
              decoration: TextDecoration.underline,
            ),
      ),
    );
  }
}
