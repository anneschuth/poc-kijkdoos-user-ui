// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:user_ui/usecases/financial_claims_information/models/financial_claim_detail_view_model.dart';
import 'package:user_ui/utils/saldo_formatter.dart';

class FinancialClaimDetailScreenEventListItem extends StatelessWidget {
  final FinancialClaimEvent event;

  const FinancialClaimDetailScreenEventListItem(this.event, {super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: 0.0),
      title: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  event.description,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
              ),
              Text(formatAmount(event.amount),
                  style: Theme.of(context).textTheme.bodyLarge),
            ],
          ),
        ],
      ),
      subtitle: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  DateFormat.yMMMMd("nl").format(event.dateTime),
                  style: Theme.of(context).textTheme.titleSmall,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
