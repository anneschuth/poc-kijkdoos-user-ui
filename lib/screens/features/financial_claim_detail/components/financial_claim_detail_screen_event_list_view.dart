// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/screens/features/financial_claim_detail/components/financial_claim_detail_screen_event_list_item.dart';
import 'package:user_ui/theme/themed_divider.dart';
import 'package:user_ui/usecases/financial_claims_information/models/financial_claim_detail_view_model.dart';

class FinancialClaimDetailScreenEventListView extends StatelessWidget {
  final List<FinancialClaimEvent> events;

  const FinancialClaimDetailScreenEventListView(this.events, {super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      separatorBuilder: (context, index) {
        return const ThemedDivider();
      },
      itemCount: events.length,
      itemBuilder: (context, index) {
        var event = events[index];

        return FinancialClaimDetailScreenEventListItem(
          event,
        );
      },
    );
  }
}
