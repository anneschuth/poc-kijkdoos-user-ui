// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/screens/common/loading_indicator_screen.dart';
import 'package:user_ui/screens/features/financial_claim_detail/components/financial_claim_detail_screen_event_list_view.dart';
import 'package:user_ui/screens/features/financial_claim_detail/components/organization_info_block.dart';
import 'package:user_ui/screens/features/overview/financial_claims_overview_screen.dart';
import 'package:user_ui/screens/providers/financial_claim_detail_view_model_future.dart';
import 'package:user_ui/theme/basic_scaffold.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/custom_expansion_tile.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_divider.dart';
import 'package:user_ui/usecases/financial_claims_information/models/financial_claim_detail_view_model.dart';
import 'package:user_ui/utils/saldo_formatter.dart';

class FinancialClaimDetailScreen extends ConsumerWidget {
  final int financialClaimsInformationStorageId;
  final String zaakkenmerk;

  const FinancialClaimDetailScreen({
    required this.financialClaimsInformationStorageId,
    required this.zaakkenmerk,
    super.key,
  });

  static const routeName = 'FinancialClaimDetailScreen';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var financialClaimDetailViewModelAsync = ref.watch(
      financialClaimDetailViewModelProvider(
        FinancialClaimDetailViewModelQuery(
          financialClaimsInformationStorageId:
              financialClaimsInformationStorageId,
          zaakkenmerk: zaakkenmerk,
        ),
      ),
    );

    return financialClaimDetailViewModelAsync.when(
      data: (financialClaimDetailViewModel) => BasicScaffold(
        useGreyBackground: true,
        onBackbuttonPress: () =>
            context.goNamed(FinancialClaimsOverviewScreen.routeName),
        content: SingleChildScrollView(
          child: Column(
            children: [
              _Header(viewModel: financialClaimDetailViewModel),
              addVerticalSpace(Spacing.large.value),
              _Body(viewModel: financialClaimDetailViewModel),
              addVerticalSpace(Spacing.large.value),
              const _Footer(),
              addVerticalSpace(Spacing.large.value),
            ],
          ),
        ),
      ),
      error: (err, stack) => Text('Error: $err'),
      loading: () => LoadingIndicatorScreen(
        text: "Details laden...",
        onBackbuttonPress: () =>
            context.goNamed(FinancialClaimsOverviewScreen.routeName),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({
    required this.viewModel,
  });

  final FinancialClaimDetailViewModel viewModel;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (!viewModel.isResolved && !viewModel.isOverdue) ...[
          SvgPicture.asset(
            width: 96,
            height: 96,
            "assets/icons/direct-betalen-2.svg",
          ),
          Text(
            "Eenmalig te betalen",
            style: Theme.of(context)
                .textTheme
                .displaySmall
                ?.copyWith(color: colorOrange),
            textAlign: TextAlign.center,
          ),
        ],
        if (!viewModel.isResolved && viewModel.isOverdue) ...[
          SvgPicture.asset(
            width: 96,
            height: 96,
            "assets/icons/achterstand-2.svg",
          ),
          Text(
            "Betalingsachterstand",
            style: Theme.of(context)
                .textTheme
                .displaySmall
                ?.copyWith(color: colorRed),
            textAlign: TextAlign.center,
          ),
        ],
        if (viewModel.isResolved) ...[
          SvgPicture.asset(
            width: 96,
            height: 96,
            "assets/icons/alert-infopage.svg",
          ),
          Text(
            "Betaald",
            style: Theme.of(context)
                .textTheme
                .displaySmall
                ?.copyWith(color: colorGreen),
            textAlign: TextAlign.center,
          ),
        ],
        addVerticalSpace(Spacing.large.value),
        Text(
          viewModel.title,
          style: Theme.of(context).textTheme.displayLarge,
          textAlign: TextAlign.center,
        ),
        addVerticalSpace(Spacing.medium.value),
        Text(
          viewModel.subTitle,
          style: Theme.of(context).textTheme.titleMedium,
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required this.viewModel,
  });

  final FinancialClaimDetailViewModel viewModel;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: responsiveValue(
          context,
          xs: 0.0,
          md: Spacing.large.value,
        ),
        horizontal: responsiveValue(
          context,
          xs: Spacing.large.value,
          md: Spacing.xxlarge.value,
          xl: Spacing.xxxlarge.value,
        ),
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.background,
        borderRadius: BorderRadius.all(
          Radius.circular(RadiusSize.medium.value),
        ),
      ),
      child: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: maxWidthConstraints(context)),
        child: Column(
          children: [
            addVerticalSpace(Spacing.large.value),
            const ThemedDivider(),
            CustomExpansionTile(
              tilePadding: const EdgeInsets.symmetric(horizontal: 0.0),
              title: Text(
                "Info over de vordering",
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              maintainState: true,
              expandedAlignment: Alignment.centerLeft,
              children: [
                Text(
                  viewModel.info,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                addVerticalSpace(Spacing.large.value)
              ],
            ),
            const ThemedDivider(),
            addVerticalSpace(Spacing.xlarge.value),
            const ThemedDivider(),
            addVerticalSpace(Spacing.medium.value),
            FinancialClaimDetailScreenEventListView(viewModel.events),
            addVerticalSpace(Spacing.large.value),
            const ThemedDivider(),
            ListTile(
              contentPadding: const EdgeInsets.symmetric(horizontal: 0.0),
              title: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          "Door jou nog te betalen",
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                      ),
                      Text(
                        formatAmount(viewModel.saldo),
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const ThemedDivider(),
            addVerticalSpace(Spacing.large.value),
            if (!viewModel.isResolved) ...[
              ListTile(
                contentPadding: const EdgeInsets.symmetric(horizontal: 0.0),
                horizontalTitleGap: Spacing.medium.value,
                minLeadingWidth: 0,
                leading: SvgPicture.asset(
                  width: 30,
                  height: 30,
                  "assets/icons/direct-betalen.svg",
                ),
                title: Text(
                  'Te betalen voor ${DateFormat.yMMMMEEEEd("nl").format(viewModel.dueDate!)}',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ),
            ],
          ],
        ),
      ),
    );
  }
}

class _Footer extends StatelessWidget {
  const _Footer();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: responsiveValue(
          context,
          xs: Spacing.medium.value,
          sm: Spacing.large.value,
        ),
        horizontal: responsiveValue(
          context,
          xs: Spacing.large.value,
          md: Spacing.xxlarge.value,
          xl: Spacing.xxxlarge.value,
        ),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(RadiusSize.medium.value),
        ),
        color: responsiveValue(
          context,
          xs: Theme.of(context).colorScheme.surface,
          md: Theme.of(context).colorScheme.surfaceVariant,
        ),
      ),
      child: const OrganizationInfoBlock(
        phoneNumber: '(058) 234 21 30',
        emailAddress: 'test-account@test.nl',
        websiteUrl: 'https://www.cjib.nl/digitaalloket',
      ),
    );
  }
}
