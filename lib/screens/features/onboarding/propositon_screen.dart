// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/screens/features/onboarding/components/proposition_text_row.dart';
import 'package:user_ui/screens/features/onboarding/data_use_screen.dart';
import 'package:user_ui/theme/basic_scaffold.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_button.dart';

class PropositionScreen extends StatelessWidget {
  static const routeName = 'PropositionScreen';

  const PropositionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      content: Container(
        alignment: Alignment.topCenter,
        constraints: BoxConstraints(maxWidth: maxWidthConstraints(context)),
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                addVerticalSpace(Spacing.large.value),
                const _Header(),
                const _Body(),
                const _Footer(),
                addVerticalSpace(Spacing.large.value),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header();

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Text(
        "Dit heb je aan de app",
        style: Theme.of(context).textTheme.displayLarge,
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  static const double imageAspectRatio = 4 / 3;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        addVerticalSpace(Spacing.xlarge.value),
        PropositionTextRow(
          text: TextSpan(
            style: Theme.of(context).textTheme.bodyLarge,
            children: const [
              TextSpan(text: 'Je ziet welke rekeningen openstaan bij '),
              TextSpan(
                text: 'publieke dienstverleners ',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              TextSpan(
                  text: '(bijvoorbeeld belastingen of een verkeersboete).'),
            ],
          ),
        ),
        addVerticalSpace(Spacing.xlarge.value),
        PropositionTextRow(
          text: TextSpan(
            style: Theme.of(context).textTheme.bodyLarge,
            children: const <TextSpan>[
              TextSpan(text: 'Je ziet alle informatie '),
              TextSpan(
                text: 'makkelijk en snel in één overzicht.',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        addVerticalSpace(Spacing.xlarge.value),
        PropositionTextRow(
          text: TextSpan(
            style: Theme.of(context).textTheme.bodyLarge,
            children: const <TextSpan>[
              TextSpan(
                text: 'Gemakkelijk je ',
              ),
              TextSpan(
                text: 'geldzaken ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              TextSpan(
                text: 'regelen.',
              ),
            ],
          ),
        ),
        addVerticalSpace(Spacing.xlarge.value),
        AspectRatio(
          aspectRatio: imageAspectRatio,
          child: SvgPicture.asset(
            "assets/images/video_placeholder_cropped.svg",
            semanticsLabel: 'Lock Icon',
            fit: BoxFit.fitWidth,
            clipBehavior: Clip.hardEdge,
          ),
        ),
        addVerticalSpace(Spacing.xlarge.value),
      ],
    );
  }
}

class _Footer extends StatelessWidget {
  const _Footer();

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return ThemedButton(
        width: constraints.maxWidth *
            responsiveValue(context, xs: 1, md: maxWidthPercentage),
        buttonText: "Volgende",
        style: ThemedButtonStyle.primary,
        onPress: () => context.goNamed(DataUseScreen.routeName),
      );
    });
  }
}
