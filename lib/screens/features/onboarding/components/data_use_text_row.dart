import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:user_ui/theme/spacing.dart';

class DataUseTextRow extends StatelessWidget {
  const DataUseTextRow({
    super.key,
    this.text,
  });

  final InlineSpan? text;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 4.0),
          child: SvgPicture.asset(
            width: 30,
            height: 30,
            "assets/icons/lock.svg",
            semanticsLabel: 'Lock Icon',
          ),
        ),
        addHorizontalSpace(defaultPadding),
        Flexible(
          child: RichText(
            softWrap: true,
            text: text ?? const TextSpan(text: ''),
          ),
        ),
      ],
    );
  }
}
