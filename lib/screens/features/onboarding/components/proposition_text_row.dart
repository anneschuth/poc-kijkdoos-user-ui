import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:user_ui/theme/spacing.dart';

class PropositionTextRow extends StatelessWidget {
  const PropositionTextRow({
    super.key,
    this.text,
  });

  final InlineSpan? text;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SvgPicture.asset(
          width: 30,
          height: 30,
          "assets/icons/checkmark.svg",
          semanticsLabel: 'Checkmark Icon',
        ),
        addHorizontalSpace(defaultPadding),
        Flexible(
          child: RichText(
            softWrap: true,
            text: text ?? const TextSpan(text: ''),
          ),
        ),
      ],
    );
  }
}
