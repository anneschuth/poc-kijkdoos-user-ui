// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/onboarding/components/data_use_text_row.dart';
import 'package:user_ui/theme/basic_scaffold.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/usecases/onboarding_usecase.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_button.dart';

class DataUseScreen extends ConsumerWidget {
  static const routeName = 'DataUseScreen';

  const DataUseScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return BasicScaffold(
      content: Container(
        alignment: Alignment.topCenter,
        constraints: BoxConstraints(maxWidth: maxWidthConstraints(context)),
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                addVerticalSpace(Spacing.large.value),
                const _Header(),
                const _Body(),
                const _Footer(),
                addVerticalSpace(Spacing.large.value),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header();

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Text(
        "Zo gebruikt de app je gegevens",
        style: Theme.of(context).textTheme.displayLarge,
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        addVerticalSpace(Spacing.xlarge.value),
        DataUseTextRow(
          text: TextSpan(
            style: Theme.of(context).textTheme.bodyLarge,
            children: const <TextSpan>[
              TextSpan(text: 'Gegevens worden '),
              TextSpan(
                text: 'versleuteld ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              TextSpan(text: 'en via een '),
              TextSpan(
                text: 'veilige ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              TextSpan(text: 'verbinding opgehaald.'),
            ],
          ),
        ),
        addVerticalSpace(Spacing.xlarge.value),
        DataUseTextRow(
          text: TextSpan(
            style: Theme.of(context).textTheme.bodyLarge,
            children: const <TextSpan>[
              TextSpan(
                text:
                    'Met de app haal je je gegevens rechtstreeks op bij elke organisatie die je selecteert. ',
              ),
              TextSpan(
                text:
                    'Niemand anders dan jijzelf verwerkt je gegevens of krijgt ze te zien.',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        addVerticalSpace(Spacing.xlarge.value),
        DataUseTextRow(
          text: TextSpan(
            style: Theme.of(context).textTheme.bodyLarge,
            children: const [
              TextSpan(
                text: 'Je kiest zelf ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              TextSpan(
                text:
                    'hoe je je gegevens wilt gebruiken en of je ze wel of niet wilt delen met anderen.',
              ),
            ],
          ),
        ),
        addVerticalSpace(Spacing.xlarge.value),
        DataUseTextRow(
          text: TextSpan(
            style: Theme.of(context).textTheme.bodyLarge,
            children: const [
              TextSpan(
                text: 'Je kunt het overzicht altijd weer ',
              ),
              TextSpan(
                text: 'eenvoudig weggooien of opnieuw oproepen.',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        addVerticalSpace(Spacing.xlarge.value),
      ],
    );
  }
}

class _Footer extends ConsumerWidget {
  const _Footer();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return ThemedButton(
          width: constraints.maxWidth *
              responsiveValue(context, xs: 1, md: maxWidthPercentage),
          buttonText: "Volgende",
          style: ThemedButtonStyle.primary,
          onPress: () {
            final onboardingUsecase = ref.read(onboardingUsecaseProvider);
            onboardingUsecase.setHasSeenOnboarding(true).then(
              (value) {
                if (ref
                    .read(userSettingsRepositoryProvider)
                    .hasCompletedRegistration) {
                  context.goNamed(HomeScreen.routeName);
                } else {
                  context.goNamed(ActivationInfoScreen.routeName);
                }
              },
            );
          },
        );
      },
    );
  }
}
