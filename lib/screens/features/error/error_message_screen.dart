import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/theme/themed_button.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/theme/spacing.dart';

class ErrorMessageScreen extends StatelessWidget {
  const ErrorMessageScreen(this.errorMessage, {super.key});

  static const routeName = 'ErrorMessageScreen';

  final String errorMessage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            errorMessage,
            style: Theme.of(context)
                .textTheme
                .titleLarge!
                .copyWith(color: Colors.red),
          ),
          addVerticalSpace(defaultPadding),
          ThemedButton(
              buttonText: "Verder",
              onPress: () => context.goNamed(HomeScreen.routeName))
        ],
      ),
    );
  }
}
