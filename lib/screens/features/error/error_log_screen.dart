import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/logging/logging_repository.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/basic_scaffold.dart';

class ErrorLogScreen extends ConsumerWidget {
  const ErrorLogScreen({super.key});

  static const routeName = 'ErrorLogScreen';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logs = ref.watch(loggingRepositoryProvider);

    return BasicScaffold(
      title: 'Foutenlogboek',
      showDrawer: true,
      content: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: logs.length,
              itemBuilder: (context, i) {
                return ListTile(
                  title: Row(
                    children: [
                      Container(
                        width: 4,
                        height: Theme.of(context).textTheme.bodyLarge?.fontSize,
                        color: _getColorForSeverity(logs[i].severity),
                      ),
                      addHorizontalSpace(Spacing.medium.value),
                      Flexible(
                        child: Text(
                            '${logs[i].timestamp.toIso8601String()} - ${logs[i].deviceEventClassId.toUpperCase()} ${logs[i].name}'),
                      ),
                    ],
                  ),
                  titleTextStyle: Theme.of(context).textTheme.bodyLarge,
                  subtitle: Text(logs[i].flexString1),
                  subtitleTextStyle: Theme.of(context).textTheme.bodyMedium,
                  tileColor: i % 2 == 0 ? colorLightGrey : colorWhite,
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Color _getColorForSeverity(int severity) {
    if (severity >= 0 && severity <= 3) {
      return colorDarkBlue;
    } else if (severity >= 4 && severity <= 6) {
      return colorYellow;
    } else if (severity >= 7 && severity <= 8) {
      return colorOrange;
    } else if (severity >= 9 && severity <= 10) {
      return colorRed;
    } else {
      return colorGrey;
    }
  }
}
