// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:user_ui/screens/common/loading_indicator_screen.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/usecases/registration/start_registration_and_get_redirect_uri.dart';

class StartActivationScreen extends ConsumerWidget {
  static const routeName = 'StartActivationScreen';

  const StartActivationScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final loadingIndicatorScreen = LoadingIndicatorScreen(
      onBackbuttonPress: () => context.goNamed(ActivationInfoScreen.routeName),
      text: "App activeren voorbereiden...",
      addDeepLink: true,
    );

    return ref.watch(startRegistrationAndGetRedirectUriProvider).when(
          data: (redirectUri) {
            launchUrl(
              redirectUri,
              mode: LaunchMode.externalApplication,
              webOnlyWindowName: '_self',
            );

            return loadingIndicatorScreen;
          },
          error: (err, stack) => Text('Error: $err'),
          loading: () => loadingIndicatorScreen,
        );
  }
}
