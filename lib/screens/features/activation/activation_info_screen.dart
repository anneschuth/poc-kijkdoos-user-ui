// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/entities/scheme_app_manager.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/loading_indicator_screen.dart';
import 'package:user_ui/screens/features/activation/start_activation_screen.dart';
import 'package:user_ui/screens/features/onboarding/data_use_screen.dart';
import 'package:user_ui/theme/basic_scaffold.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_button.dart';
import 'package:user_ui/usecases/app_manager_selection_usecase.dart';
import 'package:user_ui/usecases/scheme_usecase.dart';

class ActivationInfoScreen extends ConsumerStatefulWidget {
  static const routeName = 'ActivationInfoScreen';

  const ActivationInfoScreen({super.key});

  @override
  ActivationInfoScreenState createState() => ActivationInfoScreenState();
}

class ActivationInfoScreenState extends ConsumerState<ActivationInfoScreen> {
  bool _loading = true;

  @override
  void initState() {
    super.initState();
    updateLocalScheme(
      onComplete: () => setState(() {
        _loading = false;
      }),
    );
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return _loading
        ? LoadingIndicatorScreen(
            onBackbuttonPress: () => context.goNamed(DataUseScreen.routeName),
            text: "Bezig met het voorbereiden van de app...")
        : BasicScaffold(
            onBackbuttonPress: () => context.goNamed(DataUseScreen.routeName),
            content: Container(
              alignment: Alignment.topCenter,
              constraints:
                  BoxConstraints(maxWidth: maxWidthConstraints(context)),
              child: Scrollbar(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      addVerticalSpace(Spacing.large.value),
                      const _Header(),
                      const _Body(),
                      const _Footer(),
                      addVerticalSpace(Spacing.large.value),
                    ],
                  ),
                ),
              ),
            ),
          );
  }

  void updateLocalScheme({required Function onComplete}) {
    final schemeUsecase = ref.read(schemeUsecaseProvider);

    schemeUsecase.updateLocalScheme().then(
      (_) async {
        if (ref.read(userSettingsRepositoryProvider).delay) {
          await Future.delayed(const Duration(milliseconds: 3000));
        }
      },
    ).then((_) => onComplete());
  }
}

class _Header extends StatelessWidget {
  const _Header();

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Text(
        "Je gaat de VO Rijk app activeren",
        style: Theme.of(context).textTheme.displayLarge,
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  static const double imageAspectRatio = 4 / 3;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addVerticalSpace(Spacing.xlarge.value),
        Text(
          "Om deze app te kunnen gebruiken moet je hem eerst activeren.",
          style: Theme.of(context)
              .textTheme
              .titleLarge
              ?.copyWith(color: colorBlack),
        ),
        addVerticalSpace(Spacing.large.value),
        Text(
          "Dit doe je door één keer in te loggen met jouw DigiD.",
          style: Theme.of(context)
              .textTheme
              .titleLarge
              ?.copyWith(color: colorBlack),
        ),
        addVerticalSpace(Spacing.large.value),
        Text(
          "Hierdoor komen je naam, geboortedatum en burgerservice-nummer in de app te staan.",
          style: Theme.of(context)
              .textTheme
              .titleLarge
              ?.copyWith(color: colorBlack),
        ),
        addVerticalSpace(Spacing.large.value),
        AspectRatio(
          aspectRatio: imageAspectRatio,
          child: SvgPicture.asset(
            "assets/images/digid.svg",
            semanticsLabel: 'Lock Icon',
            fit: BoxFit.fitWidth,
            clipBehavior: Clip.hardEdge,
          ),
        ),
        addVerticalSpace(Spacing.xlarge.value),
      ],
    );
  }
}

class _Footer extends ConsumerWidget {
  const _Footer();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final schemeAppManagerRepository =
        ref.watch(schemeAppManagerRepositoryProvider);
    return LayoutBuilder(builder: (context, constraints) {
      final double buttonWidth = constraints.maxWidth *
          responsiveValue(context, xs: 1, md: maxWidthPercentage);
      return Column(
        children: [
          ThemedButton(
            width: buttonWidth,
            buttonText: "Activeer met DigiD",
            buttonIcon: SvgPicture.asset(
              width: 24,
              height: 24,
              "assets/images/digid_icon.svg",
            ),
            style: ThemedButtonStyle.primary,
            onPress: () => selectAppManager(
              schemeAppManagerRepository
                  .where((appManager) => appManager.available)
                  .first, //TODO: error handling if no appManagers are found
              ref,
              onComplete: () =>
                  context.goNamed(StartActivationScreen.routeName),
            ),
          ),
          addVerticalSpace(Spacing.large.value),
          ThemedButton(
            width: buttonWidth,
            style: ThemedButtonStyle.tertiary,
            onPress: null,
            buttonText: 'ik heb geen DigiD',
          ),
        ],
      );
    });
  }

  void selectAppManager(SchemeAppManager appManager, WidgetRef ref,
      {required Function onComplete}) {
    final appManagerSelectionUsecase =
        ref.read(appManagerSelectionUsecaseProvider);

    appManagerSelectionUsecase
        .setAppManagerSelection(appManager.oin)
        .catchError((error) {
      debugPrint(error.toString()); // TODO: error handling

      throw error;
    }).then((_) => onComplete());
  }
}
