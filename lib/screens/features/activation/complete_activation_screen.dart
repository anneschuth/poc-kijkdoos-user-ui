// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/loading_indicator_screen.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/usecases/app_usecase.dart';
import 'package:user_ui/usecases/registration/fetch_and_store_certificate.dart';

class CompleteActivationScreen extends ConsumerWidget {
  static const routeName = 'CompleteActivationScreen';

  const CompleteActivationScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(fetchAndStoreCertificateProvider).when(
          data: (_) {
            final appUsecase = ref.read(appUsecaseProvider);
            appUsecase.setHasCompletedRegistration(true);
            context.goNamed(HomeScreen.routeName);
            return const SizedBox.shrink();
          },
          error: (err, stack) {
            context.goNamed(ActivationInfoScreen.routeName);
            return Text('Error: $err');
          },
          loading: () => const LoadingIndicatorScreen(
            showDrawer: true,
            text: "Bezig met activeren van de app...",
          ),
        );
  }
}
