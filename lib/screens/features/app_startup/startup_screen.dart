// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/app_identity/app_key_pair_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/error/error_message_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/onboarding/propositon_screen.dart';
import 'package:user_ui/usecases/app_identity_usecase.dart';

class StartupScreen extends ConsumerWidget {
  const StartupScreen({super.key});

  static const routeName = 'StartupScreen';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final loadingSaffold = Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.asset(
                  'assets/icons/app-icon.png',
                  height: 128,
                  width: 128,
                ),
              ),
            ],
          ),
          const SizedBox(height: 72.0),
          const Text('Nog heel even geduld,\n de app wordt ingesteld'),
          const SizedBox(height: 48.0),
          const SizedBox(
            width: 32,
            height: 32,
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    );

    return ref.watch(appKeyPairRepositoryProvider).when(
          data: (appKeyPair) {
            if (appKeyPair == null) {
              ref.read(appIdentityUsecaseProvider).generateKeypair();
            } else {
              if (!ref.read(userSettingsRepositoryProvider).hasSeenOnboarding) {
                context.goNamed(PropositionScreen.routeName);
              } else if (!ref
                  .read(userSettingsRepositoryProvider)
                  .hasCompletedRegistration) {
                context.goNamed(ActivationInfoScreen.routeName);
              } else {
                context.goNamed(HomeScreen.routeName);
              }
            }

            return loadingSaffold;
          },
          error: (err, stack) {
            context.goNamed(ErrorMessageScreen.routeName);
            return Text('Error: $err');
          },
          loading: () => loadingSaffold,
        );
  }
}
