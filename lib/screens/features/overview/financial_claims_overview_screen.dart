// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/screens/providers/models/financial_claims_overview_view_model.dart';
import 'package:user_ui/screens/providers/number_of_completed_financial_claims_information_requests_state_notifier.dart';
import 'package:user_ui/screens/features/overview/components/overdue_financial_claims_card.dart';
import 'package:user_ui/screens/features/overview/components/resolved_financial_claims_card.dart';
import 'package:user_ui/screens/features/overview/components/needs_payment_financial_claims_card.dart';
import 'package:user_ui/screens/features/overview/components/loading_header.dart';
import 'package:user_ui/screens/features/overview/components/unopened_inbox_snack_bar.dart';
import 'package:user_ui/screens/providers/number_of_selected_organizations_state_notifier.dart';
import 'package:user_ui/screens/providers/financial_claims_overview_view_model_state_notifier.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/basic_scaffold.dart';
import 'package:user_ui/usecases/financial_claims_information/financial_claims_information_usecase.dart';
import 'package:user_ui/usecases/request_financial_claims_information/request_financial_claims_information_usecase.dart';

class FinancialClaimsOverviewScreen extends ConsumerWidget {
  const FinancialClaimsOverviewScreen({super.key});

  static const routeName = 'FinancialClaimsOverviewScreen';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var numberOfSelectedOrganizations =
        ref.watch(numberOfSelectedOrganizationsProvider);

    var numberOfCompletedFinancialClaimsInformationRequests = ref.watch(
        numberOfCompletedFinancialClaimsInformationRequestsStateNotifierProvider);

    var financialClaimsOverviewViewModel =
        ref.watch(financialClaimsOverviewViewModelStateNotifierProvider);

    var financialClaimsInformationInboxRepository = ref.watch(
        financialClaimsInformationInboxRepositoryProvider.select((inboxItems) =>
            inboxItems.where((inboxItem) => !inboxItem.copiedToStorage)));

    return BasicScaffold(
      title: 'Mijn overzicht',
      showDrawer: true,
      useGreyBackground: true,
      content: Column(
        children: [
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async => {
                await ref
                    .read(requestFinancialClaimsInformationUsecaseProvider)
                    .createFinancialClaimsInformationRequestsForSelectedOrganizations(),
              },
              child: ListView(
                clipBehavior: Clip.none,
                children: [
                  _Body(
                    numberOfSelectedOrganizations:
                        numberOfSelectedOrganizations,
                    numberOfCompletedFinancialClaimsInformationRequests:
                        numberOfCompletedFinancialClaimsInformationRequests,
                    financialClaimsOverviewViewModel:
                        financialClaimsOverviewViewModel,
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              children: [
                if (financialClaimsInformationInboxRepository.isNotEmpty)
                  UnopenedInboxSnackBar(
                    onPressed: ref
                        .read(financialClaimsInformationUsecaseProvider)
                        .copyFinancialClaimsInformationInboxToStorage,
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required this.numberOfSelectedOrganizations,
    required this.numberOfCompletedFinancialClaimsInformationRequests,
    required this.financialClaimsOverviewViewModel,
  });

  final int numberOfSelectedOrganizations;
  final int numberOfCompletedFinancialClaimsInformationRequests;
  final FinancialClaimsOverviewViewModel financialClaimsOverviewViewModel;

  @override
  Widget build(BuildContext context) {
    final responsivePadding = responsiveValue(
      context,
      xs: 0.0,
      md: 8.0,
      lg: 10.0,
      xl: 12.0,
    );

    return ResponsiveGridRow(
      children: [
        ResponsiveGridCol(
          xs: 12,
          child: Column(
            children: [
              const Divider(),
              LoadingHeader(
                numberOfSelectedOrganizations,
                numberOfCompletedFinancialClaimsInformationRequests,
              ),
              const Divider(),
            ],
          ),
        ),
        if (financialClaimsOverviewViewModel.achterstand.isNotEmpty)
          ResponsiveGridCol(
            xs: 12,
            md: 6,
            child: Column(
              children: [
                addVerticalSpace(defaultPadding),
                Padding(
                  padding: EdgeInsets.only(
                    right: responsivePadding,
                  ),
                  child: OverdueFinancialClaimsCard(
                    financialClaimsOverviewViewModel.achterstand.values
                        .toList(),
                  ),
                ),
              ],
            ),
          ),
        if (financialClaimsOverviewViewModel.directBetalen.isNotEmpty)
          ResponsiveGridCol(
            xs: 12,
            md: 6,
            child: Column(
              children: [
                addVerticalSpace(defaultPadding),
                Padding(
                  padding: EdgeInsets.only(left: responsivePadding),
                  child: NeedsPaymentFinancialClaimsCard(
                    financialClaimsOverviewViewModel.directBetalen.values
                        .toList(),
                  ),
                ),
              ],
            ),
          ),
        if (financialClaimsOverviewViewModel.betaald.isNotEmpty)
          ResponsiveGridCol(
            xs: 12,
            child: Column(
              children: [
                addVerticalSpace(defaultPadding),
                ResolvedFinancialClaimsCard(
                  financialClaimsOverviewViewModel.betaald,
                ),
              ],
            ),
          ),
        if (financialClaimsOverviewViewModel.achterstand.isEmpty &&
            financialClaimsOverviewViewModel.directBetalen.isEmpty &&
            financialClaimsOverviewViewModel.betaald.isEmpty)
          ResponsiveGridCol(
            xs: 12,
            child: Padding(
              padding: EdgeInsets.only(left: Spacing.medium.value),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  addVerticalSpace(3 * defaultPadding),
                  Text(
                    "Bij de bevraagde organisaties zijn geen openstaande rekeningen voor jou bekend die via VO rijk beschikbaar zijn.",
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                  addVerticalSpace(3 * defaultPadding),
                  const Text(
                    "Disclaimer: VO Rijk toont de meestvoorkomende financiële verplichtingen, maar kan alleen financiële veprlichtingen tonen die digitaal beschikbaar zijn bij de deelnemende organisaties. Zij werken er aan om steeds meer typen financiële verplichtingen via VO Rijk beschikbaar te maken. ",
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }
}
