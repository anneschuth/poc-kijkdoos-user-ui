// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/theme/spacing.dart';

class UnopenedInboxSnackBar extends StatelessWidget {
  final VoidCallback onPressed;

  const UnopenedInboxSnackBar({super.key, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: const Color(0xFF323232),
      ),
      child: Padding(
        padding: const EdgeInsets.only(
          left: defaultPadding,
          top: defaultPadding,
          right: defaultPadding,
        ),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Nieuwe gegevens ontvangen.",
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: colorWhite,
                    ),
              ),
            ),
            addVerticalSpace(defaultPadding),
            Align(
              alignment: Alignment.centerRight,
              child: TextButton(
                onPressed: onPressed,
                child: Text(
                  "Voeg toe aan overzicht",
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium
                      ?.copyWith(color: colorLightBlue),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
