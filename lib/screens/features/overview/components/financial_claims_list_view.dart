// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/screens/features/overview/components/financial_claims_list_item.dart';
import 'package:user_ui/screens/providers/models/financial_claim.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/themed_divider.dart';

class FinancialClaimsListView extends StatelessWidget {
  final List<FinancialClaim> financialClaims;

  const FinancialClaimsListView(this.financialClaims, {super.key});

  @override
  Widget build(BuildContext context) {
    final borderRadiusTop = BorderRadius.only(
      topLeft: Radius.circular(RadiusSize.medium.value),
      topRight: Radius.circular(RadiusSize.medium.value),
    );
    final borderRadiusBottom = BorderRadius.only(
      bottomLeft: Radius.circular(RadiusSize.medium.value),
      bottomRight: Radius.circular(RadiusSize.medium.value),
    );

    return ListView.separated(
      shrinkWrap: true,
      separatorBuilder: (context, index) {
        return const ThemedDivider();
      },
      itemCount: financialClaims.length,
      itemBuilder: (context, index) {
        return ResponsiveWidget(
          xs: FinancialClaimsListItem(
            financialClaims[index],
          ),
          md: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.background,
              borderRadius: financialClaims[index] == financialClaims.first
                  ? borderRadiusTop
                  : financialClaims[index] == financialClaims.last
                      ? borderRadiusBottom
                      : null,
            ),
            child: FinancialClaimsListItem(
              financialClaims[index],
            ),
          ),
        );
      },
    );
  }
}
