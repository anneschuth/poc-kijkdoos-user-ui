// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:user_ui/screens/features/overview/components/financial_claims_card_content.dart';
import 'package:user_ui/screens/providers/models/financial_claim.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_card.dart';

class NeedsPaymentFinancialClaimsCard extends StatelessWidget {
  final List<FinancialClaim> needsPaymentFinancialClaimsList;

  const NeedsPaymentFinancialClaimsCard(this.needsPaymentFinancialClaimsList,
      {super.key});

  @override
  Widget build(BuildContext context) {
    return ThemedCard(
      width: MediaQuery.of(context).size.width - 40,
      child: Padding(
        padding: EdgeInsets.all(Spacing.large.value),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            addVerticalSpace(Spacing.large.value),
            Row(
              children: [
                SvgPicture.asset(
                  width: 30,
                  height: 30,
                  "assets/icons/direct-betalen.svg",
                ),
                addHorizontalSpace(Spacing.large.value / 2),
                Text(
                  'Direct betalen',
                  style: Theme.of(context).textTheme.displayMedium,
                ),
              ],
            ),
            addVerticalSpace(Spacing.large.value),
            FinancialClaimsCardContent(
                financialClaimsList: needsPaymentFinancialClaimsList),
          ],
        ),
      ),
    );
  }
}
