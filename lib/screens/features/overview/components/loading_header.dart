// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/theme/colors.dart';

class LoadingHeader extends StatelessWidget {
  const LoadingHeader(this.numberOfSelectedOrganizations,
      this.numberOfCompletedFinancialClaimsInformationRequests,
      {super.key});

  final int numberOfSelectedOrganizations;
  final int numberOfCompletedFinancialClaimsInformationRequests;

  @override
  Widget build(BuildContext context) {
    var determinateValue = 0.0;

    if (numberOfSelectedOrganizations > 0) {
      determinateValue = numberOfCompletedFinancialClaimsInformationRequests /
          numberOfSelectedOrganizations;
    }

    return Column(children: [
      ListTile(
        leading: Stack(
          alignment: Alignment.center,
          children: [
            const Icon(
              Icons.check,
              color: colorGreen,
              size: 28.0,
            ),
            CircularProgressIndicator(
              value: determinateValue,
              color: colorGreen,
              backgroundColor: colorLightGrey2,
              strokeWidth: 3.0,
            ),
          ],
        ),
        title: Text(
          'Organisaties',
          style: Theme.of(context).textTheme.displayMedium,
        ),
        subtitle: Text(
          '$numberOfCompletedFinancialClaimsInformationRequests van de $numberOfSelectedOrganizations hebben gereageerd.',
          style: Theme.of(context).textTheme.titleMedium,
        ),
        // trailing: const Icon(
        //   Icons.chevron_right,
        //   color: colorLightGrey2,
        // ),
      ),
    ]);
  }
}
