import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/screens/features/overview/components/financial_claims_list_view.dart';
import 'package:user_ui/screens/providers/models/financial_claim.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_divider.dart';

class FinancialClaimsCardContent extends StatelessWidget {
  const FinancialClaimsCardContent({
    super.key,
    required this.financialClaimsList,
  });

  final List<FinancialClaim> financialClaimsList;
  static const double cardHeight = 112;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: responsiveValue(
        context,
        xs: const BoxConstraints(minHeight: 0),
        md: const BoxConstraints(
          minHeight: cardHeight,
          maxHeight: cardHeight,
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            const ThemedDivider(),
            addVerticalSpace(Spacing.medium.value),
            FinancialClaimsListView(financialClaimsList),
            addVerticalSpace(Spacing.large.value),
            const ThemedDivider(),
          ],
        ),
      ),
    );
  }
}
