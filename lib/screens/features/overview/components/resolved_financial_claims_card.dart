// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/screens/features/overview/components/financial_claims_list_view.dart';
import 'package:user_ui/screens/providers/models/financial_claim.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';

class ResolvedFinancialClaimsCard extends StatelessWidget {
  final List<FinancialClaim> resolvedFinancialClaimsList;

  const ResolvedFinancialClaimsCard(this.resolvedFinancialClaimsList,
      {super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addVerticalSpace(defaultPadding),
        Padding(
          padding: EdgeInsets.only(
            left: responsiveValue(
              context,
              xs: 0,
              md: Spacing.large.value,
            ),
          ),
          child: Text(
            'Betaald',
            style: Theme.of(context).textTheme.displayMedium,
          ),
        ),
        FinancialClaimsListView(resolvedFinancialClaimsList),
      ],
    );
  }
}
