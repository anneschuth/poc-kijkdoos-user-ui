// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:user_ui/screens/features/financial_claim_detail/financial_claim_detail_screen.dart';
import 'package:user_ui/screens/providers/models/financial_claim.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/utils/saldo_formatter.dart';

class FinancialClaimsListItem extends StatelessWidget {
  final FinancialClaim financialClaim;

  const FinancialClaimsListItem(this.financialClaim, {super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        context.pushNamed(
          FinancialClaimDetailScreen.routeName,
          pathParameters: <String, String>{
            "financialClaimsInformationStorageId":
                financialClaim.financialClaimsInformationStorageId.toString(),
            "zaakkenmerk": financialClaim.zaakkenmerk
          },
        );
      },
      title: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  financialClaim.title,
                  maxLines: 1,
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge
                      ?.copyWith(color: colorDarkBlue),
                ),
              ),
              Text(
                formatAmount(financialClaim.saldo),
                style: Theme.of(context)
                    .textTheme
                    .titleLarge
                    ?.copyWith(color: colorBlack2),
              ),
            ],
          ),
        ],
      ),
      subtitle: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  '${DateFormat.yMMMMd("nl").format(financialClaim.saldoDateTime)} | ${financialClaim.organizationName}',
                  maxLines: 2,
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.titleSmall,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
