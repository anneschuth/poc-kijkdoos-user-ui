// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:lottie/lottie.dart';
import 'package:user_ui/screens/providers/number_of_completed_financial_claims_information_requests_state_notifier.dart';
import 'package:user_ui/screens/features/overview/financial_claims_overview_screen.dart';
import 'package:user_ui/screens/providers/number_of_selected_organizations_state_notifier.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_button.dart';
import 'package:user_ui/theme/themed_screen.dart';
import 'package:user_ui/usecases/app_usecase.dart';
import 'package:user_ui/usecases/financial_claims_information/financial_claims_information_usecase.dart';
import 'package:user_ui/usecases/request_financial_claims_information/request_financial_claims_information_usecase.dart';

class LoadingScreen extends ConsumerStatefulWidget {
  const LoadingScreen({super.key});

  static const routeName = 'LoadingScreen';

  @override
  LoadingScreenViewState createState() => LoadingScreenViewState();
}

class LoadingScreenViewState extends ConsumerState<LoadingScreen> {
  int _phase = 0;

  @override
  void initState() {
    super.initState();
    ref
        .read(appUsecaseProvider)
        .setHasCompletedOrganizationSelection(true)
        .then((_) => ref
            .read(requestFinancialClaimsInformationUsecaseProvider)
            .createFinancialClaimsInformationRequestsForSelectedOrganizations())
        .then((_) {
      Future.delayed(const Duration(seconds: 3)).then((_) {
        setState(() {
          _phase = 1;
        });
      }).then((value) {
        Future.delayed(const Duration(seconds: 3)).then((_) {
          setState(() {
            _phase = 2;
          });
        });
      });
    });
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    var numberOfSelectedOrganizations =
        ref.watch(numberOfSelectedOrganizationsProvider);

    var numberOfCompletedFinancialClaimsInformationRequests = ref.watch(
        numberOfCompletedFinancialClaimsInformationRequestsStateNotifierProvider);

    String asset;
    String headerText;
    String? bodyText;
    bool showButton = false;

    if (_phase == 2) {
      showButton = numberOfCompletedFinancialClaimsInformationRequests > 0;

      if (numberOfCompletedFinancialClaimsInformationRequests ==
          numberOfSelectedOrganizations) {
        asset = "assets/lottie/108373-job-list.json";

        headerText = numberOfSelectedOrganizations == 1
            ? "De organisatie heeft gereageerd"
            : "Alle organisaties hebben gereageerd";

        bodyText = null;
      } else {
        asset = "assets/lottie/42766-paper-planes.json";

        headerText = "Je gegevens worden opgehaald...";

        bodyText = numberOfSelectedOrganizations == 1
            ? "De organisatie heeft nog niet gereageerd"
            : "$numberOfCompletedFinancialClaimsInformationRequests van de $numberOfSelectedOrganizations organisaties hebben gereageerd";
      }
    } else if (_phase == 1) {
      asset = "assets/lottie/42766-paper-planes.json";

      headerText = "Je gegevens worden opgehaald...";

      bodyText = numberOfSelectedOrganizations == 1
          ? "De organisatie heeft nog niet gereageerd"
          : "$numberOfCompletedFinancialClaimsInformationRequests van de $numberOfSelectedOrganizations organisaties hebben gereageerd";

      showButton = numberOfCompletedFinancialClaimsInformationRequests > 0;
    } else {
      asset = "assets/lottie/9688-origami-email-to-paper-plane-poof.json";

      headerText = numberOfSelectedOrganizations == 1
          ? "Je verzoek wordt verstuurd..."
          : "Je verzoeken worden verstuurd...";

      bodyText = numberOfSelectedOrganizations == 1
          ? "De organisatie wordt bevraagd"
          : "$numberOfSelectedOrganizations organisaties worden bevraagd";
    }

    return ThemedScreen(
      childrenPadding: true,
      showDrawer: true,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      footer: [
        if (showButton)
          Container(
            margin: const EdgeInsets.all(defaultPadding),
            child: ThemedButton(
              buttonText: "Bekijk het overzicht",
              style: ThemedButtonStyle.primary,
              onPress: () {
                ref
                    .read(financialClaimsInformationUsecaseProvider)
                    .copyFinancialClaimsInformationInboxToStorage()
                    .then((_) => context
                        .goNamed(FinancialClaimsOverviewScreen.routeName));
              },
            ),
          ),
        if (!showButton) addVerticalSpace(defaultPadding * 3)
      ],
      children: [
        Column(
          children: <Widget>[
            addVerticalSpace(defaultPadding * 2),
            Lottie.asset(asset, height: 200),
            addVerticalSpace(defaultPadding * 2),
            Text(
              headerText,
              style: Theme.of(context).textTheme.displayMedium,
              textAlign: TextAlign.center,
            ),
            addVerticalSpace(defaultPadding * 2),
            if (bodyText != null)
              Text(
                bodyText,
                style: Theme.of(context).textTheme.bodyMedium,
                textAlign: TextAlign.center,
              ),
          ],
        ),
      ],
    );
  }
}
