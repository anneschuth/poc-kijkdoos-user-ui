// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/loading_indicator_screen.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/screens/features/organization_selection/auto_select_all_organizations_screen.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_notifier.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_button.dart';
import 'package:user_ui/theme/themed_screen.dart';
import 'package:user_ui/usecases/organization_selection_usecase.dart';

class OrganizationSelectionScreen extends ConsumerWidget {
  static const routeName = 'OrganizationSelectionScreen';

  const OrganizationSelectionScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final organizationSelectionScreenViewModelAsyncValue =
        ref.watch(organizationSelectionScreenNotifierProvider);

    return organizationSelectionScreenViewModelAsyncValue.when(
      skipLoadingOnReload: true,
      data: (organizationSelectionScreenViewModel) => ThemedScreen(
        headingText: 'Welke organisaties wil je om informatie vragen?',
        childrenPadding: true,
        onBackbuttonPress: () =>
            context.goNamed(AutoSelectAllOrganizationsScreen.routeName),
        footer: [
          Container(
            margin: const EdgeInsets.all(defaultPadding),
            child: ThemedButton(
              buttonText: "Volgende",
              onPress: (organizationSelectionScreenViewModel
                      .hasSelectedOrganizations)
                  ? () => context.goNamed(LoadingScreen.routeName)
                  : null,
              style: ThemedButtonStyle.primary,
            ),
          ),
        ],
        children: [
          Text(
            "Kies zelf welke organisaties je om informatie wilt vragen. "
            "Als je een organisatie niet selecteert, weet je niet of je "
            "nog iets moet betalen aan die organisatie "
            "en is je overzicht misschien niet compleet.",
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          addVerticalSpace(defaultPadding),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ListView.separated(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                separatorBuilder: (context, index) {
                  return const Padding(
                    padding: EdgeInsets.only(
                      left: defaultPadding,
                      right: defaultPadding,
                    ),
                    child: Divider(
                      height: 1,
                      color: colorWhite,
                    ),
                  );
                },
                itemBuilder: (context, index) {
                  var organizationSelection =
                      organizationSelectionScreenViewModel
                          .organizationSelectionViewModels[index];
                  return CheckboxListTile(
                    value: organizationSelection.selected,
                    title: Text(
                      organizationSelection.name,
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                    onChanged: organizationSelection.available
                        ? (selected) {
                            if (selected == null) {
                              debugPrint("Disabled checkbox!");
                              return;
                            }
                            final organizationSelectionUsecase =
                                ref.read(organizationSelectionUsecaseProvider);
                            organizationSelectionUsecase
                                .setOrganizationSelection(
                                    organizationSelection.oin, selected);
                          }
                        : null,
                    tileColor: colorLightGrey,
                    checkColor: colorWhite,
                    selectedTileColor: colorDarkBlue,
                    checkboxShape: const CircleBorder(),
                    controlAffinity: ListTileControlAffinity.leading,
                  );
                },
                itemCount: organizationSelectionScreenViewModel
                    .organizationSelectionViewModels.length,
              ),
            ],
          ),
        ],
      ),
      error: (err, stack) => Text('Error: $err'),
      loading: () => LoadingIndicatorScreen(
        text: "Details laden...",
        onBackbuttonPress: () =>
            context.goNamed(AutoSelectAllOrganizationsScreen.routeName),
      ),
    );
  }
}
