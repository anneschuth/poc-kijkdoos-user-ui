// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization_selection_screen_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$organizationSelectionScreenNotifierHash() =>
    r'b31c432d8c03c4b10f96ea9593264e1c7863549e';

/// See also [OrganizationSelectionScreenNotifier].
@ProviderFor(OrganizationSelectionScreenNotifier)
final organizationSelectionScreenNotifierProvider =
    AutoDisposeAsyncNotifierProvider<OrganizationSelectionScreenNotifier,
        OrganizationSelectionScreenViewModel>.internal(
  OrganizationSelectionScreenNotifier.new,
  name: r'organizationSelectionScreenNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$organizationSelectionScreenNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$OrganizationSelectionScreenNotifier
    = AutoDisposeAsyncNotifier<OrganizationSelectionScreenViewModel>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
