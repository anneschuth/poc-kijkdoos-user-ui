// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/loading_indicator_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_button.dart';
import 'package:user_ui/theme/themed_screen.dart';
import 'package:user_ui/usecases/organization_selection_usecase.dart';
import 'package:user_ui/usecases/scheme_usecase.dart';

class AutoSelectAllOrganizationsScreen extends ConsumerStatefulWidget {
  static const routeName = 'AutoSelectAllOrganizationsScreen';

  const AutoSelectAllOrganizationsScreen({super.key});

  @override
  AutoSelectAllOrganizationsScreenViewState createState() =>
      AutoSelectAllOrganizationsScreenViewState();
}

class AutoSelectAllOrganizationsScreenViewState
    extends ConsumerState<AutoSelectAllOrganizationsScreen> {
  bool _loading = true;

  @override
  void initState() {
    super.initState();
    refreshOrganizationSelectionsFromScheme(
      onComplete: () => setState(() {
        _loading = false;
      }),
    );
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    final organizationSelections =
        ref.watch(organizationSelectionRepositoryProvider);
    return _loading
        ? LoadingIndicatorScreen(
            onBackbuttonPress: () => context.goNamed(HomeScreen.routeName),
            text: "Bezig met ophalen deelnemende organisaties...")
        : ThemedScreen(
            headingText: "Welke organisaties wil je om informatie vragen?",
            childrenPadding: true,
            onBackbuttonPress: () => context.goNamed(HomeScreen.routeName),
            footer: [
              Container(
                margin: const EdgeInsets.all(defaultPadding),
                child: Column(
                  children: [
                    ThemedButton(
                      style: ThemedButtonStyle.primary,
                      onPress: () => autoSelectAllOrganizations(
                          onComplete: () =>
                              context.goNamed(LoadingScreen.routeName)),
                      buttonText:
                          'Ja vraag alle ${organizationSelections.length} organisaties',
                    ),
                    addVerticalSpace(defaultPadding),
                    ThemedButton(
                      style: ThemedButtonStyle.tertiary,
                      onPress: () => customSelectOrganizations(
                          onComplete: () => context
                              .goNamed(OrganizationSelectionScreen.routeName)),
                      buttonText: 'Nee, ik wil zelf organisaties kiezen',
                    ),
                  ],
                ),
              ),
            ],
            children: [
              Text(
                "Je kunt bij alle organisaties informatie opvragen of zelf "
                "kiezen bij welke organisaties je informatie opvraagt.\n\n"
                "Wil je alle deelnemende organisaties om informatie vragen?",
                style: Theme.of(context).textTheme.bodyLarge,
              ),
            ],
          );
  }

  void refreshOrganizationSelectionsFromScheme({required Function onComplete}) {
    final schemeUsecase = ref.read(schemeUsecaseProvider);
    final organizationSelectionUsecase =
        ref.read(organizationSelectionUsecaseProvider);
    schemeUsecase
        .updateLocalScheme()
        .then(
          (_) async {
            if (ref.read(userSettingsRepositoryProvider).delay) {
              await Future.delayed(const Duration(milliseconds: 2000));
            }
          },
        )
        .then((_) =>
            organizationSelectionUsecase.syncSelectedOrganizationsWithScheme())
        .catchError((error) {
          debugPrint(error.toString()); // TODO error handling

          throw error;
        })
        .then((_) => onComplete());
  }

  void autoSelectAllOrganizations({required Function onComplete}) {
    ref
        .read(organizationSelectionUsecaseProvider)
        .setAutoSelectAllOrganizations(true)
        .then((_) => ref
            .read(organizationSelectionUsecaseProvider)
            .selectAllOrganizations())
        .then((_) => onComplete());
  }

  void customSelectOrganizations({required Function onComplete}) {
    final organizationSelectionUsecase =
        ref.read(organizationSelectionUsecaseProvider);
    organizationSelectionUsecase
        .setAutoSelectAllOrganizations(false)
        .then((_) => onComplete());
  }
}
