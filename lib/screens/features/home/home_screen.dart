// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/repositories/registration/latest_completed_registration_notifier.dart';
import 'package:user_ui/screens/providers/number_of_completed_financial_claims_information_requests_state_notifier.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/features/home/components/start_organization_selection_theme_card.dart';
import 'package:user_ui/screens/features/home/components/summary_theme_card.dart';
import 'package:user_ui/screens/providers/number_of_selected_organizations_state_notifier.dart';
import 'package:user_ui/theme/basic_scaffold.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';

class HomeScreen extends ConsumerWidget {
  static const String routeName = 'HomeScreen';
  static const double negativePadding = -32;

  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //TODO: Change all ref watches to 1 screen notifier
    final userSettings = ref.watch(userSettingsRepositoryProvider);

    int numberOfSelectedOrganizations = 0;
    int numberOfCompletedFinancialClaimsInformationRequests = 0;
    int numberOfFinancialClaimsInformationStorages = 0;

    if (userSettings.hasCompletedOrganizationSelection) {
      ref.watch(organizationSelectionRepositoryProvider);

      numberOfFinancialClaimsInformationStorages =
          ref.watch(financialClaimsInformationStorageRepositoryProvider).length;

      numberOfSelectedOrganizations =
          ref.watch(numberOfSelectedOrganizationsProvider);

      numberOfCompletedFinancialClaimsInformationRequests = ref.watch(
          numberOfCompletedFinancialClaimsInformationRequestsStateNotifierProvider);
    }

    return BasicScaffold(
      showDrawer: true,
      title: ref.watch(latestCompletedRegistrationProvider).when(
          data: (latestCompletedRegistration) =>
              "Hallo ${latestCompletedRegistration.givenName}",
          error: (err, stack) => "Error bij ophalen van naam",
          loading: () => "Hallo"),
      content: Container(
        transform: Matrix4.translationValues(
            0.0, -negativePadding, 0.0), // Needed to dissolve negative padding
        child: Column(
          mainAxisAlignment: responsiveValue(context,
              xs: MainAxisAlignment.end, md: MainAxisAlignment.center),
          children: [
            Flexible(
              child: SvgPicture.asset(
                "assets/images/home.svg",
              ),
            ),
            Container(
              constraints:
                  BoxConstraints(maxWidth: maxWidthConstraints(context)),
              transform: Matrix4.translationValues(0.0, negativePadding, 0.0),
              child: userSettings.hasCompletedOrganizationSelection
                  ? SummaryThemeCard(
                      numberOfSelectedOrganizations:
                          numberOfSelectedOrganizations,
                      numberOfCompletedFinancialClaimsInformationRequests:
                          numberOfCompletedFinancialClaimsInformationRequests,
                      numberOfFinancialClaimsInformationStorages:
                          numberOfFinancialClaimsInformationStorages,
                    )
                  : const StartOrganizationSelectionThemeCard(),
            ),
            addVerticalSpace(
              responsiveValue(
                context,
                xs: Spacing.large.value,
                sm: Spacing.small.value,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
