import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/screens/features/organization_selection/auto_select_all_organizations_screen.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_button.dart';
import 'package:user_ui/theme/themed_card.dart';

class StartOrganizationSelectionThemeCard extends StatelessWidget {
  const StartOrganizationSelectionThemeCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ThemedCard(
      child: Padding(
        padding: EdgeInsets.all(Spacing.large.value),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(Spacing.large.value),
              child: Text(
                'Stel je eigen overzicht van je openstaande rekeningen bij publieke organisaties samen (bijvoorbeeld belastingen of een verkeersboete).',
                style: Theme.of(context).textTheme.bodyLarge,
              ),
            ),
            addVerticalSpace(Spacing.medium.value),
            LayoutBuilder(builder: (context, constraints) {
              return Align(
                alignment: Alignment.center,
                child: ThemedButton(
                  width: constraints.maxWidth *
                      responsiveValue(context, xs: 1, md: maxWidthPercentage),
                  buttonText: 'Vraag je gegevens op',
                  style: ThemedButtonStyle.primary,
                  onPress: () => context
                      .goNamed(AutoSelectAllOrganizationsScreen.routeName),
                ),
              );
            })
          ],
        ),
      ),
    );
  }
}
