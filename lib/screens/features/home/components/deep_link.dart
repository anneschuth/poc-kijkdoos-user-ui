// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/features/activation/complete_activation_screen.dart';
import 'package:uni_links/uni_links.dart';

class DeepLink extends StatefulWidget {
  const DeepLink({super.key});

  @override
  DeepLinkState createState() => DeepLinkState();
}

class DeepLinkState extends State<DeepLink> {
  bool _initialURILinkHandled = false;
  StreamSubscription? _streamSubscription;

  @override
  void initState() {
    super.initState();

    _initURIHandler();
    _incomingLinkHandler();
  }

  @override
  Widget build(BuildContext context) {
    return const SizedBox.shrink();
  }

  Future<void> _initURIHandler() async {
    if (!_initialURILinkHandled) {
      _initialURILinkHandled = true;
      try {
        final initialURI = await getInitialUri();
        // Use the initialURI and warn the user if it is not correct,
        // but keep in mind it could be `null`.
        if (initialURI != null) {
          debugPrint("Initial URI received $initialURI");
          if (!mounted) {
            return;
          }
        } else {
          debugPrint("Null Initial URI received");
        }
      } on PlatformException {
        // Platform messages may fail, so we use a try/catch PlatformException.
        // Handle exception by warning the user their action did not succeed
        debugPrint("Failed to receive initial uri");
        // TODO: show message to user of the exception
      } on FormatException {
        if (!mounted) {
          return;
        }
        debugPrint('Malformed Initial URI received');
        // TODO: show message to user of the exception
      }
    }
  }

  /// Handle incoming links - the ones that the app will receive from the OS
  /// while already started.
  void _incomingLinkHandler() {
    if (!kIsWeb) {
      // It will handle app links while the app is already started - be it in
      // the foreground or in the background.
      _streamSubscription = uriLinkStream.listen((Uri? uri) {
        if (!mounted) {
          return;
        }
        debugPrint('Received URI: $uri');
        context.goNamed(CompleteActivationScreen.routeName);
      }, onError: (Object err) {
        if (!mounted) {
          return;
        }
        debugPrint('Error occurred: $err');
      });
    }
  }

  @override
  void dispose() {
    _streamSubscription?.cancel();
    super.dispose();
  }
}
