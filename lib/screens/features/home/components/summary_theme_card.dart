import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/screens/features/overview/financial_claims_overview_screen.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/theme/themed_button.dart';
import 'package:user_ui/theme/themed_card.dart';

class SummaryThemeCard extends StatelessWidget {
  const SummaryThemeCard({
    required this.numberOfSelectedOrganizations,
    required this.numberOfCompletedFinancialClaimsInformationRequests,
    required this.numberOfFinancialClaimsInformationStorages,
    super.key,
  });

  final int numberOfSelectedOrganizations;
  final int numberOfCompletedFinancialClaimsInformationRequests;
  final int numberOfFinancialClaimsInformationStorages;

  @override
  Widget build(BuildContext context) {
    return ThemedCard(
      child: Padding(
        padding: EdgeInsets.all(Spacing.large.value),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            addVerticalSpace(Spacing.large.value),
            Text(
              'Vorderingenoverzicht',
              style: Theme.of(context).textTheme.displayMedium,
            ),
            addVerticalSpace(Spacing.large.value),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  width: 30,
                  height: 30,
                  "assets/icons/selected.svg",
                ),
                addHorizontalSpace(Spacing.large.value),
                Flexible(
                  child: Text(
                    numberOfSelectedOrganizations == 1
                        ? '1 organisatie geselecteerd'
                        : '$numberOfSelectedOrganizations organisaties geselecteerd',
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                ),
              ],
            ),
            if (numberOfSelectedOrganizations > 0)
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    width: 30,
                    height: 30,
                    "assets/icons/selected.svg",
                  ),
                  addHorizontalSpace(Spacing.large.value),
                  Flexible(
                    child: Text(
                      numberOfCompletedFinancialClaimsInformationRequests ==
                              numberOfSelectedOrganizations
                          ? (numberOfSelectedOrganizations == 1
                              ? 'De organisatie heeft gereageerd'
                              : 'Alle organisaties hebben gereageerd')
                          : '$numberOfCompletedFinancialClaimsInformationRequests van $numberOfSelectedOrganizations hebben gereageerd',
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                ],
              ),
            addVerticalSpace(Spacing.large.value),
            LayoutBuilder(
              builder: (context, constraints) {
                return Align(
                  alignment: Alignment.center,
                  child: ThemedButton(
                    width: constraints.maxWidth *
                        responsiveValue(context, xs: 1, md: maxWidthPercentage),
                    buttonText: 'Bekijk overzicht',
                    style: ThemedButtonStyle.primary,
                    onPress: numberOfFinancialClaimsInformationStorages > 0
                        ? () => context
                            .goNamed(FinancialClaimsOverviewScreen.routeName)
                        : () => context.goNamed(LoadingScreen.routeName),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
