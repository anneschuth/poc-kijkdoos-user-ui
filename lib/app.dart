// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/router/router.dart';
import 'package:user_ui/theme/theme_manager.dart';
import 'package:user_ui/utils/custom_scroll_behavior.dart';

class App extends ConsumerWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    initializeDateFormatting();

    final goRouter = ref.watch(goRouterProvider);

    final bool isSmallScreenSize =
        responsiveValue(context, xs: true, sm: false);

    return MaterialApp.router(
      title: 'Financiële verplichtingen',
      debugShowCheckedModeBanner: false,
      theme: ThemeManager.light(isSmallScreenSize),
      routerConfig: goRouter,
      scrollBehavior: CustomScrollBehavior(),
    );
  }
}
