// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'financial_claims_information_configuration.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FinancialClaimsInformationConfiguration {
  int get id => throw _privateConstructorUsedError;
  String get oin => throw _privateConstructorUsedError;
  String? get document => throw _privateConstructorUsedError;
  String? get documentSignature => throw _privateConstructorUsedError;
  int? get certificateId => throw _privateConstructorUsedError;
  String? get envelope => throw _privateConstructorUsedError;
  String? get configurationRequest => throw _privateConstructorUsedError;
  String? get configuration => throw _privateConstructorUsedError;
  bool get expired => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FinancialClaimsInformationConfigurationCopyWith<
          FinancialClaimsInformationConfiguration>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FinancialClaimsInformationConfigurationCopyWith<$Res> {
  factory $FinancialClaimsInformationConfigurationCopyWith(
          FinancialClaimsInformationConfiguration value,
          $Res Function(FinancialClaimsInformationConfiguration) then) =
      _$FinancialClaimsInformationConfigurationCopyWithImpl<$Res,
          FinancialClaimsInformationConfiguration>;
  @useResult
  $Res call(
      {int id,
      String oin,
      String? document,
      String? documentSignature,
      int? certificateId,
      String? envelope,
      String? configurationRequest,
      String? configuration,
      bool expired});
}

/// @nodoc
class _$FinancialClaimsInformationConfigurationCopyWithImpl<$Res,
        $Val extends FinancialClaimsInformationConfiguration>
    implements $FinancialClaimsInformationConfigurationCopyWith<$Res> {
  _$FinancialClaimsInformationConfigurationCopyWithImpl(
      this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? document = freezed,
    Object? documentSignature = freezed,
    Object? certificateId = freezed,
    Object? envelope = freezed,
    Object? configurationRequest = freezed,
    Object? configuration = freezed,
    Object? expired = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      document: freezed == document
          ? _value.document
          : document // ignore: cast_nullable_to_non_nullable
              as String?,
      documentSignature: freezed == documentSignature
          ? _value.documentSignature
          : documentSignature // ignore: cast_nullable_to_non_nullable
              as String?,
      certificateId: freezed == certificateId
          ? _value.certificateId
          : certificateId // ignore: cast_nullable_to_non_nullable
              as int?,
      envelope: freezed == envelope
          ? _value.envelope
          : envelope // ignore: cast_nullable_to_non_nullable
              as String?,
      configurationRequest: freezed == configurationRequest
          ? _value.configurationRequest
          : configurationRequest // ignore: cast_nullable_to_non_nullable
              as String?,
      configuration: freezed == configuration
          ? _value.configuration
          : configuration // ignore: cast_nullable_to_non_nullable
              as String?,
      expired: null == expired
          ? _value.expired
          : expired // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FinancialClaimsInformationConfigurationImplCopyWith<$Res>
    implements $FinancialClaimsInformationConfigurationCopyWith<$Res> {
  factory _$$FinancialClaimsInformationConfigurationImplCopyWith(
          _$FinancialClaimsInformationConfigurationImpl value,
          $Res Function(_$FinancialClaimsInformationConfigurationImpl) then) =
      __$$FinancialClaimsInformationConfigurationImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String oin,
      String? document,
      String? documentSignature,
      int? certificateId,
      String? envelope,
      String? configurationRequest,
      String? configuration,
      bool expired});
}

/// @nodoc
class __$$FinancialClaimsInformationConfigurationImplCopyWithImpl<$Res>
    extends _$FinancialClaimsInformationConfigurationCopyWithImpl<$Res,
        _$FinancialClaimsInformationConfigurationImpl>
    implements _$$FinancialClaimsInformationConfigurationImplCopyWith<$Res> {
  __$$FinancialClaimsInformationConfigurationImplCopyWithImpl(
      _$FinancialClaimsInformationConfigurationImpl _value,
      $Res Function(_$FinancialClaimsInformationConfigurationImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? document = freezed,
    Object? documentSignature = freezed,
    Object? certificateId = freezed,
    Object? envelope = freezed,
    Object? configurationRequest = freezed,
    Object? configuration = freezed,
    Object? expired = null,
  }) {
    return _then(_$FinancialClaimsInformationConfigurationImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      document: freezed == document
          ? _value.document
          : document // ignore: cast_nullable_to_non_nullable
              as String?,
      documentSignature: freezed == documentSignature
          ? _value.documentSignature
          : documentSignature // ignore: cast_nullable_to_non_nullable
              as String?,
      certificateId: freezed == certificateId
          ? _value.certificateId
          : certificateId // ignore: cast_nullable_to_non_nullable
              as int?,
      envelope: freezed == envelope
          ? _value.envelope
          : envelope // ignore: cast_nullable_to_non_nullable
              as String?,
      configurationRequest: freezed == configurationRequest
          ? _value.configurationRequest
          : configurationRequest // ignore: cast_nullable_to_non_nullable
              as String?,
      configuration: freezed == configuration
          ? _value.configuration
          : configuration // ignore: cast_nullable_to_non_nullable
              as String?,
      expired: null == expired
          ? _value.expired
          : expired // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$FinancialClaimsInformationConfigurationImpl
    with DiagnosticableTreeMixin
    implements _FinancialClaimsInformationConfiguration {
  const _$FinancialClaimsInformationConfigurationImpl(
      {required this.id,
      required this.oin,
      required this.document,
      required this.documentSignature,
      required this.certificateId,
      required this.envelope,
      required this.configurationRequest,
      required this.configuration,
      required this.expired});

  @override
  final int id;
  @override
  final String oin;
  @override
  final String? document;
  @override
  final String? documentSignature;
  @override
  final int? certificateId;
  @override
  final String? envelope;
  @override
  final String? configurationRequest;
  @override
  final String? configuration;
  @override
  final bool expired;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FinancialClaimsInformationConfiguration(id: $id, oin: $oin, document: $document, documentSignature: $documentSignature, certificateId: $certificateId, envelope: $envelope, configurationRequest: $configurationRequest, configuration: $configuration, expired: $expired)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty(
          'type', 'FinancialClaimsInformationConfiguration'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('oin', oin))
      ..add(DiagnosticsProperty('document', document))
      ..add(DiagnosticsProperty('documentSignature', documentSignature))
      ..add(DiagnosticsProperty('certificateId', certificateId))
      ..add(DiagnosticsProperty('envelope', envelope))
      ..add(DiagnosticsProperty('configurationRequest', configurationRequest))
      ..add(DiagnosticsProperty('configuration', configuration))
      ..add(DiagnosticsProperty('expired', expired));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FinancialClaimsInformationConfigurationImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.oin, oin) || other.oin == oin) &&
            (identical(other.document, document) ||
                other.document == document) &&
            (identical(other.documentSignature, documentSignature) ||
                other.documentSignature == documentSignature) &&
            (identical(other.certificateId, certificateId) ||
                other.certificateId == certificateId) &&
            (identical(other.envelope, envelope) ||
                other.envelope == envelope) &&
            (identical(other.configurationRequest, configurationRequest) ||
                other.configurationRequest == configurationRequest) &&
            (identical(other.configuration, configuration) ||
                other.configuration == configuration) &&
            (identical(other.expired, expired) || other.expired == expired));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      oin,
      document,
      documentSignature,
      certificateId,
      envelope,
      configurationRequest,
      configuration,
      expired);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FinancialClaimsInformationConfigurationImplCopyWith<
          _$FinancialClaimsInformationConfigurationImpl>
      get copyWith =>
          __$$FinancialClaimsInformationConfigurationImplCopyWithImpl<
              _$FinancialClaimsInformationConfigurationImpl>(this, _$identity);
}

abstract class _FinancialClaimsInformationConfiguration
    implements FinancialClaimsInformationConfiguration {
  const factory _FinancialClaimsInformationConfiguration(
          {required final int id,
          required final String oin,
          required final String? document,
          required final String? documentSignature,
          required final int? certificateId,
          required final String? envelope,
          required final String? configurationRequest,
          required final String? configuration,
          required final bool expired}) =
      _$FinancialClaimsInformationConfigurationImpl;

  @override
  int get id;
  @override
  String get oin;
  @override
  String? get document;
  @override
  String? get documentSignature;
  @override
  int? get certificateId;
  @override
  String? get envelope;
  @override
  String? get configurationRequest;
  @override
  String? get configuration;
  @override
  bool get expired;
  @override
  @JsonKey(ignore: true)
  _$$FinancialClaimsInformationConfigurationImplCopyWith<
          _$FinancialClaimsInformationConfigurationImpl>
      get copyWith => throw _privateConstructorUsedError;
}
