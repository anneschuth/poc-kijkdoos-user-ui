import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'user_settings.freezed.dart';

@freezed
class UserSettings with _$UserSettings {
  const factory UserSettings({
    required bool hasSeenOnboarding,
    required bool autoSelectAllOrganizations,
    required bool hasCompletedRegistration,
    required bool hasCompletedOrganizationSelection,
    required bool delay,
  }) = _UserSettings;
}
