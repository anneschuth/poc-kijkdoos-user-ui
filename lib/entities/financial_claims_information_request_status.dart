enum FinancialClaimsInformationRequestStatus {
  queue("queue"),
  configurationCreated("configuration_created"),
  documentCreated("document_created"),
  documentSigned("document_signed"),
  certificateAdded("certificate_added"),
  certificateInvalid("certificate_invalid"),
  certificateValidated("certificate_validated"),
  envelopeCreated("envelope_created"),
  envelopeEncrypted("envelope_encrypted"),
  encryptedEnvelopeMailed("encrypted_envelope_mailed"),
  financialClaimsInformationReceived("financial_claims_information_received");

  const FinancialClaimsInformationRequestStatus(this.value);

  final String value;
}
