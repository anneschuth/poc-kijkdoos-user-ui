import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'financial_claims_information_configuration.freezed.dart';

@freezed
class FinancialClaimsInformationConfiguration
    with _$FinancialClaimsInformationConfiguration {
  const factory FinancialClaimsInformationConfiguration({
    required int id,
    required String oin,
    required String? document,
    required String? documentSignature,
    required int? certificateId,
    required String? envelope,
    required String? configurationRequest,
    required String? configuration,
    required bool expired,
  }) = _FinancialClaimsInformationConfiguration;
}
