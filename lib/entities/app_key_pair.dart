import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'app_key_pair.freezed.dart';

@freezed
class AppKeyPair with _$AppKeyPair {
  const factory AppKeyPair({
    required int id,
    required String publicKey,
    required String privateKey,
  }) = _AppKeyPair;
}
