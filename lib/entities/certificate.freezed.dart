// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'certificate.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$Certificate {
  int get id => throw _privateConstructorUsedError;
  CertificateType get type => throw _privateConstructorUsedError;
  @Uint8ListJsonConverter()
  Uint8List get value => throw _privateConstructorUsedError;
  String get givenName => throw _privateConstructorUsedError;
  DateTime get expiresAt => throw _privateConstructorUsedError;
  bool get deemedExpiredBySourceOrganization =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CertificateCopyWith<Certificate> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CertificateCopyWith<$Res> {
  factory $CertificateCopyWith(
          Certificate value, $Res Function(Certificate) then) =
      _$CertificateCopyWithImpl<$Res, Certificate>;
  @useResult
  $Res call(
      {int id,
      CertificateType type,
      @Uint8ListJsonConverter() Uint8List value,
      String givenName,
      DateTime expiresAt,
      bool deemedExpiredBySourceOrganization});
}

/// @nodoc
class _$CertificateCopyWithImpl<$Res, $Val extends Certificate>
    implements $CertificateCopyWith<$Res> {
  _$CertificateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? type = null,
    Object? value = null,
    Object? givenName = null,
    Object? expiresAt = null,
    Object? deemedExpiredBySourceOrganization = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as CertificateType,
      value: null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Uint8List,
      givenName: null == givenName
          ? _value.givenName
          : givenName // ignore: cast_nullable_to_non_nullable
              as String,
      expiresAt: null == expiresAt
          ? _value.expiresAt
          : expiresAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      deemedExpiredBySourceOrganization: null ==
              deemedExpiredBySourceOrganization
          ? _value.deemedExpiredBySourceOrganization
          : deemedExpiredBySourceOrganization // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CertificateImplCopyWith<$Res>
    implements $CertificateCopyWith<$Res> {
  factory _$$CertificateImplCopyWith(
          _$CertificateImpl value, $Res Function(_$CertificateImpl) then) =
      __$$CertificateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      CertificateType type,
      @Uint8ListJsonConverter() Uint8List value,
      String givenName,
      DateTime expiresAt,
      bool deemedExpiredBySourceOrganization});
}

/// @nodoc
class __$$CertificateImplCopyWithImpl<$Res>
    extends _$CertificateCopyWithImpl<$Res, _$CertificateImpl>
    implements _$$CertificateImplCopyWith<$Res> {
  __$$CertificateImplCopyWithImpl(
      _$CertificateImpl _value, $Res Function(_$CertificateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? type = null,
    Object? value = null,
    Object? givenName = null,
    Object? expiresAt = null,
    Object? deemedExpiredBySourceOrganization = null,
  }) {
    return _then(_$CertificateImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as CertificateType,
      value: null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Uint8List,
      givenName: null == givenName
          ? _value.givenName
          : givenName // ignore: cast_nullable_to_non_nullable
              as String,
      expiresAt: null == expiresAt
          ? _value.expiresAt
          : expiresAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      deemedExpiredBySourceOrganization: null ==
              deemedExpiredBySourceOrganization
          ? _value.deemedExpiredBySourceOrganization
          : deemedExpiredBySourceOrganization // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$CertificateImpl implements _Certificate {
  const _$CertificateImpl(
      {required this.id,
      required this.type,
      @Uint8ListJsonConverter() required this.value,
      required this.givenName,
      required this.expiresAt,
      required this.deemedExpiredBySourceOrganization});

  @override
  final int id;
  @override
  final CertificateType type;
  @override
  @Uint8ListJsonConverter()
  final Uint8List value;
  @override
  final String givenName;
  @override
  final DateTime expiresAt;
  @override
  final bool deemedExpiredBySourceOrganization;

  @override
  String toString() {
    return 'Certificate(id: $id, type: $type, value: $value, givenName: $givenName, expiresAt: $expiresAt, deemedExpiredBySourceOrganization: $deemedExpiredBySourceOrganization)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CertificateImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.type, type) || other.type == type) &&
            const DeepCollectionEquality().equals(other.value, value) &&
            (identical(other.givenName, givenName) ||
                other.givenName == givenName) &&
            (identical(other.expiresAt, expiresAt) ||
                other.expiresAt == expiresAt) &&
            (identical(other.deemedExpiredBySourceOrganization,
                    deemedExpiredBySourceOrganization) ||
                other.deemedExpiredBySourceOrganization ==
                    deemedExpiredBySourceOrganization));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      type,
      const DeepCollectionEquality().hash(value),
      givenName,
      expiresAt,
      deemedExpiredBySourceOrganization);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CertificateImplCopyWith<_$CertificateImpl> get copyWith =>
      __$$CertificateImplCopyWithImpl<_$CertificateImpl>(this, _$identity);
}

abstract class _Certificate implements Certificate {
  const factory _Certificate(
          {required final int id,
          required final CertificateType type,
          @Uint8ListJsonConverter() required final Uint8List value,
          required final String givenName,
          required final DateTime expiresAt,
          required final bool deemedExpiredBySourceOrganization}) =
      _$CertificateImpl;

  @override
  int get id;
  @override
  CertificateType get type;
  @override
  @Uint8ListJsonConverter()
  Uint8List get value;
  @override
  String get givenName;
  @override
  DateTime get expiresAt;
  @override
  bool get deemedExpiredBySourceOrganization;
  @override
  @JsonKey(ignore: true)
  _$$CertificateImplCopyWith<_$CertificateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
