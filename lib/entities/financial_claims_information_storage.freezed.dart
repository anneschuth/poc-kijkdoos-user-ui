// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'financial_claims_information_storage.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FinancialClaimsInformationStorage {
  int get id => throw _privateConstructorUsedError;
  String get oin => throw _privateConstructorUsedError;
  DateTime get dateTimeRequested => throw _privateConstructorUsedError;
  DateTime get dateTimeReceived => throw _privateConstructorUsedError;
  String get financialClaimsInformationDocument =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FinancialClaimsInformationStorageCopyWith<FinancialClaimsInformationStorage>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FinancialClaimsInformationStorageCopyWith<$Res> {
  factory $FinancialClaimsInformationStorageCopyWith(
          FinancialClaimsInformationStorage value,
          $Res Function(FinancialClaimsInformationStorage) then) =
      _$FinancialClaimsInformationStorageCopyWithImpl<$Res,
          FinancialClaimsInformationStorage>;
  @useResult
  $Res call(
      {int id,
      String oin,
      DateTime dateTimeRequested,
      DateTime dateTimeReceived,
      String financialClaimsInformationDocument});
}

/// @nodoc
class _$FinancialClaimsInformationStorageCopyWithImpl<$Res,
        $Val extends FinancialClaimsInformationStorage>
    implements $FinancialClaimsInformationStorageCopyWith<$Res> {
  _$FinancialClaimsInformationStorageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? dateTimeRequested = null,
    Object? dateTimeReceived = null,
    Object? financialClaimsInformationDocument = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeRequested: null == dateTimeRequested
          ? _value.dateTimeRequested
          : dateTimeRequested // ignore: cast_nullable_to_non_nullable
              as DateTime,
      dateTimeReceived: null == dateTimeReceived
          ? _value.dateTimeReceived
          : dateTimeReceived // ignore: cast_nullable_to_non_nullable
              as DateTime,
      financialClaimsInformationDocument: null ==
              financialClaimsInformationDocument
          ? _value.financialClaimsInformationDocument
          : financialClaimsInformationDocument // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FinancialClaimsInformationStorageImplCopyWith<$Res>
    implements $FinancialClaimsInformationStorageCopyWith<$Res> {
  factory _$$FinancialClaimsInformationStorageImplCopyWith(
          _$FinancialClaimsInformationStorageImpl value,
          $Res Function(_$FinancialClaimsInformationStorageImpl) then) =
      __$$FinancialClaimsInformationStorageImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String oin,
      DateTime dateTimeRequested,
      DateTime dateTimeReceived,
      String financialClaimsInformationDocument});
}

/// @nodoc
class __$$FinancialClaimsInformationStorageImplCopyWithImpl<$Res>
    extends _$FinancialClaimsInformationStorageCopyWithImpl<$Res,
        _$FinancialClaimsInformationStorageImpl>
    implements _$$FinancialClaimsInformationStorageImplCopyWith<$Res> {
  __$$FinancialClaimsInformationStorageImplCopyWithImpl(
      _$FinancialClaimsInformationStorageImpl _value,
      $Res Function(_$FinancialClaimsInformationStorageImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? dateTimeRequested = null,
    Object? dateTimeReceived = null,
    Object? financialClaimsInformationDocument = null,
  }) {
    return _then(_$FinancialClaimsInformationStorageImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeRequested: null == dateTimeRequested
          ? _value.dateTimeRequested
          : dateTimeRequested // ignore: cast_nullable_to_non_nullable
              as DateTime,
      dateTimeReceived: null == dateTimeReceived
          ? _value.dateTimeReceived
          : dateTimeReceived // ignore: cast_nullable_to_non_nullable
              as DateTime,
      financialClaimsInformationDocument: null ==
              financialClaimsInformationDocument
          ? _value.financialClaimsInformationDocument
          : financialClaimsInformationDocument // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FinancialClaimsInformationStorageImpl
    with DiagnosticableTreeMixin
    implements _FinancialClaimsInformationStorage {
  const _$FinancialClaimsInformationStorageImpl(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.dateTimeReceived,
      required this.financialClaimsInformationDocument});

  @override
  final int id;
  @override
  final String oin;
  @override
  final DateTime dateTimeRequested;
  @override
  final DateTime dateTimeReceived;
  @override
  final String financialClaimsInformationDocument;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FinancialClaimsInformationStorage(id: $id, oin: $oin, dateTimeRequested: $dateTimeRequested, dateTimeReceived: $dateTimeReceived, financialClaimsInformationDocument: $financialClaimsInformationDocument)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FinancialClaimsInformationStorage'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('oin', oin))
      ..add(DiagnosticsProperty('dateTimeRequested', dateTimeRequested))
      ..add(DiagnosticsProperty('dateTimeReceived', dateTimeReceived))
      ..add(DiagnosticsProperty('financialClaimsInformationDocument',
          financialClaimsInformationDocument));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FinancialClaimsInformationStorageImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.oin, oin) || other.oin == oin) &&
            (identical(other.dateTimeRequested, dateTimeRequested) ||
                other.dateTimeRequested == dateTimeRequested) &&
            (identical(other.dateTimeReceived, dateTimeReceived) ||
                other.dateTimeReceived == dateTimeReceived) &&
            (identical(other.financialClaimsInformationDocument,
                    financialClaimsInformationDocument) ||
                other.financialClaimsInformationDocument ==
                    financialClaimsInformationDocument));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, oin, dateTimeRequested,
      dateTimeReceived, financialClaimsInformationDocument);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FinancialClaimsInformationStorageImplCopyWith<
          _$FinancialClaimsInformationStorageImpl>
      get copyWith => __$$FinancialClaimsInformationStorageImplCopyWithImpl<
          _$FinancialClaimsInformationStorageImpl>(this, _$identity);
}

abstract class _FinancialClaimsInformationStorage
    implements FinancialClaimsInformationStorage {
  const factory _FinancialClaimsInformationStorage(
          {required final int id,
          required final String oin,
          required final DateTime dateTimeRequested,
          required final DateTime dateTimeReceived,
          required final String financialClaimsInformationDocument}) =
      _$FinancialClaimsInformationStorageImpl;

  @override
  int get id;
  @override
  String get oin;
  @override
  DateTime get dateTimeRequested;
  @override
  DateTime get dateTimeReceived;
  @override
  String get financialClaimsInformationDocument;
  @override
  @JsonKey(ignore: true)
  _$$FinancialClaimsInformationStorageImplCopyWith<
          _$FinancialClaimsInformationStorageImpl>
      get copyWith => throw _privateConstructorUsedError;
}
