// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'log_record.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LogRecord {
  DateTime get timestamp => throw _privateConstructorUsedError;
  String get host => throw _privateConstructorUsedError;
  int get cef => throw _privateConstructorUsedError;
  String get deviceVendor => throw _privateConstructorUsedError;
  String get deviceProduct => throw _privateConstructorUsedError;
  String get deviceVersion => throw _privateConstructorUsedError;
  String get deviceEventClassId => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  int get severity => throw _privateConstructorUsedError;
  String get flexString1Label => throw _privateConstructorUsedError;
  String get flexString1 => throw _privateConstructorUsedError;
  String get flexString2Label => throw _privateConstructorUsedError;
  String get flexString2 => throw _privateConstructorUsedError;
  String get act => throw _privateConstructorUsedError;
  String get app => throw _privateConstructorUsedError;
  String get request => throw _privateConstructorUsedError;
  String get requestMethod => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LogRecordCopyWith<LogRecord> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LogRecordCopyWith<$Res> {
  factory $LogRecordCopyWith(LogRecord value, $Res Function(LogRecord) then) =
      _$LogRecordCopyWithImpl<$Res, LogRecord>;
  @useResult
  $Res call(
      {DateTime timestamp,
      String host,
      int cef,
      String deviceVendor,
      String deviceProduct,
      String deviceVersion,
      String deviceEventClassId,
      String name,
      int severity,
      String flexString1Label,
      String flexString1,
      String flexString2Label,
      String flexString2,
      String act,
      String app,
      String request,
      String requestMethod});
}

/// @nodoc
class _$LogRecordCopyWithImpl<$Res, $Val extends LogRecord>
    implements $LogRecordCopyWith<$Res> {
  _$LogRecordCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? timestamp = null,
    Object? host = null,
    Object? cef = null,
    Object? deviceVendor = null,
    Object? deviceProduct = null,
    Object? deviceVersion = null,
    Object? deviceEventClassId = null,
    Object? name = null,
    Object? severity = null,
    Object? flexString1Label = null,
    Object? flexString1 = null,
    Object? flexString2Label = null,
    Object? flexString2 = null,
    Object? act = null,
    Object? app = null,
    Object? request = null,
    Object? requestMethod = null,
  }) {
    return _then(_value.copyWith(
      timestamp: null == timestamp
          ? _value.timestamp
          : timestamp // ignore: cast_nullable_to_non_nullable
              as DateTime,
      host: null == host
          ? _value.host
          : host // ignore: cast_nullable_to_non_nullable
              as String,
      cef: null == cef
          ? _value.cef
          : cef // ignore: cast_nullable_to_non_nullable
              as int,
      deviceVendor: null == deviceVendor
          ? _value.deviceVendor
          : deviceVendor // ignore: cast_nullable_to_non_nullable
              as String,
      deviceProduct: null == deviceProduct
          ? _value.deviceProduct
          : deviceProduct // ignore: cast_nullable_to_non_nullable
              as String,
      deviceVersion: null == deviceVersion
          ? _value.deviceVersion
          : deviceVersion // ignore: cast_nullable_to_non_nullable
              as String,
      deviceEventClassId: null == deviceEventClassId
          ? _value.deviceEventClassId
          : deviceEventClassId // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      severity: null == severity
          ? _value.severity
          : severity // ignore: cast_nullable_to_non_nullable
              as int,
      flexString1Label: null == flexString1Label
          ? _value.flexString1Label
          : flexString1Label // ignore: cast_nullable_to_non_nullable
              as String,
      flexString1: null == flexString1
          ? _value.flexString1
          : flexString1 // ignore: cast_nullable_to_non_nullable
              as String,
      flexString2Label: null == flexString2Label
          ? _value.flexString2Label
          : flexString2Label // ignore: cast_nullable_to_non_nullable
              as String,
      flexString2: null == flexString2
          ? _value.flexString2
          : flexString2 // ignore: cast_nullable_to_non_nullable
              as String,
      act: null == act
          ? _value.act
          : act // ignore: cast_nullable_to_non_nullable
              as String,
      app: null == app
          ? _value.app
          : app // ignore: cast_nullable_to_non_nullable
              as String,
      request: null == request
          ? _value.request
          : request // ignore: cast_nullable_to_non_nullable
              as String,
      requestMethod: null == requestMethod
          ? _value.requestMethod
          : requestMethod // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$LogRecordImplCopyWith<$Res>
    implements $LogRecordCopyWith<$Res> {
  factory _$$LogRecordImplCopyWith(
          _$LogRecordImpl value, $Res Function(_$LogRecordImpl) then) =
      __$$LogRecordImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {DateTime timestamp,
      String host,
      int cef,
      String deviceVendor,
      String deviceProduct,
      String deviceVersion,
      String deviceEventClassId,
      String name,
      int severity,
      String flexString1Label,
      String flexString1,
      String flexString2Label,
      String flexString2,
      String act,
      String app,
      String request,
      String requestMethod});
}

/// @nodoc
class __$$LogRecordImplCopyWithImpl<$Res>
    extends _$LogRecordCopyWithImpl<$Res, _$LogRecordImpl>
    implements _$$LogRecordImplCopyWith<$Res> {
  __$$LogRecordImplCopyWithImpl(
      _$LogRecordImpl _value, $Res Function(_$LogRecordImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? timestamp = null,
    Object? host = null,
    Object? cef = null,
    Object? deviceVendor = null,
    Object? deviceProduct = null,
    Object? deviceVersion = null,
    Object? deviceEventClassId = null,
    Object? name = null,
    Object? severity = null,
    Object? flexString1Label = null,
    Object? flexString1 = null,
    Object? flexString2Label = null,
    Object? flexString2 = null,
    Object? act = null,
    Object? app = null,
    Object? request = null,
    Object? requestMethod = null,
  }) {
    return _then(_$LogRecordImpl(
      timestamp: null == timestamp
          ? _value.timestamp
          : timestamp // ignore: cast_nullable_to_non_nullable
              as DateTime,
      host: null == host
          ? _value.host
          : host // ignore: cast_nullable_to_non_nullable
              as String,
      cef: null == cef
          ? _value.cef
          : cef // ignore: cast_nullable_to_non_nullable
              as int,
      deviceVendor: null == deviceVendor
          ? _value.deviceVendor
          : deviceVendor // ignore: cast_nullable_to_non_nullable
              as String,
      deviceProduct: null == deviceProduct
          ? _value.deviceProduct
          : deviceProduct // ignore: cast_nullable_to_non_nullable
              as String,
      deviceVersion: null == deviceVersion
          ? _value.deviceVersion
          : deviceVersion // ignore: cast_nullable_to_non_nullable
              as String,
      deviceEventClassId: null == deviceEventClassId
          ? _value.deviceEventClassId
          : deviceEventClassId // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      severity: null == severity
          ? _value.severity
          : severity // ignore: cast_nullable_to_non_nullable
              as int,
      flexString1Label: null == flexString1Label
          ? _value.flexString1Label
          : flexString1Label // ignore: cast_nullable_to_non_nullable
              as String,
      flexString1: null == flexString1
          ? _value.flexString1
          : flexString1 // ignore: cast_nullable_to_non_nullable
              as String,
      flexString2Label: null == flexString2Label
          ? _value.flexString2Label
          : flexString2Label // ignore: cast_nullable_to_non_nullable
              as String,
      flexString2: null == flexString2
          ? _value.flexString2
          : flexString2 // ignore: cast_nullable_to_non_nullable
              as String,
      act: null == act
          ? _value.act
          : act // ignore: cast_nullable_to_non_nullable
              as String,
      app: null == app
          ? _value.app
          : app // ignore: cast_nullable_to_non_nullable
              as String,
      request: null == request
          ? _value.request
          : request // ignore: cast_nullable_to_non_nullable
              as String,
      requestMethod: null == requestMethod
          ? _value.requestMethod
          : requestMethod // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$LogRecordImpl with DiagnosticableTreeMixin implements _LogRecord {
  const _$LogRecordImpl(
      {required this.timestamp,
      required this.host,
      required this.cef,
      required this.deviceVendor,
      required this.deviceProduct,
      required this.deviceVersion,
      required this.deviceEventClassId,
      required this.name,
      required this.severity,
      required this.flexString1Label,
      required this.flexString1,
      required this.flexString2Label,
      required this.flexString2,
      required this.act,
      required this.app,
      required this.request,
      required this.requestMethod});

  @override
  final DateTime timestamp;
  @override
  final String host;
  @override
  final int cef;
  @override
  final String deviceVendor;
  @override
  final String deviceProduct;
  @override
  final String deviceVersion;
  @override
  final String deviceEventClassId;
  @override
  final String name;
  @override
  final int severity;
  @override
  final String flexString1Label;
  @override
  final String flexString1;
  @override
  final String flexString2Label;
  @override
  final String flexString2;
  @override
  final String act;
  @override
  final String app;
  @override
  final String request;
  @override
  final String requestMethod;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'LogRecord(timestamp: $timestamp, host: $host, cef: $cef, deviceVendor: $deviceVendor, deviceProduct: $deviceProduct, deviceVersion: $deviceVersion, deviceEventClassId: $deviceEventClassId, name: $name, severity: $severity, flexString1Label: $flexString1Label, flexString1: $flexString1, flexString2Label: $flexString2Label, flexString2: $flexString2, act: $act, app: $app, request: $request, requestMethod: $requestMethod)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'LogRecord'))
      ..add(DiagnosticsProperty('timestamp', timestamp))
      ..add(DiagnosticsProperty('host', host))
      ..add(DiagnosticsProperty('cef', cef))
      ..add(DiagnosticsProperty('deviceVendor', deviceVendor))
      ..add(DiagnosticsProperty('deviceProduct', deviceProduct))
      ..add(DiagnosticsProperty('deviceVersion', deviceVersion))
      ..add(DiagnosticsProperty('deviceEventClassId', deviceEventClassId))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('severity', severity))
      ..add(DiagnosticsProperty('flexString1Label', flexString1Label))
      ..add(DiagnosticsProperty('flexString1', flexString1))
      ..add(DiagnosticsProperty('flexString2Label', flexString2Label))
      ..add(DiagnosticsProperty('flexString2', flexString2))
      ..add(DiagnosticsProperty('act', act))
      ..add(DiagnosticsProperty('app', app))
      ..add(DiagnosticsProperty('request', request))
      ..add(DiagnosticsProperty('requestMethod', requestMethod));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LogRecordImpl &&
            (identical(other.timestamp, timestamp) ||
                other.timestamp == timestamp) &&
            (identical(other.host, host) || other.host == host) &&
            (identical(other.cef, cef) || other.cef == cef) &&
            (identical(other.deviceVendor, deviceVendor) ||
                other.deviceVendor == deviceVendor) &&
            (identical(other.deviceProduct, deviceProduct) ||
                other.deviceProduct == deviceProduct) &&
            (identical(other.deviceVersion, deviceVersion) ||
                other.deviceVersion == deviceVersion) &&
            (identical(other.deviceEventClassId, deviceEventClassId) ||
                other.deviceEventClassId == deviceEventClassId) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.severity, severity) ||
                other.severity == severity) &&
            (identical(other.flexString1Label, flexString1Label) ||
                other.flexString1Label == flexString1Label) &&
            (identical(other.flexString1, flexString1) ||
                other.flexString1 == flexString1) &&
            (identical(other.flexString2Label, flexString2Label) ||
                other.flexString2Label == flexString2Label) &&
            (identical(other.flexString2, flexString2) ||
                other.flexString2 == flexString2) &&
            (identical(other.act, act) || other.act == act) &&
            (identical(other.app, app) || other.app == app) &&
            (identical(other.request, request) || other.request == request) &&
            (identical(other.requestMethod, requestMethod) ||
                other.requestMethod == requestMethod));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      timestamp,
      host,
      cef,
      deviceVendor,
      deviceProduct,
      deviceVersion,
      deviceEventClassId,
      name,
      severity,
      flexString1Label,
      flexString1,
      flexString2Label,
      flexString2,
      act,
      app,
      request,
      requestMethod);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LogRecordImplCopyWith<_$LogRecordImpl> get copyWith =>
      __$$LogRecordImplCopyWithImpl<_$LogRecordImpl>(this, _$identity);
}

abstract class _LogRecord implements LogRecord {
  const factory _LogRecord(
      {required final DateTime timestamp,
      required final String host,
      required final int cef,
      required final String deviceVendor,
      required final String deviceProduct,
      required final String deviceVersion,
      required final String deviceEventClassId,
      required final String name,
      required final int severity,
      required final String flexString1Label,
      required final String flexString1,
      required final String flexString2Label,
      required final String flexString2,
      required final String act,
      required final String app,
      required final String request,
      required final String requestMethod}) = _$LogRecordImpl;

  @override
  DateTime get timestamp;
  @override
  String get host;
  @override
  int get cef;
  @override
  String get deviceVendor;
  @override
  String get deviceProduct;
  @override
  String get deviceVersion;
  @override
  String get deviceEventClassId;
  @override
  String get name;
  @override
  int get severity;
  @override
  String get flexString1Label;
  @override
  String get flexString1;
  @override
  String get flexString2Label;
  @override
  String get flexString2;
  @override
  String get act;
  @override
  String get app;
  @override
  String get request;
  @override
  String get requestMethod;
  @override
  @JsonKey(ignore: true)
  _$$LogRecordImplCopyWith<_$LogRecordImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
