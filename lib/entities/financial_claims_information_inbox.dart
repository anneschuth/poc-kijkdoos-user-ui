import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'financial_claims_information_inbox.freezed.dart';

@freezed
class FinancialClaimsInformationInbox with _$FinancialClaimsInformationInbox {
  const factory FinancialClaimsInformationInbox({
    required int id,
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String encryptedFinancialClaimsInformationDocument,
    required bool copiedToStorage,
  }) = _FinancialClaimsInformationInbox;
}
