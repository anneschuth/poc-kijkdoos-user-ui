// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'financial_claims_information_request.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FinancialClaimsInformationRequest {
  int get id => throw _privateConstructorUsedError;
  String get oin => throw _privateConstructorUsedError;
  DateTime get dateTimeRequested => throw _privateConstructorUsedError;
  FinancialClaimsInformationRequestStatus get status =>
      throw _privateConstructorUsedError;
  bool get lock => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FinancialClaimsInformationRequestCopyWith<FinancialClaimsInformationRequest>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FinancialClaimsInformationRequestCopyWith<$Res> {
  factory $FinancialClaimsInformationRequestCopyWith(
          FinancialClaimsInformationRequest value,
          $Res Function(FinancialClaimsInformationRequest) then) =
      _$FinancialClaimsInformationRequestCopyWithImpl<$Res,
          FinancialClaimsInformationRequest>;
  @useResult
  $Res call(
      {int id,
      String oin,
      DateTime dateTimeRequested,
      FinancialClaimsInformationRequestStatus status,
      bool lock});
}

/// @nodoc
class _$FinancialClaimsInformationRequestCopyWithImpl<$Res,
        $Val extends FinancialClaimsInformationRequest>
    implements $FinancialClaimsInformationRequestCopyWith<$Res> {
  _$FinancialClaimsInformationRequestCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? dateTimeRequested = null,
    Object? status = null,
    Object? lock = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeRequested: null == dateTimeRequested
          ? _value.dateTimeRequested
          : dateTimeRequested // ignore: cast_nullable_to_non_nullable
              as DateTime,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FinancialClaimsInformationRequestStatus,
      lock: null == lock
          ? _value.lock
          : lock // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FinancialClaimsInformationRequestImplCopyWith<$Res>
    implements $FinancialClaimsInformationRequestCopyWith<$Res> {
  factory _$$FinancialClaimsInformationRequestImplCopyWith(
          _$FinancialClaimsInformationRequestImpl value,
          $Res Function(_$FinancialClaimsInformationRequestImpl) then) =
      __$$FinancialClaimsInformationRequestImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String oin,
      DateTime dateTimeRequested,
      FinancialClaimsInformationRequestStatus status,
      bool lock});
}

/// @nodoc
class __$$FinancialClaimsInformationRequestImplCopyWithImpl<$Res>
    extends _$FinancialClaimsInformationRequestCopyWithImpl<$Res,
        _$FinancialClaimsInformationRequestImpl>
    implements _$$FinancialClaimsInformationRequestImplCopyWith<$Res> {
  __$$FinancialClaimsInformationRequestImplCopyWithImpl(
      _$FinancialClaimsInformationRequestImpl _value,
      $Res Function(_$FinancialClaimsInformationRequestImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? dateTimeRequested = null,
    Object? status = null,
    Object? lock = null,
  }) {
    return _then(_$FinancialClaimsInformationRequestImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeRequested: null == dateTimeRequested
          ? _value.dateTimeRequested
          : dateTimeRequested // ignore: cast_nullable_to_non_nullable
              as DateTime,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FinancialClaimsInformationRequestStatus,
      lock: null == lock
          ? _value.lock
          : lock // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$FinancialClaimsInformationRequestImpl
    with DiagnosticableTreeMixin
    implements _FinancialClaimsInformationRequest {
  const _$FinancialClaimsInformationRequestImpl(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.status,
      required this.lock});

  @override
  final int id;
  @override
  final String oin;
  @override
  final DateTime dateTimeRequested;
  @override
  final FinancialClaimsInformationRequestStatus status;
  @override
  final bool lock;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FinancialClaimsInformationRequest(id: $id, oin: $oin, dateTimeRequested: $dateTimeRequested, status: $status, lock: $lock)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FinancialClaimsInformationRequest'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('oin', oin))
      ..add(DiagnosticsProperty('dateTimeRequested', dateTimeRequested))
      ..add(DiagnosticsProperty('status', status))
      ..add(DiagnosticsProperty('lock', lock));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FinancialClaimsInformationRequestImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.oin, oin) || other.oin == oin) &&
            (identical(other.dateTimeRequested, dateTimeRequested) ||
                other.dateTimeRequested == dateTimeRequested) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.lock, lock) || other.lock == lock));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, id, oin, dateTimeRequested, status, lock);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FinancialClaimsInformationRequestImplCopyWith<
          _$FinancialClaimsInformationRequestImpl>
      get copyWith => __$$FinancialClaimsInformationRequestImplCopyWithImpl<
          _$FinancialClaimsInformationRequestImpl>(this, _$identity);
}

abstract class _FinancialClaimsInformationRequest
    implements FinancialClaimsInformationRequest {
  const factory _FinancialClaimsInformationRequest(
      {required final int id,
      required final String oin,
      required final DateTime dateTimeRequested,
      required final FinancialClaimsInformationRequestStatus status,
      required final bool lock}) = _$FinancialClaimsInformationRequestImpl;

  @override
  int get id;
  @override
  String get oin;
  @override
  DateTime get dateTimeRequested;
  @override
  FinancialClaimsInformationRequestStatus get status;
  @override
  bool get lock;
  @override
  @JsonKey(ignore: true)
  _$$FinancialClaimsInformationRequestImplCopyWith<
          _$FinancialClaimsInformationRequestImpl>
      get copyWith => throw _privateConstructorUsedError;
}
