// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'scheme_organization.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SchemeOrganization {
  int get id => throw _privateConstructorUsedError;
  String get oin => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get publicKey => throw _privateConstructorUsedError;
  String get discoveryUrl => throw _privateConstructorUsedError;
  bool get available => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SchemeOrganizationCopyWith<SchemeOrganization> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SchemeOrganizationCopyWith<$Res> {
  factory $SchemeOrganizationCopyWith(
          SchemeOrganization value, $Res Function(SchemeOrganization) then) =
      _$SchemeOrganizationCopyWithImpl<$Res, SchemeOrganization>;
  @useResult
  $Res call(
      {int id,
      String oin,
      String name,
      String publicKey,
      String discoveryUrl,
      bool available});
}

/// @nodoc
class _$SchemeOrganizationCopyWithImpl<$Res, $Val extends SchemeOrganization>
    implements $SchemeOrganizationCopyWith<$Res> {
  _$SchemeOrganizationCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? name = null,
    Object? publicKey = null,
    Object? discoveryUrl = null,
    Object? available = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      publicKey: null == publicKey
          ? _value.publicKey
          : publicKey // ignore: cast_nullable_to_non_nullable
              as String,
      discoveryUrl: null == discoveryUrl
          ? _value.discoveryUrl
          : discoveryUrl // ignore: cast_nullable_to_non_nullable
              as String,
      available: null == available
          ? _value.available
          : available // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SchemeOrganizationImplCopyWith<$Res>
    implements $SchemeOrganizationCopyWith<$Res> {
  factory _$$SchemeOrganizationImplCopyWith(_$SchemeOrganizationImpl value,
          $Res Function(_$SchemeOrganizationImpl) then) =
      __$$SchemeOrganizationImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String oin,
      String name,
      String publicKey,
      String discoveryUrl,
      bool available});
}

/// @nodoc
class __$$SchemeOrganizationImplCopyWithImpl<$Res>
    extends _$SchemeOrganizationCopyWithImpl<$Res, _$SchemeOrganizationImpl>
    implements _$$SchemeOrganizationImplCopyWith<$Res> {
  __$$SchemeOrganizationImplCopyWithImpl(_$SchemeOrganizationImpl _value,
      $Res Function(_$SchemeOrganizationImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? name = null,
    Object? publicKey = null,
    Object? discoveryUrl = null,
    Object? available = null,
  }) {
    return _then(_$SchemeOrganizationImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      publicKey: null == publicKey
          ? _value.publicKey
          : publicKey // ignore: cast_nullable_to_non_nullable
              as String,
      discoveryUrl: null == discoveryUrl
          ? _value.discoveryUrl
          : discoveryUrl // ignore: cast_nullable_to_non_nullable
              as String,
      available: null == available
          ? _value.available
          : available // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$SchemeOrganizationImpl
    with DiagnosticableTreeMixin
    implements _SchemeOrganization {
  const _$SchemeOrganizationImpl(
      {required this.id,
      required this.oin,
      required this.name,
      required this.publicKey,
      required this.discoveryUrl,
      required this.available});

  @override
  final int id;
  @override
  final String oin;
  @override
  final String name;
  @override
  final String publicKey;
  @override
  final String discoveryUrl;
  @override
  final bool available;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SchemeOrganization(id: $id, oin: $oin, name: $name, publicKey: $publicKey, discoveryUrl: $discoveryUrl, available: $available)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SchemeOrganization'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('oin', oin))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('publicKey', publicKey))
      ..add(DiagnosticsProperty('discoveryUrl', discoveryUrl))
      ..add(DiagnosticsProperty('available', available));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SchemeOrganizationImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.oin, oin) || other.oin == oin) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.publicKey, publicKey) ||
                other.publicKey == publicKey) &&
            (identical(other.discoveryUrl, discoveryUrl) ||
                other.discoveryUrl == discoveryUrl) &&
            (identical(other.available, available) ||
                other.available == available));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, id, oin, name, publicKey, discoveryUrl, available);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SchemeOrganizationImplCopyWith<_$SchemeOrganizationImpl> get copyWith =>
      __$$SchemeOrganizationImplCopyWithImpl<_$SchemeOrganizationImpl>(
          this, _$identity);
}

abstract class _SchemeOrganization implements SchemeOrganization {
  const factory _SchemeOrganization(
      {required final int id,
      required final String oin,
      required final String name,
      required final String publicKey,
      required final String discoveryUrl,
      required final bool available}) = _$SchemeOrganizationImpl;

  @override
  int get id;
  @override
  String get oin;
  @override
  String get name;
  @override
  String get publicKey;
  @override
  String get discoveryUrl;
  @override
  bool get available;
  @override
  @JsonKey(ignore: true)
  _$$SchemeOrganizationImplCopyWith<_$SchemeOrganizationImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
