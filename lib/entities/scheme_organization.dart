import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'scheme_organization.freezed.dart';

@freezed
class SchemeOrganization with _$SchemeOrganization {
  const factory SchemeOrganization({
    required int id,
    required String oin,
    required String name,
    required String publicKey,
    required String discoveryUrl,
    required bool available,
  }) = _SchemeOrganization;
}
