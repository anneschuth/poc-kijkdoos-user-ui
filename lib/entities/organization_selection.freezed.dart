// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'organization_selection.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$OrganizationSelection {
  int get id => throw _privateConstructorUsedError;
  String get oin => throw _privateConstructorUsedError;
  bool get selected => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $OrganizationSelectionCopyWith<OrganizationSelection> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrganizationSelectionCopyWith<$Res> {
  factory $OrganizationSelectionCopyWith(OrganizationSelection value,
          $Res Function(OrganizationSelection) then) =
      _$OrganizationSelectionCopyWithImpl<$Res, OrganizationSelection>;
  @useResult
  $Res call({int id, String oin, bool selected});
}

/// @nodoc
class _$OrganizationSelectionCopyWithImpl<$Res,
        $Val extends OrganizationSelection>
    implements $OrganizationSelectionCopyWith<$Res> {
  _$OrganizationSelectionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? selected = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      selected: null == selected
          ? _value.selected
          : selected // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$OrganizationSelectionImplCopyWith<$Res>
    implements $OrganizationSelectionCopyWith<$Res> {
  factory _$$OrganizationSelectionImplCopyWith(
          _$OrganizationSelectionImpl value,
          $Res Function(_$OrganizationSelectionImpl) then) =
      __$$OrganizationSelectionImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String oin, bool selected});
}

/// @nodoc
class __$$OrganizationSelectionImplCopyWithImpl<$Res>
    extends _$OrganizationSelectionCopyWithImpl<$Res,
        _$OrganizationSelectionImpl>
    implements _$$OrganizationSelectionImplCopyWith<$Res> {
  __$$OrganizationSelectionImplCopyWithImpl(_$OrganizationSelectionImpl _value,
      $Res Function(_$OrganizationSelectionImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? selected = null,
  }) {
    return _then(_$OrganizationSelectionImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      selected: null == selected
          ? _value.selected
          : selected // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$OrganizationSelectionImpl
    with DiagnosticableTreeMixin
    implements _OrganizationSelection {
  const _$OrganizationSelectionImpl(
      {required this.id, required this.oin, required this.selected});

  @override
  final int id;
  @override
  final String oin;
  @override
  final bool selected;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'OrganizationSelection(id: $id, oin: $oin, selected: $selected)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'OrganizationSelection'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('oin', oin))
      ..add(DiagnosticsProperty('selected', selected));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OrganizationSelectionImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.oin, oin) || other.oin == oin) &&
            (identical(other.selected, selected) ||
                other.selected == selected));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, oin, selected);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$OrganizationSelectionImplCopyWith<_$OrganizationSelectionImpl>
      get copyWith => __$$OrganizationSelectionImplCopyWithImpl<
          _$OrganizationSelectionImpl>(this, _$identity);
}

abstract class _OrganizationSelection implements OrganizationSelection {
  const factory _OrganizationSelection(
      {required final int id,
      required final String oin,
      required final bool selected}) = _$OrganizationSelectionImpl;

  @override
  int get id;
  @override
  String get oin;
  @override
  bool get selected;
  @override
  @JsonKey(ignore: true)
  _$$OrganizationSelectionImplCopyWith<_$OrganizationSelectionImpl>
      get copyWith => throw _privateConstructorUsedError;
}
