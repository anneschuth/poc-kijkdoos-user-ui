import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'financial_claims_information_storage.freezed.dart';

@freezed
class FinancialClaimsInformationStorage
    with _$FinancialClaimsInformationStorage {
  const factory FinancialClaimsInformationStorage({
    required int id,
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String financialClaimsInformationDocument,
  }) = _FinancialClaimsInformationStorage;
}
