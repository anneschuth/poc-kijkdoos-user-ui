import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

enum EnvironmentType {
  development("development"),
  demo("demo"),
  mock("mock");

  const EnvironmentType(this.value);

  final String value;
}

class Environment {
  final String schemeUrl;
  late Enum type;

  Environment({
    this.schemeUrl = 'https://api.stelsel.vorijk-demo.blauweknop.app/v1',
  }) {
    rootBundle.loadString('assets/env.json').then((value) {
      if (value.isEmpty) {
        debugPrint("assets/env.json not found");
        //no logging to db since there is no db yet
        throw Exception("assets/env.json not found");
      }

      var responseJson = json.decode(value);
      if (responseJson == null) {
        debugPrint("assets/env.json is invalid");
        //no logging to db since there is no db yet
        throw Exception("assets/env.json is invalid");
      }

      var typeStringFromEnvironment = responseJson["enviromentType"];
      if (typeStringFromEnvironment == null) {
        debugPrint("enviromentType is not set in assets/env.json");
        //no logging to db since there is no db yet
        throw Exception("enviromentType is not set in assets/env.json");
      }

      var types = EnvironmentType.values.where(
        (type) => type.value == typeStringFromEnvironment,
      );

      if (types.isNotEmpty) {
        type = types.first;
      } else {
        debugPrint(
            "EnvironmentType string $typeStringFromEnvironment not found");
        //no logging to db since there is no db yet
        throw Exception(
            "EnvironmentType string $typeStringFromEnvironment not found");
      }
    });
  }
}

final environmentProvider = Provider<Environment>((ref) {
  return Environment();
});
