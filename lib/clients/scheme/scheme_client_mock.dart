// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:user_ui/clients/scheme/model/scheme.dart';
import 'package:user_ui/clients/scheme/model/scheme_document_type.dart';
import 'package:user_ui/clients/scheme/model/scheme_app_manager.dart';
import 'package:user_ui/clients/scheme/model/scheme_organization.dart';
import 'package:user_ui/clients/scheme/scheme_client.dart';

class SchemeClientMock implements SchemeClient {
  SchemeClientMock();

  @override
  Future<Scheme> fetchScheme() async {
    var organizations = [
      SchemeOrganization(
        oin: '00000000000000000001',
        name: 'mock-org 1',
        discoveryUrl: "http://mock1-session-api.mock/v1",
        publicKey: """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxlibRfmDYIrD9MJRh0BS
utHKiYlXeGopRHT2G8FK8WGvN2faR1ZFdWjTxgncmb1nds7OoG5u2E1KrDMiZ3SH
M7T95Ynle1LHE9D0VhUXA0iAYOq4aumHiyHpmT8XQhRsF/3RjGoGagtnaeQ3uSyd
NBkOiuVm/JTmcU3QDhLwj8st/GvxwaViuLsIH4WNX/LmkPkVbW5/CLNtD9mTmMgt
x3tEBYHyIctPoBrXfYBaJN4tv6JY1vI37cafG4MBzunAr4iP/1GBhh6ql8bpdqd7
cD6cHvGmlubrmdILm9V0erNDV9O9NrjxIalYRU/5E10qGIAuBdBubfOvJQX8QgFa
WQIDAQAB
-----END PUBLIC KEY-----""",
      ),
      SchemeOrganization(
        oin: '00000000000000000002',
        name: 'mock-org 2',
        discoveryUrl: "http://mock2-session-api.mock/v1",
        publicKey: """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxlibRfmDYIrD9MJRh0BS
utHKiYlXeGopRHT2G8FK8WGvN2faR1ZFdWjTxgncmb1nds7OoG5u2E1KrDMiZ3SH
M7T95Ynle1LHE9D0VhUXA0iAYOq4aumHiyHpmT8XQhRsF/3RjGoGagtnaeQ3uSyd
NBkOiuVm/JTmcU3QDhLwj8st/GvxwaViuLsIH4WNX/LmkPkVbW5/CLNtD9mTmMgt
x3tEBYHyIctPoBrXfYBaJN4tv6JY1vI37cafG4MBzunAr4iP/1GBhh6ql8bpdqd7
cD6cHvGmlubrmdILm9V0erNDV9O9NrjxIalYRU/5E10qGIAuBdBubfOvJQX8QgFa
WQIDAQAB
-----END PUBLIC KEY-----""",
      ),
      SchemeOrganization(
        oin: '00000000000000000003',
        name: 'mock-org 3',
        discoveryUrl: "http://mock3-session-api.mock/v1",
        publicKey: """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxlibRfmDYIrD9MJRh0BS
utHKiYlXeGopRHT2G8FK8WGvN2faR1ZFdWjTxgncmb1nds7OoG5u2E1KrDMiZ3SH
M7T95Ynle1LHE9D0VhUXA0iAYOq4aumHiyHpmT8XQhRsF/3RjGoGagtnaeQ3uSyd
NBkOiuVm/JTmcU3QDhLwj8st/GvxwaViuLsIH4WNX/LmkPkVbW5/CLNtD9mTmMgt
x3tEBYHyIctPoBrXfYBaJN4tv6JY1vI37cafG4MBzunAr4iP/1GBhh6ql8bpdqd7
cD6cHvGmlubrmdILm9V0erNDV9O9NrjxIalYRU/5E10qGIAuBdBubfOvJQX8QgFa
WQIDAQAB
-----END PUBLIC KEY-----""",
      ),
      SchemeOrganization(
        oin: '00000000000000000004',
        name: 'mock-org 4',
        discoveryUrl: "http://mock4-session-api.mock/v1",
        publicKey: """-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgH0c30wpdjLu27DoTQzD49ZWom3a
FophOGx+HHRSXXgQaWdqM4yG8y7El7ICCxyyW5Ai32P5cVY+OARtXsYfXnFOwR2g
4BQLVYIhusGLjLD2VYmYbOBe3DpJr4ID5C4732nCa+lcVhQ1FCXq7mBUAjffSxoA
1IN2rW9o8vzQqhNlAgMBAAE=
-----END PUBLIC KEY-----""",
      ),
    ];

    var documentTypes = [
      SchemeDocumentType(
        name: 'mock-document-type',
      ),
    ];

    var appManagers = [
      SchemeAppManager(
        oin: '00000000000000000004',
        name: 'mock-app-manager',
        discoveryUrl: "http://mock5-session-api.mock/v1",
        publicKey: """-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgH0c30wpdjLu27DoTQzD49ZWom3a
FophOGx+HHRSXXgQaWdqM4yG8y7El7ICCxyyW5Ai32P5cVY+OARtXsYfXnFOwR2g
4BQLVYIhusGLjLD2VYmYbOBe3DpJr4ID5C4732nCa+lcVhQ1FCXq7mBUAjffSxoA
1IN2rW9o8vzQqhNlAgMBAAE=
-----END PUBLIC KEY-----""",
      ),
    ];

    return Scheme(
      organizations: organizations,
      documentTypes: documentTypes,
      appManagers: appManagers,
    );
  }
}
