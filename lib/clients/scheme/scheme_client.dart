// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:user_ui/clients/scheme/model/scheme.dart';

abstract class SchemeClient {
  Future<Scheme> fetchScheme();
}
