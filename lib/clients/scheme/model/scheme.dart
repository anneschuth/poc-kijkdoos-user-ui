// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:user_ui/clients/scheme/model/scheme_document_type.dart';
import 'package:user_ui/clients/scheme/model/scheme_app_manager.dart';
import 'package:user_ui/clients/scheme/model/scheme_organization.dart';

class Scheme {
  List<SchemeOrganization> organizations;
  List<SchemeDocumentType> documentTypes;
  List<SchemeAppManager> appManagers;

  Scheme({
    required this.organizations,
    required this.documentTypes,
    required this.appManagers,
  });

  factory Scheme.fromJson(Map<String, dynamic> json) {
    var organizationsList = (json["organizations"] ?? []) as List;
    var documentTypesList = (json["documentTypes"] ?? []) as List;
    var appManagerList = (json["appManagers"] ?? []) as List;

    return Scheme(
      organizations:
          organizationsList.map((i) => SchemeOrganization.fromJson(i)).toList(),
      documentTypes:
          documentTypesList.map((i) => SchemeDocumentType.fromJson(i)).toList(),
      appManagers:
          appManagerList.map((i) => SchemeAppManager.fromJson(i)).toList(),
    );
  }

  Map<String, dynamic> toJson() => {
        'organizations': organizations,
        'documentTypes': documentTypes,
        'appManagers': appManagers,
      };
}
