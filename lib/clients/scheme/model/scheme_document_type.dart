// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

class SchemeDocumentType {
  String name;

  SchemeDocumentType({
    required this.name,
  });

  factory SchemeDocumentType.fromJson(Map<String, dynamic> json) {
    return SchemeDocumentType(
      name: json["name"],
    );
  }

  Map<String, dynamic> toJson() => {
        'name': name,
      };
}
