// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:user_ui/env_provider.dart';
import 'package:user_ui/clients/scheme/model/scheme.dart';
import 'package:user_ui/clients/scheme/scheme_client.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

class SchemeClientLive implements SchemeClient {
  SchemeClientLive(this.env, this.loggingHelper);

  final LoggingHelper loggingHelper;
  final Environment env;

  @override
  Future<Scheme> fetchScheme() async {
    final Uri fetchSchemeURI = Uri.parse('${env.schemeUrl}/fetch_scheme');
    final response = await http.get(fetchSchemeURI);

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);

      var result = Scheme.fromJson(responseJson);

      return Future.value(result);
    } else {
      loggingHelper.addLog(DeviceEvent.uu_20, "Failed to fetch scheme");
      throw Exception('Failed to fetch scheme');
    }
  }
}

final schemeClientProvider = Provider<SchemeClient>((ref) {
  final env = ref.watch(environmentProvider);
  final logging = ref.watch(loggingProvider);
  return SchemeClientLive(env, logging);
});
