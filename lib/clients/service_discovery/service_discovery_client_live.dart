// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:user_ui/clients/service_discovery/service_discovery_client.dart';
import 'package:user_ui/clients/service_discovery/models/discovered_services.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

class ServiceDiscoveryClientLive implements ServiceDiscoveryClient {
  ServiceDiscoveryClientLive(this.loggingHelper);

  final LoggingHelper loggingHelper;

  @override
  Future<DiscoveredServices> fetchDiscoveredServices(
    String endpoint,
  ) async {
    final uri = Uri.parse(endpoint);

    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final responseJson = json.decode(response.body);

      final result = DiscoveredServices.fromJson(responseJson);

      return result;
    } else {
      loggingHelper.addLog(
          DeviceEvent.uu_21, "Failed to fetch bk configuration");
      throw Exception('Failed to fetch bk configuration');
    }
  }
}

final serviceDiscoveryClientProvider = Provider<ServiceDiscoveryClient>((ref) {
  final logging = ref.watch(loggingProvider);
  return ServiceDiscoveryClientLive(logging);
});
