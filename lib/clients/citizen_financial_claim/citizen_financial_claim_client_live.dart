// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client.dart';
import 'package:user_ui/clients/citizen_financial_claim/models/configuration.dart';
import 'package:user_ui/clients/citizen_financial_claim/models/configuration_request.dart';
import 'package:user_ui/clients/citizen_financial_claim/models/financial_claims_information.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

class SessionExpiredException implements Exception {
  String cause;
  SessionExpiredException(this.cause);
}

class AppCertificateExpiredException implements Exception {
  String cause;
  AppCertificateExpiredException(this.cause);
}

class CitizenFinancialClaimClientLive extends CitizenFinancialClaimClient {
  CitizenFinancialClaimClientLive(this.loggingHelper);

  final LoggingHelper loggingHelper;

  @override
  Future<Configuration> configureClaimsRequest(
    String serviceUrl,
    String sessionToken,
    ConfigurationRequest configurationRequest,
  ) async {
    var uri = Uri.parse("$serviceUrl/configure_claims_request");

    final response = await http.post(
      uri,
      body: json.encode(configurationRequest),
      headers: {
        "Authorization": sessionToken,
      },
    );

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);

      var result = Configuration.fromJson(responseJson);

      return result;
    } else {
      if (response.body.contains("session expired")) {
        loggingHelper.addLog(DeviceEvent.uu_17,
            "failed to fetch bk configuration - session expired");
        throw SessionExpiredException(
            'Failed to fetch bk configuration - session expired');
      }

      if (response.body.contains("certificate expired")) {
        loggingHelper.addLog(DeviceEvent.uu_18,
            "Failed to fetch bk configuration - certificate expired");
        throw AppCertificateExpiredException(
            'Failed to fetch bk configuration - certificate expired');
      }
      loggingHelper.addLog(
          DeviceEvent.uu_16, "Failed to configure_claims_request");
      throw Exception('Failed to configure_claims_request');
    }
  }

  @override
  Future<FinancialClaimsInformation> requestFinancialClaimsInformation(
      String serviceUrl, String sessionToken, String configurationToken) async {
    var requestFinancialClaimsInformationUri =
        Uri.parse("$serviceUrl/request_financial_claims_information");

    var uri = requestFinancialClaimsInformationUri
        .replace(queryParameters: {"configurationToken": configurationToken});

    final response = await http.get(
      uri,
      headers: {
        "Authorization": sessionToken,
      },
    );

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body);

      var result = FinancialClaimsInformation.fromJson(responseJson);

      return result;
    } else {
      loggingHelper.addLog(
          DeviceEvent.uu_19, "failed to requestFinancialClaimsInformation");
      throw Exception('Failed to requestFinancialClaimsInformation');
    }
  }
}

final citizenFinancialClaimClientProvider =
    Provider<CitizenFinancialClaimClient>((ref) {
  final logging = ref.watch(loggingProvider);
  return CitizenFinancialClaimClientLive(logging);
});
