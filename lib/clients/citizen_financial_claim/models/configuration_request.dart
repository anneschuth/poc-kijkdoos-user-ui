// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class ConfigurationRequest {
  String encryptedEnvelope;

  ConfigurationRequest({
    required this.encryptedEnvelope,
  });

  factory ConfigurationRequest.fromJson(Map<String, dynamic> jsonObject) {
    return ConfigurationRequest(
      encryptedEnvelope: jsonObject["encryptedEnvelope"],
    );
  }

  Map<String, dynamic> toJson() => {
        'encryptedEnvelope': encryptedEnvelope,
      };
}
