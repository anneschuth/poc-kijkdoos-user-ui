// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class FinancialClaimsInformation {
  String encryptedFinancialClaimsInformationDocument;

  FinancialClaimsInformation({
    required this.encryptedFinancialClaimsInformationDocument,
  });

  factory FinancialClaimsInformation.fromJson(Map<String, dynamic> jsonObject) {
    return FinancialClaimsInformation(
      encryptedFinancialClaimsInformationDocument:
          jsonObject["encryptedFinancialClaimsInformationDocument"],
    );
  }

  Map<String, dynamic> toJson() => {
        'encryptedFinancialClaimsInformationDocument':
            encryptedFinancialClaimsInformationDocument,
      };
}
