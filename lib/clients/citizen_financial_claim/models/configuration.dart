// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class Configuration {
  String token;

  Configuration({
    required this.token,
  });

  factory Configuration.fromJson(Map<String, dynamic> jsonObject) {
    return Configuration(
      token: jsonObject["token"],
    );
  }

  Map<String, dynamic> toJson() => {
        'token': token,
      };
}
