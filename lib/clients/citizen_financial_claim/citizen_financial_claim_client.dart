// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:user_ui/clients/citizen_financial_claim/models/configuration.dart';
import 'package:user_ui/clients/citizen_financial_claim/models/financial_claims_information.dart';
import 'package:user_ui/clients/citizen_financial_claim/models/configuration_request.dart';

abstract class CitizenFinancialClaimClient {
  Future<Configuration> configureClaimsRequest(
    String serviceUrl,
    String sessionToken,
    ConfigurationRequest configurationRequest,
  );
  Future<FinancialClaimsInformation> requestFinancialClaimsInformation(
    String serviceUrl,
    String sessionToken,
    String configurationToken,
  );
}
