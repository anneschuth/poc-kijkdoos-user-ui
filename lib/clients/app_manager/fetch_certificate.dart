// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:http/http.dart' as http;
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'dart:async';
import 'dart:convert';

import 'package:user_ui/clients/app_manager/models/fetch_certificate_response.dart';
import 'package:user_ui/clients/app_manager/models/registration_expired_exception.dart';
import 'package:user_ui/clients/app_manager/models/session_expired_exception.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'fetch_certificate.g.dart';

@riverpod
Future<FetchCertificateResponse> fetchCertificate(
  FetchCertificateRef ref, {
  required String serviceUrl,
  required String sessionToken,
  required String registrationToken,
}) async {
  final loggingHelper = ref.read(loggingProvider);
  final fetchRegistrationUri = Uri.parse("$serviceUrl/fetch_certificate");

  final uri = fetchRegistrationUri
      .replace(queryParameters: {"registrationToken": registrationToken});

  final response = await http.post(
    uri,
    headers: {
      "Authorization": sessionToken,
    },
  );

  if (response.statusCode == 200) {
    final responseJson = json.decode(response.body);

    final result = FetchCertificateResponse.fromJson(responseJson);

    return result;
  } else {
    if (response.body.contains("session expired")) {
      loggingHelper.addLog(
          DeviceEvent.uu_2, "Failed to fetch certificate - sessions expired");
      throw SessionExpiredException(
          'Failed to fetch certificate - session expired');
    }

    if (response.body.contains("registration expired")) {
      loggingHelper.addLog(DeviceEvent.uu_3,
          "Failed to fetch certificate - registration expired");
      throw RegistrationExpiredException(
          'Failed to fetch certificate - registration expired');
    }
    loggingHelper.addLog(DeviceEvent.uu_4, "Failed to fetch certificate");
    throw Exception('Failed to fetch certificate');
  }
}
