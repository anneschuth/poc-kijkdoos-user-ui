// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'register_app_request.freezed.dart';
part 'register_app_request.g.dart';

@freezed
class RegisterAppRequest with _$RegisterAppRequest {
  const factory RegisterAppRequest({
    required String encryptedAppIdentity,
  }) = _RegisterAppRequest;

  factory RegisterAppRequest.fromJson(Map<String, Object?> json) =>
      _$RegisterAppRequestFromJson(json);
}
