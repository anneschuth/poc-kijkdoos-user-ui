// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'fetch_certificate_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FetchCertificateResponse _$FetchCertificateResponseFromJson(
    Map<String, dynamic> json) {
  return _FetchCertificateResponse.fromJson(json);
}

/// @nodoc
mixin _$FetchCertificateResponse {
  String get encryptedCertificate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FetchCertificateResponseCopyWith<FetchCertificateResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FetchCertificateResponseCopyWith<$Res> {
  factory $FetchCertificateResponseCopyWith(FetchCertificateResponse value,
          $Res Function(FetchCertificateResponse) then) =
      _$FetchCertificateResponseCopyWithImpl<$Res, FetchCertificateResponse>;
  @useResult
  $Res call({String encryptedCertificate});
}

/// @nodoc
class _$FetchCertificateResponseCopyWithImpl<$Res,
        $Val extends FetchCertificateResponse>
    implements $FetchCertificateResponseCopyWith<$Res> {
  _$FetchCertificateResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? encryptedCertificate = null,
  }) {
    return _then(_value.copyWith(
      encryptedCertificate: null == encryptedCertificate
          ? _value.encryptedCertificate
          : encryptedCertificate // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FetchCertificateResponseImplCopyWith<$Res>
    implements $FetchCertificateResponseCopyWith<$Res> {
  factory _$$FetchCertificateResponseImplCopyWith(
          _$FetchCertificateResponseImpl value,
          $Res Function(_$FetchCertificateResponseImpl) then) =
      __$$FetchCertificateResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String encryptedCertificate});
}

/// @nodoc
class __$$FetchCertificateResponseImplCopyWithImpl<$Res>
    extends _$FetchCertificateResponseCopyWithImpl<$Res,
        _$FetchCertificateResponseImpl>
    implements _$$FetchCertificateResponseImplCopyWith<$Res> {
  __$$FetchCertificateResponseImplCopyWithImpl(
      _$FetchCertificateResponseImpl _value,
      $Res Function(_$FetchCertificateResponseImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? encryptedCertificate = null,
  }) {
    return _then(_$FetchCertificateResponseImpl(
      encryptedCertificate: null == encryptedCertificate
          ? _value.encryptedCertificate
          : encryptedCertificate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$FetchCertificateResponseImpl
    with DiagnosticableTreeMixin
    implements _FetchCertificateResponse {
  const _$FetchCertificateResponseImpl({required this.encryptedCertificate});

  factory _$FetchCertificateResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$FetchCertificateResponseImplFromJson(json);

  @override
  final String encryptedCertificate;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FetchCertificateResponse(encryptedCertificate: $encryptedCertificate)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FetchCertificateResponse'))
      ..add(DiagnosticsProperty('encryptedCertificate', encryptedCertificate));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FetchCertificateResponseImpl &&
            (identical(other.encryptedCertificate, encryptedCertificate) ||
                other.encryptedCertificate == encryptedCertificate));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, encryptedCertificate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FetchCertificateResponseImplCopyWith<_$FetchCertificateResponseImpl>
      get copyWith => __$$FetchCertificateResponseImplCopyWithImpl<
          _$FetchCertificateResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$FetchCertificateResponseImplToJson(
      this,
    );
  }
}

abstract class _FetchCertificateResponse implements FetchCertificateResponse {
  const factory _FetchCertificateResponse(
          {required final String encryptedCertificate}) =
      _$FetchCertificateResponseImpl;

  factory _FetchCertificateResponse.fromJson(Map<String, dynamic> json) =
      _$FetchCertificateResponseImpl.fromJson;

  @override
  String get encryptedCertificate;
  @override
  @JsonKey(ignore: true)
  _$$FetchCertificateResponseImplCopyWith<_$FetchCertificateResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
