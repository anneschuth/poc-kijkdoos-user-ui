// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_app_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RegisterAppRequestImpl _$$RegisterAppRequestImplFromJson(
        Map<String, dynamic> json) =>
    _$RegisterAppRequestImpl(
      encryptedAppIdentity: json['encryptedAppIdentity'] as String,
    );

Map<String, dynamic> _$$RegisterAppRequestImplToJson(
        _$RegisterAppRequestImpl instance) =>
    <String, dynamic>{
      'encryptedAppIdentity': instance.encryptedAppIdentity,
    };
