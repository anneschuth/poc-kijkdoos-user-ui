// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'register_app_request.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RegisterAppRequest _$RegisterAppRequestFromJson(Map<String, dynamic> json) {
  return _RegisterAppRequest.fromJson(json);
}

/// @nodoc
mixin _$RegisterAppRequest {
  String get encryptedAppIdentity => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RegisterAppRequestCopyWith<RegisterAppRequest> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterAppRequestCopyWith<$Res> {
  factory $RegisterAppRequestCopyWith(
          RegisterAppRequest value, $Res Function(RegisterAppRequest) then) =
      _$RegisterAppRequestCopyWithImpl<$Res, RegisterAppRequest>;
  @useResult
  $Res call({String encryptedAppIdentity});
}

/// @nodoc
class _$RegisterAppRequestCopyWithImpl<$Res, $Val extends RegisterAppRequest>
    implements $RegisterAppRequestCopyWith<$Res> {
  _$RegisterAppRequestCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? encryptedAppIdentity = null,
  }) {
    return _then(_value.copyWith(
      encryptedAppIdentity: null == encryptedAppIdentity
          ? _value.encryptedAppIdentity
          : encryptedAppIdentity // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RegisterAppRequestImplCopyWith<$Res>
    implements $RegisterAppRequestCopyWith<$Res> {
  factory _$$RegisterAppRequestImplCopyWith(_$RegisterAppRequestImpl value,
          $Res Function(_$RegisterAppRequestImpl) then) =
      __$$RegisterAppRequestImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String encryptedAppIdentity});
}

/// @nodoc
class __$$RegisterAppRequestImplCopyWithImpl<$Res>
    extends _$RegisterAppRequestCopyWithImpl<$Res, _$RegisterAppRequestImpl>
    implements _$$RegisterAppRequestImplCopyWith<$Res> {
  __$$RegisterAppRequestImplCopyWithImpl(_$RegisterAppRequestImpl _value,
      $Res Function(_$RegisterAppRequestImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? encryptedAppIdentity = null,
  }) {
    return _then(_$RegisterAppRequestImpl(
      encryptedAppIdentity: null == encryptedAppIdentity
          ? _value.encryptedAppIdentity
          : encryptedAppIdentity // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RegisterAppRequestImpl
    with DiagnosticableTreeMixin
    implements _RegisterAppRequest {
  const _$RegisterAppRequestImpl({required this.encryptedAppIdentity});

  factory _$RegisterAppRequestImpl.fromJson(Map<String, dynamic> json) =>
      _$$RegisterAppRequestImplFromJson(json);

  @override
  final String encryptedAppIdentity;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RegisterAppRequest(encryptedAppIdentity: $encryptedAppIdentity)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'RegisterAppRequest'))
      ..add(DiagnosticsProperty('encryptedAppIdentity', encryptedAppIdentity));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RegisterAppRequestImpl &&
            (identical(other.encryptedAppIdentity, encryptedAppIdentity) ||
                other.encryptedAppIdentity == encryptedAppIdentity));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, encryptedAppIdentity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RegisterAppRequestImplCopyWith<_$RegisterAppRequestImpl> get copyWith =>
      __$$RegisterAppRequestImplCopyWithImpl<_$RegisterAppRequestImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RegisterAppRequestImplToJson(
      this,
    );
  }
}

abstract class _RegisterAppRequest implements RegisterAppRequest {
  const factory _RegisterAppRequest(
      {required final String encryptedAppIdentity}) = _$RegisterAppRequestImpl;

  factory _RegisterAppRequest.fromJson(Map<String, dynamic> json) =
      _$RegisterAppRequestImpl.fromJson;

  @override
  String get encryptedAppIdentity;
  @override
  @JsonKey(ignore: true)
  _$$RegisterAppRequestImplCopyWith<_$RegisterAppRequestImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
