// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'register_app_response.freezed.dart';
part 'register_app_response.g.dart';

@freezed
class RegisterAppResponse with _$RegisterAppResponse {
  const factory RegisterAppResponse({
    required String registrationToken,
  }) = _RegisterAppResponse;

  factory RegisterAppResponse.fromJson(Map<String, Object?> json) =>
      _$RegisterAppResponseFromJson(json);
}
