// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class SessionExpiredException implements Exception {
  String cause;
  SessionExpiredException(this.cause);
}
