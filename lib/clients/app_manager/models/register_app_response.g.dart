// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_app_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RegisterAppResponseImpl _$$RegisterAppResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$RegisterAppResponseImpl(
      registrationToken: json['registrationToken'] as String,
    );

Map<String, dynamic> _$$RegisterAppResponseImplToJson(
        _$RegisterAppResponseImpl instance) =>
    <String, dynamic>{
      'registrationToken': instance.registrationToken,
    };
