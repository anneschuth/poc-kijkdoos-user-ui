// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_certificate_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$FetchCertificateResponseImpl _$$FetchCertificateResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$FetchCertificateResponseImpl(
      encryptedCertificate: json['encryptedCertificate'] as String,
    );

Map<String, dynamic> _$$FetchCertificateResponseImplToJson(
        _$FetchCertificateResponseImpl instance) =>
    <String, dynamic>{
      'encryptedCertificate': instance.encryptedCertificate,
    };
