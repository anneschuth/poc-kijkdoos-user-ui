// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'fetch_certificate_response.freezed.dart';
part 'fetch_certificate_response.g.dart';

@freezed
class FetchCertificateResponse with _$FetchCertificateResponse {
  const factory FetchCertificateResponse({
    required String encryptedCertificate,
  }) = _FetchCertificateResponse;

  factory FetchCertificateResponse.fromJson(Map<String, Object?> json) =>
      _$FetchCertificateResponseFromJson(json);
}
