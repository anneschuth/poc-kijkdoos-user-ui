// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/models/session_expired_exception.dart';
import 'dart:async';

import 'package:user_ui/clients/app_manager/models/unregister_app_request.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'unregister_app.g.dart';

@riverpod
Future<void> unregisterApp(
  UnregisterAppRef ref, {
  required String serviceUrl,
  required String sessionToken,
  required UnregisterAppRequest unregisterAppRequest,
}) async {
  final loggingHelper = ref.read(loggingProvider);
  final uri = Uri.parse("$serviceUrl/unregister_app");

  final response = await http.post(
    uri,
    body: json.encode(unregisterAppRequest),
    headers: {
      "Authorization": sessionToken,
    },
  );

  if (response.statusCode == 200) {
    return;
  } else {
    if (response.body.contains("session expired")) {
      loggingHelper.addLog(
          DeviceEvent.uu_7, "Failed to unregister app - session expired");
      throw SessionExpiredException(
          'Failed to unregister app - session expired');
    }
    loggingHelper.addLog(DeviceEvent.uu_8, "Failed to unregister app");
    throw Exception('Failed to unregister app');
  }
}
