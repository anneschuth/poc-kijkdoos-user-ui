// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_app.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$registerAppHash() => r'71a6078cddc4c726643bb51d4d3fb2fda426ee80';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [registerApp].
@ProviderFor(registerApp)
const registerAppProvider = RegisterAppFamily();

/// See also [registerApp].
class RegisterAppFamily extends Family<AsyncValue<RegisterAppResponse>> {
  /// See also [registerApp].
  const RegisterAppFamily();

  /// See also [registerApp].
  RegisterAppProvider call({
    required String serviceUrl,
    required String sessionToken,
    required RegisterAppRequest registerAppRequest,
  }) {
    return RegisterAppProvider(
      serviceUrl: serviceUrl,
      sessionToken: sessionToken,
      registerAppRequest: registerAppRequest,
    );
  }

  @override
  RegisterAppProvider getProviderOverride(
    covariant RegisterAppProvider provider,
  ) {
    return call(
      serviceUrl: provider.serviceUrl,
      sessionToken: provider.sessionToken,
      registerAppRequest: provider.registerAppRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'registerAppProvider';
}

/// See also [registerApp].
class RegisterAppProvider
    extends AutoDisposeFutureProvider<RegisterAppResponse> {
  /// See also [registerApp].
  RegisterAppProvider({
    required String serviceUrl,
    required String sessionToken,
    required RegisterAppRequest registerAppRequest,
  }) : this._internal(
          (ref) => registerApp(
            ref as RegisterAppRef,
            serviceUrl: serviceUrl,
            sessionToken: sessionToken,
            registerAppRequest: registerAppRequest,
          ),
          from: registerAppProvider,
          name: r'registerAppProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$registerAppHash,
          dependencies: RegisterAppFamily._dependencies,
          allTransitiveDependencies:
              RegisterAppFamily._allTransitiveDependencies,
          serviceUrl: serviceUrl,
          sessionToken: sessionToken,
          registerAppRequest: registerAppRequest,
        );

  RegisterAppProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.serviceUrl,
    required this.sessionToken,
    required this.registerAppRequest,
  }) : super.internal();

  final String serviceUrl;
  final String sessionToken;
  final RegisterAppRequest registerAppRequest;

  @override
  Override overrideWith(
    FutureOr<RegisterAppResponse> Function(RegisterAppRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: RegisterAppProvider._internal(
        (ref) => create(ref as RegisterAppRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        serviceUrl: serviceUrl,
        sessionToken: sessionToken,
        registerAppRequest: registerAppRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<RegisterAppResponse> createElement() {
    return _RegisterAppProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RegisterAppProvider &&
        other.serviceUrl == serviceUrl &&
        other.sessionToken == sessionToken &&
        other.registerAppRequest == registerAppRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, serviceUrl.hashCode);
    hash = _SystemHash.combine(hash, sessionToken.hashCode);
    hash = _SystemHash.combine(hash, registerAppRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin RegisterAppRef on AutoDisposeFutureProviderRef<RegisterAppResponse> {
  /// The parameter `serviceUrl` of this provider.
  String get serviceUrl;

  /// The parameter `sessionToken` of this provider.
  String get sessionToken;

  /// The parameter `registerAppRequest` of this provider.
  RegisterAppRequest get registerAppRequest;
}

class _RegisterAppProviderElement
    extends AutoDisposeFutureProviderElement<RegisterAppResponse>
    with RegisterAppRef {
  _RegisterAppProviderElement(super.provider);

  @override
  String get serviceUrl => (origin as RegisterAppProvider).serviceUrl;
  @override
  String get sessionToken => (origin as RegisterAppProvider).sessionToken;
  @override
  RegisterAppRequest get registerAppRequest =>
      (origin as RegisterAppProvider).registerAppRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
