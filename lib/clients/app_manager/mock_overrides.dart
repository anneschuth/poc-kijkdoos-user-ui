// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/clients/app_manager/fetch_certificate.dart';
import 'package:user_ui/clients/app_manager/models/fetch_certificate_response.dart';
import 'package:user_ui/clients/app_manager/models/register_app_request.dart';
import 'package:user_ui/clients/app_manager/models/register_app_response.dart';
import 'package:user_ui/clients/app_manager/models/unregister_app_request.dart';
import 'package:user_ui/clients/app_manager/register_app.dart';
import 'package:user_ui/clients/app_manager/unregister_app.dart';

final List<Override> appManagerClientMockOverrides = [
  registerAppProvider
      .call(
        serviceUrl: "serviceUrl",
        sessionToken: "sessionToken",
        registerAppRequest: const RegisterAppRequest(
          encryptedAppIdentity: "encryptedAppIdentity",
        ),
      )
      .overrideWith(
        (provider) => const RegisterAppResponse(
          registrationToken: "registrationToken",
        ),
      ),
  fetchCertificateProvider
      .call(
        serviceUrl: "serviceUrl",
        sessionToken: "sessionToken",
        registrationToken: "registrationToken",
      )
      .overrideWith(
        (provider) => const FetchCertificateResponse(
          encryptedCertificate: "encryptedCertificate",
        ),
      ),
  unregisterAppProvider
      .call(
        serviceUrl: "serviceUrl",
        sessionToken: "sessionToken",
        unregisterAppRequest: const UnregisterAppRequest(
          registrationToken: "registrationToken",
          signature: "signature",
        ),
      )
      .overrideWith(
        (provider) => Future.value(),
      )
];
