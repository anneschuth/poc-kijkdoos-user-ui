// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:http/http.dart' as http;
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'dart:async';
import 'dart:convert';

import 'package:user_ui/clients/app_manager/models/register_app_request.dart';
import 'package:user_ui/clients/app_manager/models/register_app_response.dart';
import 'package:user_ui/clients/app_manager/models/session_expired_exception.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'register_app.g.dart';

@riverpod
Future<RegisterAppResponse> registerApp(
  RegisterAppRef ref, {
  required String serviceUrl,
  required String sessionToken,
  required RegisterAppRequest registerAppRequest,
}) async {
  final loggingHelper = ref.read(loggingProvider);
  final uri = Uri.parse("$serviceUrl/register_app");

  final response = await http.post(
    uri,
    body: json.encode(registerAppRequest),
    headers: {
      "Authorization": sessionToken,
    },
  );

  if (response.statusCode == 200) {
    final responseJson = json.decode(response.body);

    final result = RegisterAppResponse.fromJson(responseJson);

    return result;
  } else {
    if (response.body.contains("session expired")) {
      loggingHelper.addLog(
          DeviceEvent.uu_5, "Failed to fetch certificate - session expired");
      throw SessionExpiredException(
          'Failed to fetch certificate - session expired');
    }

    loggingHelper.addLog(DeviceEvent.uu_6, "Failed to register app");
    throw Exception('Failed to register app');
  }
}
