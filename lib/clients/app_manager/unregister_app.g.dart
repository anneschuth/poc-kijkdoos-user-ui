// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unregister_app.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$unregisterAppHash() => r'9a3f11443059fab3d2b6855305ca278fcc79c5f0';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [unregisterApp].
@ProviderFor(unregisterApp)
const unregisterAppProvider = UnregisterAppFamily();

/// See also [unregisterApp].
class UnregisterAppFamily extends Family<AsyncValue<void>> {
  /// See also [unregisterApp].
  const UnregisterAppFamily();

  /// See also [unregisterApp].
  UnregisterAppProvider call({
    required String serviceUrl,
    required String sessionToken,
    required UnregisterAppRequest unregisterAppRequest,
  }) {
    return UnregisterAppProvider(
      serviceUrl: serviceUrl,
      sessionToken: sessionToken,
      unregisterAppRequest: unregisterAppRequest,
    );
  }

  @override
  UnregisterAppProvider getProviderOverride(
    covariant UnregisterAppProvider provider,
  ) {
    return call(
      serviceUrl: provider.serviceUrl,
      sessionToken: provider.sessionToken,
      unregisterAppRequest: provider.unregisterAppRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'unregisterAppProvider';
}

/// See also [unregisterApp].
class UnregisterAppProvider extends AutoDisposeFutureProvider<void> {
  /// See also [unregisterApp].
  UnregisterAppProvider({
    required String serviceUrl,
    required String sessionToken,
    required UnregisterAppRequest unregisterAppRequest,
  }) : this._internal(
          (ref) => unregisterApp(
            ref as UnregisterAppRef,
            serviceUrl: serviceUrl,
            sessionToken: sessionToken,
            unregisterAppRequest: unregisterAppRequest,
          ),
          from: unregisterAppProvider,
          name: r'unregisterAppProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$unregisterAppHash,
          dependencies: UnregisterAppFamily._dependencies,
          allTransitiveDependencies:
              UnregisterAppFamily._allTransitiveDependencies,
          serviceUrl: serviceUrl,
          sessionToken: sessionToken,
          unregisterAppRequest: unregisterAppRequest,
        );

  UnregisterAppProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.serviceUrl,
    required this.sessionToken,
    required this.unregisterAppRequest,
  }) : super.internal();

  final String serviceUrl;
  final String sessionToken;
  final UnregisterAppRequest unregisterAppRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(UnregisterAppRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: UnregisterAppProvider._internal(
        (ref) => create(ref as UnregisterAppRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        serviceUrl: serviceUrl,
        sessionToken: sessionToken,
        unregisterAppRequest: unregisterAppRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _UnregisterAppProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is UnregisterAppProvider &&
        other.serviceUrl == serviceUrl &&
        other.sessionToken == sessionToken &&
        other.unregisterAppRequest == unregisterAppRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, serviceUrl.hashCode);
    hash = _SystemHash.combine(hash, sessionToken.hashCode);
    hash = _SystemHash.combine(hash, unregisterAppRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin UnregisterAppRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `serviceUrl` of this provider.
  String get serviceUrl;

  /// The parameter `sessionToken` of this provider.
  String get sessionToken;

  /// The parameter `unregisterAppRequest` of this provider.
  UnregisterAppRequest get unregisterAppRequest;
}

class _UnregisterAppProviderElement
    extends AutoDisposeFutureProviderElement<void> with UnregisterAppRef {
  _UnregisterAppProviderElement(super.provider);

  @override
  String get serviceUrl => (origin as UnregisterAppProvider).serviceUrl;
  @override
  String get sessionToken => (origin as UnregisterAppProvider).sessionToken;
  @override
  UnregisterAppRequest get unregisterAppRequest =>
      (origin as UnregisterAppProvider).unregisterAppRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
