// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:user_ui/clients/session/models/challenge.dart';
import 'package:user_ui/clients/session/models/challenge_request.dart';
import 'package:user_ui/clients/session/models/challenge_response.dart';
import 'package:user_ui/clients/session/models/session.dart';

abstract class SessionClient {
  Future<Challenge> createChallenge(
    String serviceUrl,
    ChallengeRequest challengeRequest,
  );
  Future<Session> completeChallenge(
    String serviceUrl,
    ChallengeResponse challengeResponse,
  );
}
