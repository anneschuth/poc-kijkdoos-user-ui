// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class Challenge {
  String nonce;
  String signature;

  Challenge({
    required this.nonce,
    required this.signature,
  });

  factory Challenge.fromJson(Map<String, dynamic> jsonObject) {
    return Challenge(
      nonce: jsonObject["nonce"],
      signature: jsonObject["signature"],
    );
  }

  Map<String, dynamic> toJson() => {
        'nonce': nonce,
        'signature': signature,
      };
}
