// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class Session {
  String token;

  Session({
    required this.token,
  });

  factory Session.fromJson(Map<String, dynamic> jsonObject) {
    return Session(
      token: jsonObject["token"],
    );
  }

  Map<String, dynamic> toJson() => {
        'token': token,
      };
}
