// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class ChallengeResult {
  String appSignature;

  ChallengeResult({
    required this.appSignature,
  });

  factory ChallengeResult.fromJson(Map<String, dynamic> jsonObject) {
    return ChallengeResult(
      appSignature: jsonObject["appSignature"],
    );
  }

  Map<String, dynamic> toJson() => {
        'appSignature': appSignature,
      };
}
