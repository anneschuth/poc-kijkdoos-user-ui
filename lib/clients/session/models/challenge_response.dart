// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class ChallengeResponse {
  String nonce;
  String response;

  ChallengeResponse({
    required this.nonce,
    required this.response,
  });

  factory ChallengeResponse.fromJson(Map<String, dynamic> jsonObject) {
    return ChallengeResponse(
      nonce: jsonObject["nonce"],
      response: jsonObject["response"],
    );
  }

  Map<String, dynamic> toJson() => {
        'nonce': nonce,
        'response': response,
      };
}
