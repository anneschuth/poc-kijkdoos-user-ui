// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class ChallengeRequestMessage {
  String appPublicKey;

  ChallengeRequestMessage({
    required this.appPublicKey,
  });

  factory ChallengeRequestMessage.fromJson(Map<String, dynamic> jsonObject) {
    return ChallengeRequestMessage(
      appPublicKey: jsonObject["appPublicKey"],
    );
  }

  Map<String, dynamic> toJson() => {
        'appPublicKey': appPublicKey,
      };
}
