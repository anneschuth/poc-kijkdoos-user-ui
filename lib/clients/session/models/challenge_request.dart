// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class ChallengeRequest {
  String request;

  ChallengeRequest({
    required this.request,
  });

  factory ChallengeRequest.fromJson(Map<String, dynamic> jsonObject) {
    return ChallengeRequest(
      request: jsonObject["request"],
    );
  }

  Map<String, dynamic> toJson() => {
        'request': request,
      };
}
