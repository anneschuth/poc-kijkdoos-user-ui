// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:user_ui/clients/session/models/challenge.dart';
import 'package:user_ui/clients/session/models/challenge_request.dart';
import 'package:user_ui/clients/session/models/challenge_response.dart';
import 'package:user_ui/clients/session/models/session.dart';
import 'package:user_ui/clients/session/session_client.dart';

class SessionClientMock implements SessionClient {
  SessionClientMock();

  @override
  Future<Challenge> createChallenge(
      String serviceUrl, ChallengeRequest challengeRequest) async {
    return Challenge(nonce: "nonce", signature: "signature");
  }

  @override
  Future<Session> completeChallenge(
      String serviceUrl, ChallengeResponse challengeResponse) async {
    return Session(token: "token");
  }
}
