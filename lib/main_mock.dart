// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_ui/app.dart';
import 'package:user_ui/clients/app_manager/mock_overrides.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client_live.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client_mock.dart';
import 'package:user_ui/clients/scheme/scheme_client_live.dart';
import 'package:user_ui/clients/scheme/scheme_client_mock.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_mock.dart';
import 'package:user_ui/clients/session/session_client_live.dart';
import 'package:user_ui/clients/session/session_client_mock.dart';
import 'package:user_ui/theme/theme_manager.dart';
import 'package:user_ui/utils/shared_preferences_provider.dart';
import 'package:user_ui/worker.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final sharedPreferences = await SharedPreferences.getInstance();

  final container = ProviderContainer(
    // observers: const [StateLogger()],
    overrides: <Override>[
      schemeClientProvider.overrideWith((ref) {
        return SchemeClientMock();
      }),
      sessionClientProvider.overrideWith((ref) {
        return SessionClientMock();
      }),
      serviceDiscoveryClientProvider.overrideWith((ref) {
        return ServiceDiscoveryClientMock();
      }),
      citizenFinancialClaimClientProvider.overrideWith((ref) {
        return CitizenFinancialClaimClientMock();
      }),
      sharedPreferencesProvider.overrideWithValue(sharedPreferences),
      ...appManagerClientMockOverrides
    ],
  );

  container.read(workerProvider);

  ThemeManager.setBreakpoints();

  runApp(
    UncontrolledProviderScope(
      container: container,
      child: const App(),
    ),
  );
}
