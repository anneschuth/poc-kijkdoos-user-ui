// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_ui/app.dart';
import 'package:user_ui/theme/theme_manager.dart';
import 'package:user_ui/utils/shared_preferences_provider.dart';
import 'package:user_ui/worker.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final sharedPreferences = await SharedPreferences.getInstance();

  final container = ProviderContainer(
    // observers: const [StateLogger()],
    overrides: <Override>[
      sharedPreferencesProvider.overrideWithValue(sharedPreferences),
    ],
  );

  container.read(workerProvider);

  ThemeManager.setBreakpoints();

  runApp(
    UncontrolledProviderScope(
      container: container,
      child: const App(),
    ),
  );
}
