// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/app_database.dart';
import 'package:user_ui/entities/app_manager_selection.dart';

abstract class AppManagerSelectionRepository {
  Future<void> setAppManagerSelection({required String oin});
  Future<void> clear();
  Future<AppManagerSelection?> get selectedAppManager;
}

class DriftAppManagerSelectionRepository
    extends StateNotifier<AppManagerSelection?>
    implements AppManagerSelectionRepository {
  final AppDatabase database;

  DriftAppManagerSelectionRepository(this.database) : super(null) {
    selectedAppManager.then((selectedAppManager) => state = selectedAppManager);
  }

  @override
  Future<void> setAppManagerSelection({required String oin}) async {
    await database.transaction(() async {
      await database.delete(database.appManagerSelectionTable).go();

      await database
          .into(database.appManagerSelectionTable)
          .insert(AppManagerSelectionTableCompanion.insert(oin: oin));
    });

    state = await selectedAppManager;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.appManagerSelectionTable).go();

    state = await selectedAppManager;
  }

  @override
  Future<AppManagerSelection?> get selectedAppManager async {
    var selectedAppManagerModel = await database
        .select(database.appManagerSelectionTable)
        .getSingleOrNull();

    if (selectedAppManagerModel == null) {
      return null;
    }

    return AppManagerSelection(
      id: selectedAppManagerModel.id,
      oin: selectedAppManagerModel.oin,
    );
  }
}

final appManagerSelectionRepositoryProvider = StateNotifierProvider<
    DriftAppManagerSelectionRepository, AppManagerSelection?>((ref) {
  final database = ref.watch(appDatabaseProvider);

  return DriftAppManagerSelectionRepository(database);
});
