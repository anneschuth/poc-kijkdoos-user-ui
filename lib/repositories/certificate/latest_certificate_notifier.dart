// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/certificate.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';

part 'latest_certificate_notifier.g.dart';

@riverpod
class LatestCertificate extends _$LatestCertificate {
  @override
  Future<Certificate?> build() async {
    final certificates = await ref.watch(certificateRepositoryProvider.future);

    if (certificates.isEmpty) {
      return null;
    }

    return certificates.last;
  }
}
