// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_certificate_by_id.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$getCertificateByIdHash() =>
    r'8a6ae84c9a21f60a7c4153ce45e9544226c9126c';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$GetCertificateById
    extends BuildlessAutoDisposeAsyncNotifier<Certificate> {
  late final int id;

  FutureOr<Certificate> build({
    required int id,
  });
}

/// See also [GetCertificateById].
@ProviderFor(GetCertificateById)
const getCertificateByIdProvider = GetCertificateByIdFamily();

/// See also [GetCertificateById].
class GetCertificateByIdFamily extends Family<AsyncValue<Certificate>> {
  /// See also [GetCertificateById].
  const GetCertificateByIdFamily();

  /// See also [GetCertificateById].
  GetCertificateByIdProvider call({
    required int id,
  }) {
    return GetCertificateByIdProvider(
      id: id,
    );
  }

  @override
  GetCertificateByIdProvider getProviderOverride(
    covariant GetCertificateByIdProvider provider,
  ) {
    return call(
      id: provider.id,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'getCertificateByIdProvider';
}

/// See also [GetCertificateById].
class GetCertificateByIdProvider extends AutoDisposeAsyncNotifierProviderImpl<
    GetCertificateById, Certificate> {
  /// See also [GetCertificateById].
  GetCertificateByIdProvider({
    required int id,
  }) : this._internal(
          () => GetCertificateById()..id = id,
          from: getCertificateByIdProvider,
          name: r'getCertificateByIdProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$getCertificateByIdHash,
          dependencies: GetCertificateByIdFamily._dependencies,
          allTransitiveDependencies:
              GetCertificateByIdFamily._allTransitiveDependencies,
          id: id,
        );

  GetCertificateByIdProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.id,
  }) : super.internal();

  final int id;

  @override
  FutureOr<Certificate> runNotifierBuild(
    covariant GetCertificateById notifier,
  ) {
    return notifier.build(
      id: id,
    );
  }

  @override
  Override overrideWith(GetCertificateById Function() create) {
    return ProviderOverride(
      origin: this,
      override: GetCertificateByIdProvider._internal(
        () => create()..id = id,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        id: id,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<GetCertificateById, Certificate>
      createElement() {
    return _GetCertificateByIdProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GetCertificateByIdProvider && other.id == id;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, id.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin GetCertificateByIdRef
    on AutoDisposeAsyncNotifierProviderRef<Certificate> {
  /// The parameter `id` of this provider.
  int get id;
}

class _GetCertificateByIdProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<GetCertificateById,
        Certificate> with GetCertificateByIdRef {
  _GetCertificateByIdProviderElement(super.provider);

  @override
  int get id => (origin as GetCertificateByIdProvider).id;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
