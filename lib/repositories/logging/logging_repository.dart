import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/app_database.dart';
import 'package:user_ui/entities/log_record.dart';

abstract class LoggingRepository {
  Future<void> add(LogRecord log);
  Future<void> clear();
  Future<List<LogRecord>> get logs;
}

class DriftLoggingRepository extends StateNotifier<List<LogRecord>>
    implements LoggingRepository {
  final AppDatabase database;

  DriftLoggingRepository(this.database) : super([]) {
    logs.then((logs) => state = logs);
  }

  @override
  Future<void> add(LogRecord log) async {
    await database.into(database.logRecordTable).insert(
          LogRecordTableCompanion.insert(
            timestamp: log.timestamp,
            host: log.host,
            cef: log.cef,
            deviceVendor: log.deviceVendor,
            deviceProduct: log.deviceProduct,
            deviceVersion: log.deviceVersion,
            deviceEventClassId: log.deviceEventClassId,
            name: log.name,
            severity: log.severity,
            flexString1Label: log.flexString1Label,
            flexString1: log.flexString1,
            flexString2Label: log.flexString2Label,
            flexString2: log.flexString2,
            act: log.act,
            app: log.app,
            request: log.request,
            requestMethod: log.requestMethod,
          ),
        );

    state = await logs;
  }

  @override
  Future<List<LogRecord>> get logs async {
    List<LogRecord> entries = [];
    await database
        .select(database.logRecordTable)
        .get()
        .then((logRecordsList) => {
              for (var logRecord in logRecordsList)
                {
                  entries.add(LogRecord(
                      timestamp: logRecord.timestamp,
                      host: logRecord.host,
                      cef: logRecord.cef,
                      deviceVendor: logRecord.deviceVendor,
                      deviceProduct: logRecord.deviceProduct,
                      deviceVersion: logRecord.deviceVersion,
                      deviceEventClassId: logRecord.deviceEventClassId,
                      severity: logRecord.severity,
                      flexString1Label: logRecord.flexString1Label,
                      flexString1: logRecord.flexString1,
                      flexString2Label: logRecord.flexString2Label,
                      flexString2: logRecord.flexString2,
                      act: logRecord.act,
                      app: logRecord.app,
                      request: logRecord.request,
                      requestMethod: logRecord.requestMethod,
                      name: logRecord.name))
                }
            });
    return entries;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.logRecordTable).go();

    state = await logs;
  }
}

final loggingRepositoryProvider =
    StateNotifierProvider<DriftLoggingRepository, List<LogRecord>>((ref) {
  final database = ref.watch(appDatabaseProvider);
  return DriftLoggingRepository(database);
});
