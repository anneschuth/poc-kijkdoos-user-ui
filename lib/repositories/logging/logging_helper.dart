import 'dart:developer';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/device_type.dart';
import 'package:user_ui/entities/log_record.dart';
import 'package:user_ui/repositories/logging/logging_repository.dart';

abstract class LoggingHelper {
  void addLog(DeviceEvent eventClassId, String message);
  void addApiLog(
      DeviceEvent eventClassId,
      String message,
      DeviceType deviceType,
      String action,
      String protocol,
      String requestUri,
      String requestMethod);

  static String _createCEFString(
      String eventClassId,
      String eventName,
      String severityLevel,
      String errorMessage,
      String device,
      String action,
      String applicationProtocol,
      String requestUri,
      String method) {
    var timestamp = DateTime.now().toIso8601String();
    var host = "FIX THIS!";
    var cef = "CEF:1";
    var vendor = "VORIJK";
    var product = "user_ui";
    var version = "fix";
    var deviceEventClassId = eventClassId;
    var name = eventName;
    var severity = severityLevel;
    var flexString1Label = "error message";
    var flexString1 = errorMessage;
    var flexString2Label = "device";
    var flexString2 = device;
    var act = action;
    var app = applicationProtocol;
    var request = requestUri;
    var requestMethod = method;
    return "$timestamp $host $cef|$vendor|$product|$version|$deviceEventClassId|$name|"
        "$severity|$flexString1Label|$flexString1|$flexString2Label|$flexString2|"
        "$act|$app|$request|$requestMethod";
  }
}

class ConsoleLoggingHelper extends LoggingHelper {
  @override
  void addApiLog(
      DeviceEvent eventClassId,
      String message,
      DeviceType deviceType,
      String action,
      String protocol,
      String requestUri,
      String requestMethod) {
    log(LoggingHelper._createCEFString(
        eventClassId.name,
        eventClassId.eventName,
        eventClassId.level.value,
        message,
        deviceType.value,
        action,
        protocol,
        requestUri,
        requestMethod));
  }

  @override
  void addLog(DeviceEvent eventClassId, String message) {
    log(LoggingHelper._createCEFString(
        eventClassId.name,
        eventClassId.eventName,
        eventClassId.level.value,
        message,
        "-",
        "-",
        "-",
        "-",
        "-"));
  }
}

class DatabaseLoggingHelper extends LoggingHelper {
  final LoggingRepository loggingRepository;

  DatabaseLoggingHelper(this.loggingRepository);

  @override
  void addApiLog(
      DeviceEvent eventClassId,
      String message,
      DeviceType deviceType,
      String action,
      String protocol,
      String requestUri,
      String requestMethod) {
    var timestamp = DateTime.now();
    var host = "TODO";
    var cef = 1;
    var deviceVendor = "VORIJK";
    var deviceProduct = "user_ui";
    var deviceVersion = "TODO";
    var flexString1Label = "error message";
    var flexString2Label = "device";

    loggingRepository.add(LogRecord(
        timestamp: timestamp,
        host: host,
        cef: cef,
        deviceVendor: deviceVendor,
        deviceProduct: deviceProduct,
        deviceVersion: deviceVersion,
        deviceEventClassId: eventClassId.name,
        severity: eventClassId.level.toInt(),
        flexString1Label: flexString1Label,
        flexString1: message,
        flexString2Label: flexString2Label,
        flexString2: deviceType.value,
        act: action,
        app: protocol,
        request: requestUri,
        requestMethod: requestMethod,
        name: eventClassId.eventName));
  }

  @override
  void addLog(DeviceEvent eventClassId, String message) {
    var timestamp = DateTime.now();
    var host = "TODO";
    var cef = 1;
    var deviceVendor = "VORIJK";
    var deviceProduct = "user_ui";
    var deviceVersion = "TODO";
    var flexString1Label = "error message";
    var flexString2Label = "device";
    loggingRepository.add(LogRecord(
        timestamp: timestamp,
        host: host,
        cef: cef,
        deviceVendor: deviceVendor,
        deviceProduct: deviceProduct,
        deviceVersion: deviceVersion,
        deviceEventClassId: eventClassId.name,
        severity: eventClassId.level.toInt(),
        flexString1Label: flexString1Label,
        flexString1: message,
        flexString2Label: flexString2Label,
        flexString2: DeviceType.blank.value,
        act: "",
        app: "",
        request: "",
        requestMethod: "",
        name: eventClassId.eventName));
  }
}

final loggingProvider = Provider<LoggingHelper>((ref) {
  final loggingRepository = ref.watch(loggingRepositoryProvider.notifier);
  return DatabaseLoggingHelper(loggingRepository);
});
