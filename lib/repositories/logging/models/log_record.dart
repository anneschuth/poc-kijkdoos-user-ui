import 'package:drift/drift.dart';

@DataClassName('LogRecordDriftModel')
class LogRecordTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  DateTimeColumn get timestamp => dateTime()();
  TextColumn get host => text()();
  IntColumn get cef => integer()();
  TextColumn get deviceVendor => text()();
  TextColumn get deviceProduct => text()();
  TextColumn get deviceVersion => text()();
  TextColumn get deviceEventClassId => text()();
  TextColumn get name => text()();
  IntColumn get severity => integer()();
  TextColumn get flexString1Label => text()();
  TextColumn get flexString1 => text()();
  TextColumn get flexString2Label => text()();
  TextColumn get flexString2 => text()();
  TextColumn get act => text()();
  TextColumn get app => text()();
  TextColumn get request => text()();
  TextColumn get requestMethod => text()();
}
