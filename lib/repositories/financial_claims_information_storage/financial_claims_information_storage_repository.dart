// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/app_database.dart';
import 'package:user_ui/entities/financial_claims_information_storage.dart';

abstract class FinancialClaimsInformationStorageRepository {
  Future<void> add(
    String oin,
    DateTime dateTimeRequested,
    DateTime dateTimeReceived,
    String financialClaimsInformationDocument,
  );
  Future<List<FinancialClaimsInformationStorage>>
      get financialClaimsInformationStorage;
  Future<FinancialClaimsInformationStorage> getById(int id);
  Future<void> clear();
}

class DriftFinancialClaimsInformationRepository
    extends StateNotifier<List<FinancialClaimsInformationStorage>>
    implements FinancialClaimsInformationStorageRepository {
  final AppDatabase database;

  DriftFinancialClaimsInformationRepository(this.database) : super([]) {
    financialClaimsInformationStorage.then(
        (financialClaimsInformationStorage) =>
            state = financialClaimsInformationStorage);
  }

  @override
  Future<void> add(
    String oin,
    DateTime dateTimeRequested,
    DateTime dateTimeReceived,
    String financialClaimsInformationDocument,
  ) async {
    await database.into(database.financialClaimsInformationStorageTable).insert(
          FinancialClaimsInformationStorageTableCompanion.insert(
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            dateTimeReceived: dateTimeReceived,
            financialClaimsInformationDocument:
                financialClaimsInformationDocument,
          ),
        );

    state = await financialClaimsInformationStorage;
  }

  @override
  Future<List<FinancialClaimsInformationStorage>>
      get financialClaimsInformationStorage async {
    var financialClaimsInformationStorageModelList = await database
        .select(database.financialClaimsInformationStorageTable)
        .get();

    return financialClaimsInformationStorageModelList
        .map(
          (model) => FinancialClaimsInformationStorage(
            id: model.id,
            oin: model.oin,
            dateTimeReceived: model.dateTimeReceived,
            dateTimeRequested: model.dateTimeRequested,
            financialClaimsInformationDocument:
                model.financialClaimsInformationDocument,
          ),
        )
        .toList();
  }

  @override
  Future<FinancialClaimsInformationStorage> getById(int id) async {
    var financialClaimsInformationStorageModel =
        await (database.select(database.financialClaimsInformationStorageTable)
              ..where((table) => table.id.equals(id)))
            .getSingle();

    return FinancialClaimsInformationStorage(
      id: financialClaimsInformationStorageModel.id,
      oin: financialClaimsInformationStorageModel.oin,
      dateTimeReceived: financialClaimsInformationStorageModel.dateTimeReceived,
      dateTimeRequested:
          financialClaimsInformationStorageModel.dateTimeRequested,
      financialClaimsInformationDocument: financialClaimsInformationStorageModel
          .financialClaimsInformationDocument,
    );
  }

  @override
  Future<void> clear() async {
    await database.delete(database.financialClaimsInformationStorageTable).go();

    state = await financialClaimsInformationStorage;
  }
}

final financialClaimsInformationStorageRepositoryProvider =
    StateNotifierProvider<DriftFinancialClaimsInformationRepository,
        List<FinancialClaimsInformationStorage>>((ref) {
  final database = ref.watch(appDatabaseProvider);
  return DriftFinancialClaimsInformationRepository(database);
});
