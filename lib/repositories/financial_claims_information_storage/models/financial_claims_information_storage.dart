import 'package:drift/drift.dart';

@DataClassName('FinancialClaimsInformationStorageDriftModel')
class FinancialClaimsInformationStorageTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get oin => text()();
  DateTimeColumn get dateTimeRequested => dateTime()();
  DateTimeColumn get dateTimeReceived => dateTime()();
  TextColumn get financialClaimsInformationDocument => text()();
}
