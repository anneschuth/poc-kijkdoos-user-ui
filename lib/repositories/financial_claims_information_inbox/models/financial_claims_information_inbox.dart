import 'package:drift/drift.dart';

@DataClassName('FinancialClaimsInformationInboxDriftModel')
class FinancialClaimsInformationInboxTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get oin => text()();
  DateTimeColumn get dateTimeRequested => dateTime()();
  DateTimeColumn get dateTimeReceived => dateTime()();
  TextColumn get encryptedFinancialClaimsInformationDocument => text()();
  BoolColumn get copiedToStorage => boolean()();
}
