// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/app_database.dart';
import 'package:user_ui/entities/financial_claims_information_inbox.dart';

abstract class FinancialClaimsInformationInboxRepository {
  Future<void> add({
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String encryptedFinancialClaimsInformationDocument,
  });

  Future<void> update(
      FinancialClaimsInformationInbox financialClaimsInformationInbox);

  Future<List<FinancialClaimsInformationInbox>>
      get latestFinancialClaimsInformationInboxPerOrganization;

  Future<List<FinancialClaimsInformationInbox>>
      get financialClaimsInformationInboxes;

  Future<void> clear();
}

class DriftFinancialClaimsInformationRepository
    extends StateNotifier<List<FinancialClaimsInformationInbox>>
    implements FinancialClaimsInformationInboxRepository {
  final AppDatabase database;

  DriftFinancialClaimsInformationRepository(this.database) : super([]) {
    latestFinancialClaimsInformationInboxPerOrganization.then(
        (financialClaimsInformationInboxes) =>
            state = financialClaimsInformationInboxes);
  }

  @override
  Future<List<FinancialClaimsInformationInbox>>
      get financialClaimsInformationInboxes async {
    var financialClaimsInformationInboxModelList = await database
        .select(database.financialClaimsInformationInboxTable)
        .get();

    return financialClaimsInformationInboxModelList
        .map(
          (model) => FinancialClaimsInformationInbox(
            id: model.id,
            oin: model.oin,
            copiedToStorage: model.copiedToStorage,
            dateTimeReceived: model.dateTimeReceived,
            dateTimeRequested: model.dateTimeRequested,
            encryptedFinancialClaimsInformationDocument:
                model.encryptedFinancialClaimsInformationDocument,
          ),
        )
        .toList();
  }

  @override
  Future<void> add({
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String encryptedFinancialClaimsInformationDocument,
  }) async {
    await database.into(database.financialClaimsInformationInboxTable).insert(
        FinancialClaimsInformationInboxTableCompanion.insert(
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            dateTimeReceived: dateTimeReceived,
            encryptedFinancialClaimsInformationDocument:
                encryptedFinancialClaimsInformationDocument,
            copiedToStorage: false));
    state = await latestFinancialClaimsInformationInboxPerOrganization;
  }

  @override
  Future<void> update(
      FinancialClaimsInformationInbox financialClaimsInformationInbox) async {
    var financialClaimsInformationConfigurationModel = await (database
            .select(database.financialClaimsInformationInboxTable)
          ..where(
              (table) => table.id.equals(financialClaimsInformationInbox.id)))
        .getSingle();

    var financialClaimsInformationConfigurationUpdateModel =
        financialClaimsInformationConfigurationModel.copyWith(
      copiedToStorage: financialClaimsInformationInbox.copiedToStorage,
      dateTimeReceived: financialClaimsInformationInbox.dateTimeReceived,
      dateTimeRequested: financialClaimsInformationInbox.dateTimeRequested,
      encryptedFinancialClaimsInformationDocument:
          financialClaimsInformationInbox
              .encryptedFinancialClaimsInformationDocument,
      oin: financialClaimsInformationInbox.oin,
    );

    await database
        .update(database.financialClaimsInformationInboxTable)
        .replace(financialClaimsInformationConfigurationUpdateModel);

    state = await latestFinancialClaimsInformationInboxPerOrganization;
  }

  @override
  Future<List<FinancialClaimsInformationInbox>>
      get latestFinancialClaimsInformationInboxPerOrganization async {
    Set<String> organizationOins = {};
    List<FinancialClaimsInformationInbox>
        financialClaimsInformationInboxesList =
        await financialClaimsInformationInboxes;

    for (var financialClaimsInformationInbox
        in financialClaimsInformationInboxesList) {
      organizationOins.add(financialClaimsInformationInbox.oin);
    }

    List<FinancialClaimsInformationInbox> latestInboxes = [];

    for (var organizationOin in organizationOins) {
      latestInboxes.add(financialClaimsInformationInboxesList
          .where((element) => element.oin == organizationOin)
          .last);
    }

    return latestInboxes;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.financialClaimsInformationInboxTable).go();

    state = await financialClaimsInformationInboxes;
  }
}

final financialClaimsInformationInboxRepositoryProvider = StateNotifierProvider<
    DriftFinancialClaimsInformationRepository,
    List<FinancialClaimsInformationInbox>>((ref) {
  final database = ref.watch(appDatabaseProvider);
  return DriftFinancialClaimsInformationRepository(database);
});
