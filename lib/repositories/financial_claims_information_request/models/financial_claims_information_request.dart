import 'package:drift/drift.dart';

@DataClassName('FinancialClaimsInformationRequestDriftModel')
class FinancialClaimsInformationRequestTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get oin => text()();
  DateTimeColumn get dateTimeRequested => dateTime()();
  TextColumn get status => text()();
  BoolColumn get lock => boolean()();
}
