// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:drift/drift.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/app_database.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';

abstract class FinancialClaimsInformationRequestRepository {
  Future<void> add(
      {required String oin,
      required DateTime dateTimeRequested,
      required FinancialClaimsInformationRequestStatus status});

  Future<void> update(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  );

  Future<List<FinancialClaimsInformationRequest>>
      get financialClaimsInformationRequests;
  Stream<void> watchProcessable();
  Future<FinancialClaimsInformationRequest?> getAndLockNextProcessable();
  Future<void> releaseLockForUncompletedRequests();
  Future<void> clear();

  Future<void> releaseLock(int id);
}

class DriftFinancialClaimsInformationRequestRepository
    extends StateNotifier<List<FinancialClaimsInformationRequest>>
    implements FinancialClaimsInformationRequestRepository {
  final AppDatabase database;

  DriftFinancialClaimsInformationRequestRepository(this.database) : super([]) {
    financialClaimsInformationRequests.then(
        (financialClaimInformationRequests) =>
            state = financialClaimInformationRequests);
  }

  @override
  Future<void> add(
      {required String oin,
      required DateTime dateTimeRequested,
      required FinancialClaimsInformationRequestStatus status}) async {
    await database.into(database.financialClaimsInformationRequestTable).insert(
        FinancialClaimsInformationRequestTableCompanion.insert(
            oin: oin,
            dateTimeRequested: dateTimeRequested,
            status: status.value,
            lock: false));

    state = await financialClaimsInformationRequests;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.financialClaimsInformationRequestTable).go();

    state = await financialClaimsInformationRequests;
  }

  @override
  Future<List<FinancialClaimsInformationRequest>>
      get financialClaimsInformationRequests async {
    var financialClaimsInformationRequestModelList = await database
        .select(database.financialClaimsInformationRequestTable)
        .get();

    return financialClaimsInformationRequestModelList
        .map(
          (model) => FinancialClaimsInformationRequest(
            id: model.id,
            oin: model.oin,
            dateTimeRequested: model.dateTimeRequested,
            lock: model.lock,
            status: FinancialClaimsInformationRequestStatus.values
                .firstWhere((e) => e.value == model.status),
          ),
        )
        .toList();
  }

  @override
  Future<FinancialClaimsInformationRequest?> getAndLockNextProcessable() async {
    return database.transaction(() async {
      var financialClaimsInformationRequestModel = await (database
              .select(database.financialClaimsInformationRequestTable)
            ..where((financialClaimsInformationRequestTable) =>
                financialClaimsInformationRequestTable.lock.not() &
                financialClaimsInformationRequestTable.status.isNotValue(
                    FinancialClaimsInformationRequestStatus
                        .financialClaimsInformationReceived.value))
            ..limit(1))
          .getSingleOrNull();

      if (financialClaimsInformationRequestModel == null) {
        return null;
      }

      var financialClaimsInformationRequestUpdateModel =
          financialClaimsInformationRequestModel.copyWith(lock: true);

      await database
          .update(database.financialClaimsInformationRequestTable)
          .replace(financialClaimsInformationRequestUpdateModel);

      state = await financialClaimsInformationRequests;

      return FinancialClaimsInformationRequest(
        id: financialClaimsInformationRequestUpdateModel.id,
        oin: financialClaimsInformationRequestUpdateModel.oin,
        dateTimeRequested:
            financialClaimsInformationRequestUpdateModel.dateTimeRequested,
        lock: financialClaimsInformationRequestUpdateModel.lock,
        status: FinancialClaimsInformationRequestStatus.values.firstWhere((e) =>
            e.value == financialClaimsInformationRequestUpdateModel.status),
      );
    });
  }

  @override
  Future<void> releaseLock(int id) async {
    await (database.update(database.financialClaimsInformationRequestTable)
          ..where((financialClaimsInformationRequestTable) =>
              financialClaimsInformationRequestTable.id.isValue(id)))
        .write(const FinancialClaimsInformationRequestTableCompanion(
            lock: Value(false)));

    state = await financialClaimsInformationRequests;
  }

  @override
  Future<void> releaseLockForUncompletedRequests() async {
    await (database.update(database.financialClaimsInformationRequestTable)
          ..where((financialClaimsInformationRequestTable) =>
              financialClaimsInformationRequestTable.lock.isValue(false) &
              financialClaimsInformationRequestTable.status.isNotValue(
                  FinancialClaimsInformationRequestStatus
                      .financialClaimsInformationReceived.value)))
        .write(const FinancialClaimsInformationRequestTableCompanion(
            lock: Value(false)));

    state = await financialClaimsInformationRequests;
  }

  @override
  Future<void> update(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    var financialClaimsInformationRequestModel = await (database
            .select(database.financialClaimsInformationRequestTable)
          ..where(
              (table) => table.id.equals(financialClaimsInformationRequest.id)))
        .getSingle();

    var financialClaimsInformationRequestUpdateModel =
        financialClaimsInformationRequestModel.copyWith(
            dateTimeRequested:
                financialClaimsInformationRequest.dateTimeRequested,
            lock: financialClaimsInformationRequest.lock,
            oin: financialClaimsInformationRequest.oin,
            status: financialClaimsInformationRequest.status.value);

    await database
        .update(database.financialClaimsInformationRequestTable)
        .replace(financialClaimsInformationRequestUpdateModel);

    state = await financialClaimsInformationRequests;
  }

  @override
  Stream<void> watchProcessable() {
    return database.customSelect(
        'SELECT EXISTS ('
        ' SELECT 1 FROM financial_claims_information_request_table'
        ' WHERE lock = 0 AND status = ?'
        ')',
        variables: [
          Variable.withString("financial_claims_information_received")
        ],
        readsFrom: {
          database.financialClaimsInformationRequestTable
        }).watch();
  }
}

final financialClaimsInformationRequestRepositoryProvider =
    StateNotifierProvider<DriftFinancialClaimsInformationRequestRepository,
        List<FinancialClaimsInformationRequest>>((ref) {
  final database = ref.watch(appDatabaseProvider);

  return DriftFinancialClaimsInformationRequestRepository(database);
});
