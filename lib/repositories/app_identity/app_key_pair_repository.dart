import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/database/app_database.dart';
import 'package:user_ui/entities/app_key_pair.dart';

part 'app_key_pair_repository.g.dart';

@riverpod
class AppKeyPairRepository extends _$AppKeyPairRepository {
  Future<AppKeyPair?> get _keyPair async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(appDatabaseProvider);

    var appKeyPairModel = await (database.select(database.appKeyPairTable)
          ..limit(1))
        .getSingleOrNull();

    if (appKeyPairModel == null) {
      return null;
    }

    return AppKeyPair(
      id: appKeyPairModel.id,
      publicKey: appKeyPairModel.publicKey,
      privateKey: appKeyPairModel.privateKey,
    );
  }

  @override
  Future<AppKeyPair?> build() {
    return _keyPair;
  }

  Future<void> setKeyPair(
      {required String publicKey, required String privateKey}) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(appDatabaseProvider);

    await database.into(database.appKeyPairTable).insert(
          AppKeyPairTableCompanion.insert(
            publicKey: publicKey,
            privateKey: privateKey,
          ),
        );

    state = AsyncData(await _keyPair);
  }

  Future<void> deleteKeyPair() async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(appDatabaseProvider);

    await database.delete(database.appKeyPairTable).go();

    state = AsyncData(await _keyPair);
  }
}
