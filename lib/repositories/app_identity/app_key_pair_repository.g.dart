// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_key_pair_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$appKeyPairRepositoryHash() =>
    r'2e581b1bd8e6a6a9223381e0b3fce63effcbfd5a';

/// See also [AppKeyPairRepository].
@ProviderFor(AppKeyPairRepository)
final appKeyPairRepositoryProvider = AutoDisposeAsyncNotifierProvider<
    AppKeyPairRepository, AppKeyPair?>.internal(
  AppKeyPairRepository.new,
  name: r'appKeyPairRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$appKeyPairRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AppKeyPairRepository = AutoDisposeAsyncNotifier<AppKeyPair?>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
