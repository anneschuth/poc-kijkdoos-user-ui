// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_scheme_app_manager_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchSchemeAppManagerHash() =>
    r'6965a54234d009644f2e27a732ce958ffac28034';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$FetchSchemeAppManager
    extends BuildlessAutoDisposeAsyncNotifier<SchemeAppManager> {
  late final String oin;

  FutureOr<SchemeAppManager> build({
    required String oin,
  });
}

/// See also [FetchSchemeAppManager].
@ProviderFor(FetchSchemeAppManager)
const fetchSchemeAppManagerProvider = FetchSchemeAppManagerFamily();

/// See also [FetchSchemeAppManager].
class FetchSchemeAppManagerFamily extends Family<AsyncValue<SchemeAppManager>> {
  /// See also [FetchSchemeAppManager].
  const FetchSchemeAppManagerFamily();

  /// See also [FetchSchemeAppManager].
  FetchSchemeAppManagerProvider call({
    required String oin,
  }) {
    return FetchSchemeAppManagerProvider(
      oin: oin,
    );
  }

  @override
  FetchSchemeAppManagerProvider getProviderOverride(
    covariant FetchSchemeAppManagerProvider provider,
  ) {
    return call(
      oin: provider.oin,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'fetchSchemeAppManagerProvider';
}

/// See also [FetchSchemeAppManager].
class FetchSchemeAppManagerProvider
    extends AutoDisposeAsyncNotifierProviderImpl<FetchSchemeAppManager,
        SchemeAppManager> {
  /// See also [FetchSchemeAppManager].
  FetchSchemeAppManagerProvider({
    required String oin,
  }) : this._internal(
          () => FetchSchemeAppManager()..oin = oin,
          from: fetchSchemeAppManagerProvider,
          name: r'fetchSchemeAppManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$fetchSchemeAppManagerHash,
          dependencies: FetchSchemeAppManagerFamily._dependencies,
          allTransitiveDependencies:
              FetchSchemeAppManagerFamily._allTransitiveDependencies,
          oin: oin,
        );

  FetchSchemeAppManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.oin,
  }) : super.internal();

  final String oin;

  @override
  FutureOr<SchemeAppManager> runNotifierBuild(
    covariant FetchSchemeAppManager notifier,
  ) {
    return notifier.build(
      oin: oin,
    );
  }

  @override
  Override overrideWith(FetchSchemeAppManager Function() create) {
    return ProviderOverride(
      origin: this,
      override: FetchSchemeAppManagerProvider._internal(
        () => create()..oin = oin,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        oin: oin,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<FetchSchemeAppManager,
      SchemeAppManager> createElement() {
    return _FetchSchemeAppManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FetchSchemeAppManagerProvider && other.oin == oin;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, oin.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin FetchSchemeAppManagerRef
    on AutoDisposeAsyncNotifierProviderRef<SchemeAppManager> {
  /// The parameter `oin` of this provider.
  String get oin;
}

class _FetchSchemeAppManagerProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<FetchSchemeAppManager,
        SchemeAppManager> with FetchSchemeAppManagerRef {
  _FetchSchemeAppManagerProviderElement(super.provider);

  @override
  String get oin => (origin as FetchSchemeAppManagerProvider).oin;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
