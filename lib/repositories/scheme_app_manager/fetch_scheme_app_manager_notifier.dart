// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/scheme_app_manager.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';

part 'fetch_scheme_app_manager_notifier.g.dart';

@riverpod
class FetchSchemeAppManager extends _$FetchSchemeAppManager {
  @override
  Future<SchemeAppManager> build({required String oin}) async {
    final loggingHelper = ref.read(loggingProvider);
    var schemeAppManagers = await ref
        .watch(schemeAppManagerRepositoryProvider.notifier)
        .appManagers;

    List<SchemeAppManager> schemeAppManagersByOin =
        schemeAppManagers.where((l) => l.oin == oin).toList();
    if (schemeAppManagersByOin.isEmpty) {
      loggingHelper.addLog(
          DeviceEvent.uu_10, "App manager $oin does not exists in the scheme.");
      throw Exception(
        "App manager $oin does not exists in the scheme.",
      );
    }
    return schemeAppManagersByOin.first;
  }
}
