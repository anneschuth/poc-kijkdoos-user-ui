// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/app_database.dart';
import 'package:user_ui/entities/scheme_document_type.dart';

abstract class SchemeDocumentTypeRepository {
  Future<List<SchemeDocumentType>> get documentTypes;
  Future<void> add({required String name, required bool available});
  Future<void> update(SchemeDocumentType documentType);
  Future<void> clear();
}

class DriftSchemeDocumentTypeRepository
    extends StateNotifier<List<SchemeDocumentType>>
    implements SchemeDocumentTypeRepository {
  final AppDatabase database;

  DriftSchemeDocumentTypeRepository(this.database) : super([]) {
    documentTypes.then((documentTypes) => state = documentTypes);
  }

  @override
  Future<void> add({required String name, required bool available}) async {
    await database
        .into(database.schemeDocumentTypeTable)
        .insert(SchemeDocumentTypeTableCompanion.insert(
          name: name,
          available: available,
        ));

    state = await documentTypes;
  }

  @override
  Future<List<SchemeDocumentType>> get documentTypes async {
    var schemeDocumentTypeModelList =
        await database.select(database.schemeDocumentTypeTable).get();

    return schemeDocumentTypeModelList
        .map(
          (model) => SchemeDocumentType(
            id: model.id,
            available: model.available,
            name: model.name,
          ),
        )
        .toList();
  }

  @override
  Future<void> update(
    SchemeDocumentType documentType,
  ) async {
    var schemeDocumentTypeModel =
        await (database.select(database.schemeDocumentTypeTable)
              ..where((table) => table.id.equals(documentType.id)))
            .getSingle();

    var schemeDocumentTypeUpdateModel = schemeDocumentTypeModel.copyWith(
      available: documentType.available,
      name: documentType.name,
    );

    await database
        .update(database.schemeDocumentTypeTable)
        .replace(schemeDocumentTypeUpdateModel);

    state = await documentTypes;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.schemeDocumentTypeTable).go();

    state = await documentTypes;
  }
}

final schemeDocumentTypeRepositoryProvider = StateNotifierProvider<
    DriftSchemeDocumentTypeRepository, List<SchemeDocumentType>>((ref) {
  final database = ref.watch(appDatabaseProvider);

  return DriftSchemeDocumentTypeRepository(database);
});
