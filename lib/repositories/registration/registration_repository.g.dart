// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registration_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$registrationRepositoryHash() =>
    r'3e1a243a0f6fee0a6e4abaf3708625b73959b533';

/// See also [RegistrationRepository].
@ProviderFor(RegistrationRepository)
final registrationRepositoryProvider = AutoDisposeAsyncNotifierProvider<
    RegistrationRepository, List<Registration>>.internal(
  RegistrationRepository.new,
  name: r'registrationRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$registrationRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$RegistrationRepository = AutoDisposeAsyncNotifier<List<Registration>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
