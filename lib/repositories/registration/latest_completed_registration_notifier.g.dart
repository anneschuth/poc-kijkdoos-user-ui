// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_completed_registration_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$latestCompletedRegistrationHash() =>
    r'2e2eeb96dc3df23efa7e0293fe6b5b578fa409f7';

/// See also [LatestCompletedRegistration].
@ProviderFor(LatestCompletedRegistration)
final latestCompletedRegistrationProvider = AutoDisposeAsyncNotifierProvider<
    LatestCompletedRegistration, Registration>.internal(
  LatestCompletedRegistration.new,
  name: r'latestCompletedRegistrationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$latestCompletedRegistrationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LatestCompletedRegistration = AutoDisposeAsyncNotifier<Registration>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
