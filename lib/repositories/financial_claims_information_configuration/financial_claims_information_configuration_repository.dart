// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:drift/drift.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/app_database.dart';
import 'package:user_ui/entities/financial_claims_information_configuration.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

abstract class FinancialClaimsInformationConfigurationRepository {
  Future<FinancialClaimsInformationConfiguration?>
      getUnexpiredConfigurationForOin(String oin);
  Future<void> add({required String oin});
  Future<void> update(
    FinancialClaimsInformationConfiguration
        financialClaimsInformationConfiguration,
  );
  Future<List<FinancialClaimsInformationConfiguration>>
      get financialClaimsInformationConfigurations;
  Future<void> clear();
}

class DriftFinancialClaimsInformationConfigurationRepository
    extends StateNotifier<List<FinancialClaimsInformationConfiguration>>
    implements FinancialClaimsInformationConfigurationRepository {
  final AppDatabase database;
  final LoggingHelper loggingHelper;
  DriftFinancialClaimsInformationConfigurationRepository(
      this.database, this.loggingHelper)
      : super([]) {
    financialClaimsInformationConfigurations.then(
        (financialClaimsInformationConfigurations) =>
            state = financialClaimsInformationConfigurations);
  }

  @override
  Future<FinancialClaimsInformationConfiguration?>
      getUnexpiredConfigurationForOin(String oin) async {
    var financialClaimsInformationConfigurations = await (database
            .select(database.financialClaimsInformationConfigurationTable)
          ..where((table) => table.oin.isValue(oin) & table.expired.not()))
        .get();

    if (financialClaimsInformationConfigurations.isEmpty) {
      return null;
    }

    if (financialClaimsInformationConfigurations.length > 1) {
      loggingHelper.addLog(DeviceEvent.uu_9,
          "Multiple unexpired configurations found for organization");
      throw Exception(
          "multiple unexpired configurations found for organization");
    }

    var financialClaimsInformationConfiguration =
        financialClaimsInformationConfigurations.single;

    return FinancialClaimsInformationConfiguration(
      id: financialClaimsInformationConfiguration.id,
      certificateId: financialClaimsInformationConfiguration.certificateId,
      configuration: financialClaimsInformationConfiguration.configuration,
      configurationRequest:
          financialClaimsInformationConfiguration.configurationRequest,
      document: financialClaimsInformationConfiguration.document,
      documentSignature:
          financialClaimsInformationConfiguration.documentSignature,
      envelope: financialClaimsInformationConfiguration.envelope,
      expired: financialClaimsInformationConfiguration.expired,
      oin: financialClaimsInformationConfiguration.oin,
    );
  }

  @override
  Future<void> add({required String oin}) async {
    await database
        .into(database.financialClaimsInformationConfigurationTable)
        .insert(FinancialClaimsInformationConfigurationTableCompanion.insert(
            oin: oin, expired: false));

    state = await financialClaimsInformationConfigurations;
  }

  @override
  Future<void> update(
    FinancialClaimsInformationConfiguration
        financialClaimsInformationConfiguration,
  ) async {
    var financialClaimsInformationConfigurationModel = await (database
            .select(database.financialClaimsInformationConfigurationTable)
          ..where((table) =>
              table.id.equals(financialClaimsInformationConfiguration.id)))
        .getSingle();

    var financialClaimsInformationConfigurationUpdateModel =
        financialClaimsInformationConfigurationModel.copyWith(
      certificateId:
          Value(financialClaimsInformationConfiguration.certificateId),
      configuration:
          Value(financialClaimsInformationConfiguration.configuration),
      configurationRequest:
          Value(financialClaimsInformationConfiguration.configurationRequest),
      document: Value(financialClaimsInformationConfiguration.document),
      documentSignature:
          Value(financialClaimsInformationConfiguration.documentSignature),
      envelope: Value(financialClaimsInformationConfiguration.envelope),
      expired: financialClaimsInformationConfiguration.expired,
      oin: financialClaimsInformationConfiguration.oin,
    );

    await database
        .update(database.financialClaimsInformationConfigurationTable)
        .replace(financialClaimsInformationConfigurationUpdateModel);

    state = await financialClaimsInformationConfigurations;
  }

  @override
  Future<List<FinancialClaimsInformationConfiguration>>
      get financialClaimsInformationConfigurations async {
    var financialClaimsInformationConfigurationModelList = await database
        .select(database.financialClaimsInformationConfigurationTable)
        .get();

    return financialClaimsInformationConfigurationModelList
        .map(
          (model) => FinancialClaimsInformationConfiguration(
            id: model.id,
            certificateId: model.certificateId,
            configuration: model.configuration,
            configurationRequest: model.configurationRequest,
            document: model.document,
            documentSignature: model.documentSignature,
            envelope: model.envelope,
            expired: model.expired,
            oin: model.oin,
          ),
        )
        .toList();
  }

  @override
  Future<void> clear() async {
    await database
        .delete(database.financialClaimsInformationConfigurationTable)
        .go();

    state = await financialClaimsInformationConfigurations;
  }
}

final financialClaimsInformationConfigurationRepositoryProvider =
    StateNotifierProvider<
        DriftFinancialClaimsInformationConfigurationRepository,
        List<FinancialClaimsInformationConfiguration>>((ref) {
  final database = ref.watch(appDatabaseProvider);
  final logging = ref.watch(loggingProvider);
  return DriftFinancialClaimsInformationConfigurationRepository(
      database, logging);
});
