import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_ui/entities/user_settings.dart';
import 'package:user_ui/utils/shared_preferences_provider.dart';

abstract class UserSettingsRepository {
  Future<UserSettings> get userSettings;
  Future<void> setHasSeenOnboarding(bool hasSeenOnboarding);
  Future<void> setAutoSelectAllOrganizations(bool autoSelectAllOrganizations);
  Future<void> setHasCompletedRegistration(bool hasCompletedRegistration);
  Future<void> setHasCompletedOrganizationSelection(
      bool hasCompletedOrganizationSelection);
  Future<void> setDelay(bool delay);
  Future<void> clear();
}

class SharedPreferencesUserSettingsRepository
    extends StateNotifier<UserSettings> implements UserSettingsRepository {
  SharedPreferencesUserSettingsRepository(this.sharedPreferences)
      : super(const UserSettings(
          hasSeenOnboarding: false,
          autoSelectAllOrganizations: false,
          delay: false,
          hasCompletedRegistration: false,
          hasCompletedOrganizationSelection: false,
        )) {
    userSettings.then((settings) => state = settings);
  }

  final SharedPreferences sharedPreferences;
  final String hasSeenOnboardingKey = 'hasSeenOnboarding';
  final String autoSelectAllOrganizationsKey = 'autoSelectAllOrganizations';
  final String hasCompletedRegistrationKey = 'hasCompletedRegistration';
  final String hasCompletedOrganizationSelectionKey =
      'hasCompletedOrganizationSelection';
  final String delayKey = 'delay';

  @override
  Future<UserSettings> get userSettings {
    return Future<UserSettings>.value(
      UserSettings(
        hasSeenOnboarding:
            sharedPreferences.getBool(hasSeenOnboardingKey) ?? false,
        autoSelectAllOrganizations:
            sharedPreferences.getBool(autoSelectAllOrganizationsKey) ?? false,
        hasCompletedRegistration:
            sharedPreferences.getBool(hasCompletedRegistrationKey) ?? false,
        hasCompletedOrganizationSelection:
            sharedPreferences.getBool(hasCompletedOrganizationSelectionKey) ??
                false,
        delay: sharedPreferences.getBool(delayKey) ?? false,
      ),
    );
  }

  @override
  Future<void> setHasSeenOnboarding(bool hasSeenOnboarding) async {
    debugPrint('Setting to setHasSeenOnBoarding to $hasSeenOnboarding');
    await sharedPreferences.setBool(hasSeenOnboardingKey, hasSeenOnboarding);

    state = await userSettings;
  }

  @override
  Future<void> setAutoSelectAllOrganizations(
      bool autoSelectAllOrganizations) async {
    debugPrint(
        'Setting to autoSelectAllOrganizations to $autoSelectAllOrganizations');
    await sharedPreferences.setBool(
        autoSelectAllOrganizationsKey, autoSelectAllOrganizations);

    state = await userSettings;
  }

  @override
  Future<void> setHasCompletedRegistration(
      bool hasCompletedRegistration) async {
    debugPrint(
        'Setting to setHasCompletedRegistration to $hasCompletedRegistration');
    await sharedPreferences.setBool(
        hasCompletedRegistrationKey, hasCompletedRegistration);

    state = await userSettings;
  }

  @override
  Future<void> setHasCompletedOrganizationSelection(
      bool hasCompletedOrganizationSelection) async {
    debugPrint(
        'Setting to setHasCompletedOrganizationSelection to $hasCompletedOrganizationSelection');
    await sharedPreferences.setBool(hasCompletedOrganizationSelectionKey,
        hasCompletedOrganizationSelection);

    state = await userSettings;
  }

  @override
  Future<void> setDelay(bool delay) async {
    debugPrint('Setting to delay to $delay');
    await sharedPreferences.setBool(delayKey, delay);

    state = await userSettings;
  }

  @override
  Future<void> clear() async {
    await sharedPreferences.remove(hasSeenOnboardingKey);
    await sharedPreferences.remove(autoSelectAllOrganizationsKey);
    await sharedPreferences.remove(hasCompletedRegistrationKey);
    await sharedPreferences.remove(hasCompletedOrganizationSelectionKey);
    //NOTE: delay not removed

    state = await userSettings;
  }
}

final userSettingsRepositoryProvider = StateNotifierProvider<
    SharedPreferencesUserSettingsRepository, UserSettings>((ref) {
  final sharedPreferences = ref.watch(sharedPreferencesProvider);
  return SharedPreferencesUserSettingsRepository(sharedPreferences);
});
