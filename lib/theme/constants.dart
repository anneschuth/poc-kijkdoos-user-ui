import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';

enum Spacing {
  /// 2 dp
  tiny(2),

  /// 4 dp
  small(4),

  /// 8 dp
  medium(8),

  /// 16 dp
  large(16),

  /// 32 dp
  xlarge(32),

  /// 48 dp
  xxlarge(48),

  /// 96 dp
  xxxlarge(96);

  const Spacing(this.value);
  final double value;
}

enum RadiusSize {
  /// 4 dp
  small(4),

  /// 8 dp
  medium(8),

  /// 16 dp
  large(16),

  /// 24 dp
  xlarge(24);

  const RadiusSize(this.value);
  final double value;
}

const double maxWidthPercentage = 0.7;

double maxWidthConstraints(BuildContext context) =>
    responsiveValue(context, xs: 528.0, md: 480.0, lg: 624.0, xl: 560.0);
