import 'package:flutter/material.dart';
import 'package:user_ui/theme/side_menu.dart';
import 'package:user_ui/theme/themed_backbutton.dart';

class ThemedScreenV2 extends StatelessWidget {
  const ThemedScreenV2({
    super.key,
    this.title,
    this.onBackbuttonPress,
    this.onRefresh,
    this.header = const [],
    this.body = const [],
    this.footer = const [],
    this.showDrawer = false,
  });

  final String? title;
  final bool showDrawer;
  final void Function()? onBackbuttonPress;
  final Future<void> Function()? onRefresh;

  final List<Widget> header;
  final List<Widget> body;
  final List<Widget> footer;

  static const double backButtonLeadingWidth = 130;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: showDrawer ? const SideMenu() : null,
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(
        leading: onBackbuttonPress != null
            ? ThemedBackbutton(
                onBackbuttonPress: onBackbuttonPress,
              )
            : showDrawer
                ? null
                : const SizedBox.shrink(),
        leadingWidth: onBackbuttonPress != null ? backButtonLeadingWidth : null,
        backgroundColor: Theme.of(context).colorScheme.background,
        foregroundColor: Theme.of(context).colorScheme.onBackground,
        elevation: 0.0,
        title: title != null ? Text(title!) : null,
        titleTextStyle: Theme.of(context).textTheme.displaySmall,
      ),
      body: SafeArea(
        child: Align(
          alignment: Alignment.center,
          child: Container(
            constraints: const BoxConstraints(maxWidth: 1080),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                header.isNotEmpty
                    ? _Header(content: header)
                    : const SizedBox.shrink(),
                body.isNotEmpty
                    ? _Body(content: body, onRefresh: onRefresh)
                    : const SizedBox.shrink(),
                footer.isNotEmpty
                    ? _Footer(content: footer)
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({required this.content});
  final List<Widget> content;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: content,
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({required this.content, required this.onRefresh});
  final List<Widget> content;
  final Future<void> Function()? onRefresh;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Align(
        alignment: Alignment.topCenter,
        child: RefreshIndicator(
          onRefresh: onRefresh != null ? onRefresh! : () async => {},
          child: CustomScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            slivers: [
              SliverFillRemaining(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: content,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _Footer extends StatelessWidget {
  const _Footer({required this.content});
  final List<Widget> content;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: content,
        ),
      ),
    );
  }
}
