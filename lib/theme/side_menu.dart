import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/features/error/error_log_screen.dart';
import 'package:user_ui/screens/features/onboarding/propositon_screen.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/spacing.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/usecases/app_usecase.dart';

class SideMenu extends ConsumerWidget {
  const SideMenu({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var userSettingsRepository = ref.watch(userSettingsRepositoryProvider);
    var selectedDelayForDemoPurpose = <bool>[
      userSettingsRepository.delay,
      !userSettingsRepository.delay
    ];

    return Drawer(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconButton(
              onPressed: () => Scaffold.of(context).closeDrawer(),
              icon: const Icon(
                Icons.close,
                size: 20,
              ),
              padding: EdgeInsets.all(Spacing.large.value),
            ),
            ListTile(
              title: Text(
                "Menu",
                style: Theme.of(context).textTheme.displayMedium,
              ),
            ),
            addVerticalSpace(Spacing.xlarge.value),
            ListTile(
              title: const Text(
                "Home",
              ),
              onTap: () {
                context.goNamed(HomeScreen.routeName);
                Scaffold.of(context).closeDrawer();
              },
            ),
            ListTile(
              title: const Text(
                "Informatie & hulp",
              ),
              onTap: () => debugPrint("Informatie & hulp"),
            ),
            ListTile(
              title: const Text(
                "Over deze app",
              ),
              onTap: () => debugPrint("Over deze app"),
            ),
            addVerticalSpace(Spacing.xlarge.value),
            ListTile(
              title: const Text("App leegmaken"),
              onTap: () {
                ref
                    .read(appUsecaseProvider)
                    .removeAllDataAndUserSettings()
                    .then(
                  (_) {
                    Scaffold.of(context).closeDrawer();
                    context.goNamed(PropositionScreen.routeName);
                  },
                );
              },
            ),
            ListTile(
              title: const Text("Foutenlogboek"),
              onTap: () {
                context.goNamed(ErrorLogScreen.routeName);
                Scaffold.of(context).closeDrawer();
              },
            ),
            addVerticalSpace(Spacing.xxlarge.value),
            ToggleButtons(
              onPressed: (int index) {
                ref
                    .read(userSettingsRepositoryProvider.notifier)
                    .setDelay(index == 0);
              },
              borderRadius:
                  BorderRadius.all(Radius.circular(RadiusSize.medium.value)),
              selectedColor: Theme.of(context).colorScheme.onSecondary,
              fillColor: Theme.of(context).colorScheme.secondaryContainer,
              constraints: const BoxConstraints(
                minHeight: 40.0,
                minWidth: 80.0,
              ),
              isSelected: selectedDelayForDemoPurpose,
              children: const [
                Icon(CupertinoIcons.tortoise_fill),
                Icon(CupertinoIcons.hare_fill),
              ],
            )
          ],
        ),
      ),
    );
  }
}
