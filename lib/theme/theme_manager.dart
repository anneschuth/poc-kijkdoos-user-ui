// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';

import 'card.dart';
import 'colors.dart';

class ThemeManager {
  static const String fontMontserrat = 'Montserrat';
  static const String fontSFProTR = 'SF Pro Text';

  static setBreakpoints() =>
      ResponsiveGridBreakpoints.value = ResponsiveGridBreakpoints(
        xs: 330,
        sm: 560,
        md: 768,
        lg: 992,
        xl: 1200,
      );

  static light(bool isSmallScreenSize) => ThemeData(
        primaryColor: colorDarkBlue,
        textTheme: isSmallScreenSize ? textThemeSmall : textThemeDefault,
        cardTheme: cardTheme,
        fontFamily: "Montserrat",
        colorScheme: const ColorScheme(
          brightness: Brightness.light,
          primary: colorDarkBlue,
          onPrimary: colorWhite,
          primaryContainer: colorLightBlue,
          onPrimaryContainer: colorBlack,
          secondary: colorMintGreen,
          onSecondary: colorWhite,
          secondaryContainer: colorLightMintGreen,
          onSecondaryContainer: colorBlack,
          error: colorRed,
          onError: colorWhite,
          background: colorWhite,
          onBackground: colorBlack,
          surface: colorLightGrey,
          onSurface: colorBlack,
          surfaceVariant: colorLightGrey5,
          onSurfaceVariant: colorBlack,
          outline: colorGrey,
          outlineVariant: colorLightGrey,
        ),
      );

  //TODO: Add Dark Theme

  static const textThemeDefault = TextTheme(
    displayLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 26,
      height: 32 / 26,
    ),
    displayMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 20,
      height: 28 / 20,
    ),
    displaySmall: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 18,
      height: 21 / 18,
    ),
    headlineMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w600,
      fontSize: 17,
      height: 22 / 17,
    ),
    bodyLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 17,
      height: 22 / 17,
    ),
    bodyMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 16,
      height: 20 / 16,
    ),
    bodySmall: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 14,
      height: 18 / 14,
    ),
    titleLarge: TextStyle(
      color: colorGrey,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 17,
      height: 22 / 17,
    ),
    titleMedium: TextStyle(
      color: colorGrey,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 15,
      height: 18 / 15,
    ),
    titleSmall: TextStyle(
      color: colorGrey,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 13,
      height: 18 / 13,
    ),
    labelLarge: TextStyle(
      fontFamily: fontMontserrat,
    ),
  );

  static const textThemeSmall = TextTheme(
    displayLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 22,
      height: 24 / 22,
    ),
    displayMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 16,
      height: 20 / 16,
    ),
    displaySmall: TextStyle(
      color: colorBlack,
      fontFamily: fontMontserrat,
      fontWeight: FontWeight.w700,
      fontSize: 14,
      height: 17 / 14,
    ),
    headlineMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w600,
      fontSize: 17,
      height: 22 / 17,
    ),
    bodyLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 14,
      height: 16 / 14,
    ),
    bodyMedium: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 12,
      height: 16 / 12,
    ),
    bodySmall: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w500,
      fontSize: 10,
      height: 14 / 10,
    ),
    titleLarge: TextStyle(
      color: colorBlack,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 14,
      height: 17 / 14,
    ),
    titleMedium: TextStyle(
      color: colorGrey,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 10,
      height: 14 / 12,
    ),
    titleSmall: TextStyle(
      color: colorGrey,
      fontFamily: fontSFProTR,
      fontWeight: FontWeight.w400,
      fontSize: 10,
      height: 12 / 10,
    ),
    labelLarge: TextStyle(
      fontFamily: fontMontserrat,
    ),
  );
}
