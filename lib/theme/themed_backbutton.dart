import 'package:flutter/material.dart';

class ThemedBackbutton extends StatelessWidget {
  const ThemedBackbutton({
    super.key,
    this.onBackbuttonPress,
  });

  final void Function()? onBackbuttonPress;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onBackbuttonPress,
      style: TextButton.styleFrom(
        foregroundColor: Theme.of(context).colorScheme.primary,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const BackButtonIcon(),
          Text(
            'Vorige',
            style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                  color: Theme.of(context).colorScheme.primary,
                ),
          ),
        ],
      ),
    );
  }
}
