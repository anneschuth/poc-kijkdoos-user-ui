// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:flutter/material.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/theme/constants.dart';

class ThemedCard extends StatelessWidget {
  const ThemedCard({
    super.key,
    this.child,
    this.width = double.infinity,
  });

  final Widget? child;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration: BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.circular(RadiusSize.large.value),
        boxShadow: [
          BoxShadow(
            color: colorBlack.withOpacity(0.3),
            offset: const Offset(0, 1),
            blurRadius: RadiusSize.xlarge.value,
          )
        ],
      ),
      child: child,
    );
  }
}
