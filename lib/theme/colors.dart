// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:flutter/material.dart';

/// primary colors
const colorDarkBlue = Color(0xff0047ff);
const colorLightBlue = Color(0xffa8c1ff);

/// secondary colors
const colorMintGreen = Color(0xff76d2b6);
const colorLightMintGreen = Color(0xff6abda3);

/// type and surface colors
const colorBlack = Color(0xff2b2b2b);
const colorBlack2 = Color(0xff383836);
const colorGrey = Color(0xff757575);
const colorLightGrey = Color(0xfff2f2f7);
const colorLightGrey2 = Color(0xffd0d0db);
const colorLightGrey3 = Color(0xfffafafc);
const colorLightGrey4 = Color(0xffafafaf);
const colorLightGrey5 = Color(0xffe6e6e6);
const colorWhite = Colors.white;

/// error color
const colorRed = Colors.red;

/// decoration colors
const colorGreen = Color(0xFF23cc52);
const colorOrange = Color(0xFFFFB612);
const colorYellow = Colors.yellow;
