import 'package:flutter/material.dart';
import 'package:user_ui/theme/constants.dart';

class ThemedDivider extends StatelessWidget {
  const ThemedDivider({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.outlineVariant,
      height: Spacing.tiny.value,
    );
  }
}
