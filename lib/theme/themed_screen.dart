import 'package:flutter/material.dart';
import 'package:user_ui/theme/side_menu.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/theme/themed_backbutton.dart';

import 'spacing.dart';

class ThemedScreen extends StatelessWidget {
  const ThemedScreen({
    super.key,
    this.children = const <Widget>[],
    this.footer = const <Widget>[],
    this.appBarText = '',
    this.headingText = '',
    this.childrenPadding = false,
    this.showDrawer = false,
    this.onBackbuttonPress,
    this.mainAxisAlignment = MainAxisAlignment.start,
    this.scrollable = false,
  });

  final List<Widget> children;
  final List<Widget> footer;
  final String appBarText;
  final void Function()? onBackbuttonPress;
  final bool showDrawer;
  final String headingText;
  final bool childrenPadding;
  final MainAxisAlignment mainAxisAlignment;
  final bool scrollable;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: showDrawer ? const SideMenu() : null,
      backgroundColor: colorWhite,
      appBar: AppBar(
        leading: onBackbuttonPress != null
            ? ThemedBackbutton(
                onBackbuttonPress: onBackbuttonPress,
              )
            : showDrawer
                ? null
                : const SizedBox.shrink(),
        leadingWidth: onBackbuttonPress != null ? 130 : null,
        backgroundColor: colorWhite,
        foregroundColor: colorBlack,
        elevation: 0.0,
        title: appBarText.isNotEmpty ? Text(appBarText) : null,
        titleTextStyle: Theme.of(context).textTheme.displaySmall,
      ),
      body: SafeArea(
        child: Align(
          alignment: Alignment.center,
          child: Container(
            constraints: const BoxConstraints(maxWidth: 1080),
            child: LayoutBuilder(
              builder: (context, constraint) {
                return scrollable
                    ? scrollableBody(context)
                    : fixedBody(constraint, context);
              },
            ),
          ),
        ),
      ),
    );
  }

  SingleChildScrollView scrollableBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: mainAxisAlignment,
        mainAxisSize: MainAxisSize.max,
        children: [
          childrenPadding
              ? Padding(
                  padding: const EdgeInsets.all(defaultPadding),
                  child: bodyContent(context),
                )
              : bodyContent(context),
        ],
      ),
    );
  }

  SizedBox fixedBody(BoxConstraints constraint, BuildContext context) {
    return SizedBox(
      height: constraint.maxHeight,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: mainAxisAlignment,
        mainAxisSize: MainAxisSize.max,
        children: [
          childrenPadding
              ? Padding(
                  padding: const EdgeInsets.all(defaultPadding),
                  child: bodyContent(context),
                )
              : bodyContent(context),
          if (footer.isNotEmpty) const Spacer(),
          ...footer,
        ],
      ),
    );
  }

  Widget bodyContent(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.max,
      children: [
        if (headingText.isNotEmpty) ...headingTextContent(context),
        ...children,
      ],
    );
  }

  List<Widget> headingTextContent(BuildContext context) {
    return [
      SizedBox(
        child: Text(
          headingText,
          style: Theme.of(context).textTheme.displayLarge,
        ),
      ),
      addVerticalSpace(defaultPadding),
    ];
  }
}
