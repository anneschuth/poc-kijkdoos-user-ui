import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:user_ui/theme/constants.dart';
import 'package:user_ui/theme/side_menu.dart';
import 'package:user_ui/theme/themed_backbutton.dart';

class BasicScaffold extends StatelessWidget {
  const BasicScaffold({
    super.key,
    this.title,
    this.onBackbuttonPress,
    this.showDrawer = false,
    this.useGreyBackground = false,
    required this.content,
  });

  final String? title;
  final bool showDrawer;
  final void Function()? onBackbuttonPress;
  final Widget content;
  final bool useGreyBackground;

  static const double backButtonLeadingWidth = 130;
  static const double maxContentWidth = 840;

  // Temporary to show the screen size while editing
  static const bool developerMode = true;

  @override
  Widget build(BuildContext context) {
    final double screenMargin = responsiveValue(
      context,
      xs: Spacing.large.value,
      md: Spacing.xlarge.value,
    );

    final Color backgroundColor = responsiveValue(
      context,
      xs: Theme.of(context).colorScheme.background,
      md: useGreyBackground
          ? Theme.of(context).colorScheme.surface
          : Theme.of(context).colorScheme.background,
    );

    return Scaffold(
      drawer: showDrawer ? const SideMenu() : null,
      backgroundColor: backgroundColor,
      appBar: showDrawer == false && onBackbuttonPress == null && title == null
          ? null
          : AppBar(
              leading: onBackbuttonPress != null
                  ? ThemedBackbutton(
                      onBackbuttonPress: onBackbuttonPress,
                    )
                  : showDrawer
                      ? null
                      : const SizedBox.shrink(),
              leadingWidth:
                  onBackbuttonPress != null ? backButtonLeadingWidth : null,
              backgroundColor: backgroundColor,
              foregroundColor: Theme.of(context).colorScheme.onBackground,
              elevation: 0.0,
              title: title != null ? Text(title!) : null,
              titleTextStyle: Theme.of(context).textTheme.displaySmall,
            ),
      body: SafeArea(
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: screenMargin),
              child: Center(
                child: Container(
                  constraints: const BoxConstraints(maxWidth: maxContentWidth),
                  child: content,
                ),
              ),
            ),
            if (developerMode)
              Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  responsiveValue(
                    context,
                    xs: showScreenSize(context, 'xsmall'),
                    sm: showScreenSize(context, 'small'),
                    md: showScreenSize(context, 'medium'),
                    lg: showScreenSize(context, 'large'),
                    xl: showScreenSize(context, 'xlarge'),
                  ),
                  style: Theme.of(context)
                      .textTheme
                      .bodySmall
                      ?.copyWith(fontWeight: FontWeight.bold),
                ),
              ),
          ],
        ),
      ),
    );
  }

  String showScreenSize(BuildContext context, String size) =>
      '$size ${MediaQuery.of(context).size.width}';
}
