// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/theme/spacing.dart';

enum ThemedButtonStyle {
  normal,
  primary,
  tertiary,
  delete,
  quarternary,
  quinary,
}

extension ThemedButtonStyles on ThemedButtonStyle {
  Color get textColor {
    switch (this) {
      case ThemedButtonStyle.primary:
        return colorLightGrey;
      case ThemedButtonStyle.normal:
      case ThemedButtonStyle.tertiary:
        return colorDarkBlue;
      case ThemedButtonStyle.delete:
        return colorRed;
      case ThemedButtonStyle.quarternary:
        return colorGrey;
      case ThemedButtonStyle.quinary:
        return colorBlack;
    }
  }

  Color get backgroundColor {
    switch (this) {
      case ThemedButtonStyle.primary:
        return colorDarkBlue;
      case ThemedButtonStyle.normal:
      case ThemedButtonStyle.quinary:
        return colorLightGrey;
      case ThemedButtonStyle.tertiary:
      case ThemedButtonStyle.delete:
      case ThemedButtonStyle.quarternary:
        return colorWhite;
    }
  }
}

class ThemedButton extends StatelessWidget {
  const ThemedButton({
    super.key,
    required this.buttonText,
    this.style = ThemedButtonStyle.normal,
    required this.onPress,
    this.width = double.infinity,
    this.buttonIcon,
  });

  final void Function()? onPress;
  final String buttonText;
  final double width;
  final ThemedButtonStyle style;
  final Widget? buttonIcon;

  @override
  Widget build(BuildContext context) {
    final Color textColor, backgroundColor;
    if (onPress == null) {
      textColor = colorLightGrey4;
      backgroundColor = colorLightGrey3;
    } else {
      textColor = style.textColor;
      backgroundColor = style.backgroundColor;
    }

    return SizedBox(
      width: width,
      child: ElevatedButton(
        onPressed: onPress,
        style: ElevatedButton.styleFrom(
          foregroundColor: textColor,
          disabledForegroundColor: onPress == null ? colorGrey : null,
          disabledBackgroundColor: backgroundColor,
          backgroundColor: backgroundColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          minimumSize: const Size(0, 48),
          elevation: 0.0,
        ),
        child: (style != ThemedButtonStyle.quinary)
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (buttonIcon != null) ...[
                    buttonIcon!,
                    addHorizontalSpace(defaultPadding),
                  ],
                  Flexible(
                    child: FittedBox(
                      child: Text(
                        buttonText,
                        style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                              color: textColor,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                  ),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (buttonIcon != null) ...[
                    buttonIcon!,
                    addHorizontalSpace(defaultPadding),
                  ],
                  Flexible(
                    child: FittedBox(
                      child: Text(
                        buttonText,
                        style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                              color: textColor,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                  ),
                  const Icon(
                    Icons.chevron_right,
                    color: colorLightGrey4,
                  )
                ],
              ),
      ),
    );
  }
}
