// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:user_ui/theme/colors.dart';
import 'package:user_ui/theme/constants.dart';

final cardTheme = CardTheme(
  color: colorWhite,
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(RadiusSize.large.value),
  ),
  elevation: 20.0,
);
