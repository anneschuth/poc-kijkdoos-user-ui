// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// ignore_for_file: type=lint
class $FinancialClaimsInformationRequestTableTable
    extends FinancialClaimsInformationRequestTable
    with
        TableInfo<$FinancialClaimsInformationRequestTableTable,
            FinancialClaimsInformationRequestDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FinancialClaimsInformationRequestTableTable(this.attachedDatabase,
      [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeRequestedMeta =
      const VerificationMeta('dateTimeRequested');
  @override
  late final GeneratedColumn<DateTime> dateTimeRequested =
      GeneratedColumn<DateTime>('date_time_requested', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _statusMeta = const VerificationMeta('status');
  @override
  late final GeneratedColumn<String> status = GeneratedColumn<String>(
      'status', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _lockMeta = const VerificationMeta('lock');
  @override
  late final GeneratedColumn<bool> lock = GeneratedColumn<bool>(
      'lock', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("lock" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns =>
      [id, oin, dateTimeRequested, status, lock];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'financial_claims_information_request_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<FinancialClaimsInformationRequestDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('date_time_requested')) {
      context.handle(
          _dateTimeRequestedMeta,
          dateTimeRequested.isAcceptableOrUnknown(
              data['date_time_requested']!, _dateTimeRequestedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeRequestedMeta);
    }
    if (data.containsKey('status')) {
      context.handle(_statusMeta,
          status.isAcceptableOrUnknown(data['status']!, _statusMeta));
    } else if (isInserting) {
      context.missing(_statusMeta);
    }
    if (data.containsKey('lock')) {
      context.handle(
          _lockMeta, lock.isAcceptableOrUnknown(data['lock']!, _lockMeta));
    } else if (isInserting) {
      context.missing(_lockMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FinancialClaimsInformationRequestDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FinancialClaimsInformationRequestDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      dateTimeRequested: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime,
          data['${effectivePrefix}date_time_requested'])!,
      status: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}status'])!,
      lock: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}lock'])!,
    );
  }

  @override
  $FinancialClaimsInformationRequestTableTable createAlias(String alias) {
    return $FinancialClaimsInformationRequestTableTable(
        attachedDatabase, alias);
  }
}

class FinancialClaimsInformationRequestDriftModel extends DataClass
    implements Insertable<FinancialClaimsInformationRequestDriftModel> {
  final int id;
  final String oin;
  final DateTime dateTimeRequested;
  final String status;
  final bool lock;
  const FinancialClaimsInformationRequestDriftModel(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.status,
      required this.lock});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['date_time_requested'] = Variable<DateTime>(dateTimeRequested);
    map['status'] = Variable<String>(status);
    map['lock'] = Variable<bool>(lock);
    return map;
  }

  FinancialClaimsInformationRequestTableCompanion toCompanion(
      bool nullToAbsent) {
    return FinancialClaimsInformationRequestTableCompanion(
      id: Value(id),
      oin: Value(oin),
      dateTimeRequested: Value(dateTimeRequested),
      status: Value(status),
      lock: Value(lock),
    );
  }

  factory FinancialClaimsInformationRequestDriftModel.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FinancialClaimsInformationRequestDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      dateTimeRequested:
          serializer.fromJson<DateTime>(json['dateTimeRequested']),
      status: serializer.fromJson<String>(json['status']),
      lock: serializer.fromJson<bool>(json['lock']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'dateTimeRequested': serializer.toJson<DateTime>(dateTimeRequested),
      'status': serializer.toJson<String>(status),
      'lock': serializer.toJson<bool>(lock),
    };
  }

  FinancialClaimsInformationRequestDriftModel copyWith(
          {int? id,
          String? oin,
          DateTime? dateTimeRequested,
          String? status,
          bool? lock}) =>
      FinancialClaimsInformationRequestDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
        status: status ?? this.status,
        lock: lock ?? this.lock,
      );
  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationRequestDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('status: $status, ')
          ..write('lock: $lock')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, dateTimeRequested, status, lock);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FinancialClaimsInformationRequestDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.dateTimeRequested == this.dateTimeRequested &&
          other.status == this.status &&
          other.lock == this.lock);
}

class FinancialClaimsInformationRequestTableCompanion
    extends UpdateCompanion<FinancialClaimsInformationRequestDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<DateTime> dateTimeRequested;
  final Value<String> status;
  final Value<bool> lock;
  const FinancialClaimsInformationRequestTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.dateTimeRequested = const Value.absent(),
    this.status = const Value.absent(),
    this.lock = const Value.absent(),
  });
  FinancialClaimsInformationRequestTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required DateTime dateTimeRequested,
    required String status,
    required bool lock,
  })  : oin = Value(oin),
        dateTimeRequested = Value(dateTimeRequested),
        status = Value(status),
        lock = Value(lock);
  static Insertable<FinancialClaimsInformationRequestDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<DateTime>? dateTimeRequested,
    Expression<String>? status,
    Expression<bool>? lock,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (dateTimeRequested != null) 'date_time_requested': dateTimeRequested,
      if (status != null) 'status': status,
      if (lock != null) 'lock': lock,
    });
  }

  FinancialClaimsInformationRequestTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<DateTime>? dateTimeRequested,
      Value<String>? status,
      Value<bool>? lock}) {
    return FinancialClaimsInformationRequestTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
      status: status ?? this.status,
      lock: lock ?? this.lock,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (dateTimeRequested.present) {
      map['date_time_requested'] = Variable<DateTime>(dateTimeRequested.value);
    }
    if (status.present) {
      map['status'] = Variable<String>(status.value);
    }
    if (lock.present) {
      map['lock'] = Variable<bool>(lock.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationRequestTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('status: $status, ')
          ..write('lock: $lock')
          ..write(')'))
        .toString();
  }
}

class $FinancialClaimsInformationConfigurationTableTable
    extends FinancialClaimsInformationConfigurationTable
    with
        TableInfo<$FinancialClaimsInformationConfigurationTableTable,
            FinancialClaimsInformationConfigurationDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FinancialClaimsInformationConfigurationTableTable(this.attachedDatabase,
      [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _documentMeta =
      const VerificationMeta('document');
  @override
  late final GeneratedColumn<String> document = GeneratedColumn<String>(
      'document', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _documentSignatureMeta =
      const VerificationMeta('documentSignature');
  @override
  late final GeneratedColumn<String> documentSignature =
      GeneratedColumn<String>('document_signature', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _certificateIdMeta =
      const VerificationMeta('certificateId');
  @override
  late final GeneratedColumn<int> certificateId = GeneratedColumn<int>(
      'certificate_id', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _envelopeMeta =
      const VerificationMeta('envelope');
  @override
  late final GeneratedColumn<String> envelope = GeneratedColumn<String>(
      'envelope', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _configurationRequestMeta =
      const VerificationMeta('configurationRequest');
  @override
  late final GeneratedColumn<String> configurationRequest =
      GeneratedColumn<String>('configuration_request', aliasedName, true,
          type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _configurationMeta =
      const VerificationMeta('configuration');
  @override
  late final GeneratedColumn<String> configuration = GeneratedColumn<String>(
      'configuration', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _expiredMeta =
      const VerificationMeta('expired');
  @override
  late final GeneratedColumn<bool> expired = GeneratedColumn<bool>(
      'expired', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("expired" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        oin,
        document,
        documentSignature,
        certificateId,
        envelope,
        configurationRequest,
        configuration,
        expired
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name =
      'financial_claims_information_configuration_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<FinancialClaimsInformationConfigurationDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('document')) {
      context.handle(_documentMeta,
          document.isAcceptableOrUnknown(data['document']!, _documentMeta));
    }
    if (data.containsKey('document_signature')) {
      context.handle(
          _documentSignatureMeta,
          documentSignature.isAcceptableOrUnknown(
              data['document_signature']!, _documentSignatureMeta));
    }
    if (data.containsKey('certificate_id')) {
      context.handle(
          _certificateIdMeta,
          certificateId.isAcceptableOrUnknown(
              data['certificate_id']!, _certificateIdMeta));
    }
    if (data.containsKey('envelope')) {
      context.handle(_envelopeMeta,
          envelope.isAcceptableOrUnknown(data['envelope']!, _envelopeMeta));
    }
    if (data.containsKey('configuration_request')) {
      context.handle(
          _configurationRequestMeta,
          configurationRequest.isAcceptableOrUnknown(
              data['configuration_request']!, _configurationRequestMeta));
    }
    if (data.containsKey('configuration')) {
      context.handle(
          _configurationMeta,
          configuration.isAcceptableOrUnknown(
              data['configuration']!, _configurationMeta));
    }
    if (data.containsKey('expired')) {
      context.handle(_expiredMeta,
          expired.isAcceptableOrUnknown(data['expired']!, _expiredMeta));
    } else if (isInserting) {
      context.missing(_expiredMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FinancialClaimsInformationConfigurationDriftModel map(
      Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FinancialClaimsInformationConfigurationDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      document: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}document']),
      documentSignature: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}document_signature']),
      certificateId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}certificate_id']),
      envelope: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}envelope']),
      configurationRequest: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}configuration_request']),
      configuration: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}configuration']),
      expired: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}expired'])!,
    );
  }

  @override
  $FinancialClaimsInformationConfigurationTableTable createAlias(String alias) {
    return $FinancialClaimsInformationConfigurationTableTable(
        attachedDatabase, alias);
  }
}

class FinancialClaimsInformationConfigurationDriftModel extends DataClass
    implements Insertable<FinancialClaimsInformationConfigurationDriftModel> {
  final int id;
  final String oin;
  final String? document;
  final String? documentSignature;
  final int? certificateId;
  final String? envelope;
  final String? configurationRequest;
  final String? configuration;
  final bool expired;
  const FinancialClaimsInformationConfigurationDriftModel(
      {required this.id,
      required this.oin,
      this.document,
      this.documentSignature,
      this.certificateId,
      this.envelope,
      this.configurationRequest,
      this.configuration,
      required this.expired});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    if (!nullToAbsent || document != null) {
      map['document'] = Variable<String>(document);
    }
    if (!nullToAbsent || documentSignature != null) {
      map['document_signature'] = Variable<String>(documentSignature);
    }
    if (!nullToAbsent || certificateId != null) {
      map['certificate_id'] = Variable<int>(certificateId);
    }
    if (!nullToAbsent || envelope != null) {
      map['envelope'] = Variable<String>(envelope);
    }
    if (!nullToAbsent || configurationRequest != null) {
      map['configuration_request'] = Variable<String>(configurationRequest);
    }
    if (!nullToAbsent || configuration != null) {
      map['configuration'] = Variable<String>(configuration);
    }
    map['expired'] = Variable<bool>(expired);
    return map;
  }

  FinancialClaimsInformationConfigurationTableCompanion toCompanion(
      bool nullToAbsent) {
    return FinancialClaimsInformationConfigurationTableCompanion(
      id: Value(id),
      oin: Value(oin),
      document: document == null && nullToAbsent
          ? const Value.absent()
          : Value(document),
      documentSignature: documentSignature == null && nullToAbsent
          ? const Value.absent()
          : Value(documentSignature),
      certificateId: certificateId == null && nullToAbsent
          ? const Value.absent()
          : Value(certificateId),
      envelope: envelope == null && nullToAbsent
          ? const Value.absent()
          : Value(envelope),
      configurationRequest: configurationRequest == null && nullToAbsent
          ? const Value.absent()
          : Value(configurationRequest),
      configuration: configuration == null && nullToAbsent
          ? const Value.absent()
          : Value(configuration),
      expired: Value(expired),
    );
  }

  factory FinancialClaimsInformationConfigurationDriftModel.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FinancialClaimsInformationConfigurationDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      document: serializer.fromJson<String?>(json['document']),
      documentSignature:
          serializer.fromJson<String?>(json['documentSignature']),
      certificateId: serializer.fromJson<int?>(json['certificateId']),
      envelope: serializer.fromJson<String?>(json['envelope']),
      configurationRequest:
          serializer.fromJson<String?>(json['configurationRequest']),
      configuration: serializer.fromJson<String?>(json['configuration']),
      expired: serializer.fromJson<bool>(json['expired']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'document': serializer.toJson<String?>(document),
      'documentSignature': serializer.toJson<String?>(documentSignature),
      'certificateId': serializer.toJson<int?>(certificateId),
      'envelope': serializer.toJson<String?>(envelope),
      'configurationRequest': serializer.toJson<String?>(configurationRequest),
      'configuration': serializer.toJson<String?>(configuration),
      'expired': serializer.toJson<bool>(expired),
    };
  }

  FinancialClaimsInformationConfigurationDriftModel copyWith(
          {int? id,
          String? oin,
          Value<String?> document = const Value.absent(),
          Value<String?> documentSignature = const Value.absent(),
          Value<int?> certificateId = const Value.absent(),
          Value<String?> envelope = const Value.absent(),
          Value<String?> configurationRequest = const Value.absent(),
          Value<String?> configuration = const Value.absent(),
          bool? expired}) =>
      FinancialClaimsInformationConfigurationDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        document: document.present ? document.value : this.document,
        documentSignature: documentSignature.present
            ? documentSignature.value
            : this.documentSignature,
        certificateId:
            certificateId.present ? certificateId.value : this.certificateId,
        envelope: envelope.present ? envelope.value : this.envelope,
        configurationRequest: configurationRequest.present
            ? configurationRequest.value
            : this.configurationRequest,
        configuration:
            configuration.present ? configuration.value : this.configuration,
        expired: expired ?? this.expired,
      );
  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationConfigurationDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('document: $document, ')
          ..write('documentSignature: $documentSignature, ')
          ..write('certificateId: $certificateId, ')
          ..write('envelope: $envelope, ')
          ..write('configurationRequest: $configurationRequest, ')
          ..write('configuration: $configuration, ')
          ..write('expired: $expired')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, document, documentSignature,
      certificateId, envelope, configurationRequest, configuration, expired);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FinancialClaimsInformationConfigurationDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.document == this.document &&
          other.documentSignature == this.documentSignature &&
          other.certificateId == this.certificateId &&
          other.envelope == this.envelope &&
          other.configurationRequest == this.configurationRequest &&
          other.configuration == this.configuration &&
          other.expired == this.expired);
}

class FinancialClaimsInformationConfigurationTableCompanion
    extends UpdateCompanion<FinancialClaimsInformationConfigurationDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<String?> document;
  final Value<String?> documentSignature;
  final Value<int?> certificateId;
  final Value<String?> envelope;
  final Value<String?> configurationRequest;
  final Value<String?> configuration;
  final Value<bool> expired;
  const FinancialClaimsInformationConfigurationTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.document = const Value.absent(),
    this.documentSignature = const Value.absent(),
    this.certificateId = const Value.absent(),
    this.envelope = const Value.absent(),
    this.configurationRequest = const Value.absent(),
    this.configuration = const Value.absent(),
    this.expired = const Value.absent(),
  });
  FinancialClaimsInformationConfigurationTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    this.document = const Value.absent(),
    this.documentSignature = const Value.absent(),
    this.certificateId = const Value.absent(),
    this.envelope = const Value.absent(),
    this.configurationRequest = const Value.absent(),
    this.configuration = const Value.absent(),
    required bool expired,
  })  : oin = Value(oin),
        expired = Value(expired);
  static Insertable<FinancialClaimsInformationConfigurationDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<String>? document,
    Expression<String>? documentSignature,
    Expression<int>? certificateId,
    Expression<String>? envelope,
    Expression<String>? configurationRequest,
    Expression<String>? configuration,
    Expression<bool>? expired,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (document != null) 'document': document,
      if (documentSignature != null) 'document_signature': documentSignature,
      if (certificateId != null) 'certificate_id': certificateId,
      if (envelope != null) 'envelope': envelope,
      if (configurationRequest != null)
        'configuration_request': configurationRequest,
      if (configuration != null) 'configuration': configuration,
      if (expired != null) 'expired': expired,
    });
  }

  FinancialClaimsInformationConfigurationTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<String?>? document,
      Value<String?>? documentSignature,
      Value<int?>? certificateId,
      Value<String?>? envelope,
      Value<String?>? configurationRequest,
      Value<String?>? configuration,
      Value<bool>? expired}) {
    return FinancialClaimsInformationConfigurationTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      document: document ?? this.document,
      documentSignature: documentSignature ?? this.documentSignature,
      certificateId: certificateId ?? this.certificateId,
      envelope: envelope ?? this.envelope,
      configurationRequest: configurationRequest ?? this.configurationRequest,
      configuration: configuration ?? this.configuration,
      expired: expired ?? this.expired,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (document.present) {
      map['document'] = Variable<String>(document.value);
    }
    if (documentSignature.present) {
      map['document_signature'] = Variable<String>(documentSignature.value);
    }
    if (certificateId.present) {
      map['certificate_id'] = Variable<int>(certificateId.value);
    }
    if (envelope.present) {
      map['envelope'] = Variable<String>(envelope.value);
    }
    if (configurationRequest.present) {
      map['configuration_request'] =
          Variable<String>(configurationRequest.value);
    }
    if (configuration.present) {
      map['configuration'] = Variable<String>(configuration.value);
    }
    if (expired.present) {
      map['expired'] = Variable<bool>(expired.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer(
            'FinancialClaimsInformationConfigurationTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('document: $document, ')
          ..write('documentSignature: $documentSignature, ')
          ..write('certificateId: $certificateId, ')
          ..write('envelope: $envelope, ')
          ..write('configurationRequest: $configurationRequest, ')
          ..write('configuration: $configuration, ')
          ..write('expired: $expired')
          ..write(')'))
        .toString();
  }
}

class $FinancialClaimsInformationInboxTableTable
    extends FinancialClaimsInformationInboxTable
    with
        TableInfo<$FinancialClaimsInformationInboxTableTable,
            FinancialClaimsInformationInboxDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FinancialClaimsInformationInboxTableTable(this.attachedDatabase,
      [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeRequestedMeta =
      const VerificationMeta('dateTimeRequested');
  @override
  late final GeneratedColumn<DateTime> dateTimeRequested =
      GeneratedColumn<DateTime>('date_time_requested', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeReceivedMeta =
      const VerificationMeta('dateTimeReceived');
  @override
  late final GeneratedColumn<DateTime> dateTimeReceived =
      GeneratedColumn<DateTime>('date_time_received', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta
      _encryptedFinancialClaimsInformationDocumentMeta =
      const VerificationMeta('encryptedFinancialClaimsInformationDocument');
  @override
  late final GeneratedColumn<String>
      encryptedFinancialClaimsInformationDocument = GeneratedColumn<String>(
          'encrypted_financial_claims_information_document', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _copiedToStorageMeta =
      const VerificationMeta('copiedToStorage');
  @override
  late final GeneratedColumn<bool> copiedToStorage = GeneratedColumn<bool>(
      'copied_to_storage', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("copied_to_storage" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        oin,
        dateTimeRequested,
        dateTimeReceived,
        encryptedFinancialClaimsInformationDocument,
        copiedToStorage
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'financial_claims_information_inbox_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<FinancialClaimsInformationInboxDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('date_time_requested')) {
      context.handle(
          _dateTimeRequestedMeta,
          dateTimeRequested.isAcceptableOrUnknown(
              data['date_time_requested']!, _dateTimeRequestedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeRequestedMeta);
    }
    if (data.containsKey('date_time_received')) {
      context.handle(
          _dateTimeReceivedMeta,
          dateTimeReceived.isAcceptableOrUnknown(
              data['date_time_received']!, _dateTimeReceivedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeReceivedMeta);
    }
    if (data.containsKey('encrypted_financial_claims_information_document')) {
      context.handle(
          _encryptedFinancialClaimsInformationDocumentMeta,
          encryptedFinancialClaimsInformationDocument.isAcceptableOrUnknown(
              data['encrypted_financial_claims_information_document']!,
              _encryptedFinancialClaimsInformationDocumentMeta));
    } else if (isInserting) {
      context.missing(_encryptedFinancialClaimsInformationDocumentMeta);
    }
    if (data.containsKey('copied_to_storage')) {
      context.handle(
          _copiedToStorageMeta,
          copiedToStorage.isAcceptableOrUnknown(
              data['copied_to_storage']!, _copiedToStorageMeta));
    } else if (isInserting) {
      context.missing(_copiedToStorageMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FinancialClaimsInformationInboxDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FinancialClaimsInformationInboxDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      dateTimeRequested: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime,
          data['${effectivePrefix}date_time_requested'])!,
      dateTimeReceived: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime, data['${effectivePrefix}date_time_received'])!,
      encryptedFinancialClaimsInformationDocument: attachedDatabase.typeMapping
          .read(
              DriftSqlType.string,
              data[
                  '${effectivePrefix}encrypted_financial_claims_information_document'])!,
      copiedToStorage: attachedDatabase.typeMapping.read(
          DriftSqlType.bool, data['${effectivePrefix}copied_to_storage'])!,
    );
  }

  @override
  $FinancialClaimsInformationInboxTableTable createAlias(String alias) {
    return $FinancialClaimsInformationInboxTableTable(attachedDatabase, alias);
  }
}

class FinancialClaimsInformationInboxDriftModel extends DataClass
    implements Insertable<FinancialClaimsInformationInboxDriftModel> {
  final int id;
  final String oin;
  final DateTime dateTimeRequested;
  final DateTime dateTimeReceived;
  final String encryptedFinancialClaimsInformationDocument;
  final bool copiedToStorage;
  const FinancialClaimsInformationInboxDriftModel(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.dateTimeReceived,
      required this.encryptedFinancialClaimsInformationDocument,
      required this.copiedToStorage});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['date_time_requested'] = Variable<DateTime>(dateTimeRequested);
    map['date_time_received'] = Variable<DateTime>(dateTimeReceived);
    map['encrypted_financial_claims_information_document'] =
        Variable<String>(encryptedFinancialClaimsInformationDocument);
    map['copied_to_storage'] = Variable<bool>(copiedToStorage);
    return map;
  }

  FinancialClaimsInformationInboxTableCompanion toCompanion(bool nullToAbsent) {
    return FinancialClaimsInformationInboxTableCompanion(
      id: Value(id),
      oin: Value(oin),
      dateTimeRequested: Value(dateTimeRequested),
      dateTimeReceived: Value(dateTimeReceived),
      encryptedFinancialClaimsInformationDocument:
          Value(encryptedFinancialClaimsInformationDocument),
      copiedToStorage: Value(copiedToStorage),
    );
  }

  factory FinancialClaimsInformationInboxDriftModel.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FinancialClaimsInformationInboxDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      dateTimeRequested:
          serializer.fromJson<DateTime>(json['dateTimeRequested']),
      dateTimeReceived: serializer.fromJson<DateTime>(json['dateTimeReceived']),
      encryptedFinancialClaimsInformationDocument: serializer.fromJson<String>(
          json['encryptedFinancialClaimsInformationDocument']),
      copiedToStorage: serializer.fromJson<bool>(json['copiedToStorage']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'dateTimeRequested': serializer.toJson<DateTime>(dateTimeRequested),
      'dateTimeReceived': serializer.toJson<DateTime>(dateTimeReceived),
      'encryptedFinancialClaimsInformationDocument': serializer
          .toJson<String>(encryptedFinancialClaimsInformationDocument),
      'copiedToStorage': serializer.toJson<bool>(copiedToStorage),
    };
  }

  FinancialClaimsInformationInboxDriftModel copyWith(
          {int? id,
          String? oin,
          DateTime? dateTimeRequested,
          DateTime? dateTimeReceived,
          String? encryptedFinancialClaimsInformationDocument,
          bool? copiedToStorage}) =>
      FinancialClaimsInformationInboxDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
        dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
        encryptedFinancialClaimsInformationDocument:
            encryptedFinancialClaimsInformationDocument ??
                this.encryptedFinancialClaimsInformationDocument,
        copiedToStorage: copiedToStorage ?? this.copiedToStorage,
      );
  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationInboxDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'encryptedFinancialClaimsInformationDocument: $encryptedFinancialClaimsInformationDocument, ')
          ..write('copiedToStorage: $copiedToStorage')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, dateTimeRequested, dateTimeReceived,
      encryptedFinancialClaimsInformationDocument, copiedToStorage);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FinancialClaimsInformationInboxDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.dateTimeRequested == this.dateTimeRequested &&
          other.dateTimeReceived == this.dateTimeReceived &&
          other.encryptedFinancialClaimsInformationDocument ==
              this.encryptedFinancialClaimsInformationDocument &&
          other.copiedToStorage == this.copiedToStorage);
}

class FinancialClaimsInformationInboxTableCompanion
    extends UpdateCompanion<FinancialClaimsInformationInboxDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<DateTime> dateTimeRequested;
  final Value<DateTime> dateTimeReceived;
  final Value<String> encryptedFinancialClaimsInformationDocument;
  final Value<bool> copiedToStorage;
  const FinancialClaimsInformationInboxTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.dateTimeRequested = const Value.absent(),
    this.dateTimeReceived = const Value.absent(),
    this.encryptedFinancialClaimsInformationDocument = const Value.absent(),
    this.copiedToStorage = const Value.absent(),
  });
  FinancialClaimsInformationInboxTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String encryptedFinancialClaimsInformationDocument,
    required bool copiedToStorage,
  })  : oin = Value(oin),
        dateTimeRequested = Value(dateTimeRequested),
        dateTimeReceived = Value(dateTimeReceived),
        encryptedFinancialClaimsInformationDocument =
            Value(encryptedFinancialClaimsInformationDocument),
        copiedToStorage = Value(copiedToStorage);
  static Insertable<FinancialClaimsInformationInboxDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<DateTime>? dateTimeRequested,
    Expression<DateTime>? dateTimeReceived,
    Expression<String>? encryptedFinancialClaimsInformationDocument,
    Expression<bool>? copiedToStorage,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (dateTimeRequested != null) 'date_time_requested': dateTimeRequested,
      if (dateTimeReceived != null) 'date_time_received': dateTimeReceived,
      if (encryptedFinancialClaimsInformationDocument != null)
        'encrypted_financial_claims_information_document':
            encryptedFinancialClaimsInformationDocument,
      if (copiedToStorage != null) 'copied_to_storage': copiedToStorage,
    });
  }

  FinancialClaimsInformationInboxTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<DateTime>? dateTimeRequested,
      Value<DateTime>? dateTimeReceived,
      Value<String>? encryptedFinancialClaimsInformationDocument,
      Value<bool>? copiedToStorage}) {
    return FinancialClaimsInformationInboxTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
      dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
      encryptedFinancialClaimsInformationDocument:
          encryptedFinancialClaimsInformationDocument ??
              this.encryptedFinancialClaimsInformationDocument,
      copiedToStorage: copiedToStorage ?? this.copiedToStorage,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (dateTimeRequested.present) {
      map['date_time_requested'] = Variable<DateTime>(dateTimeRequested.value);
    }
    if (dateTimeReceived.present) {
      map['date_time_received'] = Variable<DateTime>(dateTimeReceived.value);
    }
    if (encryptedFinancialClaimsInformationDocument.present) {
      map['encrypted_financial_claims_information_document'] =
          Variable<String>(encryptedFinancialClaimsInformationDocument.value);
    }
    if (copiedToStorage.present) {
      map['copied_to_storage'] = Variable<bool>(copiedToStorage.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationInboxTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'encryptedFinancialClaimsInformationDocument: $encryptedFinancialClaimsInformationDocument, ')
          ..write('copiedToStorage: $copiedToStorage')
          ..write(')'))
        .toString();
  }
}

class $RegistrationTableTable extends RegistrationTable
    with TableInfo<$RegistrationTableTable, RegistrationDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $RegistrationTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _dateTimeStartedMeta =
      const VerificationMeta('dateTimeStarted');
  @override
  late final GeneratedColumn<DateTime> dateTimeStarted =
      GeneratedColumn<DateTime>('date_time_started', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _appPublicKeyMeta =
      const VerificationMeta('appPublicKey');
  @override
  late final GeneratedColumn<String> appPublicKey = GeneratedColumn<String>(
      'app_public_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _appManagerOinMeta =
      const VerificationMeta('appManagerOin');
  @override
  late final GeneratedColumn<String> appManagerOin = GeneratedColumn<String>(
      'app_manager_oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _appManagerPublicKeyMeta =
      const VerificationMeta('appManagerPublicKey');
  @override
  late final GeneratedColumn<String> appManagerPublicKey =
      GeneratedColumn<String>('app_manager_public_key', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _registrationTokenMeta =
      const VerificationMeta('registrationToken');
  @override
  late final GeneratedColumn<String> registrationToken =
      GeneratedColumn<String>('registration_token', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeCompletedMeta =
      const VerificationMeta('dateTimeCompleted');
  @override
  late final GeneratedColumn<DateTime> dateTimeCompleted =
      GeneratedColumn<DateTime>('date_time_completed', aliasedName, true,
          type: DriftSqlType.dateTime, requiredDuringInsert: false);
  static const VerificationMeta _givenNameMeta =
      const VerificationMeta('givenName');
  @override
  late final GeneratedColumn<String> givenName = GeneratedColumn<String>(
      'given_name', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        dateTimeStarted,
        appPublicKey,
        appManagerOin,
        appManagerPublicKey,
        registrationToken,
        dateTimeCompleted,
        givenName
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'registration_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<RegistrationDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('date_time_started')) {
      context.handle(
          _dateTimeStartedMeta,
          dateTimeStarted.isAcceptableOrUnknown(
              data['date_time_started']!, _dateTimeStartedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeStartedMeta);
    }
    if (data.containsKey('app_public_key')) {
      context.handle(
          _appPublicKeyMeta,
          appPublicKey.isAcceptableOrUnknown(
              data['app_public_key']!, _appPublicKeyMeta));
    } else if (isInserting) {
      context.missing(_appPublicKeyMeta);
    }
    if (data.containsKey('app_manager_oin')) {
      context.handle(
          _appManagerOinMeta,
          appManagerOin.isAcceptableOrUnknown(
              data['app_manager_oin']!, _appManagerOinMeta));
    } else if (isInserting) {
      context.missing(_appManagerOinMeta);
    }
    if (data.containsKey('app_manager_public_key')) {
      context.handle(
          _appManagerPublicKeyMeta,
          appManagerPublicKey.isAcceptableOrUnknown(
              data['app_manager_public_key']!, _appManagerPublicKeyMeta));
    } else if (isInserting) {
      context.missing(_appManagerPublicKeyMeta);
    }
    if (data.containsKey('registration_token')) {
      context.handle(
          _registrationTokenMeta,
          registrationToken.isAcceptableOrUnknown(
              data['registration_token']!, _registrationTokenMeta));
    } else if (isInserting) {
      context.missing(_registrationTokenMeta);
    }
    if (data.containsKey('date_time_completed')) {
      context.handle(
          _dateTimeCompletedMeta,
          dateTimeCompleted.isAcceptableOrUnknown(
              data['date_time_completed']!, _dateTimeCompletedMeta));
    }
    if (data.containsKey('given_name')) {
      context.handle(_givenNameMeta,
          givenName.isAcceptableOrUnknown(data['given_name']!, _givenNameMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  RegistrationDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return RegistrationDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      dateTimeStarted: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime, data['${effectivePrefix}date_time_started'])!,
      appPublicKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}app_public_key'])!,
      appManagerOin: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}app_manager_oin'])!,
      appManagerPublicKey: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}app_manager_public_key'])!,
      registrationToken: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}registration_token'])!,
      dateTimeCompleted: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime, data['${effectivePrefix}date_time_completed']),
      givenName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}given_name']),
    );
  }

  @override
  $RegistrationTableTable createAlias(String alias) {
    return $RegistrationTableTable(attachedDatabase, alias);
  }
}

class RegistrationDriftModel extends DataClass
    implements Insertable<RegistrationDriftModel> {
  final int id;
  final DateTime dateTimeStarted;
  final String appPublicKey;
  final String appManagerOin;
  final String appManagerPublicKey;
  final String registrationToken;
  final DateTime? dateTimeCompleted;
  final String? givenName;
  const RegistrationDriftModel(
      {required this.id,
      required this.dateTimeStarted,
      required this.appPublicKey,
      required this.appManagerOin,
      required this.appManagerPublicKey,
      required this.registrationToken,
      this.dateTimeCompleted,
      this.givenName});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['date_time_started'] = Variable<DateTime>(dateTimeStarted);
    map['app_public_key'] = Variable<String>(appPublicKey);
    map['app_manager_oin'] = Variable<String>(appManagerOin);
    map['app_manager_public_key'] = Variable<String>(appManagerPublicKey);
    map['registration_token'] = Variable<String>(registrationToken);
    if (!nullToAbsent || dateTimeCompleted != null) {
      map['date_time_completed'] = Variable<DateTime>(dateTimeCompleted);
    }
    if (!nullToAbsent || givenName != null) {
      map['given_name'] = Variable<String>(givenName);
    }
    return map;
  }

  RegistrationTableCompanion toCompanion(bool nullToAbsent) {
    return RegistrationTableCompanion(
      id: Value(id),
      dateTimeStarted: Value(dateTimeStarted),
      appPublicKey: Value(appPublicKey),
      appManagerOin: Value(appManagerOin),
      appManagerPublicKey: Value(appManagerPublicKey),
      registrationToken: Value(registrationToken),
      dateTimeCompleted: dateTimeCompleted == null && nullToAbsent
          ? const Value.absent()
          : Value(dateTimeCompleted),
      givenName: givenName == null && nullToAbsent
          ? const Value.absent()
          : Value(givenName),
    );
  }

  factory RegistrationDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return RegistrationDriftModel(
      id: serializer.fromJson<int>(json['id']),
      dateTimeStarted: serializer.fromJson<DateTime>(json['dateTimeStarted']),
      appPublicKey: serializer.fromJson<String>(json['appPublicKey']),
      appManagerOin: serializer.fromJson<String>(json['appManagerOin']),
      appManagerPublicKey:
          serializer.fromJson<String>(json['appManagerPublicKey']),
      registrationToken: serializer.fromJson<String>(json['registrationToken']),
      dateTimeCompleted:
          serializer.fromJson<DateTime?>(json['dateTimeCompleted']),
      givenName: serializer.fromJson<String?>(json['givenName']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'dateTimeStarted': serializer.toJson<DateTime>(dateTimeStarted),
      'appPublicKey': serializer.toJson<String>(appPublicKey),
      'appManagerOin': serializer.toJson<String>(appManagerOin),
      'appManagerPublicKey': serializer.toJson<String>(appManagerPublicKey),
      'registrationToken': serializer.toJson<String>(registrationToken),
      'dateTimeCompleted': serializer.toJson<DateTime?>(dateTimeCompleted),
      'givenName': serializer.toJson<String?>(givenName),
    };
  }

  RegistrationDriftModel copyWith(
          {int? id,
          DateTime? dateTimeStarted,
          String? appPublicKey,
          String? appManagerOin,
          String? appManagerPublicKey,
          String? registrationToken,
          Value<DateTime?> dateTimeCompleted = const Value.absent(),
          Value<String?> givenName = const Value.absent()}) =>
      RegistrationDriftModel(
        id: id ?? this.id,
        dateTimeStarted: dateTimeStarted ?? this.dateTimeStarted,
        appPublicKey: appPublicKey ?? this.appPublicKey,
        appManagerOin: appManagerOin ?? this.appManagerOin,
        appManagerPublicKey: appManagerPublicKey ?? this.appManagerPublicKey,
        registrationToken: registrationToken ?? this.registrationToken,
        dateTimeCompleted: dateTimeCompleted.present
            ? dateTimeCompleted.value
            : this.dateTimeCompleted,
        givenName: givenName.present ? givenName.value : this.givenName,
      );
  @override
  String toString() {
    return (StringBuffer('RegistrationDriftModel(')
          ..write('id: $id, ')
          ..write('dateTimeStarted: $dateTimeStarted, ')
          ..write('appPublicKey: $appPublicKey, ')
          ..write('appManagerOin: $appManagerOin, ')
          ..write('appManagerPublicKey: $appManagerPublicKey, ')
          ..write('registrationToken: $registrationToken, ')
          ..write('dateTimeCompleted: $dateTimeCompleted, ')
          ..write('givenName: $givenName')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      dateTimeStarted,
      appPublicKey,
      appManagerOin,
      appManagerPublicKey,
      registrationToken,
      dateTimeCompleted,
      givenName);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is RegistrationDriftModel &&
          other.id == this.id &&
          other.dateTimeStarted == this.dateTimeStarted &&
          other.appPublicKey == this.appPublicKey &&
          other.appManagerOin == this.appManagerOin &&
          other.appManagerPublicKey == this.appManagerPublicKey &&
          other.registrationToken == this.registrationToken &&
          other.dateTimeCompleted == this.dateTimeCompleted &&
          other.givenName == this.givenName);
}

class RegistrationTableCompanion
    extends UpdateCompanion<RegistrationDriftModel> {
  final Value<int> id;
  final Value<DateTime> dateTimeStarted;
  final Value<String> appPublicKey;
  final Value<String> appManagerOin;
  final Value<String> appManagerPublicKey;
  final Value<String> registrationToken;
  final Value<DateTime?> dateTimeCompleted;
  final Value<String?> givenName;
  const RegistrationTableCompanion({
    this.id = const Value.absent(),
    this.dateTimeStarted = const Value.absent(),
    this.appPublicKey = const Value.absent(),
    this.appManagerOin = const Value.absent(),
    this.appManagerPublicKey = const Value.absent(),
    this.registrationToken = const Value.absent(),
    this.dateTimeCompleted = const Value.absent(),
    this.givenName = const Value.absent(),
  });
  RegistrationTableCompanion.insert({
    this.id = const Value.absent(),
    required DateTime dateTimeStarted,
    required String appPublicKey,
    required String appManagerOin,
    required String appManagerPublicKey,
    required String registrationToken,
    this.dateTimeCompleted = const Value.absent(),
    this.givenName = const Value.absent(),
  })  : dateTimeStarted = Value(dateTimeStarted),
        appPublicKey = Value(appPublicKey),
        appManagerOin = Value(appManagerOin),
        appManagerPublicKey = Value(appManagerPublicKey),
        registrationToken = Value(registrationToken);
  static Insertable<RegistrationDriftModel> custom({
    Expression<int>? id,
    Expression<DateTime>? dateTimeStarted,
    Expression<String>? appPublicKey,
    Expression<String>? appManagerOin,
    Expression<String>? appManagerPublicKey,
    Expression<String>? registrationToken,
    Expression<DateTime>? dateTimeCompleted,
    Expression<String>? givenName,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (dateTimeStarted != null) 'date_time_started': dateTimeStarted,
      if (appPublicKey != null) 'app_public_key': appPublicKey,
      if (appManagerOin != null) 'app_manager_oin': appManagerOin,
      if (appManagerPublicKey != null)
        'app_manager_public_key': appManagerPublicKey,
      if (registrationToken != null) 'registration_token': registrationToken,
      if (dateTimeCompleted != null) 'date_time_completed': dateTimeCompleted,
      if (givenName != null) 'given_name': givenName,
    });
  }

  RegistrationTableCompanion copyWith(
      {Value<int>? id,
      Value<DateTime>? dateTimeStarted,
      Value<String>? appPublicKey,
      Value<String>? appManagerOin,
      Value<String>? appManagerPublicKey,
      Value<String>? registrationToken,
      Value<DateTime?>? dateTimeCompleted,
      Value<String?>? givenName}) {
    return RegistrationTableCompanion(
      id: id ?? this.id,
      dateTimeStarted: dateTimeStarted ?? this.dateTimeStarted,
      appPublicKey: appPublicKey ?? this.appPublicKey,
      appManagerOin: appManagerOin ?? this.appManagerOin,
      appManagerPublicKey: appManagerPublicKey ?? this.appManagerPublicKey,
      registrationToken: registrationToken ?? this.registrationToken,
      dateTimeCompleted: dateTimeCompleted ?? this.dateTimeCompleted,
      givenName: givenName ?? this.givenName,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (dateTimeStarted.present) {
      map['date_time_started'] = Variable<DateTime>(dateTimeStarted.value);
    }
    if (appPublicKey.present) {
      map['app_public_key'] = Variable<String>(appPublicKey.value);
    }
    if (appManagerOin.present) {
      map['app_manager_oin'] = Variable<String>(appManagerOin.value);
    }
    if (appManagerPublicKey.present) {
      map['app_manager_public_key'] =
          Variable<String>(appManagerPublicKey.value);
    }
    if (registrationToken.present) {
      map['registration_token'] = Variable<String>(registrationToken.value);
    }
    if (dateTimeCompleted.present) {
      map['date_time_completed'] = Variable<DateTime>(dateTimeCompleted.value);
    }
    if (givenName.present) {
      map['given_name'] = Variable<String>(givenName.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('RegistrationTableCompanion(')
          ..write('id: $id, ')
          ..write('dateTimeStarted: $dateTimeStarted, ')
          ..write('appPublicKey: $appPublicKey, ')
          ..write('appManagerOin: $appManagerOin, ')
          ..write('appManagerPublicKey: $appManagerPublicKey, ')
          ..write('registrationToken: $registrationToken, ')
          ..write('dateTimeCompleted: $dateTimeCompleted, ')
          ..write('givenName: $givenName')
          ..write(')'))
        .toString();
  }
}

class $CertificateTableTable extends CertificateTable
    with TableInfo<$CertificateTableTable, CertificateDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $CertificateTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String> type = GeneratedColumn<String>(
      'type', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _valueMeta = const VerificationMeta('value');
  @override
  late final GeneratedColumn<Uint8List> value = GeneratedColumn<Uint8List>(
      'value', aliasedName, false,
      type: DriftSqlType.blob, requiredDuringInsert: true);
  static const VerificationMeta _givenNameMeta =
      const VerificationMeta('givenName');
  @override
  late final GeneratedColumn<String> givenName = GeneratedColumn<String>(
      'given_name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _expiresAtMeta =
      const VerificationMeta('expiresAt');
  @override
  late final GeneratedColumn<DateTime> expiresAt = GeneratedColumn<DateTime>(
      'expires_at', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _deemedExpiredBySourceOrganizationMeta =
      const VerificationMeta('deemedExpiredBySourceOrganization');
  @override
  late final GeneratedColumn<bool> deemedExpiredBySourceOrganization =
      GeneratedColumn<bool>(
          'deemed_expired_by_source_organization', aliasedName, false,
          type: DriftSqlType.bool,
          requiredDuringInsert: true,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'CHECK ("deemed_expired_by_source_organization" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        type,
        value,
        givenName,
        expiresAt,
        deemedExpiredBySourceOrganization
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'certificate_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<CertificateDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    } else if (isInserting) {
      context.missing(_typeMeta);
    }
    if (data.containsKey('value')) {
      context.handle(
          _valueMeta, value.isAcceptableOrUnknown(data['value']!, _valueMeta));
    } else if (isInserting) {
      context.missing(_valueMeta);
    }
    if (data.containsKey('given_name')) {
      context.handle(_givenNameMeta,
          givenName.isAcceptableOrUnknown(data['given_name']!, _givenNameMeta));
    } else if (isInserting) {
      context.missing(_givenNameMeta);
    }
    if (data.containsKey('expires_at')) {
      context.handle(_expiresAtMeta,
          expiresAt.isAcceptableOrUnknown(data['expires_at']!, _expiresAtMeta));
    } else if (isInserting) {
      context.missing(_expiresAtMeta);
    }
    if (data.containsKey('deemed_expired_by_source_organization')) {
      context.handle(
          _deemedExpiredBySourceOrganizationMeta,
          deemedExpiredBySourceOrganization.isAcceptableOrUnknown(
              data['deemed_expired_by_source_organization']!,
              _deemedExpiredBySourceOrganizationMeta));
    } else if (isInserting) {
      context.missing(_deemedExpiredBySourceOrganizationMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  CertificateDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return CertificateDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      type: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}type'])!,
      value: attachedDatabase.typeMapping
          .read(DriftSqlType.blob, data['${effectivePrefix}value'])!,
      givenName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}given_name'])!,
      expiresAt: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}expires_at'])!,
      deemedExpiredBySourceOrganization: attachedDatabase.typeMapping.read(
          DriftSqlType.bool,
          data['${effectivePrefix}deemed_expired_by_source_organization'])!,
    );
  }

  @override
  $CertificateTableTable createAlias(String alias) {
    return $CertificateTableTable(attachedDatabase, alias);
  }
}

class CertificateDriftModel extends DataClass
    implements Insertable<CertificateDriftModel> {
  final int id;
  final String type;
  final Uint8List value;
  final String givenName;
  final DateTime expiresAt;
  final bool deemedExpiredBySourceOrganization;
  const CertificateDriftModel(
      {required this.id,
      required this.type,
      required this.value,
      required this.givenName,
      required this.expiresAt,
      required this.deemedExpiredBySourceOrganization});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['type'] = Variable<String>(type);
    map['value'] = Variable<Uint8List>(value);
    map['given_name'] = Variable<String>(givenName);
    map['expires_at'] = Variable<DateTime>(expiresAt);
    map['deemed_expired_by_source_organization'] =
        Variable<bool>(deemedExpiredBySourceOrganization);
    return map;
  }

  CertificateTableCompanion toCompanion(bool nullToAbsent) {
    return CertificateTableCompanion(
      id: Value(id),
      type: Value(type),
      value: Value(value),
      givenName: Value(givenName),
      expiresAt: Value(expiresAt),
      deemedExpiredBySourceOrganization:
          Value(deemedExpiredBySourceOrganization),
    );
  }

  factory CertificateDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CertificateDriftModel(
      id: serializer.fromJson<int>(json['id']),
      type: serializer.fromJson<String>(json['type']),
      value: serializer.fromJson<Uint8List>(json['value']),
      givenName: serializer.fromJson<String>(json['givenName']),
      expiresAt: serializer.fromJson<DateTime>(json['expiresAt']),
      deemedExpiredBySourceOrganization:
          serializer.fromJson<bool>(json['deemedExpiredBySourceOrganization']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'type': serializer.toJson<String>(type),
      'value': serializer.toJson<Uint8List>(value),
      'givenName': serializer.toJson<String>(givenName),
      'expiresAt': serializer.toJson<DateTime>(expiresAt),
      'deemedExpiredBySourceOrganization':
          serializer.toJson<bool>(deemedExpiredBySourceOrganization),
    };
  }

  CertificateDriftModel copyWith(
          {int? id,
          String? type,
          Uint8List? value,
          String? givenName,
          DateTime? expiresAt,
          bool? deemedExpiredBySourceOrganization}) =>
      CertificateDriftModel(
        id: id ?? this.id,
        type: type ?? this.type,
        value: value ?? this.value,
        givenName: givenName ?? this.givenName,
        expiresAt: expiresAt ?? this.expiresAt,
        deemedExpiredBySourceOrganization: deemedExpiredBySourceOrganization ??
            this.deemedExpiredBySourceOrganization,
      );
  @override
  String toString() {
    return (StringBuffer('CertificateDriftModel(')
          ..write('id: $id, ')
          ..write('type: $type, ')
          ..write('value: $value, ')
          ..write('givenName: $givenName, ')
          ..write('expiresAt: $expiresAt, ')
          ..write(
              'deemedExpiredBySourceOrganization: $deemedExpiredBySourceOrganization')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, type, $driftBlobEquality.hash(value),
      givenName, expiresAt, deemedExpiredBySourceOrganization);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CertificateDriftModel &&
          other.id == this.id &&
          other.type == this.type &&
          $driftBlobEquality.equals(other.value, this.value) &&
          other.givenName == this.givenName &&
          other.expiresAt == this.expiresAt &&
          other.deemedExpiredBySourceOrganization ==
              this.deemedExpiredBySourceOrganization);
}

class CertificateTableCompanion extends UpdateCompanion<CertificateDriftModel> {
  final Value<int> id;
  final Value<String> type;
  final Value<Uint8List> value;
  final Value<String> givenName;
  final Value<DateTime> expiresAt;
  final Value<bool> deemedExpiredBySourceOrganization;
  const CertificateTableCompanion({
    this.id = const Value.absent(),
    this.type = const Value.absent(),
    this.value = const Value.absent(),
    this.givenName = const Value.absent(),
    this.expiresAt = const Value.absent(),
    this.deemedExpiredBySourceOrganization = const Value.absent(),
  });
  CertificateTableCompanion.insert({
    this.id = const Value.absent(),
    required String type,
    required Uint8List value,
    required String givenName,
    required DateTime expiresAt,
    required bool deemedExpiredBySourceOrganization,
  })  : type = Value(type),
        value = Value(value),
        givenName = Value(givenName),
        expiresAt = Value(expiresAt),
        deemedExpiredBySourceOrganization =
            Value(deemedExpiredBySourceOrganization);
  static Insertable<CertificateDriftModel> custom({
    Expression<int>? id,
    Expression<String>? type,
    Expression<Uint8List>? value,
    Expression<String>? givenName,
    Expression<DateTime>? expiresAt,
    Expression<bool>? deemedExpiredBySourceOrganization,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (type != null) 'type': type,
      if (value != null) 'value': value,
      if (givenName != null) 'given_name': givenName,
      if (expiresAt != null) 'expires_at': expiresAt,
      if (deemedExpiredBySourceOrganization != null)
        'deemed_expired_by_source_organization':
            deemedExpiredBySourceOrganization,
    });
  }

  CertificateTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? type,
      Value<Uint8List>? value,
      Value<String>? givenName,
      Value<DateTime>? expiresAt,
      Value<bool>? deemedExpiredBySourceOrganization}) {
    return CertificateTableCompanion(
      id: id ?? this.id,
      type: type ?? this.type,
      value: value ?? this.value,
      givenName: givenName ?? this.givenName,
      expiresAt: expiresAt ?? this.expiresAt,
      deemedExpiredBySourceOrganization: deemedExpiredBySourceOrganization ??
          this.deemedExpiredBySourceOrganization,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (value.present) {
      map['value'] = Variable<Uint8List>(value.value);
    }
    if (givenName.present) {
      map['given_name'] = Variable<String>(givenName.value);
    }
    if (expiresAt.present) {
      map['expires_at'] = Variable<DateTime>(expiresAt.value);
    }
    if (deemedExpiredBySourceOrganization.present) {
      map['deemed_expired_by_source_organization'] =
          Variable<bool>(deemedExpiredBySourceOrganization.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CertificateTableCompanion(')
          ..write('id: $id, ')
          ..write('type: $type, ')
          ..write('value: $value, ')
          ..write('givenName: $givenName, ')
          ..write('expiresAt: $expiresAt, ')
          ..write(
              'deemedExpiredBySourceOrganization: $deemedExpiredBySourceOrganization')
          ..write(')'))
        .toString();
  }
}

class $OrganizationSelectionTableTable extends OrganizationSelectionTable
    with
        TableInfo<$OrganizationSelectionTableTable,
            OrganizationSelectionDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $OrganizationSelectionTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _selectedMeta =
      const VerificationMeta('selected');
  @override
  late final GeneratedColumn<bool> selected = GeneratedColumn<bool>(
      'selected', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("selected" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [id, oin, selected];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'organization_selection_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<OrganizationSelectionDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('selected')) {
      context.handle(_selectedMeta,
          selected.isAcceptableOrUnknown(data['selected']!, _selectedMeta));
    } else if (isInserting) {
      context.missing(_selectedMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  OrganizationSelectionDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return OrganizationSelectionDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      selected: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}selected'])!,
    );
  }

  @override
  $OrganizationSelectionTableTable createAlias(String alias) {
    return $OrganizationSelectionTableTable(attachedDatabase, alias);
  }
}

class OrganizationSelectionDriftModel extends DataClass
    implements Insertable<OrganizationSelectionDriftModel> {
  final int id;
  final String oin;
  final bool selected;
  const OrganizationSelectionDriftModel(
      {required this.id, required this.oin, required this.selected});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['selected'] = Variable<bool>(selected);
    return map;
  }

  OrganizationSelectionTableCompanion toCompanion(bool nullToAbsent) {
    return OrganizationSelectionTableCompanion(
      id: Value(id),
      oin: Value(oin),
      selected: Value(selected),
    );
  }

  factory OrganizationSelectionDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return OrganizationSelectionDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      selected: serializer.fromJson<bool>(json['selected']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'selected': serializer.toJson<bool>(selected),
    };
  }

  OrganizationSelectionDriftModel copyWith(
          {int? id, String? oin, bool? selected}) =>
      OrganizationSelectionDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        selected: selected ?? this.selected,
      );
  @override
  String toString() {
    return (StringBuffer('OrganizationSelectionDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('selected: $selected')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, selected);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is OrganizationSelectionDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.selected == this.selected);
}

class OrganizationSelectionTableCompanion
    extends UpdateCompanion<OrganizationSelectionDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<bool> selected;
  const OrganizationSelectionTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.selected = const Value.absent(),
  });
  OrganizationSelectionTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required bool selected,
  })  : oin = Value(oin),
        selected = Value(selected);
  static Insertable<OrganizationSelectionDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<bool>? selected,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (selected != null) 'selected': selected,
    });
  }

  OrganizationSelectionTableCompanion copyWith(
      {Value<int>? id, Value<String>? oin, Value<bool>? selected}) {
    return OrganizationSelectionTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      selected: selected ?? this.selected,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (selected.present) {
      map['selected'] = Variable<bool>(selected.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('OrganizationSelectionTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('selected: $selected')
          ..write(')'))
        .toString();
  }
}

class $AppManagerSelectionTableTable extends AppManagerSelectionTable
    with
        TableInfo<$AppManagerSelectionTableTable,
            AppManagerSelectionDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $AppManagerSelectionTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, oin];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'app_manager_selection_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<AppManagerSelectionDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  AppManagerSelectionDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return AppManagerSelectionDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
    );
  }

  @override
  $AppManagerSelectionTableTable createAlias(String alias) {
    return $AppManagerSelectionTableTable(attachedDatabase, alias);
  }
}

class AppManagerSelectionDriftModel extends DataClass
    implements Insertable<AppManagerSelectionDriftModel> {
  final int id;
  final String oin;
  const AppManagerSelectionDriftModel({required this.id, required this.oin});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    return map;
  }

  AppManagerSelectionTableCompanion toCompanion(bool nullToAbsent) {
    return AppManagerSelectionTableCompanion(
      id: Value(id),
      oin: Value(oin),
    );
  }

  factory AppManagerSelectionDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return AppManagerSelectionDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
    };
  }

  AppManagerSelectionDriftModel copyWith({int? id, String? oin}) =>
      AppManagerSelectionDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
      );
  @override
  String toString() {
    return (StringBuffer('AppManagerSelectionDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is AppManagerSelectionDriftModel &&
          other.id == this.id &&
          other.oin == this.oin);
}

class AppManagerSelectionTableCompanion
    extends UpdateCompanion<AppManagerSelectionDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  const AppManagerSelectionTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
  });
  AppManagerSelectionTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
  }) : oin = Value(oin);
  static Insertable<AppManagerSelectionDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
    });
  }

  AppManagerSelectionTableCompanion copyWith(
      {Value<int>? id, Value<String>? oin}) {
    return AppManagerSelectionTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AppManagerSelectionTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin')
          ..write(')'))
        .toString();
  }
}

class $SchemeAppManagerTableTable extends SchemeAppManagerTable
    with TableInfo<$SchemeAppManagerTableTable, SchemeAppManagerDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SchemeAppManagerTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _publicKeyMeta =
      const VerificationMeta('publicKey');
  @override
  late final GeneratedColumn<String> publicKey = GeneratedColumn<String>(
      'public_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _discoveryUrlMeta =
      const VerificationMeta('discoveryUrl');
  @override
  late final GeneratedColumn<String> discoveryUrl = GeneratedColumn<String>(
      'discovery_url', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _availableMeta =
      const VerificationMeta('available');
  @override
  late final GeneratedColumn<bool> available = GeneratedColumn<bool>(
      'available', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("available" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns =>
      [id, oin, name, publicKey, discoveryUrl, available];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'scheme_app_manager_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<SchemeAppManagerDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('public_key')) {
      context.handle(_publicKeyMeta,
          publicKey.isAcceptableOrUnknown(data['public_key']!, _publicKeyMeta));
    } else if (isInserting) {
      context.missing(_publicKeyMeta);
    }
    if (data.containsKey('discovery_url')) {
      context.handle(
          _discoveryUrlMeta,
          discoveryUrl.isAcceptableOrUnknown(
              data['discovery_url']!, _discoveryUrlMeta));
    } else if (isInserting) {
      context.missing(_discoveryUrlMeta);
    }
    if (data.containsKey('available')) {
      context.handle(_availableMeta,
          available.isAcceptableOrUnknown(data['available']!, _availableMeta));
    } else if (isInserting) {
      context.missing(_availableMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SchemeAppManagerDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SchemeAppManagerDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      publicKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}public_key'])!,
      discoveryUrl: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}discovery_url'])!,
      available: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}available'])!,
    );
  }

  @override
  $SchemeAppManagerTableTable createAlias(String alias) {
    return $SchemeAppManagerTableTable(attachedDatabase, alias);
  }
}

class SchemeAppManagerDriftModel extends DataClass
    implements Insertable<SchemeAppManagerDriftModel> {
  final int id;
  final String oin;
  final String name;
  final String publicKey;
  final String discoveryUrl;
  final bool available;
  const SchemeAppManagerDriftModel(
      {required this.id,
      required this.oin,
      required this.name,
      required this.publicKey,
      required this.discoveryUrl,
      required this.available});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['name'] = Variable<String>(name);
    map['public_key'] = Variable<String>(publicKey);
    map['discovery_url'] = Variable<String>(discoveryUrl);
    map['available'] = Variable<bool>(available);
    return map;
  }

  SchemeAppManagerTableCompanion toCompanion(bool nullToAbsent) {
    return SchemeAppManagerTableCompanion(
      id: Value(id),
      oin: Value(oin),
      name: Value(name),
      publicKey: Value(publicKey),
      discoveryUrl: Value(discoveryUrl),
      available: Value(available),
    );
  }

  factory SchemeAppManagerDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SchemeAppManagerDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      name: serializer.fromJson<String>(json['name']),
      publicKey: serializer.fromJson<String>(json['publicKey']),
      discoveryUrl: serializer.fromJson<String>(json['discoveryUrl']),
      available: serializer.fromJson<bool>(json['available']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'name': serializer.toJson<String>(name),
      'publicKey': serializer.toJson<String>(publicKey),
      'discoveryUrl': serializer.toJson<String>(discoveryUrl),
      'available': serializer.toJson<bool>(available),
    };
  }

  SchemeAppManagerDriftModel copyWith(
          {int? id,
          String? oin,
          String? name,
          String? publicKey,
          String? discoveryUrl,
          bool? available}) =>
      SchemeAppManagerDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        name: name ?? this.name,
        publicKey: publicKey ?? this.publicKey,
        discoveryUrl: discoveryUrl ?? this.discoveryUrl,
        available: available ?? this.available,
      );
  @override
  String toString() {
    return (StringBuffer('SchemeAppManagerDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('name: $name, ')
          ..write('publicKey: $publicKey, ')
          ..write('discoveryUrl: $discoveryUrl, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, oin, name, publicKey, discoveryUrl, available);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SchemeAppManagerDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.name == this.name &&
          other.publicKey == this.publicKey &&
          other.discoveryUrl == this.discoveryUrl &&
          other.available == this.available);
}

class SchemeAppManagerTableCompanion
    extends UpdateCompanion<SchemeAppManagerDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<String> name;
  final Value<String> publicKey;
  final Value<String> discoveryUrl;
  final Value<bool> available;
  const SchemeAppManagerTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.name = const Value.absent(),
    this.publicKey = const Value.absent(),
    this.discoveryUrl = const Value.absent(),
    this.available = const Value.absent(),
  });
  SchemeAppManagerTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required String name,
    required String publicKey,
    required String discoveryUrl,
    required bool available,
  })  : oin = Value(oin),
        name = Value(name),
        publicKey = Value(publicKey),
        discoveryUrl = Value(discoveryUrl),
        available = Value(available);
  static Insertable<SchemeAppManagerDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<String>? name,
    Expression<String>? publicKey,
    Expression<String>? discoveryUrl,
    Expression<bool>? available,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (name != null) 'name': name,
      if (publicKey != null) 'public_key': publicKey,
      if (discoveryUrl != null) 'discovery_url': discoveryUrl,
      if (available != null) 'available': available,
    });
  }

  SchemeAppManagerTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<String>? name,
      Value<String>? publicKey,
      Value<String>? discoveryUrl,
      Value<bool>? available}) {
    return SchemeAppManagerTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      name: name ?? this.name,
      publicKey: publicKey ?? this.publicKey,
      discoveryUrl: discoveryUrl ?? this.discoveryUrl,
      available: available ?? this.available,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (publicKey.present) {
      map['public_key'] = Variable<String>(publicKey.value);
    }
    if (discoveryUrl.present) {
      map['discovery_url'] = Variable<String>(discoveryUrl.value);
    }
    if (available.present) {
      map['available'] = Variable<bool>(available.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SchemeAppManagerTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('name: $name, ')
          ..write('publicKey: $publicKey, ')
          ..write('discoveryUrl: $discoveryUrl, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }
}

class $SchemeDocumentTypeTableTable extends SchemeDocumentTypeTable
    with
        TableInfo<$SchemeDocumentTypeTableTable, SchemeDocumentTypeDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SchemeDocumentTypeTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _availableMeta =
      const VerificationMeta('available');
  @override
  late final GeneratedColumn<bool> available = GeneratedColumn<bool>(
      'available', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("available" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [id, name, available];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'scheme_document_type_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<SchemeDocumentTypeDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('available')) {
      context.handle(_availableMeta,
          available.isAcceptableOrUnknown(data['available']!, _availableMeta));
    } else if (isInserting) {
      context.missing(_availableMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SchemeDocumentTypeDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SchemeDocumentTypeDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      available: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}available'])!,
    );
  }

  @override
  $SchemeDocumentTypeTableTable createAlias(String alias) {
    return $SchemeDocumentTypeTableTable(attachedDatabase, alias);
  }
}

class SchemeDocumentTypeDriftModel extends DataClass
    implements Insertable<SchemeDocumentTypeDriftModel> {
  final int id;
  final String name;
  final bool available;
  const SchemeDocumentTypeDriftModel(
      {required this.id, required this.name, required this.available});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    map['available'] = Variable<bool>(available);
    return map;
  }

  SchemeDocumentTypeTableCompanion toCompanion(bool nullToAbsent) {
    return SchemeDocumentTypeTableCompanion(
      id: Value(id),
      name: Value(name),
      available: Value(available),
    );
  }

  factory SchemeDocumentTypeDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SchemeDocumentTypeDriftModel(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      available: serializer.fromJson<bool>(json['available']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'available': serializer.toJson<bool>(available),
    };
  }

  SchemeDocumentTypeDriftModel copyWith(
          {int? id, String? name, bool? available}) =>
      SchemeDocumentTypeDriftModel(
        id: id ?? this.id,
        name: name ?? this.name,
        available: available ?? this.available,
      );
  @override
  String toString() {
    return (StringBuffer('SchemeDocumentTypeDriftModel(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name, available);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SchemeDocumentTypeDriftModel &&
          other.id == this.id &&
          other.name == this.name &&
          other.available == this.available);
}

class SchemeDocumentTypeTableCompanion
    extends UpdateCompanion<SchemeDocumentTypeDriftModel> {
  final Value<int> id;
  final Value<String> name;
  final Value<bool> available;
  const SchemeDocumentTypeTableCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.available = const Value.absent(),
  });
  SchemeDocumentTypeTableCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    required bool available,
  })  : name = Value(name),
        available = Value(available);
  static Insertable<SchemeDocumentTypeDriftModel> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<bool>? available,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (available != null) 'available': available,
    });
  }

  SchemeDocumentTypeTableCompanion copyWith(
      {Value<int>? id, Value<String>? name, Value<bool>? available}) {
    return SchemeDocumentTypeTableCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      available: available ?? this.available,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (available.present) {
      map['available'] = Variable<bool>(available.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SchemeDocumentTypeTableCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }
}

class $SchemeOrganizationTableTable extends SchemeOrganizationTable
    with
        TableInfo<$SchemeOrganizationTableTable, SchemeOrganizationDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SchemeOrganizationTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _publicKeyMeta =
      const VerificationMeta('publicKey');
  @override
  late final GeneratedColumn<String> publicKey = GeneratedColumn<String>(
      'public_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _discoveryUrlMeta =
      const VerificationMeta('discoveryUrl');
  @override
  late final GeneratedColumn<String> discoveryUrl = GeneratedColumn<String>(
      'discovery_url', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _availableMeta =
      const VerificationMeta('available');
  @override
  late final GeneratedColumn<bool> available = GeneratedColumn<bool>(
      'available', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("available" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns =>
      [id, oin, name, publicKey, discoveryUrl, available];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'scheme_organization_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<SchemeOrganizationDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('public_key')) {
      context.handle(_publicKeyMeta,
          publicKey.isAcceptableOrUnknown(data['public_key']!, _publicKeyMeta));
    } else if (isInserting) {
      context.missing(_publicKeyMeta);
    }
    if (data.containsKey('discovery_url')) {
      context.handle(
          _discoveryUrlMeta,
          discoveryUrl.isAcceptableOrUnknown(
              data['discovery_url']!, _discoveryUrlMeta));
    } else if (isInserting) {
      context.missing(_discoveryUrlMeta);
    }
    if (data.containsKey('available')) {
      context.handle(_availableMeta,
          available.isAcceptableOrUnknown(data['available']!, _availableMeta));
    } else if (isInserting) {
      context.missing(_availableMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SchemeOrganizationDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SchemeOrganizationDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      publicKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}public_key'])!,
      discoveryUrl: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}discovery_url'])!,
      available: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}available'])!,
    );
  }

  @override
  $SchemeOrganizationTableTable createAlias(String alias) {
    return $SchemeOrganizationTableTable(attachedDatabase, alias);
  }
}

class SchemeOrganizationDriftModel extends DataClass
    implements Insertable<SchemeOrganizationDriftModel> {
  final int id;
  final String oin;
  final String name;
  final String publicKey;
  final String discoveryUrl;
  final bool available;
  const SchemeOrganizationDriftModel(
      {required this.id,
      required this.oin,
      required this.name,
      required this.publicKey,
      required this.discoveryUrl,
      required this.available});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['name'] = Variable<String>(name);
    map['public_key'] = Variable<String>(publicKey);
    map['discovery_url'] = Variable<String>(discoveryUrl);
    map['available'] = Variable<bool>(available);
    return map;
  }

  SchemeOrganizationTableCompanion toCompanion(bool nullToAbsent) {
    return SchemeOrganizationTableCompanion(
      id: Value(id),
      oin: Value(oin),
      name: Value(name),
      publicKey: Value(publicKey),
      discoveryUrl: Value(discoveryUrl),
      available: Value(available),
    );
  }

  factory SchemeOrganizationDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SchemeOrganizationDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      name: serializer.fromJson<String>(json['name']),
      publicKey: serializer.fromJson<String>(json['publicKey']),
      discoveryUrl: serializer.fromJson<String>(json['discoveryUrl']),
      available: serializer.fromJson<bool>(json['available']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'name': serializer.toJson<String>(name),
      'publicKey': serializer.toJson<String>(publicKey),
      'discoveryUrl': serializer.toJson<String>(discoveryUrl),
      'available': serializer.toJson<bool>(available),
    };
  }

  SchemeOrganizationDriftModel copyWith(
          {int? id,
          String? oin,
          String? name,
          String? publicKey,
          String? discoveryUrl,
          bool? available}) =>
      SchemeOrganizationDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        name: name ?? this.name,
        publicKey: publicKey ?? this.publicKey,
        discoveryUrl: discoveryUrl ?? this.discoveryUrl,
        available: available ?? this.available,
      );
  @override
  String toString() {
    return (StringBuffer('SchemeOrganizationDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('name: $name, ')
          ..write('publicKey: $publicKey, ')
          ..write('discoveryUrl: $discoveryUrl, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, oin, name, publicKey, discoveryUrl, available);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SchemeOrganizationDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.name == this.name &&
          other.publicKey == this.publicKey &&
          other.discoveryUrl == this.discoveryUrl &&
          other.available == this.available);
}

class SchemeOrganizationTableCompanion
    extends UpdateCompanion<SchemeOrganizationDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<String> name;
  final Value<String> publicKey;
  final Value<String> discoveryUrl;
  final Value<bool> available;
  const SchemeOrganizationTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.name = const Value.absent(),
    this.publicKey = const Value.absent(),
    this.discoveryUrl = const Value.absent(),
    this.available = const Value.absent(),
  });
  SchemeOrganizationTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required String name,
    required String publicKey,
    required String discoveryUrl,
    required bool available,
  })  : oin = Value(oin),
        name = Value(name),
        publicKey = Value(publicKey),
        discoveryUrl = Value(discoveryUrl),
        available = Value(available);
  static Insertable<SchemeOrganizationDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<String>? name,
    Expression<String>? publicKey,
    Expression<String>? discoveryUrl,
    Expression<bool>? available,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (name != null) 'name': name,
      if (publicKey != null) 'public_key': publicKey,
      if (discoveryUrl != null) 'discovery_url': discoveryUrl,
      if (available != null) 'available': available,
    });
  }

  SchemeOrganizationTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<String>? name,
      Value<String>? publicKey,
      Value<String>? discoveryUrl,
      Value<bool>? available}) {
    return SchemeOrganizationTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      name: name ?? this.name,
      publicKey: publicKey ?? this.publicKey,
      discoveryUrl: discoveryUrl ?? this.discoveryUrl,
      available: available ?? this.available,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (publicKey.present) {
      map['public_key'] = Variable<String>(publicKey.value);
    }
    if (discoveryUrl.present) {
      map['discovery_url'] = Variable<String>(discoveryUrl.value);
    }
    if (available.present) {
      map['available'] = Variable<bool>(available.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SchemeOrganizationTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('name: $name, ')
          ..write('publicKey: $publicKey, ')
          ..write('discoveryUrl: $discoveryUrl, ')
          ..write('available: $available')
          ..write(')'))
        .toString();
  }
}

class $AppKeyPairTableTable extends AppKeyPairTable
    with TableInfo<$AppKeyPairTableTable, AppKeyPairDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $AppKeyPairTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _privateKeyMeta =
      const VerificationMeta('privateKey');
  @override
  late final GeneratedColumn<String> privateKey = GeneratedColumn<String>(
      'private_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _publicKeyMeta =
      const VerificationMeta('publicKey');
  @override
  late final GeneratedColumn<String> publicKey = GeneratedColumn<String>(
      'public_key', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, privateKey, publicKey];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'app_key_pair_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<AppKeyPairDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('private_key')) {
      context.handle(
          _privateKeyMeta,
          privateKey.isAcceptableOrUnknown(
              data['private_key']!, _privateKeyMeta));
    } else if (isInserting) {
      context.missing(_privateKeyMeta);
    }
    if (data.containsKey('public_key')) {
      context.handle(_publicKeyMeta,
          publicKey.isAcceptableOrUnknown(data['public_key']!, _publicKeyMeta));
    } else if (isInserting) {
      context.missing(_publicKeyMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  AppKeyPairDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return AppKeyPairDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      privateKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}private_key'])!,
      publicKey: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}public_key'])!,
    );
  }

  @override
  $AppKeyPairTableTable createAlias(String alias) {
    return $AppKeyPairTableTable(attachedDatabase, alias);
  }
}

class AppKeyPairDriftModel extends DataClass
    implements Insertable<AppKeyPairDriftModel> {
  final int id;
  final String privateKey;
  final String publicKey;
  const AppKeyPairDriftModel(
      {required this.id, required this.privateKey, required this.publicKey});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['private_key'] = Variable<String>(privateKey);
    map['public_key'] = Variable<String>(publicKey);
    return map;
  }

  AppKeyPairTableCompanion toCompanion(bool nullToAbsent) {
    return AppKeyPairTableCompanion(
      id: Value(id),
      privateKey: Value(privateKey),
      publicKey: Value(publicKey),
    );
  }

  factory AppKeyPairDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return AppKeyPairDriftModel(
      id: serializer.fromJson<int>(json['id']),
      privateKey: serializer.fromJson<String>(json['privateKey']),
      publicKey: serializer.fromJson<String>(json['publicKey']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'privateKey': serializer.toJson<String>(privateKey),
      'publicKey': serializer.toJson<String>(publicKey),
    };
  }

  AppKeyPairDriftModel copyWith(
          {int? id, String? privateKey, String? publicKey}) =>
      AppKeyPairDriftModel(
        id: id ?? this.id,
        privateKey: privateKey ?? this.privateKey,
        publicKey: publicKey ?? this.publicKey,
      );
  @override
  String toString() {
    return (StringBuffer('AppKeyPairDriftModel(')
          ..write('id: $id, ')
          ..write('privateKey: $privateKey, ')
          ..write('publicKey: $publicKey')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, privateKey, publicKey);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is AppKeyPairDriftModel &&
          other.id == this.id &&
          other.privateKey == this.privateKey &&
          other.publicKey == this.publicKey);
}

class AppKeyPairTableCompanion extends UpdateCompanion<AppKeyPairDriftModel> {
  final Value<int> id;
  final Value<String> privateKey;
  final Value<String> publicKey;
  const AppKeyPairTableCompanion({
    this.id = const Value.absent(),
    this.privateKey = const Value.absent(),
    this.publicKey = const Value.absent(),
  });
  AppKeyPairTableCompanion.insert({
    this.id = const Value.absent(),
    required String privateKey,
    required String publicKey,
  })  : privateKey = Value(privateKey),
        publicKey = Value(publicKey);
  static Insertable<AppKeyPairDriftModel> custom({
    Expression<int>? id,
    Expression<String>? privateKey,
    Expression<String>? publicKey,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (privateKey != null) 'private_key': privateKey,
      if (publicKey != null) 'public_key': publicKey,
    });
  }

  AppKeyPairTableCompanion copyWith(
      {Value<int>? id, Value<String>? privateKey, Value<String>? publicKey}) {
    return AppKeyPairTableCompanion(
      id: id ?? this.id,
      privateKey: privateKey ?? this.privateKey,
      publicKey: publicKey ?? this.publicKey,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (privateKey.present) {
      map['private_key'] = Variable<String>(privateKey.value);
    }
    if (publicKey.present) {
      map['public_key'] = Variable<String>(publicKey.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AppKeyPairTableCompanion(')
          ..write('id: $id, ')
          ..write('privateKey: $privateKey, ')
          ..write('publicKey: $publicKey')
          ..write(')'))
        .toString();
  }
}

class $FinancialClaimsInformationStorageTableTable
    extends FinancialClaimsInformationStorageTable
    with
        TableInfo<$FinancialClaimsInformationStorageTableTable,
            FinancialClaimsInformationStorageDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FinancialClaimsInformationStorageTableTable(this.attachedDatabase,
      [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _oinMeta = const VerificationMeta('oin');
  @override
  late final GeneratedColumn<String> oin = GeneratedColumn<String>(
      'oin', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeRequestedMeta =
      const VerificationMeta('dateTimeRequested');
  @override
  late final GeneratedColumn<DateTime> dateTimeRequested =
      GeneratedColumn<DateTime>('date_time_requested', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _dateTimeReceivedMeta =
      const VerificationMeta('dateTimeReceived');
  @override
  late final GeneratedColumn<DateTime> dateTimeReceived =
      GeneratedColumn<DateTime>('date_time_received', aliasedName, false,
          type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _financialClaimsInformationDocumentMeta =
      const VerificationMeta('financialClaimsInformationDocument');
  @override
  late final GeneratedColumn<String> financialClaimsInformationDocument =
      GeneratedColumn<String>(
          'financial_claims_information_document', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        oin,
        dateTimeRequested,
        dateTimeReceived,
        financialClaimsInformationDocument
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'financial_claims_information_storage_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<FinancialClaimsInformationStorageDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('oin')) {
      context.handle(
          _oinMeta, oin.isAcceptableOrUnknown(data['oin']!, _oinMeta));
    } else if (isInserting) {
      context.missing(_oinMeta);
    }
    if (data.containsKey('date_time_requested')) {
      context.handle(
          _dateTimeRequestedMeta,
          dateTimeRequested.isAcceptableOrUnknown(
              data['date_time_requested']!, _dateTimeRequestedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeRequestedMeta);
    }
    if (data.containsKey('date_time_received')) {
      context.handle(
          _dateTimeReceivedMeta,
          dateTimeReceived.isAcceptableOrUnknown(
              data['date_time_received']!, _dateTimeReceivedMeta));
    } else if (isInserting) {
      context.missing(_dateTimeReceivedMeta);
    }
    if (data.containsKey('financial_claims_information_document')) {
      context.handle(
          _financialClaimsInformationDocumentMeta,
          financialClaimsInformationDocument.isAcceptableOrUnknown(
              data['financial_claims_information_document']!,
              _financialClaimsInformationDocumentMeta));
    } else if (isInserting) {
      context.missing(_financialClaimsInformationDocumentMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FinancialClaimsInformationStorageDriftModel map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FinancialClaimsInformationStorageDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      oin: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}oin'])!,
      dateTimeRequested: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime,
          data['${effectivePrefix}date_time_requested'])!,
      dateTimeReceived: attachedDatabase.typeMapping.read(
          DriftSqlType.dateTime, data['${effectivePrefix}date_time_received'])!,
      financialClaimsInformationDocument: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}financial_claims_information_document'])!,
    );
  }

  @override
  $FinancialClaimsInformationStorageTableTable createAlias(String alias) {
    return $FinancialClaimsInformationStorageTableTable(
        attachedDatabase, alias);
  }
}

class FinancialClaimsInformationStorageDriftModel extends DataClass
    implements Insertable<FinancialClaimsInformationStorageDriftModel> {
  final int id;
  final String oin;
  final DateTime dateTimeRequested;
  final DateTime dateTimeReceived;
  final String financialClaimsInformationDocument;
  const FinancialClaimsInformationStorageDriftModel(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.dateTimeReceived,
      required this.financialClaimsInformationDocument});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['oin'] = Variable<String>(oin);
    map['date_time_requested'] = Variable<DateTime>(dateTimeRequested);
    map['date_time_received'] = Variable<DateTime>(dateTimeReceived);
    map['financial_claims_information_document'] =
        Variable<String>(financialClaimsInformationDocument);
    return map;
  }

  FinancialClaimsInformationStorageTableCompanion toCompanion(
      bool nullToAbsent) {
    return FinancialClaimsInformationStorageTableCompanion(
      id: Value(id),
      oin: Value(oin),
      dateTimeRequested: Value(dateTimeRequested),
      dateTimeReceived: Value(dateTimeReceived),
      financialClaimsInformationDocument:
          Value(financialClaimsInformationDocument),
    );
  }

  factory FinancialClaimsInformationStorageDriftModel.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FinancialClaimsInformationStorageDriftModel(
      id: serializer.fromJson<int>(json['id']),
      oin: serializer.fromJson<String>(json['oin']),
      dateTimeRequested:
          serializer.fromJson<DateTime>(json['dateTimeRequested']),
      dateTimeReceived: serializer.fromJson<DateTime>(json['dateTimeReceived']),
      financialClaimsInformationDocument: serializer
          .fromJson<String>(json['financialClaimsInformationDocument']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'oin': serializer.toJson<String>(oin),
      'dateTimeRequested': serializer.toJson<DateTime>(dateTimeRequested),
      'dateTimeReceived': serializer.toJson<DateTime>(dateTimeReceived),
      'financialClaimsInformationDocument':
          serializer.toJson<String>(financialClaimsInformationDocument),
    };
  }

  FinancialClaimsInformationStorageDriftModel copyWith(
          {int? id,
          String? oin,
          DateTime? dateTimeRequested,
          DateTime? dateTimeReceived,
          String? financialClaimsInformationDocument}) =>
      FinancialClaimsInformationStorageDriftModel(
        id: id ?? this.id,
        oin: oin ?? this.oin,
        dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
        dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
        financialClaimsInformationDocument:
            financialClaimsInformationDocument ??
                this.financialClaimsInformationDocument,
      );
  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationStorageDriftModel(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'financialClaimsInformationDocument: $financialClaimsInformationDocument')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, oin, dateTimeRequested, dateTimeReceived,
      financialClaimsInformationDocument);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FinancialClaimsInformationStorageDriftModel &&
          other.id == this.id &&
          other.oin == this.oin &&
          other.dateTimeRequested == this.dateTimeRequested &&
          other.dateTimeReceived == this.dateTimeReceived &&
          other.financialClaimsInformationDocument ==
              this.financialClaimsInformationDocument);
}

class FinancialClaimsInformationStorageTableCompanion
    extends UpdateCompanion<FinancialClaimsInformationStorageDriftModel> {
  final Value<int> id;
  final Value<String> oin;
  final Value<DateTime> dateTimeRequested;
  final Value<DateTime> dateTimeReceived;
  final Value<String> financialClaimsInformationDocument;
  const FinancialClaimsInformationStorageTableCompanion({
    this.id = const Value.absent(),
    this.oin = const Value.absent(),
    this.dateTimeRequested = const Value.absent(),
    this.dateTimeReceived = const Value.absent(),
    this.financialClaimsInformationDocument = const Value.absent(),
  });
  FinancialClaimsInformationStorageTableCompanion.insert({
    this.id = const Value.absent(),
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String financialClaimsInformationDocument,
  })  : oin = Value(oin),
        dateTimeRequested = Value(dateTimeRequested),
        dateTimeReceived = Value(dateTimeReceived),
        financialClaimsInformationDocument =
            Value(financialClaimsInformationDocument);
  static Insertable<FinancialClaimsInformationStorageDriftModel> custom({
    Expression<int>? id,
    Expression<String>? oin,
    Expression<DateTime>? dateTimeRequested,
    Expression<DateTime>? dateTimeReceived,
    Expression<String>? financialClaimsInformationDocument,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (oin != null) 'oin': oin,
      if (dateTimeRequested != null) 'date_time_requested': dateTimeRequested,
      if (dateTimeReceived != null) 'date_time_received': dateTimeReceived,
      if (financialClaimsInformationDocument != null)
        'financial_claims_information_document':
            financialClaimsInformationDocument,
    });
  }

  FinancialClaimsInformationStorageTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? oin,
      Value<DateTime>? dateTimeRequested,
      Value<DateTime>? dateTimeReceived,
      Value<String>? financialClaimsInformationDocument}) {
    return FinancialClaimsInformationStorageTableCompanion(
      id: id ?? this.id,
      oin: oin ?? this.oin,
      dateTimeRequested: dateTimeRequested ?? this.dateTimeRequested,
      dateTimeReceived: dateTimeReceived ?? this.dateTimeReceived,
      financialClaimsInformationDocument: financialClaimsInformationDocument ??
          this.financialClaimsInformationDocument,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (oin.present) {
      map['oin'] = Variable<String>(oin.value);
    }
    if (dateTimeRequested.present) {
      map['date_time_requested'] = Variable<DateTime>(dateTimeRequested.value);
    }
    if (dateTimeReceived.present) {
      map['date_time_received'] = Variable<DateTime>(dateTimeReceived.value);
    }
    if (financialClaimsInformationDocument.present) {
      map['financial_claims_information_document'] =
          Variable<String>(financialClaimsInformationDocument.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FinancialClaimsInformationStorageTableCompanion(')
          ..write('id: $id, ')
          ..write('oin: $oin, ')
          ..write('dateTimeRequested: $dateTimeRequested, ')
          ..write('dateTimeReceived: $dateTimeReceived, ')
          ..write(
              'financialClaimsInformationDocument: $financialClaimsInformationDocument')
          ..write(')'))
        .toString();
  }
}

class $LogRecordTableTable extends LogRecordTable
    with TableInfo<$LogRecordTableTable, LogRecordDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $LogRecordTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _timestampMeta =
      const VerificationMeta('timestamp');
  @override
  late final GeneratedColumn<DateTime> timestamp = GeneratedColumn<DateTime>(
      'timestamp', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _hostMeta = const VerificationMeta('host');
  @override
  late final GeneratedColumn<String> host = GeneratedColumn<String>(
      'host', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _cefMeta = const VerificationMeta('cef');
  @override
  late final GeneratedColumn<int> cef = GeneratedColumn<int>(
      'cef', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _deviceVendorMeta =
      const VerificationMeta('deviceVendor');
  @override
  late final GeneratedColumn<String> deviceVendor = GeneratedColumn<String>(
      'device_vendor', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _deviceProductMeta =
      const VerificationMeta('deviceProduct');
  @override
  late final GeneratedColumn<String> deviceProduct = GeneratedColumn<String>(
      'device_product', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _deviceVersionMeta =
      const VerificationMeta('deviceVersion');
  @override
  late final GeneratedColumn<String> deviceVersion = GeneratedColumn<String>(
      'device_version', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _deviceEventClassIdMeta =
      const VerificationMeta('deviceEventClassId');
  @override
  late final GeneratedColumn<String> deviceEventClassId =
      GeneratedColumn<String>('device_event_class_id', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _severityMeta =
      const VerificationMeta('severity');
  @override
  late final GeneratedColumn<int> severity = GeneratedColumn<int>(
      'severity', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _flexString1LabelMeta =
      const VerificationMeta('flexString1Label');
  @override
  late final GeneratedColumn<String> flexString1Label = GeneratedColumn<String>(
      'flex_string1_label', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _flexString1Meta =
      const VerificationMeta('flexString1');
  @override
  late final GeneratedColumn<String> flexString1 = GeneratedColumn<String>(
      'flex_string1', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _flexString2LabelMeta =
      const VerificationMeta('flexString2Label');
  @override
  late final GeneratedColumn<String> flexString2Label = GeneratedColumn<String>(
      'flex_string2_label', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _flexString2Meta =
      const VerificationMeta('flexString2');
  @override
  late final GeneratedColumn<String> flexString2 = GeneratedColumn<String>(
      'flex_string2', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _actMeta = const VerificationMeta('act');
  @override
  late final GeneratedColumn<String> act = GeneratedColumn<String>(
      'act', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _appMeta = const VerificationMeta('app');
  @override
  late final GeneratedColumn<String> app = GeneratedColumn<String>(
      'app', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _requestMeta =
      const VerificationMeta('request');
  @override
  late final GeneratedColumn<String> request = GeneratedColumn<String>(
      'request', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _requestMethodMeta =
      const VerificationMeta('requestMethod');
  @override
  late final GeneratedColumn<String> requestMethod = GeneratedColumn<String>(
      'request_method', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        timestamp,
        host,
        cef,
        deviceVendor,
        deviceProduct,
        deviceVersion,
        deviceEventClassId,
        name,
        severity,
        flexString1Label,
        flexString1,
        flexString2Label,
        flexString2,
        act,
        app,
        request,
        requestMethod
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'log_record_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<LogRecordDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('timestamp')) {
      context.handle(_timestampMeta,
          timestamp.isAcceptableOrUnknown(data['timestamp']!, _timestampMeta));
    } else if (isInserting) {
      context.missing(_timestampMeta);
    }
    if (data.containsKey('host')) {
      context.handle(
          _hostMeta, host.isAcceptableOrUnknown(data['host']!, _hostMeta));
    } else if (isInserting) {
      context.missing(_hostMeta);
    }
    if (data.containsKey('cef')) {
      context.handle(
          _cefMeta, cef.isAcceptableOrUnknown(data['cef']!, _cefMeta));
    } else if (isInserting) {
      context.missing(_cefMeta);
    }
    if (data.containsKey('device_vendor')) {
      context.handle(
          _deviceVendorMeta,
          deviceVendor.isAcceptableOrUnknown(
              data['device_vendor']!, _deviceVendorMeta));
    } else if (isInserting) {
      context.missing(_deviceVendorMeta);
    }
    if (data.containsKey('device_product')) {
      context.handle(
          _deviceProductMeta,
          deviceProduct.isAcceptableOrUnknown(
              data['device_product']!, _deviceProductMeta));
    } else if (isInserting) {
      context.missing(_deviceProductMeta);
    }
    if (data.containsKey('device_version')) {
      context.handle(
          _deviceVersionMeta,
          deviceVersion.isAcceptableOrUnknown(
              data['device_version']!, _deviceVersionMeta));
    } else if (isInserting) {
      context.missing(_deviceVersionMeta);
    }
    if (data.containsKey('device_event_class_id')) {
      context.handle(
          _deviceEventClassIdMeta,
          deviceEventClassId.isAcceptableOrUnknown(
              data['device_event_class_id']!, _deviceEventClassIdMeta));
    } else if (isInserting) {
      context.missing(_deviceEventClassIdMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('severity')) {
      context.handle(_severityMeta,
          severity.isAcceptableOrUnknown(data['severity']!, _severityMeta));
    } else if (isInserting) {
      context.missing(_severityMeta);
    }
    if (data.containsKey('flex_string1_label')) {
      context.handle(
          _flexString1LabelMeta,
          flexString1Label.isAcceptableOrUnknown(
              data['flex_string1_label']!, _flexString1LabelMeta));
    } else if (isInserting) {
      context.missing(_flexString1LabelMeta);
    }
    if (data.containsKey('flex_string1')) {
      context.handle(
          _flexString1Meta,
          flexString1.isAcceptableOrUnknown(
              data['flex_string1']!, _flexString1Meta));
    } else if (isInserting) {
      context.missing(_flexString1Meta);
    }
    if (data.containsKey('flex_string2_label')) {
      context.handle(
          _flexString2LabelMeta,
          flexString2Label.isAcceptableOrUnknown(
              data['flex_string2_label']!, _flexString2LabelMeta));
    } else if (isInserting) {
      context.missing(_flexString2LabelMeta);
    }
    if (data.containsKey('flex_string2')) {
      context.handle(
          _flexString2Meta,
          flexString2.isAcceptableOrUnknown(
              data['flex_string2']!, _flexString2Meta));
    } else if (isInserting) {
      context.missing(_flexString2Meta);
    }
    if (data.containsKey('act')) {
      context.handle(
          _actMeta, act.isAcceptableOrUnknown(data['act']!, _actMeta));
    } else if (isInserting) {
      context.missing(_actMeta);
    }
    if (data.containsKey('app')) {
      context.handle(
          _appMeta, app.isAcceptableOrUnknown(data['app']!, _appMeta));
    } else if (isInserting) {
      context.missing(_appMeta);
    }
    if (data.containsKey('request')) {
      context.handle(_requestMeta,
          request.isAcceptableOrUnknown(data['request']!, _requestMeta));
    } else if (isInserting) {
      context.missing(_requestMeta);
    }
    if (data.containsKey('request_method')) {
      context.handle(
          _requestMethodMeta,
          requestMethod.isAcceptableOrUnknown(
              data['request_method']!, _requestMethodMeta));
    } else if (isInserting) {
      context.missing(_requestMethodMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  LogRecordDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return LogRecordDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      timestamp: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}timestamp'])!,
      host: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}host'])!,
      cef: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}cef'])!,
      deviceVendor: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device_vendor'])!,
      deviceProduct: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device_product'])!,
      deviceVersion: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device_version'])!,
      deviceEventClassId: attachedDatabase.typeMapping.read(DriftSqlType.string,
          data['${effectivePrefix}device_event_class_id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      severity: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}severity'])!,
      flexString1Label: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}flex_string1_label'])!,
      flexString1: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}flex_string1'])!,
      flexString2Label: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}flex_string2_label'])!,
      flexString2: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}flex_string2'])!,
      act: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}act'])!,
      app: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}app'])!,
      request: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}request'])!,
      requestMethod: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}request_method'])!,
    );
  }

  @override
  $LogRecordTableTable createAlias(String alias) {
    return $LogRecordTableTable(attachedDatabase, alias);
  }
}

class LogRecordDriftModel extends DataClass
    implements Insertable<LogRecordDriftModel> {
  final int id;
  final DateTime timestamp;
  final String host;
  final int cef;
  final String deviceVendor;
  final String deviceProduct;
  final String deviceVersion;
  final String deviceEventClassId;
  final String name;
  final int severity;
  final String flexString1Label;
  final String flexString1;
  final String flexString2Label;
  final String flexString2;
  final String act;
  final String app;
  final String request;
  final String requestMethod;
  const LogRecordDriftModel(
      {required this.id,
      required this.timestamp,
      required this.host,
      required this.cef,
      required this.deviceVendor,
      required this.deviceProduct,
      required this.deviceVersion,
      required this.deviceEventClassId,
      required this.name,
      required this.severity,
      required this.flexString1Label,
      required this.flexString1,
      required this.flexString2Label,
      required this.flexString2,
      required this.act,
      required this.app,
      required this.request,
      required this.requestMethod});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['timestamp'] = Variable<DateTime>(timestamp);
    map['host'] = Variable<String>(host);
    map['cef'] = Variable<int>(cef);
    map['device_vendor'] = Variable<String>(deviceVendor);
    map['device_product'] = Variable<String>(deviceProduct);
    map['device_version'] = Variable<String>(deviceVersion);
    map['device_event_class_id'] = Variable<String>(deviceEventClassId);
    map['name'] = Variable<String>(name);
    map['severity'] = Variable<int>(severity);
    map['flex_string1_label'] = Variable<String>(flexString1Label);
    map['flex_string1'] = Variable<String>(flexString1);
    map['flex_string2_label'] = Variable<String>(flexString2Label);
    map['flex_string2'] = Variable<String>(flexString2);
    map['act'] = Variable<String>(act);
    map['app'] = Variable<String>(app);
    map['request'] = Variable<String>(request);
    map['request_method'] = Variable<String>(requestMethod);
    return map;
  }

  LogRecordTableCompanion toCompanion(bool nullToAbsent) {
    return LogRecordTableCompanion(
      id: Value(id),
      timestamp: Value(timestamp),
      host: Value(host),
      cef: Value(cef),
      deviceVendor: Value(deviceVendor),
      deviceProduct: Value(deviceProduct),
      deviceVersion: Value(deviceVersion),
      deviceEventClassId: Value(deviceEventClassId),
      name: Value(name),
      severity: Value(severity),
      flexString1Label: Value(flexString1Label),
      flexString1: Value(flexString1),
      flexString2Label: Value(flexString2Label),
      flexString2: Value(flexString2),
      act: Value(act),
      app: Value(app),
      request: Value(request),
      requestMethod: Value(requestMethod),
    );
  }

  factory LogRecordDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return LogRecordDriftModel(
      id: serializer.fromJson<int>(json['id']),
      timestamp: serializer.fromJson<DateTime>(json['timestamp']),
      host: serializer.fromJson<String>(json['host']),
      cef: serializer.fromJson<int>(json['cef']),
      deviceVendor: serializer.fromJson<String>(json['deviceVendor']),
      deviceProduct: serializer.fromJson<String>(json['deviceProduct']),
      deviceVersion: serializer.fromJson<String>(json['deviceVersion']),
      deviceEventClassId:
          serializer.fromJson<String>(json['deviceEventClassId']),
      name: serializer.fromJson<String>(json['name']),
      severity: serializer.fromJson<int>(json['severity']),
      flexString1Label: serializer.fromJson<String>(json['flexString1Label']),
      flexString1: serializer.fromJson<String>(json['flexString1']),
      flexString2Label: serializer.fromJson<String>(json['flexString2Label']),
      flexString2: serializer.fromJson<String>(json['flexString2']),
      act: serializer.fromJson<String>(json['act']),
      app: serializer.fromJson<String>(json['app']),
      request: serializer.fromJson<String>(json['request']),
      requestMethod: serializer.fromJson<String>(json['requestMethod']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'timestamp': serializer.toJson<DateTime>(timestamp),
      'host': serializer.toJson<String>(host),
      'cef': serializer.toJson<int>(cef),
      'deviceVendor': serializer.toJson<String>(deviceVendor),
      'deviceProduct': serializer.toJson<String>(deviceProduct),
      'deviceVersion': serializer.toJson<String>(deviceVersion),
      'deviceEventClassId': serializer.toJson<String>(deviceEventClassId),
      'name': serializer.toJson<String>(name),
      'severity': serializer.toJson<int>(severity),
      'flexString1Label': serializer.toJson<String>(flexString1Label),
      'flexString1': serializer.toJson<String>(flexString1),
      'flexString2Label': serializer.toJson<String>(flexString2Label),
      'flexString2': serializer.toJson<String>(flexString2),
      'act': serializer.toJson<String>(act),
      'app': serializer.toJson<String>(app),
      'request': serializer.toJson<String>(request),
      'requestMethod': serializer.toJson<String>(requestMethod),
    };
  }

  LogRecordDriftModel copyWith(
          {int? id,
          DateTime? timestamp,
          String? host,
          int? cef,
          String? deviceVendor,
          String? deviceProduct,
          String? deviceVersion,
          String? deviceEventClassId,
          String? name,
          int? severity,
          String? flexString1Label,
          String? flexString1,
          String? flexString2Label,
          String? flexString2,
          String? act,
          String? app,
          String? request,
          String? requestMethod}) =>
      LogRecordDriftModel(
        id: id ?? this.id,
        timestamp: timestamp ?? this.timestamp,
        host: host ?? this.host,
        cef: cef ?? this.cef,
        deviceVendor: deviceVendor ?? this.deviceVendor,
        deviceProduct: deviceProduct ?? this.deviceProduct,
        deviceVersion: deviceVersion ?? this.deviceVersion,
        deviceEventClassId: deviceEventClassId ?? this.deviceEventClassId,
        name: name ?? this.name,
        severity: severity ?? this.severity,
        flexString1Label: flexString1Label ?? this.flexString1Label,
        flexString1: flexString1 ?? this.flexString1,
        flexString2Label: flexString2Label ?? this.flexString2Label,
        flexString2: flexString2 ?? this.flexString2,
        act: act ?? this.act,
        app: app ?? this.app,
        request: request ?? this.request,
        requestMethod: requestMethod ?? this.requestMethod,
      );
  @override
  String toString() {
    return (StringBuffer('LogRecordDriftModel(')
          ..write('id: $id, ')
          ..write('timestamp: $timestamp, ')
          ..write('host: $host, ')
          ..write('cef: $cef, ')
          ..write('deviceVendor: $deviceVendor, ')
          ..write('deviceProduct: $deviceProduct, ')
          ..write('deviceVersion: $deviceVersion, ')
          ..write('deviceEventClassId: $deviceEventClassId, ')
          ..write('name: $name, ')
          ..write('severity: $severity, ')
          ..write('flexString1Label: $flexString1Label, ')
          ..write('flexString1: $flexString1, ')
          ..write('flexString2Label: $flexString2Label, ')
          ..write('flexString2: $flexString2, ')
          ..write('act: $act, ')
          ..write('app: $app, ')
          ..write('request: $request, ')
          ..write('requestMethod: $requestMethod')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      timestamp,
      host,
      cef,
      deviceVendor,
      deviceProduct,
      deviceVersion,
      deviceEventClassId,
      name,
      severity,
      flexString1Label,
      flexString1,
      flexString2Label,
      flexString2,
      act,
      app,
      request,
      requestMethod);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is LogRecordDriftModel &&
          other.id == this.id &&
          other.timestamp == this.timestamp &&
          other.host == this.host &&
          other.cef == this.cef &&
          other.deviceVendor == this.deviceVendor &&
          other.deviceProduct == this.deviceProduct &&
          other.deviceVersion == this.deviceVersion &&
          other.deviceEventClassId == this.deviceEventClassId &&
          other.name == this.name &&
          other.severity == this.severity &&
          other.flexString1Label == this.flexString1Label &&
          other.flexString1 == this.flexString1 &&
          other.flexString2Label == this.flexString2Label &&
          other.flexString2 == this.flexString2 &&
          other.act == this.act &&
          other.app == this.app &&
          other.request == this.request &&
          other.requestMethod == this.requestMethod);
}

class LogRecordTableCompanion extends UpdateCompanion<LogRecordDriftModel> {
  final Value<int> id;
  final Value<DateTime> timestamp;
  final Value<String> host;
  final Value<int> cef;
  final Value<String> deviceVendor;
  final Value<String> deviceProduct;
  final Value<String> deviceVersion;
  final Value<String> deviceEventClassId;
  final Value<String> name;
  final Value<int> severity;
  final Value<String> flexString1Label;
  final Value<String> flexString1;
  final Value<String> flexString2Label;
  final Value<String> flexString2;
  final Value<String> act;
  final Value<String> app;
  final Value<String> request;
  final Value<String> requestMethod;
  const LogRecordTableCompanion({
    this.id = const Value.absent(),
    this.timestamp = const Value.absent(),
    this.host = const Value.absent(),
    this.cef = const Value.absent(),
    this.deviceVendor = const Value.absent(),
    this.deviceProduct = const Value.absent(),
    this.deviceVersion = const Value.absent(),
    this.deviceEventClassId = const Value.absent(),
    this.name = const Value.absent(),
    this.severity = const Value.absent(),
    this.flexString1Label = const Value.absent(),
    this.flexString1 = const Value.absent(),
    this.flexString2Label = const Value.absent(),
    this.flexString2 = const Value.absent(),
    this.act = const Value.absent(),
    this.app = const Value.absent(),
    this.request = const Value.absent(),
    this.requestMethod = const Value.absent(),
  });
  LogRecordTableCompanion.insert({
    this.id = const Value.absent(),
    required DateTime timestamp,
    required String host,
    required int cef,
    required String deviceVendor,
    required String deviceProduct,
    required String deviceVersion,
    required String deviceEventClassId,
    required String name,
    required int severity,
    required String flexString1Label,
    required String flexString1,
    required String flexString2Label,
    required String flexString2,
    required String act,
    required String app,
    required String request,
    required String requestMethod,
  })  : timestamp = Value(timestamp),
        host = Value(host),
        cef = Value(cef),
        deviceVendor = Value(deviceVendor),
        deviceProduct = Value(deviceProduct),
        deviceVersion = Value(deviceVersion),
        deviceEventClassId = Value(deviceEventClassId),
        name = Value(name),
        severity = Value(severity),
        flexString1Label = Value(flexString1Label),
        flexString1 = Value(flexString1),
        flexString2Label = Value(flexString2Label),
        flexString2 = Value(flexString2),
        act = Value(act),
        app = Value(app),
        request = Value(request),
        requestMethod = Value(requestMethod);
  static Insertable<LogRecordDriftModel> custom({
    Expression<int>? id,
    Expression<DateTime>? timestamp,
    Expression<String>? host,
    Expression<int>? cef,
    Expression<String>? deviceVendor,
    Expression<String>? deviceProduct,
    Expression<String>? deviceVersion,
    Expression<String>? deviceEventClassId,
    Expression<String>? name,
    Expression<int>? severity,
    Expression<String>? flexString1Label,
    Expression<String>? flexString1,
    Expression<String>? flexString2Label,
    Expression<String>? flexString2,
    Expression<String>? act,
    Expression<String>? app,
    Expression<String>? request,
    Expression<String>? requestMethod,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (timestamp != null) 'timestamp': timestamp,
      if (host != null) 'host': host,
      if (cef != null) 'cef': cef,
      if (deviceVendor != null) 'device_vendor': deviceVendor,
      if (deviceProduct != null) 'device_product': deviceProduct,
      if (deviceVersion != null) 'device_version': deviceVersion,
      if (deviceEventClassId != null)
        'device_event_class_id': deviceEventClassId,
      if (name != null) 'name': name,
      if (severity != null) 'severity': severity,
      if (flexString1Label != null) 'flex_string1_label': flexString1Label,
      if (flexString1 != null) 'flex_string1': flexString1,
      if (flexString2Label != null) 'flex_string2_label': flexString2Label,
      if (flexString2 != null) 'flex_string2': flexString2,
      if (act != null) 'act': act,
      if (app != null) 'app': app,
      if (request != null) 'request': request,
      if (requestMethod != null) 'request_method': requestMethod,
    });
  }

  LogRecordTableCompanion copyWith(
      {Value<int>? id,
      Value<DateTime>? timestamp,
      Value<String>? host,
      Value<int>? cef,
      Value<String>? deviceVendor,
      Value<String>? deviceProduct,
      Value<String>? deviceVersion,
      Value<String>? deviceEventClassId,
      Value<String>? name,
      Value<int>? severity,
      Value<String>? flexString1Label,
      Value<String>? flexString1,
      Value<String>? flexString2Label,
      Value<String>? flexString2,
      Value<String>? act,
      Value<String>? app,
      Value<String>? request,
      Value<String>? requestMethod}) {
    return LogRecordTableCompanion(
      id: id ?? this.id,
      timestamp: timestamp ?? this.timestamp,
      host: host ?? this.host,
      cef: cef ?? this.cef,
      deviceVendor: deviceVendor ?? this.deviceVendor,
      deviceProduct: deviceProduct ?? this.deviceProduct,
      deviceVersion: deviceVersion ?? this.deviceVersion,
      deviceEventClassId: deviceEventClassId ?? this.deviceEventClassId,
      name: name ?? this.name,
      severity: severity ?? this.severity,
      flexString1Label: flexString1Label ?? this.flexString1Label,
      flexString1: flexString1 ?? this.flexString1,
      flexString2Label: flexString2Label ?? this.flexString2Label,
      flexString2: flexString2 ?? this.flexString2,
      act: act ?? this.act,
      app: app ?? this.app,
      request: request ?? this.request,
      requestMethod: requestMethod ?? this.requestMethod,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (timestamp.present) {
      map['timestamp'] = Variable<DateTime>(timestamp.value);
    }
    if (host.present) {
      map['host'] = Variable<String>(host.value);
    }
    if (cef.present) {
      map['cef'] = Variable<int>(cef.value);
    }
    if (deviceVendor.present) {
      map['device_vendor'] = Variable<String>(deviceVendor.value);
    }
    if (deviceProduct.present) {
      map['device_product'] = Variable<String>(deviceProduct.value);
    }
    if (deviceVersion.present) {
      map['device_version'] = Variable<String>(deviceVersion.value);
    }
    if (deviceEventClassId.present) {
      map['device_event_class_id'] = Variable<String>(deviceEventClassId.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (severity.present) {
      map['severity'] = Variable<int>(severity.value);
    }
    if (flexString1Label.present) {
      map['flex_string1_label'] = Variable<String>(flexString1Label.value);
    }
    if (flexString1.present) {
      map['flex_string1'] = Variable<String>(flexString1.value);
    }
    if (flexString2Label.present) {
      map['flex_string2_label'] = Variable<String>(flexString2Label.value);
    }
    if (flexString2.present) {
      map['flex_string2'] = Variable<String>(flexString2.value);
    }
    if (act.present) {
      map['act'] = Variable<String>(act.value);
    }
    if (app.present) {
      map['app'] = Variable<String>(app.value);
    }
    if (request.present) {
      map['request'] = Variable<String>(request.value);
    }
    if (requestMethod.present) {
      map['request_method'] = Variable<String>(requestMethod.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LogRecordTableCompanion(')
          ..write('id: $id, ')
          ..write('timestamp: $timestamp, ')
          ..write('host: $host, ')
          ..write('cef: $cef, ')
          ..write('deviceVendor: $deviceVendor, ')
          ..write('deviceProduct: $deviceProduct, ')
          ..write('deviceVersion: $deviceVersion, ')
          ..write('deviceEventClassId: $deviceEventClassId, ')
          ..write('name: $name, ')
          ..write('severity: $severity, ')
          ..write('flexString1Label: $flexString1Label, ')
          ..write('flexString1: $flexString1, ')
          ..write('flexString2Label: $flexString2Label, ')
          ..write('flexString2: $flexString2, ')
          ..write('act: $act, ')
          ..write('app: $app, ')
          ..write('request: $request, ')
          ..write('requestMethod: $requestMethod')
          ..write(')'))
        .toString();
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(e);
  late final $FinancialClaimsInformationRequestTableTable
      financialClaimsInformationRequestTable =
      $FinancialClaimsInformationRequestTableTable(this);
  late final $FinancialClaimsInformationConfigurationTableTable
      financialClaimsInformationConfigurationTable =
      $FinancialClaimsInformationConfigurationTableTable(this);
  late final $FinancialClaimsInformationInboxTableTable
      financialClaimsInformationInboxTable =
      $FinancialClaimsInformationInboxTableTable(this);
  late final $RegistrationTableTable registrationTable =
      $RegistrationTableTable(this);
  late final $CertificateTableTable certificateTable =
      $CertificateTableTable(this);
  late final $OrganizationSelectionTableTable organizationSelectionTable =
      $OrganizationSelectionTableTable(this);
  late final $AppManagerSelectionTableTable appManagerSelectionTable =
      $AppManagerSelectionTableTable(this);
  late final $SchemeAppManagerTableTable schemeAppManagerTable =
      $SchemeAppManagerTableTable(this);
  late final $SchemeDocumentTypeTableTable schemeDocumentTypeTable =
      $SchemeDocumentTypeTableTable(this);
  late final $SchemeOrganizationTableTable schemeOrganizationTable =
      $SchemeOrganizationTableTable(this);
  late final $AppKeyPairTableTable appKeyPairTable =
      $AppKeyPairTableTable(this);
  late final $FinancialClaimsInformationStorageTableTable
      financialClaimsInformationStorageTable =
      $FinancialClaimsInformationStorageTableTable(this);
  late final $LogRecordTableTable logRecordTable = $LogRecordTableTable(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        financialClaimsInformationRequestTable,
        financialClaimsInformationConfigurationTable,
        financialClaimsInformationInboxTable,
        registrationTable,
        certificateTable,
        organizationSelectionTable,
        appManagerSelectionTable,
        schemeAppManagerTable,
        schemeDocumentTypeTable,
        schemeOrganizationTable,
        appKeyPairTable,
        financialClaimsInformationStorageTable,
        logRecordTable
      ];
}
