import 'dart:ffi';
import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:drift_dev/api/migrations.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:sqlcipher_flutter_libs/sqlcipher_flutter_libs.dart';
import 'package:sqlite3/open.dart';
import 'package:sqlite3/sqlite3.dart';

Future<File> get databaseFile async {
  // We use `path_provider` to find a suitable path to store our data in.
  final applicationDocumentsDirectory =
      await getApplicationDocumentsDirectory();

  return File(p.join(applicationDocumentsDirectory.path, 'app.db.enc'));
}

/// Obtains a database connection for running drift in a Dart VM.
DatabaseConnection connect() {
  return DatabaseConnection.delayed(Future(() async {
    final applicationDocumentsDirectory =
        await getApplicationDocumentsDirectory();

    final file = File(p.join(applicationDocumentsDirectory.path, 'app.db.enc'));
    return NativeDatabase.createBackgroundConnection(
      file,
      isolateSetup: setupSqlCipher,
      setup: (rawDb) {
        assert(_debugCheckHasCipher(rawDb));

        //TODO: change passphrase
        rawDb.execute("PRAGMA key = 'passphrase';");
      },
    );
  }));
}

void setupSqlCipher() {
  open
    ..overrideFor(OperatingSystem.android, openCipherOnAndroid)
    ..overrideFor(
        OperatingSystem.linux, () => DynamicLibrary.open('libsqlcipher.so'))
    ..overrideFor(
        OperatingSystem.windows, () => DynamicLibrary.open('sqlcipher.dll'));
}

bool _debugCheckHasCipher(Database database) {
  return database.select('PRAGMA cipher_version;').isNotEmpty;
}

Future<void> validateDatabaseSchema(GeneratedDatabase database) async {
  // This method validates that the actual schema of the opened database matches
  // the tables, views, triggers and indices for which drift_dev has generated
  // code.
  // Validating the database's schema after opening it is generally a good idea,
  // since it allows us to get an early warning if we change a table definition
  // without writing a schema migration for it.
  //
  // For details, see: https://drift.simonbinder.eu/docs/advanced-features/migrations/#verifying-a-database-schema-at-runtime
  if (kDebugMode) {
    await VerifySelf(database).validateDatabaseSchema();
  }
}
