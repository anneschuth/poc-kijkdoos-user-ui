import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

abstract class IOnboardingUsecase {
  Future<void> setHasSeenOnboarding(bool seen);
}

class OnboardingUsecase implements IOnboardingUsecase {
  final UserSettingsRepository userSettingsRepository;

  OnboardingUsecase(this.userSettingsRepository);

  @override
  Future<void> setHasSeenOnboarding(bool seen) async {
    return await userSettingsRepository.setHasSeenOnboarding(seen);
  }
}

final onboardingUsecaseProvider = Provider<IOnboardingUsecase>((ref) {
  final userSettingsRepository =
      ref.watch(userSettingsRepositoryProvider.notifier);
  return OnboardingUsecase(userSettingsRepository);
});
