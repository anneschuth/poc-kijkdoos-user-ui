// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';

abstract class IAppManagerSelectionUsecase {
  Future<void> setAppManagerSelection(String oin);
}

class AppManagerSelectionUsecase implements IAppManagerSelectionUsecase {
  final AppManagerSelectionRepository appManagerSelectionRepository;
  final SchemeAppManagerRepository schemeAppManagerRepository;

  AppManagerSelectionUsecase(
    this.appManagerSelectionRepository,
    this.schemeAppManagerRepository,
  );

  @override
  Future<void> setAppManagerSelection(String oin) async {
    var appManagers = await schemeAppManagerRepository.appManagers;

    var selectedAppManager = appManagers.firstWhere(
        (appManager) => appManager.oin == oin && appManager.available);

    await appManagerSelectionRepository.setAppManagerSelection(
      oin: selectedAppManager.oin,
    );
  }
}

final appManagerSelectionUsecaseProvider =
    Provider<IAppManagerSelectionUsecase>((ref) {
  final appManagerSelectionRepository =
      ref.watch(appManagerSelectionRepositoryProvider.notifier);

  final schemeAppManagerRepository =
      ref.watch(schemeAppManagerRepositoryProvider.notifier);

  return AppManagerSelectionUsecase(
    appManagerSelectionRepository,
    schemeAppManagerRepository,
  );
});
