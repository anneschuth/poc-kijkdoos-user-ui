import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/app_identity/app_key_pair_repository.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/logging/logging_repository.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';
import 'package:user_ui/repositories/scheme_document_type/scheme_document_type_repository.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_document_type_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/registration/revoke_registration.dart';

abstract class IAppUsecase {
  Future<void> removeAllDataAndUserSettings();
  Future<void> setHasCompletedRegistration(bool hasCompletedRegistration);
  Future<void> setHasCompletedOrganizationSelection(
      bool hasCompletedOrganizationSelection);
}

class AppUsecase implements IAppUsecase {
  final UserSettingsRepository userSettingsRepository;
  final SchemeOrganizationRepository schemeOrganizationRepository;
  final SchemeAppManagerRepository schemeAppManagerRepository;
  final SchemeDocumentTypeRepository schemeDocumentTypeRepository;
  final OrganizationSelectionRepository organizationSelectionRepository;
  final AppManagerSelectionRepository appManagerSelectionRepository;
  final RegistrationRepository registrationRepository;
  final CertificateRepository certificateRepository;
  final FinancialClaimsInformationConfigurationRepository
      financialClaimsInformationConfigurationRepository;
  final FinancialClaimsInformationRequestRepository
      financialClaimsInformationRequestRepository;
  final FinancialClaimsInformationInboxRepository
      financialClaimsInformationInboxRepository;
  final FinancialClaimsInformationStorageRepository
      financialClaimsInformationStorageRepository;
  final LoggingRepository loggingRepository;
  final Ref ref;

  AppUsecase(
    this.userSettingsRepository,
    this.schemeOrganizationRepository,
    this.schemeAppManagerRepository,
    this.schemeDocumentTypeRepository,
    this.organizationSelectionRepository,
    this.appManagerSelectionRepository,
    this.registrationRepository,
    this.certificateRepository,
    this.financialClaimsInformationConfigurationRepository,
    this.financialClaimsInformationRequestRepository,
    this.financialClaimsInformationInboxRepository,
    this.financialClaimsInformationStorageRepository,
    this.loggingRepository,
    this.ref,
  );

  @override
  Future<void> removeAllDataAndUserSettings() async {
    try {
      await ref.read(revokeRegistrationProvider.future);
    } catch (_) {
      //TODO: add logging
    }

    await financialClaimsInformationStorageRepository.clear();
    await financialClaimsInformationInboxRepository.clear();
    await financialClaimsInformationRequestRepository.clear();
    await organizationSelectionRepository.clear();
    await financialClaimsInformationConfigurationRepository.clear();
    await registrationRepository.clear();
    await certificateRepository.clear();
    await appManagerSelectionRepository.clear();
    await schemeOrganizationRepository.clear();
    await schemeAppManagerRepository.clear();
    await schemeDocumentTypeRepository.clear();
    await ref.read(appKeyPairRepositoryProvider.notifier).deleteKeyPair();
    await userSettingsRepository.clear();
    await loggingRepository.clear();
  }

  @override
  Future<void> setHasCompletedRegistration(
      bool hasCompletedRegistration) async {
    await userSettingsRepository
        .setHasCompletedRegistration(hasCompletedRegistration);
  }

  @override
  Future<void> setHasCompletedOrganizationSelection(
      bool hasCompletedOrganizationSelection) async {
    await userSettingsRepository.setHasCompletedOrganizationSelection(
        hasCompletedOrganizationSelection);
  }
}

final appUsecaseProvider = Provider<IAppUsecase>((ref) {
  final appManagerSelectionRepository =
      ref.read(appManagerSelectionRepositoryProvider.notifier);

  final userSettingsRepository =
      ref.read(userSettingsRepositoryProvider.notifier);

  final schemeOrganizationRepository =
      ref.read(schemeOrganizationRepositoryProvider.notifier);

  final schemeAppManagerRepository =
      ref.read(schemeAppManagerRepositoryProvider.notifier);

  final schemeDocumentTypeRepository =
      ref.read(schemeDocumentTypeRepositoryProvider.notifier);

  final organizationSelectionRepository =
      ref.read(organizationSelectionRepositoryProvider.notifier);

  final registrationRepository =
      ref.read(registrationRepositoryProvider.notifier);

  final certificateRepository =
      ref.read(certificateRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationInboxRepository =
      ref.read(financialClaimsInformationInboxRepositoryProvider.notifier);

  final financialClaimsInformationStorageRepository =
      ref.read(financialClaimsInformationStorageRepositoryProvider.notifier);

  final loggingRepository = ref.watch(loggingRepositoryProvider.notifier);

  return AppUsecase(
    userSettingsRepository,
    schemeOrganizationRepository,
    schemeAppManagerRepository,
    schemeDocumentTypeRepository,
    organizationSelectionRepository,
    appManagerSelectionRepository,
    registrationRepository,
    certificateRepository,
    financialClaimsInformationConfigurationRepository,
    financialClaimsInformationRequestRepository,
    financialClaimsInformationInboxRepository,
    financialClaimsInformationStorageRepository,
    loggingRepository,
    ref,
  );
});
