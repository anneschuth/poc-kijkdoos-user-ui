// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/usecases/helpers/fetch_and_store_certificate_helper.dart';
part 'fetch_and_store_certificate.g.dart';

@riverpod
Future<void> fetchAndStoreCertificate(FetchAndStoreCertificateRef ref) async {
  await ref.read(fetchAndStoreCertificateHelperProvider.future);
}
