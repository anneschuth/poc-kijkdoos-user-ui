// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'app_identity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppIdentity _$AppIdentityFromJson(Map<String, dynamic> json) {
  return _AppIdentity.fromJson(json);
}

/// @nodoc
mixin _$AppIdentity {
  String get appPublicKey => throw _privateConstructorUsedError;
  String get clientId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppIdentityCopyWith<AppIdentity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppIdentityCopyWith<$Res> {
  factory $AppIdentityCopyWith(
          AppIdentity value, $Res Function(AppIdentity) then) =
      _$AppIdentityCopyWithImpl<$Res, AppIdentity>;
  @useResult
  $Res call({String appPublicKey, String clientId});
}

/// @nodoc
class _$AppIdentityCopyWithImpl<$Res, $Val extends AppIdentity>
    implements $AppIdentityCopyWith<$Res> {
  _$AppIdentityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? appPublicKey = null,
    Object? clientId = null,
  }) {
    return _then(_value.copyWith(
      appPublicKey: null == appPublicKey
          ? _value.appPublicKey
          : appPublicKey // ignore: cast_nullable_to_non_nullable
              as String,
      clientId: null == clientId
          ? _value.clientId
          : clientId // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AppIdentityImplCopyWith<$Res>
    implements $AppIdentityCopyWith<$Res> {
  factory _$$AppIdentityImplCopyWith(
          _$AppIdentityImpl value, $Res Function(_$AppIdentityImpl) then) =
      __$$AppIdentityImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String appPublicKey, String clientId});
}

/// @nodoc
class __$$AppIdentityImplCopyWithImpl<$Res>
    extends _$AppIdentityCopyWithImpl<$Res, _$AppIdentityImpl>
    implements _$$AppIdentityImplCopyWith<$Res> {
  __$$AppIdentityImplCopyWithImpl(
      _$AppIdentityImpl _value, $Res Function(_$AppIdentityImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? appPublicKey = null,
    Object? clientId = null,
  }) {
    return _then(_$AppIdentityImpl(
      appPublicKey: null == appPublicKey
          ? _value.appPublicKey
          : appPublicKey // ignore: cast_nullable_to_non_nullable
              as String,
      clientId: null == clientId
          ? _value.clientId
          : clientId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$AppIdentityImpl with DiagnosticableTreeMixin implements _AppIdentity {
  const _$AppIdentityImpl({required this.appPublicKey, required this.clientId});

  factory _$AppIdentityImpl.fromJson(Map<String, dynamic> json) =>
      _$$AppIdentityImplFromJson(json);

  @override
  final String appPublicKey;
  @override
  final String clientId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AppIdentity(appPublicKey: $appPublicKey, clientId: $clientId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'AppIdentity'))
      ..add(DiagnosticsProperty('appPublicKey', appPublicKey))
      ..add(DiagnosticsProperty('clientId', clientId));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AppIdentityImpl &&
            (identical(other.appPublicKey, appPublicKey) ||
                other.appPublicKey == appPublicKey) &&
            (identical(other.clientId, clientId) ||
                other.clientId == clientId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, appPublicKey, clientId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AppIdentityImplCopyWith<_$AppIdentityImpl> get copyWith =>
      __$$AppIdentityImplCopyWithImpl<_$AppIdentityImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$AppIdentityImplToJson(
      this,
    );
  }
}

abstract class _AppIdentity implements AppIdentity {
  const factory _AppIdentity(
      {required final String appPublicKey,
      required final String clientId}) = _$AppIdentityImpl;

  factory _AppIdentity.fromJson(Map<String, dynamic> json) =
      _$AppIdentityImpl.fromJson;

  @override
  String get appPublicKey;
  @override
  String get clientId;
  @override
  @JsonKey(ignore: true)
  _$$AppIdentityImplCopyWith<_$AppIdentityImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
