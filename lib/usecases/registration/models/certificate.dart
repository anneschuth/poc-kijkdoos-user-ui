// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:typed_data';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:user_ui/utils/uint8_list_json_converter.dart';

part 'certificate.freezed.dart';
part 'certificate.g.dart';

@freezed
class Certificate with _$Certificate {
  const factory Certificate({
    required String type,
    @Uint8ListJsonConverter() required Uint8List value,
  }) = _Certificate;

  factory Certificate.fromJson(Map<String, Object?> json) =>
      _$CertificateFromJson(json);
}
