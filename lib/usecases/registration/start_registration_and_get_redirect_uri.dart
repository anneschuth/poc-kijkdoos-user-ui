// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/models/register_app_request.dart';
import 'package:user_ui/clients/app_manager/register_app.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/env_provider.dart';
import 'package:user_ui/repositories/app_identity/app_key_pair_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';
import 'package:user_ui/usecases/registration/models/app_identity.dart';
import 'package:user_ui/usecases/helpers/start_session.dart';
import 'package:user_ui/utils/bk_encryption_helper.dart';

part 'start_registration_and_get_redirect_uri.g.dart';

@riverpod
Future<Uri> startRegistrationAndGetRedirectUri(
    StartRegistrationAndGetRedirectUriRef ref) async {
  final loggingHelper = ref.read(loggingProvider);
  if (ref.read(userSettingsRepositoryProvider).delay) {
    await Future.delayed(const Duration(milliseconds: 2000));
  }

  final appKeyPair = await ref.read(appKeyPairRepositoryProvider.future);
  if (appKeyPair == null) {
    loggingHelper.addLog(DeviceEvent.uu_11, "app key pair not found");
    throw Exception("app key pair not found");
  }

  final appManagerSelection = ref.read(appManagerSelectionRepositoryProvider);

  if (appManagerSelection == null) {
    loggingHelper.addLog(DeviceEvent.uu_12, "app manager not selected");
    throw Exception("app manager not selected");
  }

  final selectedSchemeAppManager = await ref.read(
      fetchSchemeAppManagerProvider.call(oin: appManagerSelection.oin).future);

  final discoveredServices = await ref
      .read(serviceDiscoveryClientProvider)
      .fetchDiscoveredServices(selectedSchemeAppManager.discoveryUrl);

  final sessionToken = await ref.read(
    startSessionProvider
        .call(
          sessionApiUrl: discoveredServices.getSessionApiV1(loggingHelper),
          appPublicKey: appKeyPair.publicKey,
          appPrivateKey: appKeyPair.privateKey,
          organizationPublicKey: selectedSchemeAppManager.publicKey,
        )
        .future,
  );

  final bkEncryptionHelper = BKEncryptionHelper();

  String clientId;
  if (!kIsWeb) {
    clientId = "native_app";
  } else if (ref.read(environmentProvider).type == EnvironmentType.demo) {
    clientId = "demo_web";
  } else {
    clientId = "development_web";
  }

  final appIdentity = AppIdentity(
    appPublicKey: appKeyPair.publicKey,
    clientId: clientId,
  );

  final encryptedAppIdentity = await bkEncryptionHelper.rsaEncrypt(
    json.encode(appIdentity),
    "app-identity",
    selectedSchemeAppManager.publicKey,
  );

  final registerAppRequest =
      RegisterAppRequest(encryptedAppIdentity: encryptedAppIdentity);

  final registerAppResponse = await ref.read(registerAppProvider
      .call(
          serviceUrl: discoveredServices.getRegistrationApiV1(loggingHelper),
          sessionToken: sessionToken,
          registerAppRequest: registerAppRequest)
      .future);

  final dateTimeStarted = DateTime.now();

  await ref.read(registrationRepositoryProvider.notifier).addRegistration(
        dateTimeStarted: dateTimeStarted,
        appPublicKey: appKeyPair.publicKey,
        appManagerOin: selectedSchemeAppManager.oin,
        appManagerPublicKey: selectedSchemeAppManager.publicKey,
        registrationToken: registerAppResponse.registrationToken,
      );

  // NOTE: skip app manager login when using mock
  // TODO: improve skip appManager
  if (ref.read(environmentProvider).type == EnvironmentType.mock) {
    return Uri.parse("app://vorijk.nl/activatie-voltooien");
  }

  final appManagerUiUri =
      Uri.parse(discoveredServices.getAppManagerUi(loggingHelper));

  final uri = appManagerUiUri.replace(queryParameters: {
    "registrationToken": registerAppResponse.registrationToken
  });

  return uri;
}
