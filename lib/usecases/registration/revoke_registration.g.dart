// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'revoke_registration.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$revokeRegistrationHash() =>
    r'3262769410b16300d7bec1040c5f441557f43842';

/// See also [revokeRegistration].
@ProviderFor(revokeRegistration)
final revokeRegistrationProvider = AutoDisposeFutureProvider<void>.internal(
  revokeRegistration,
  name: r'revokeRegistrationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$revokeRegistrationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef RevokeRegistrationRef = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
