// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_and_store_certificate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchAndStoreCertificateHash() =>
    r'139edbabe62479a1876ef567c7445dd7949c747b';

/// See also [fetchAndStoreCertificate].
@ProviderFor(fetchAndStoreCertificate)
final fetchAndStoreCertificateProvider =
    AutoDisposeFutureProvider<void>.internal(
  fetchAndStoreCertificate,
  name: r'fetchAndStoreCertificateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$fetchAndStoreCertificateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef FetchAndStoreCertificateRef = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
