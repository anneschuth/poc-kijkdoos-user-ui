// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/models/unregister_app_request.dart';
import 'package:user_ui/clients/app_manager/unregister_app.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/latest_completed_registration_notifier.dart';
import 'package:user_ui/repositories/app_identity/app_key_pair_repository.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';
import 'package:user_ui/usecases/helpers/start_session.dart';
import 'package:user_ui/utils/bk_encryption_helper.dart';

part 'revoke_registration.g.dart';

@riverpod
Future<void> revokeRegistration(RevokeRegistrationRef ref) async {
  final loggingHelper = ref.read(loggingProvider);
  final latestCompletedRegistration =
      await ref.read(latestCompletedRegistrationProvider.future);

  final appManagerSelection = ref.read(appManagerSelectionRepositoryProvider);

  if (appManagerSelection == null) {
    loggingHelper.addLog(DeviceEvent.uu_69, "app manager not selected");
    throw Exception("app manager not selected");
  }

  final selectedSchemeAppManager = await ref.read(
      fetchSchemeAppManagerProvider.call(oin: appManagerSelection.oin).future);

  final discoveredServices = await ref
      .read(serviceDiscoveryClientProvider)
      .fetchDiscoveredServices(selectedSchemeAppManager.discoveryUrl);

  final appKeyPair = await ref.read(appKeyPairRepositoryProvider.future);
  if (appKeyPair == null) {
    loggingHelper.addLog(DeviceEvent.uu_70, "app key pair not found");
    throw Exception("app key pair not found");
  }

  final bkEncryptionHelper = BKEncryptionHelper();

  final signature = await bkEncryptionHelper.sign(
    latestCompletedRegistration.registrationToken,
    appKeyPair.privateKey,
  );

  final unregisterAppRequest = UnregisterAppRequest(
      registrationToken: latestCompletedRegistration.registrationToken,
      signature: signature);

  final sessionToken = await ref.read(
    startSessionProvider
        .call(
          sessionApiUrl: discoveredServices.getSessionApiV1(loggingHelper),
          appPublicKey: appKeyPair.publicKey,
          appPrivateKey: appKeyPair.privateKey,
          organizationPublicKey: selectedSchemeAppManager.publicKey,
        )
        .future,
  );

  await ref.read(unregisterAppProvider
      .call(
        serviceUrl: discoveredServices.getRegistrationApiV1(loggingHelper),
        sessionToken: sessionToken,
        unregisterAppRequest: unregisterAppRequest,
      )
      .future);
}
