// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'start_registration_and_get_redirect_uri.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$startRegistrationAndGetRedirectUriHash() =>
    r'f04113535167d39f86540e530b0fabcb825e169a';

/// See also [startRegistrationAndGetRedirectUri].
@ProviderFor(startRegistrationAndGetRedirectUri)
final startRegistrationAndGetRedirectUriProvider =
    AutoDisposeFutureProvider<Uri>.internal(
  startRegistrationAndGetRedirectUri,
  name: r'startRegistrationAndGetRedirectUriProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$startRegistrationAndGetRedirectUriHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef StartRegistrationAndGetRedirectUriRef
    = AutoDisposeFutureProviderRef<Uri>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
