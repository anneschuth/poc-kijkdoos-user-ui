// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';
import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/fetch_certificate.dart';
import 'package:user_ui/clients/app_manager/models/fetch_certificate_response.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/latest_started_registration_notifier.dart';
import 'package:user_ui/repositories/app_identity/app_key_pair_repository.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';
import 'package:user_ui/entities/certificate_type.dart';
import 'package:user_ui/router/router.dart';
import 'package:user_ui/screens/features/activation/start_activation_screen.dart';
import 'package:user_ui/usecases/registration/models/certificate.dart';
import 'package:user_ui/usecases/helpers/start_session.dart';
import 'package:user_ui/utils/bk_encryption_helper.dart';

part 'fetch_and_store_certificate_helper.g.dart';

@riverpod
Future<void> fetchAndStoreCertificateHelper(
    FetchAndStoreCertificateHelperRef ref) async {
  final loggingHelper = ref.read(loggingProvider);

  if (ref.read(userSettingsRepositoryProvider).delay) {
    Future.delayed(const Duration(milliseconds: 2000));
  }

  final latestStartedRegistration =
      await ref.read(latestStartedRegistrationProvider.future);

  if (latestStartedRegistration.registrationToken.isEmpty) {
    loggingHelper.addLog(DeviceEvent.uu_24, "registrationToken not found.");
    throw Exception("registrationToken not found");
  }

  final appKeyPair = await ref.read(appKeyPairRepositoryProvider.future);
  if (appKeyPair == null) {
    loggingHelper.addLog(DeviceEvent.uu_28, "app key pair not found");
    throw Exception("app key pair not found");
  }

  if (latestStartedRegistration.appPublicKey != appKeyPair.publicKey) {
    loggingHelper.addLog(DeviceEvent.uu_29,
        "appPublicKey changed, cannot complete registration");
    throw Exception("appPublicKey changed, cannot complete registration");
  }

  final appManagerSelection = ref.read(appManagerSelectionRepositoryProvider);

  if (appManagerSelection == null) {
    loggingHelper.addLog(DeviceEvent.uu_25, "app manager not selected");
    throw Exception("app manager not selected");
  }

  final selectedSchemeAppManager = await ref.read(
      fetchSchemeAppManagerProvider.call(oin: appManagerSelection.oin).future);

  final discoveredServices = await ref
      .read(serviceDiscoveryClientProvider)
      .fetchDiscoveredServices(selectedSchemeAppManager.discoveryUrl);

  final sessionToken = await ref.read(
    startSessionProvider
        .call(
          sessionApiUrl: discoveredServices.getSessionApiV1(loggingHelper),
          appPublicKey: appKeyPair.publicKey,
          appPrivateKey: appKeyPair.privateKey,
          organizationPublicKey: selectedSchemeAppManager.publicKey,
        )
        .future,
  );

  FetchCertificateResponse fetchCertificateResponse;

  try {
    fetchCertificateResponse = await ref.read(fetchCertificateProvider
        .call(
          serviceUrl: discoveredServices.getRegistrationApiV1(loggingHelper),
          sessionToken: sessionToken,
          registrationToken: latestStartedRegistration.registrationToken,
        )
        .future);
  } catch (e) {
    ref.read(goRouterProvider).goNamed(StartActivationScreen.routeName);
    return;
  }

  final bkEncryptionHelper = BKEncryptionHelper();

  final decryptedCertificate = await bkEncryptionHelper.rsaDecrypt(
    fetchCertificateResponse.encryptedCertificate,
    "certificate",
    appKeyPair.privateKey,
  );

  final decryptedCertificateJson = json.decode(decryptedCertificate);

  final certificate = Certificate.fromJson(decryptedCertificateJson);

  final certificateType = CertificateType.values.firstWhere(
    (type) => type.value == certificate.type,
  );

  String givenName;
  DateTime expiresAt;

  if (certificateType == CertificateType.appManagerJWTCertificate) {
    String jwtAppPublicKey;

    try {
      final jwt = JWT.verify(
        String.fromCharCodes(certificate.value),
        RSAPublicKey(selectedSchemeAppManager.publicKey),
      );

      givenName = jwt.payload["given_name"];

      jwtAppPublicKey = jwt.payload["app_public_key"];

      expiresAt =
          DateTime.fromMillisecondsSinceEpoch(jwt.payload["exp"] * 1000);
    } on JWTExpiredException {
      loggingHelper.addLog(DeviceEvent.uu_30, "certificate expired");
      throw Exception("certificate expired");
    } on JWTException catch (ex) {
      loggingHelper.addLog(DeviceEvent.uu_31,
          "Invalid signature on registration result: ${ex.message}");
      throw Exception(
          "Invalid signature on registration result: ${ex.message}");
    }

    if (latestStartedRegistration.appPublicKey != jwtAppPublicKey) {
      loggingHelper.addLog(DeviceEvent.uu_32,
          "Invalid started registration result: wrong appPublicKey");
      throw Exception(
          "Invalid started registration result: wrong appPublicKey");
    }
  } else {
    loggingHelper.addLog(DeviceEvent.uu_33,
        "not implemented exception certificate type: ${certificate.type}");
    throw Exception(
        "not implemented exception certificate type: ${certificate.type}");
  }

  await ref.read(certificateRepositoryProvider.notifier).addCertificate(
        type: certificateType,
        value: certificate.value,
        givenName: givenName,
        expiresAt: expiresAt,
      );

  final latestStartedRegistrationUpdate = latestStartedRegistration.copyWith(
    givenName: givenName,
    dateTimeCompleted: DateTime.now(),
  );

  await ref
      .read(registrationRepositoryProvider.notifier)
      .updateRegistration(latestStartedRegistrationUpdate);
}
