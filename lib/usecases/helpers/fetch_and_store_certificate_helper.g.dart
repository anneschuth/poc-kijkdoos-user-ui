// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_and_store_certificate_helper.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchAndStoreCertificateHelperHash() =>
    r'2e75734721af7531888a4da8ef02ffa62004b347';

/// See also [fetchAndStoreCertificateHelper].
@ProviderFor(fetchAndStoreCertificateHelper)
final fetchAndStoreCertificateHelperProvider =
    AutoDisposeFutureProvider<void>.internal(
  fetchAndStoreCertificateHelper,
  name: r'fetchAndStoreCertificateHelperProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$fetchAndStoreCertificateHelperHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef FetchAndStoreCertificateHelperRef = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
