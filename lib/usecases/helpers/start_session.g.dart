// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'start_session.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$startSessionHash() => r'0efa455915abd7ac068f907f875813c0a6768059';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [startSession].
@ProviderFor(startSession)
const startSessionProvider = StartSessionFamily();

/// See also [startSession].
class StartSessionFamily extends Family<AsyncValue<String>> {
  /// See also [startSession].
  const StartSessionFamily();

  /// See also [startSession].
  StartSessionProvider call({
    required String sessionApiUrl,
    required String appPublicKey,
    required String appPrivateKey,
    required String organizationPublicKey,
  }) {
    return StartSessionProvider(
      sessionApiUrl: sessionApiUrl,
      appPublicKey: appPublicKey,
      appPrivateKey: appPrivateKey,
      organizationPublicKey: organizationPublicKey,
    );
  }

  @override
  StartSessionProvider getProviderOverride(
    covariant StartSessionProvider provider,
  ) {
    return call(
      sessionApiUrl: provider.sessionApiUrl,
      appPublicKey: provider.appPublicKey,
      appPrivateKey: provider.appPrivateKey,
      organizationPublicKey: provider.organizationPublicKey,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'startSessionProvider';
}

/// See also [startSession].
class StartSessionProvider extends AutoDisposeFutureProvider<String> {
  /// See also [startSession].
  StartSessionProvider({
    required String sessionApiUrl,
    required String appPublicKey,
    required String appPrivateKey,
    required String organizationPublicKey,
  }) : this._internal(
          (ref) => startSession(
            ref as StartSessionRef,
            sessionApiUrl: sessionApiUrl,
            appPublicKey: appPublicKey,
            appPrivateKey: appPrivateKey,
            organizationPublicKey: organizationPublicKey,
          ),
          from: startSessionProvider,
          name: r'startSessionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$startSessionHash,
          dependencies: StartSessionFamily._dependencies,
          allTransitiveDependencies:
              StartSessionFamily._allTransitiveDependencies,
          sessionApiUrl: sessionApiUrl,
          appPublicKey: appPublicKey,
          appPrivateKey: appPrivateKey,
          organizationPublicKey: organizationPublicKey,
        );

  StartSessionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.sessionApiUrl,
    required this.appPublicKey,
    required this.appPrivateKey,
    required this.organizationPublicKey,
  }) : super.internal();

  final String sessionApiUrl;
  final String appPublicKey;
  final String appPrivateKey;
  final String organizationPublicKey;

  @override
  Override overrideWith(
    FutureOr<String> Function(StartSessionRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: StartSessionProvider._internal(
        (ref) => create(ref as StartSessionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        sessionApiUrl: sessionApiUrl,
        appPublicKey: appPublicKey,
        appPrivateKey: appPrivateKey,
        organizationPublicKey: organizationPublicKey,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<String> createElement() {
    return _StartSessionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is StartSessionProvider &&
        other.sessionApiUrl == sessionApiUrl &&
        other.appPublicKey == appPublicKey &&
        other.appPrivateKey == appPrivateKey &&
        other.organizationPublicKey == organizationPublicKey;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, sessionApiUrl.hashCode);
    hash = _SystemHash.combine(hash, appPublicKey.hashCode);
    hash = _SystemHash.combine(hash, appPrivateKey.hashCode);
    hash = _SystemHash.combine(hash, organizationPublicKey.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin StartSessionRef on AutoDisposeFutureProviderRef<String> {
  /// The parameter `sessionApiUrl` of this provider.
  String get sessionApiUrl;

  /// The parameter `appPublicKey` of this provider.
  String get appPublicKey;

  /// The parameter `appPrivateKey` of this provider.
  String get appPrivateKey;

  /// The parameter `organizationPublicKey` of this provider.
  String get organizationPublicKey;
}

class _StartSessionProviderElement
    extends AutoDisposeFutureProviderElement<String> with StartSessionRef {
  _StartSessionProviderElement(super.provider);

  @override
  String get sessionApiUrl => (origin as StartSessionProvider).sessionApiUrl;
  @override
  String get appPublicKey => (origin as StartSessionProvider).appPublicKey;
  @override
  String get appPrivateKey => (origin as StartSessionProvider).appPrivateKey;
  @override
  String get organizationPublicKey =>
      (origin as StartSessionProvider).organizationPublicKey;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
