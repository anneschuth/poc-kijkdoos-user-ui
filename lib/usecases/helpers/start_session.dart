// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:convert';

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/session/models/challenge_request.dart';
import 'package:user_ui/clients/session/models/challenge_result.dart';
import 'package:user_ui/clients/session/models/challenge_request_message.dart';
import 'package:user_ui/clients/session/models/challenge_response.dart';
import 'package:user_ui/clients/session/session_client_live.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/utils/bk_encryption_helper.dart';

part 'start_session.g.dart';

@riverpod
Future<String> startSession(
  StartSessionRef ref, {
  required String sessionApiUrl,
  required String appPublicKey,
  required String appPrivateKey,
  required String organizationPublicKey,
}) async {
  final loggingHelper = ref.read(loggingProvider);
  final challengeRequestMessage =
      ChallengeRequestMessage(appPublicKey: appPublicKey);

  final bkEncryptionHelper = BKEncryptionHelper();

  final encryptedChallengeRequestMessage = await bkEncryptionHelper.rsaEncrypt(
    json.encode(challengeRequestMessage),
    "create-challenge",
    organizationPublicKey,
  );

  final challengeRequest =
      ChallengeRequest(request: encryptedChallengeRequestMessage);

  final challenge = await ref.read(sessionClientProvider).createChallenge(
        sessionApiUrl,
        challengeRequest,
      );

  final isVerified = await bkEncryptionHelper.verify(
    challenge.signature,
    challenge.nonce,
    organizationPublicKey,
  );

  if (!isVerified) {
    loggingHelper.addLog(DeviceEvent.uu_26, "invalid challenge signature");
    throw Exception('invalid challenge signature');
  }

  final signature = await bkEncryptionHelper.sign(
    challenge.nonce,
    appPrivateKey,
  );

  final challengeResult = ChallengeResult(
    appSignature: signature,
  );

  final encryptedChallengeResult = await bkEncryptionHelper.rsaEncrypt(
    json.encode(challengeResult),
    "complete-challenge",
    organizationPublicKey,
  );

  final challengeResponse = ChallengeResponse(
      nonce: challenge.nonce, response: encryptedChallengeResult);

  final session = await ref.read(sessionClientProvider).completeChallenge(
        sessionApiUrl,
        challengeResponse,
      );

  return session.token;
}
