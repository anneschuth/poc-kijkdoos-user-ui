import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/app_identity/app_key_pair_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/utils/bk_encryption_helper.dart';

abstract class IAppIdentityUsecase {
  Future<void> generateKeypair();
}

class AppIdentityUsecase implements IAppIdentityUsecase {
  final UserSettingsRepository userSettingsRepository;
  final Ref ref;

  AppIdentityUsecase(this.userSettingsRepository, this.ref);

  @override
  Future<void> generateKeypair() async {
    var userSettings = await userSettingsRepository.userSettings;
    if (userSettings.delay) {
      await Future.delayed(const Duration(seconds: 2));
    }

    var keyPair = await BKEncryptionHelper().generateRSAKeypair();

    await ref.read(appKeyPairRepositoryProvider.notifier).setKeyPair(
          publicKey: keyPair.publicKey,
          privateKey: keyPair.privateKey,
        );
  }
}

final appIdentityUsecaseProvider = Provider<IAppIdentityUsecase>((ref) {
  final userSettingsRepository =
      ref.read(userSettingsRepositoryProvider.notifier);

  return AppIdentityUsecase(userSettingsRepository, ref);
});
