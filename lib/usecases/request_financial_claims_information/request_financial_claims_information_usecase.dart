import 'dart:convert';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client_live.dart';
import 'package:user_ui/clients/citizen_financial_claim/models/configuration.dart';
import 'package:user_ui/clients/citizen_financial_claim/models/configuration_request.dart';
import 'package:user_ui/clients/citizen_financial_claim/models/financial_claims_information.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/app_identity/app_key_pair_repository.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';
import 'package:user_ui/repositories/certificate/get_certificate_by_id.dart';
import 'package:user_ui/repositories/certificate/latest_certificate_notifier.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_document_type_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/helpers/fetch_and_store_certificate_helper.dart';
import 'package:user_ui/usecases/helpers/start_session.dart';
import 'package:user_ui/usecases/request_financial_claims_information/models/document.dart';
import 'package:user_ui/usecases/request_financial_claims_information/models/document_type.dart';
import 'package:user_ui/usecases/request_financial_claims_information/models/envelope.dart';
import 'package:user_ui/utils/bk_encryption_helper.dart';

abstract class IRequestFinancialClaimsInformationUsecase {
  Future<void>
      createFinancialClaimsInformationRequestsForSelectedOrganizations();
  Future<void> process(
      FinancialClaimsInformationRequest financialClaimsInformationRequest);
}

class RequestFinancialClaimsInformationUsecase
    implements IRequestFinancialClaimsInformationUsecase {
  final ServiceDiscoveryClient serviceDiscoveryClient;
  final CitizenFinancialClaimClient citizenFinancialClaimClient;
  final OrganizationSelectionRepository organizationSelectionRepository;
  final SchemeOrganizationRepository schemeOrganizationRepository;
  final SchemeAppManagerRepository schemeAppManagerRepository;
  final FinancialClaimsInformationConfigurationRepository
      financialClaimsInformationConfigurationRepository;
  final FinancialClaimsInformationRequestRepository
      financialClaimsInformationRequestRepository;
  final FinancialClaimsInformationInboxRepository
      financialClaimsInformationInboxRepository;
  final UserSettingsRepository userSettingsRepository;
  final AppManagerSelectionRepository appManagerSelectionRepository;
  final RegistrationRepository registrationRepository;
  final ProviderRef ref;
  final LoggingHelper loggingHelper;

  RequestFinancialClaimsInformationUsecase(
    this.serviceDiscoveryClient,
    this.citizenFinancialClaimClient,
    this.organizationSelectionRepository,
    this.schemeOrganizationRepository,
    this.schemeAppManagerRepository,
    this.financialClaimsInformationConfigurationRepository,
    this.financialClaimsInformationRequestRepository,
    this.financialClaimsInformationInboxRepository,
    this.userSettingsRepository,
    this.appManagerSelectionRepository,
    this.registrationRepository,
    this.ref,
    this.loggingHelper,
  );

  @override
  Future<void>
      createFinancialClaimsInformationRequestsForSelectedOrganizations() async {
    final organizationSelections =
        await organizationSelectionRepository.organizationSelections;

    final selectedOrganizations = organizationSelections
        .where((organizationSelection) => organizationSelection.selected);

    if (selectedOrganizations.isEmpty) {
      loggingHelper.addLog(DeviceEvent.uu_39, "Empty organization selection.");
      throw Exception("Empty organization selection");
    }

    final dateTimeRequested = DateTime.now();

    final financialClaimsInformationRequests =
        await financialClaimsInformationRequestRepository
            .financialClaimsInformationRequests;

    for (final organization in selectedOrganizations) {
      if (financialClaimsInformationRequests
          .where((request) =>
              request.oin == organization.oin &&
              request.status !=
                  FinancialClaimsInformationRequestStatus
                      .financialClaimsInformationReceived)
          .isNotEmpty) {
        //Note: Do not add financial claims request for this organization as there is a uncompleted one
        continue;
      }

      await financialClaimsInformationRequestRepository.add(
        oin: organization.oin,
        dateTimeRequested: dateTimeRequested,
        status: FinancialClaimsInformationRequestStatus.queue,
      );
    }
  }

  Future<void> reuseOrCreateConfiguration(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 1/10 createConfiguration");

    final userSettings = await userSettingsRepository.userSettings;
    if (userSettings.delay) {
      await Future.delayed(const Duration(seconds: 3));
    }

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    var status = FinancialClaimsInformationRequestStatus.configurationCreated;

    if (financialClaimsInformationConfiguration != null) {
      if (financialClaimsInformationConfiguration.configuration != null) {
        status =
            FinancialClaimsInformationRequestStatus.encryptedEnvelopeMailed;
      } else {
        loggingHelper.addLog(
            DeviceEvent.uu_40, "previous request is in progress");
        throw Exception("previous request is in progress");
      }
    } else {
      await financialClaimsInformationConfigurationRepository.add(
        oin: financialClaimsInformationRequest.oin,
      );
    }

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(status: status);

    await financialClaimsInformationRequestRepository
        .update(financialClaimsInformationRequestUpdate);
  }

  Future<void> createDocument(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 2/10 CreateDocument");

    final userSettings = await userSettingsRepository.userSettings;
    if (userSettings.delay) {
      await Future.delayed(const Duration(seconds: 3));
    }

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    if (financialClaimsInformationConfiguration == null) {
      loggingHelper.addLog(
          DeviceEvent.uu_41, "Financial claims configuration not found");
      throw Exception("financial claims configuration not found");
    }

    final document = Document(
      type: DocumentType(
        name: "FINANCIAL_CLAIMS_INFORMATION_REQUEST",
      ),
    );

    final financialClaimsInformationConfigurationUpdate =
        financialClaimsInformationConfiguration.copyWith(
            document: json.encode(document));

    await financialClaimsInformationConfigurationRepository
        .update(financialClaimsInformationConfigurationUpdate);

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus.documentCreated);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
  }

  Future<void> createDocumentSignature(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 3/10 CreateDocumentSignature");

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    if (financialClaimsInformationConfiguration == null) {
      loggingHelper.addLog(
          DeviceEvent.uu_42, "financial claims configuration not found.");
      throw Exception("financial claims configuration not found");
    }

    if (financialClaimsInformationConfiguration.document == null) {
      loggingHelper.addLog(DeviceEvent.uu_43, "document not found");
      throw Exception("document not found");
    }

    final documentJson =
        json.decode(financialClaimsInformationConfiguration.document!);
    final document = Document.fromJson(documentJson);

    final appKeyPair = await ref.read(appKeyPairRepositoryProvider.future);
    if (appKeyPair == null) {
      loggingHelper.addLog(DeviceEvent.uu_15, "app key pair not found");
      throw Exception("app key pair not found");
    }

    final documentSignature = await BKEncryptionHelper().sign(
      json.encode(document),
      appKeyPair.privateKey,
    );

    final financialClaimsInformationConfigurationUpdate =
        financialClaimsInformationConfiguration.copyWith(
            documentSignature: documentSignature);

    await financialClaimsInformationConfigurationRepository
        .update(financialClaimsInformationConfigurationUpdate);

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus.documentSigned);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
  }

  Future<void> addCertificate(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 4/10 addCertificate");

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    if (financialClaimsInformationConfiguration == null) {
      loggingHelper.addLog(
          DeviceEvent.uu_27, "financial claims configuration not found");
      throw Exception("financial claims configuration not found");
    }

    final latestCertificate = await ref.read(latestCertificateProvider.future);

    if (latestCertificate == null) {
      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
              status:
                  FinancialClaimsInformationRequestStatus.certificateInvalid);

      await financialClaimsInformationRequestRepository.update(
        financialClaimsInformationRequestUpdate,
      );

      return;
    }

    final financialClaimsInformationConfigurationUpdate =
        financialClaimsInformationConfiguration.copyWith(
      certificateId: latestCertificate.id,
    );

    await financialClaimsInformationConfigurationRepository
        .update(financialClaimsInformationConfigurationUpdate);

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus.certificateAdded);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
  }

  Future<void> verifyCertificate(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 5/10 verifyCertificate");

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    if (financialClaimsInformationConfiguration == null) {
      loggingHelper.addLog(
          DeviceEvent.uu_45, "financial claims configuration not found");
      throw Exception("financial claims configuration not found");
    }

    if (financialClaimsInformationConfiguration.certificateId == null) {
      loggingHelper.addLog(DeviceEvent.uu_44, "certificateId not found");
      throw Exception("certificateId not found");
    }

    final certificate = await ref.read(getCertificateByIdProvider
        .call(id: financialClaimsInformationConfiguration.certificateId!)
        .future);

    if (certificate.deemedExpiredBySourceOrganization ||
        DateTime.now().isAfter(certificate.expiresAt)) {
      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
        status: FinancialClaimsInformationRequestStatus.certificateInvalid,
      );

      await financialClaimsInformationRequestRepository.update(
        financialClaimsInformationRequestUpdate,
      );

      return;
    }

    final financialClaimsInformationConfigurationUpdate =
        financialClaimsInformationConfiguration.copyWith(
      certificateId: certificate.id,
    );

    await financialClaimsInformationConfigurationRepository
        .update(financialClaimsInformationConfigurationUpdate);

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status:
                FinancialClaimsInformationRequestStatus.certificateValidated);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
  }

  Future<void> fetchNewCertificate(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 6/10 fetchNewCertificate");

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    if (financialClaimsInformationConfiguration == null) {
      loggingHelper.addLog(
          DeviceEvent.uu_46, "financial claims configuration not found");
      throw Exception("financial claims configuration not found");
    }

    await ref.read(fetchAndStoreCertificateHelperProvider.future);

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus.documentSigned);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
  }

  Future<void> createEnvelope(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 7/10 CreateEnvelope");

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    if (financialClaimsInformationConfiguration == null) {
      loggingHelper.addLog(
          DeviceEvent.uu_50, "financial claims configuration not found");
      throw Exception("financial claims configuration not found");
    }

    if (financialClaimsInformationConfiguration.document == null) {
      loggingHelper.addLog(DeviceEvent.uu_51, "document not found");
      throw Exception("document not found");
    }

    final documentJson =
        json.decode(financialClaimsInformationConfiguration.document!);

    final document = Document.fromJson(documentJson);

    if (financialClaimsInformationConfiguration.documentSignature == null) {
      loggingHelper.addLog(DeviceEvent.uu_52, "documentSignature not found");
      throw Exception("documentSignature not found");
    }

    if (financialClaimsInformationConfiguration.certificateId == null) {
      loggingHelper.addLog(DeviceEvent.uu_53, "certificateId not found");
      throw Exception("certificateId not found");
    }

    final certificate = await ref.read(
      getCertificateByIdProvider
          .call(
            id: financialClaimsInformationConfiguration.certificateId!,
          )
          .future,
    );

    final envelope = Envelope(
      document: document,
      documentSignature:
          financialClaimsInformationConfiguration.documentSignature!,
      certificateType: certificate.type,
      certificate: certificate.value,
    );

    final financialClaimsInformationConfigurationUpdate =
        financialClaimsInformationConfiguration.copyWith(
            envelope: json.encode(envelope));

    await financialClaimsInformationConfigurationRepository
        .update(financialClaimsInformationConfigurationUpdate);

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus.envelopeCreated);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
  }

  Future<void> createConfigurationRequest(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 8/10 createConfigurationRequest");

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    if (financialClaimsInformationConfiguration == null) {
      loggingHelper.addLog(
          DeviceEvent.uu_55, "financial claims configuration not found");
      throw Exception("financial claims configuration not found");
    }

    if (financialClaimsInformationConfiguration.envelope == null) {
      loggingHelper.addLog(DeviceEvent.uu_56, "envelope not found");
      throw Exception("envelope not found");
    }

    final organizations = await schemeOrganizationRepository.organizations;

    // TODO: what if the organization is no longer in the scheme
    final organization = organizations.firstWhere(
        (l) => l.oin == financialClaimsInformationConfiguration.oin);

    final sourceOrganizationPublicKey = organization.publicKey;

    final envelopeEncrypted = await BKEncryptionHelper().rsaEncrypt(
      financialClaimsInformationConfiguration.envelope!,
      "encrypted-envelope",
      sourceOrganizationPublicKey,
    );

    final configurationRequest = ConfigurationRequest(
      encryptedEnvelope: envelopeEncrypted,
    );

    final financialClaimsInformationConfigurationUpdate =
        financialClaimsInformationConfiguration.copyWith(
            configurationRequest: json.encode(configurationRequest));

    await financialClaimsInformationConfigurationRepository
        .update(financialClaimsInformationConfigurationUpdate);

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus.envelopeEncrypted);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
  }

  Future<void> configureClaimsRequest(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 9/10 ConfigureClaimsRequest");

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    if (financialClaimsInformationConfiguration == null) {
      loggingHelper.addLog(
          DeviceEvent.uu_57, "financial claims configuration not found");
      throw Exception("financial claims configuration not found");
    }

    if (financialClaimsInformationConfiguration.configurationRequest == null) {
      loggingHelper.addLog(DeviceEvent.uu_58, "encryptedEnvelope not found");
      throw Exception("encryptedEnvelope not found");
    }

    final configurationRequestJson = json
        .decode(financialClaimsInformationConfiguration.configurationRequest!);
    final configurationRequest =
        ConfigurationRequest.fromJson(configurationRequestJson);

    final organizations = await schemeOrganizationRepository.organizations;

    // TODO: what if the organization is no longer in the scheme
    final organization = organizations.firstWhere(
        (l) => l.oin == financialClaimsInformationConfiguration.oin);

    final appKeyPair = await ref.read(appKeyPairRepositoryProvider.future);
    if (appKeyPair == null) {
      loggingHelper.addLog(DeviceEvent.uu_59, "app key pair not found");
      throw Exception("app key pair not found");
    }

    final discoveredServices = await serviceDiscoveryClient
        .fetchDiscoveredServices(organization.discoveryUrl);

    final sessionToken = await ref.read(
      startSessionProvider
          .call(
            sessionApiUrl: discoveredServices.getSessionApiV1(loggingHelper),
            appPublicKey: appKeyPair.publicKey,
            appPrivateKey: appKeyPair.privateKey,
            organizationPublicKey: organization.publicKey,
          )
          .future,
    );

    Configuration configuration;
    try {
      configuration = await citizenFinancialClaimClient.configureClaimsRequest(
        discoveredServices.getFinancialClaimRequestApiV3(loggingHelper),
        sessionToken,
        configurationRequest,
      );
    } on AppCertificateExpiredException {
      final certificate = await ref.read(getCertificateByIdProvider
          .call(id: financialClaimsInformationConfiguration.certificateId!)
          .future);

      final certificateUpdate =
          certificate.copyWith(deemedExpiredBySourceOrganization: true);

      await ref
          .read(certificateRepositoryProvider.notifier)
          .updateCertificate(certificateUpdate);

      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
              status: FinancialClaimsInformationRequestStatus.documentSigned);

      await financialClaimsInformationRequestRepository.update(
        financialClaimsInformationRequestUpdate,
      );
      return;
    }

    final financialClaimsInformationConfigurationUpdate =
        financialClaimsInformationConfiguration.copyWith(
            configuration: json.encode(configuration));

    await financialClaimsInformationConfigurationRepository
        .update(financialClaimsInformationConfigurationUpdate);

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus
                .encryptedEnvelopeMailed);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
  }

  Future<void> requestFinancialClaimsInformation(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "     ${financialClaimsInformationRequest.id} 10/10 RequestFinancialClaimsInformation");

    final userSettings = await userSettingsRepository.userSettings;
    if (userSettings.delay) {
      final random = Random();
      await Future.delayed(Duration(seconds: 1 + random.nextInt(6)));
    }

    final financialClaimsInformationConfiguration =
        await financialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(
                financialClaimsInformationRequest.oin);

    if (financialClaimsInformationConfiguration == null) {
      loggingHelper.addLog(
          DeviceEvent.uu_60, "financial claims configuration not found");
      throw Exception("financial claims configuration not found");
    }

    if (financialClaimsInformationConfiguration.configuration == null) {
      loggingHelper.addLog(DeviceEvent.uu_61, "configuration not found");
      throw Exception("configuration not found");
    }

    final configurationJson =
        json.decode(financialClaimsInformationConfiguration.configuration!);
    final configuration = Configuration.fromJson(configurationJson);

    final organizations = await schemeOrganizationRepository.organizations;

    // TODO: what if the organization is no longer in the scheme
    final organization = organizations
        .firstWhere((l) => l.oin == financialClaimsInformationRequest.oin);

    final appKeyPair = await ref.read(appKeyPairRepositoryProvider.future);
    if (appKeyPair == null) {
      loggingHelper.addLog(DeviceEvent.uu_62, "app key pair not found");
      throw Exception("app key pair not found");
    }

    final discoveredServices = await serviceDiscoveryClient
        .fetchDiscoveredServices(organization.discoveryUrl);

    final sessionToken = await ref.read(
      startSessionProvider
          .call(
            sessionApiUrl: discoveredServices.getSessionApiV1(loggingHelper),
            appPublicKey: appKeyPair.publicKey,
            appPrivateKey: appKeyPair.privateKey,
            organizationPublicKey: organization.publicKey,
          )
          .future,
    );

    FinancialClaimsInformation financialClaimsInformation;
    try {
      financialClaimsInformation =
          await citizenFinancialClaimClient.requestFinancialClaimsInformation(
        discoveredServices.getFinancialClaimRequestApiV3(loggingHelper),
        sessionToken,
        configuration.token,
      );
    } catch (_) {
      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
              status:
                  FinancialClaimsInformationRequestStatus.envelopeEncrypted);

      await financialClaimsInformationRequestRepository.update(
        financialClaimsInformationRequestUpdate,
      );
      return;
    }

    final dateTimeReceived = DateTime.now();

    await financialClaimsInformationInboxRepository.add(
      oin: financialClaimsInformationRequest.oin,
      dateTimeRequested: financialClaimsInformationRequest.dateTimeRequested,
      dateTimeReceived: dateTimeReceived,
      encryptedFinancialClaimsInformationDocument: financialClaimsInformation
          .encryptedFinancialClaimsInformationDocument,
    );

    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus
                .financialClaimsInformationReceived);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
  }

  @override
  Future<void> process(
      FinancialClaimsInformationRequest
          financialClaimsInformationRequest) async {
    debugPrint(
        "processFinancialClaimsInformationRequest: ${financialClaimsInformationRequest.id}, ${financialClaimsInformationRequest.status}, ${financialClaimsInformationRequest.lock};");

    switch (financialClaimsInformationRequest.status) {
      case FinancialClaimsInformationRequestStatus.queue:
        await reuseOrCreateConfiguration(financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus.configurationCreated:
        await createDocument(financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus.documentCreated:
        await createDocumentSignature(financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus.documentSigned:
        await addCertificate(financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus.certificateAdded:
        await verifyCertificate(financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus.certificateInvalid:
        await fetchNewCertificate(financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus.certificateValidated:
        await createEnvelope(financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus.envelopeCreated:
        await createConfigurationRequest(financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus.envelopeEncrypted:
        await configureClaimsRequest(financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus.encryptedEnvelopeMailed:
        await requestFinancialClaimsInformation(
            financialClaimsInformationRequest);
        break;
      case FinancialClaimsInformationRequestStatus
            .financialClaimsInformationReceived:
        loggingHelper.addLog(DeviceEvent.uu_63,
            "financial claims request with status 'financial claims received' should not be processed.");
        throw Exception(
            "financial claims request with status 'financial claims received' should not be processed.");
    }
  }
}

final requestFinancialClaimsInformationUsecaseProvider =
    Provider<IRequestFinancialClaimsInformationUsecase>((ref) {
  final serviceDiscoveryClient = ref.read(serviceDiscoveryClientProvider);
  final citizenFinancialClaimClient =
      ref.read(citizenFinancialClaimClientProvider);

  final organizationSelectionRepository =
      ref.read(organizationSelectionRepositoryProvider.notifier);

  final schemeOrganizationRepository =
      ref.read(schemeOrganizationRepositoryProvider.notifier);

  final schemeAppManagerRepository =
      ref.read(schemeAppManagerRepositoryProvider.notifier);

  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final financialClaimsInformationInboxRepository =
      ref.read(financialClaimsInformationInboxRepositoryProvider.notifier);

  final userSettingsRepository =
      ref.read(userSettingsRepositoryProvider.notifier);

  final appManagerSelectionRepository =
      ref.read(appManagerSelectionRepositoryProvider.notifier);

  final registrationRepository =
      ref.read(registrationRepositoryProvider.notifier);

  final logging = ref.watch(loggingProvider);

  return RequestFinancialClaimsInformationUsecase(
    serviceDiscoveryClient,
    citizenFinancialClaimClient,
    organizationSelectionRepository,
    schemeOrganizationRepository,
    schemeAppManagerRepository,
    financialClaimsInformationConfigurationRepository,
    financialClaimsInformationRequestRepository,
    financialClaimsInformationInboxRepository,
    userSettingsRepository,
    appManagerSelectionRepository,
    registrationRepository,
    ref,
    logging,
  );
});
