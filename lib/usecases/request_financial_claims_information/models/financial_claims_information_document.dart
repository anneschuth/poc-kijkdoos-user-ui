// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class FinancialClaimsInformationDocument {
  String type;
  FinancialClaimsInformationDocumentBody body;

  FinancialClaimsInformationDocument({
    required this.type,
    required this.body,
  });

  factory FinancialClaimsInformationDocument.fromJson(
      Map<String, dynamic> jsonObject) {
    return FinancialClaimsInformationDocument(
      type: jsonObject["type"],
      body: FinancialClaimsInformationDocumentBody.fromJson(jsonObject["body"]),
    );
  }

  Map<String, dynamic> toJson() => {
        'type': type,
        'body': body,
      };
}

class FinancialClaimsInformationDocumentBody {
  int total;

  FinancialClaimsInformationDocumentBody({
    required this.total,
  });

  factory FinancialClaimsInformationDocumentBody.fromJson(
      Map<String, dynamic> jsonObject) {
    return FinancialClaimsInformationDocumentBody(
      total: jsonObject["total"],
    );
  }

  Map<String, dynamic> toJson() => {
        'total': total,
      };
}
