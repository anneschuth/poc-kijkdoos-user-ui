// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class FinancialClaimsInformationDocument {
  String type;
  String version;
  Body body;

  FinancialClaimsInformationDocument({
    required this.type,
    required this.version,
    required this.body,
  });

  factory FinancialClaimsInformationDocument.fromJson(
      Map<String, dynamic> jsonObject) {
    return FinancialClaimsInformationDocument(
      type: jsonObject["type"],
      version: jsonObject["version"],
      body: Body.fromJson(jsonObject["body"]),
    );
  }

  Map<String, dynamic> toJson() => {
        'type': type,
        'version': version,
        'body': body,
      };
}

class Body {
  String aangeleverdDoor;
  DateTime documentDatumtijd;
  String bsn;
  List<FinancieleZaak> financieleZaken = [];

  Body({
    required this.aangeleverdDoor,
    required this.documentDatumtijd,
    required this.bsn,
    required this.financieleZaken,
  });

  factory Body.fromJson(Map<String, dynamic> json) {
    var financieleZakenList = (json["financiele_zaken"] ?? []) as List;

    return Body(
      aangeleverdDoor: json['aangeleverd_door'],
      documentDatumtijd: DateTime.parse(json['document_datumtijd']),
      bsn: json['bsn'],
      financieleZaken:
          financieleZakenList.map((i) => FinancieleZaak.fromJson(i)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aangeleverd_door'] = aangeleverdDoor;
    data['document_datumtijd'] = documentDatumtijd.toString();
    data['bsn'] = bsn;
    data['financiele_zaken'] = financieleZaken.map((v) => v.toJson()).toList();
    return data;
  }
}

class FinancieleZaak {
  String zaakkenmerk;
  String bsn;
  int totaalOpgelegd;
  int totaalReedsBetaald;
  int saldo;
  DateTime saldoDatumtijd;
  List<Event> gebeurtenissen = [];
  List<String> achterstanden = [];

  FinancieleZaak({
    required this.zaakkenmerk,
    required this.bsn,
    required this.totaalOpgelegd,
    required this.totaalReedsBetaald,
    required this.saldo,
    required this.saldoDatumtijd,
    required this.gebeurtenissen,
    required this.achterstanden,
  });

  factory FinancieleZaak.fromJson(Map<String, dynamic> json) {
    var gebeurtenissenList = (json["gebeurtenissen"] ?? []) as List;
    var achterstandenList = (json["achterstanden"] ?? []) as List;

    return FinancieleZaak(
      zaakkenmerk: json['zaakkenmerk'],
      bsn: json['bsn'],
      totaalOpgelegd: json['totaal_opgelegd'],
      totaalReedsBetaald: json['totaal_reeds_betaald'],
      saldo: json['saldo'],
      saldoDatumtijd: DateTime.parse(json['saldo_datumtijd']),
      gebeurtenissen: gebeurtenissenList.map((i) => Event.fromJson(i)).toList(),
      achterstanden: achterstandenList.cast<String>(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['zaakkenmerk'] = zaakkenmerk;
    data['bsn'] = bsn;
    data['totaal_opgelegd'] = totaalOpgelegd;
    data['totaal_reeds_betaald'] = totaalReedsBetaald;
    data['saldo'] = saldo;
    data['saldo_datumtijd'] = saldoDatumtijd.toString();
    data['gebeurtenissen'] = gebeurtenissen.map((v) => v.toJson()).toList();
    data['achterstanden'] = achterstanden;
    return data;
  }
}

abstract class Event {
  String gebeurtenisType;
  String gebeurtenisKenmerk;
  DateTime datumtijdGebeurtenis;

  Event({
    required this.gebeurtenisType,
    required this.gebeurtenisKenmerk,
    required this.datumtijdGebeurtenis,
  });

  factory Event.fromJson(Map<String, dynamic> json) {
    switch (json['gebeurtenis_type']) {
      case "FinancieleVerplichtingOpgelegd":
        return FinancieleVerplichtingOpgelegd.fromJson(json);
      case "BetalingsverplichtingOpgelegd":
        return BetalingsverplichtingOpgelegd.fromJson(json);
      case "BetalingsverplichtingIngetrokken":
        return BetalingsverplichtingIngetrokken.fromJson(json);
      case "BetalingVerwerkt":
        return BetalingVerwerkt.fromJson(json);
      default:
        // Logging done at factory usage
        throw Exception(
            "unknown gebeurtenis_type: ${json['gebeurtenis_type']}");
    }
  }

  Map<String, dynamic> toJson() {
    switch (runtimeType) {
      case FinancieleVerplichtingOpgelegd:
        return (this as FinancieleVerplichtingOpgelegd).toJson();
      case BetalingsverplichtingOpgelegd:
        return (this as BetalingsverplichtingOpgelegd).toJson();
      case BetalingsverplichtingIngetrokken:
        return (this as BetalingsverplichtingIngetrokken).toJson();
      case BetalingVerwerkt:
        return (this as BetalingVerwerkt).toJson();
      default:
        // Logging done at factory usage
        throw Exception("unkown event");
    }
  }
}

class FinancieleVerplichtingOpgelegd extends Event {
  DateTime datumtijdOpgelegd;
  String zaakkenmerk;
  String bsn;
  bool primaireVerplichting;
  String type;
  String categorie;
  int bedrag;
  String omschrijving;
  String juridischeGrondslagOmschrijving;
  String juridischeGrondslagBron;
  String opgelegdDoor;
  String uitgevoerdDoor;

  FinancieleVerplichtingOpgelegd({
    required super.gebeurtenisType,
    required super.gebeurtenisKenmerk,
    required super.datumtijdGebeurtenis,
    required this.datumtijdOpgelegd,
    required this.zaakkenmerk,
    required this.bsn,
    required this.primaireVerplichting,
    required this.type,
    required this.categorie,
    required this.bedrag,
    required this.omschrijving,
    required this.juridischeGrondslagOmschrijving,
    required this.juridischeGrondslagBron,
    required this.opgelegdDoor,
    required this.uitgevoerdDoor,
  });

  factory FinancieleVerplichtingOpgelegd.fromJson(Map<String, dynamic> json) {
    return FinancieleVerplichtingOpgelegd(
      gebeurtenisType: json['gebeurtenis_type'],
      gebeurtenisKenmerk: json['gebeurtenis_kenmerk'],
      datumtijdGebeurtenis: DateTime.parse(json['datumtijd_gebeurtenis']),
      datumtijdOpgelegd: DateTime.parse(json['datumtijd_opgelegd']),
      zaakkenmerk: json['zaakkenmerk'],
      bsn: json['bsn'],
      primaireVerplichting: json['primaire_verplichting'],
      type: json['type'],
      categorie: json['categorie'],
      bedrag: json['bedrag'],
      omschrijving: json['omschrijving'],
      juridischeGrondslagOmschrijving:
          json['juridische_grondslag_omschrijving'],
      juridischeGrondslagBron: json['juridische_grondslag_bron'],
      opgelegdDoor: json['opgelegd_door'],
      uitgevoerdDoor: json['uitgevoerd_door'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    var data = <String, dynamic>{};
    data['gebeurtenis_type'] = gebeurtenisType;
    data['gebeurtenis_kenmerk'] = gebeurtenisKenmerk;
    data['datumtijd_gebeurtenis'] = datumtijdGebeurtenis.toString();
    data['datumtijd_opgelegd'] = datumtijdOpgelegd.toString();
    data['zaakkenmerk'] = zaakkenmerk;
    data['bsn'] = bsn;
    data['primaire_verplichting'] = primaireVerplichting;
    data['type'] = type;
    data['categorie'] = categorie;
    data['bedrag'] = bedrag;
    data['omschrijving'] = omschrijving;
    data['juridische_grondslag_omschrijving'] = juridischeGrondslagOmschrijving;
    data['juridische_grondslag_bron'] = juridischeGrondslagBron;
    data['opgelegd_door'] = opgelegdDoor;
    data['uitgevoerd_door'] = uitgevoerdDoor;
    return data;
  }
}

class BetalingsverplichtingOpgelegd extends Event {
  DateTime datumtijdOpgelegd;
  String zaakkenmerk;
  String bsn;
  int bedrag;
  String betaalwijze;
  String teBetalenAan;
  String rekeningnummer;
  String rekeningnummerTenaamstelling;
  String betalingskenmerk;
  DateTime vervaldatum;

  BetalingsverplichtingOpgelegd({
    required super.gebeurtenisType,
    required super.gebeurtenisKenmerk,
    required super.datumtijdGebeurtenis,
    required this.datumtijdOpgelegd,
    required this.zaakkenmerk,
    required this.bsn,
    required this.bedrag,
    required this.betaalwijze,
    required this.teBetalenAan,
    required this.rekeningnummer,
    required this.rekeningnummerTenaamstelling,
    required this.betalingskenmerk,
    required this.vervaldatum,
  });

  factory BetalingsverplichtingOpgelegd.fromJson(Map<String, dynamic> json) {
    return BetalingsverplichtingOpgelegd(
      gebeurtenisType: json['gebeurtenis_type'],
      gebeurtenisKenmerk: json['gebeurtenis_kenmerk'],
      datumtijdGebeurtenis: DateTime.parse(json['datumtijd_gebeurtenis']),
      datumtijdOpgelegd: DateTime.parse(json['datumtijd_opgelegd']),
      zaakkenmerk: json['zaakkenmerk'],
      bsn: json['bsn'],
      bedrag: json['bedrag'],
      betaalwijze: json['betaalwijze'],
      teBetalenAan: json['te_betalen_aan'],
      rekeningnummer: json['rekeningnummer'],
      rekeningnummerTenaamstelling: json['rekeningnummer_tenaamstelling'],
      betalingskenmerk: json['betalingskenmerk'],
      vervaldatum: DateTime.parse(json['vervaldatum']),
    );
  }

  @override
  Map<String, dynamic> toJson() {
    var data = <String, dynamic>{};
    data['gebeurtenis_type'] = gebeurtenisType;
    data['gebeurtenis_kenmerk'] = gebeurtenisKenmerk;
    data['datumtijd_gebeurtenis'] = datumtijdGebeurtenis.toString();
    data['datumtijd_opgelegd'] = datumtijdOpgelegd.toString();
    data['zaakkenmerk'] = zaakkenmerk;
    data['bsn'] = bsn;
    data['bedrag'] = bedrag;
    data['betaalwijze'] = betaalwijze;
    data['te_betalen_aan'] = teBetalenAan;
    data['rekeningnummer'] = rekeningnummer;
    data['rekeningnummer_tenaamstelling'] = rekeningnummerTenaamstelling;
    data['betalingskenmerk'] = betalingskenmerk;
    data['vervaldatum'] = vervaldatum.toString();
    return data;
  }
}

class BetalingsverplichtingIngetrokken extends Event {
  DateTime datumtijdIngetrokken;
  String ingetrokkenGebeurtenisKenmerk;

  BetalingsverplichtingIngetrokken({
    required super.gebeurtenisType,
    required super.gebeurtenisKenmerk,
    required super.datumtijdGebeurtenis,
    required this.datumtijdIngetrokken,
    required this.ingetrokkenGebeurtenisKenmerk,
  });

  factory BetalingsverplichtingIngetrokken.fromJson(Map<String, dynamic> json) {
    return BetalingsverplichtingIngetrokken(
      gebeurtenisType: json['gebeurtenis_type'],
      gebeurtenisKenmerk: json['gebeurtenis_kenmerk'],
      datumtijdGebeurtenis: DateTime.parse(json['datumtijd_gebeurtenis']),
      datumtijdIngetrokken: DateTime.parse(json['datumtijd_ingetrokken']),
      ingetrokkenGebeurtenisKenmerk: json['ingetrokken_gebeurtenis_kenmerk'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    var data = <String, dynamic>{};
    data['gebeurtenis_type'] = gebeurtenisType;
    data['gebeurtenis_kenmerk'] = gebeurtenisKenmerk;
    data['datumtijd_gebeurtenis'] = datumtijdGebeurtenis.toString();
    data['datumtijd_ingetrokken'] = datumtijdIngetrokken.toString();
    data['ingetrokken_gebeurtenis_kenmerk'] = ingetrokkenGebeurtenisKenmerk;
    return data;
  }
}

class BetalingVerwerkt extends Event {
  DateTime datumtijdVerwerkt;
  String betalingskenmerk;
  DateTime datumtijdOntvangen;
  String ontvangenDoor;
  String verwerktDoor;
  int bedrag;

  BetalingVerwerkt({
    required super.gebeurtenisType,
    required super.gebeurtenisKenmerk,
    required super.datumtijdGebeurtenis,
    required this.datumtijdVerwerkt,
    required this.betalingskenmerk,
    required this.datumtijdOntvangen,
    required this.ontvangenDoor,
    required this.verwerktDoor,
    required this.bedrag,
  });

  factory BetalingVerwerkt.fromJson(Map<String, dynamic> json) {
    return BetalingVerwerkt(
      gebeurtenisType: json['gebeurtenis_type'],
      gebeurtenisKenmerk: json['gebeurtenis_kenmerk'],
      datumtijdGebeurtenis: DateTime.parse(json['datumtijd_gebeurtenis']),
      datumtijdVerwerkt: DateTime.parse(json['datumtijd_verwerkt']),
      betalingskenmerk: json['betalingskenmerk'],
      datumtijdOntvangen: DateTime.parse(json['datumtijd_ontvangen']),
      ontvangenDoor: json['ontvangen_door'],
      verwerktDoor: json['verwerkt_door'],
      bedrag: json['bedrag'],
    );
  }

  @override
  Map<String, dynamic> toJson() {
    var data = <String, dynamic>{};
    data['gebeurtenis_type'] = gebeurtenisType;
    data['gebeurtenis_kenmerk'] = gebeurtenisKenmerk;
    data['datumtijd_gebeurtenis'] = datumtijdGebeurtenis.toString();
    data['datumtijd_verwerkt'] = datumtijdVerwerkt.toString();
    data['betalingskenmerk'] = betalingskenmerk;
    data['datumtijd_ontvangen'] = datumtijdOntvangen.toString();
    data['ontvangen_door'] = ontvangenDoor;
    data['verwerkt_door'] = verwerktDoor;
    data['bedrag'] = bedrag;
    return data;
  }
}
