// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class FinancialClaimsInformationDocumentV2 {
  String type;
  String version;
  FinancialClaimsInformationDocumentV2Body body;

  FinancialClaimsInformationDocumentV2({
    required this.type,
    required this.version,
    required this.body,
  });

  factory FinancialClaimsInformationDocumentV2.fromJson(
      Map<String, dynamic> jsonObject) {
    return FinancialClaimsInformationDocumentV2(
      type: jsonObject["type"],
      version: jsonObject["version"],
      body:
          FinancialClaimsInformationDocumentV2Body.fromJson(jsonObject["body"]),
    );
  }

  Map<String, dynamic> toJson() => {
        'type': type,
        'version': version,
        'body': body,
      };
}

class FinancialClaimsInformationDocumentV2Body {
  String aangeleverdDoor;
  DateTime documentDatumtijd;
  String bsn;
  List<FinancialClaimsInformationDocumentV2FinancieleZaken> financieleZaken =
      [];

  FinancialClaimsInformationDocumentV2Body(
      {required this.aangeleverdDoor,
      required this.documentDatumtijd,
      required this.bsn,
      required this.financieleZaken});

  factory FinancialClaimsInformationDocumentV2Body.fromJson(
      Map<String, dynamic> json) {
    var financieleZakenList = (json["financiele_zaken"] ?? []) as List;

    return FinancialClaimsInformationDocumentV2Body(
      aangeleverdDoor: json['aangeleverd_door'],
      documentDatumtijd: DateTime.parse(json['document_datumtijd']),
      bsn: json['bsn'],
      financieleZaken: financieleZakenList
          .map((i) =>
              FinancialClaimsInformationDocumentV2FinancieleZaken.fromJson(i))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['aangeleverd_door'] = aangeleverdDoor;
    data['document_datumtijd'] = documentDatumtijd.toString();
    data['bsn'] = bsn;
    data['financiele_zaken'] = financieleZaken.map((v) => v.toJson()).toList();
    return data;
  }
}

class FinancialClaimsInformationDocumentV2FinancieleZaken {
  String zaakkenmerk;
  String omschrijving;
  int totaalOpgelegd;
  int totaalReedsBetaald;
  int saldo;
  DateTime saldoDatumtijd;
  String valuta;
  List<FinancialClaimsInformationDocumentV2FinancieleVerplichtingen>
      financieleVerplichtingen = [];
  List<FinancialClaimsInformationDocumentV2Betalingsverplichtingen>
      betalingsverplichtingen = [];
  List<FinancialClaimsInformationDocumentV2Betalingen> betalingen = [];
  List<String> achterstanden = [];

  FinancialClaimsInformationDocumentV2FinancieleZaken(
      {required this.zaakkenmerk,
      required this.omschrijving,
      required this.totaalOpgelegd,
      required this.totaalReedsBetaald,
      required this.saldo,
      required this.saldoDatumtijd,
      required this.valuta,
      required this.financieleVerplichtingen,
      required this.betalingsverplichtingen,
      required this.betalingen,
      required this.achterstanden});

  factory FinancialClaimsInformationDocumentV2FinancieleZaken.fromJson(
      Map<String, dynamic> json) {
    var financieleVerplichtingenList =
        (json["financiele_verplichtingen"] ?? []) as List;
    var betalingsverplichtingenList =
        (json["betalingsverplichtingen"] ?? []) as List;
    var betalingenList = (json["betalingen"] ?? []) as List;

    return FinancialClaimsInformationDocumentV2FinancieleZaken(
      zaakkenmerk: json['zaakkenmerk'],
      omschrijving: json['omschrijving'],
      totaalOpgelegd: json['totaal_opgelegd'],
      totaalReedsBetaald: json['totaal_reeds_betaald'],
      saldo: json['saldo'],
      saldoDatumtijd: DateTime.parse(json['saldo_datumtijd']),
      valuta: json['valuta'],
      financieleVerplichtingen: financieleVerplichtingenList
          .map((i) =>
              FinancialClaimsInformationDocumentV2FinancieleVerplichtingen
                  .fromJson(i))
          .toList(),
      betalingsverplichtingen: betalingsverplichtingenList
          .map((i) =>
              FinancialClaimsInformationDocumentV2Betalingsverplichtingen
                  .fromJson(i))
          .toList(),
      betalingen: betalingenList
          .map(
              (i) => FinancialClaimsInformationDocumentV2Betalingen.fromJson(i))
          .toList(),
      achterstanden: json['achterstanden'].cast<String>(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['zaakkenmerk'] = zaakkenmerk;
    data['omschrijving'] = omschrijving;
    data['totaal_opgelegd'] = totaalOpgelegd;
    data['totaal_reeds_betaald'] = totaalReedsBetaald;
    data['saldo'] = saldo;
    data['saldo_datumtijd'] = saldoDatumtijd.toString();
    data['valuta'] = valuta;
    data['financiele_verplichtingen'] =
        financieleVerplichtingen.map((v) => v.toJson()).toList();
    data['betalingsverplichtingen'] =
        betalingsverplichtingen.map((v) => v.toJson()).toList();
    data['betalingen'] = betalingen.map((v) => v.toJson()).toList();
    data['achterstanden'] = achterstanden;
    return data;
  }
}

class FinancialClaimsInformationDocumentV2FinancieleVerplichtingen {
  String gebeurtenis;
  String verplichtingskenmerk;
  DateTime datumtijdOpgelegd;
  String zaakkenmerk;
  bool primaireVerplichting;
  String type;
  String categorie;
  int bedrag;
  String valuta;
  String omschrijving;
  String juridischeGrondslagOmschrijving;
  String juridischeGrondslagBron;
  String opgelegdDoor;
  String uitgevoerdDoor;

  FinancialClaimsInformationDocumentV2FinancieleVerplichtingen(
      {required this.gebeurtenis,
      required this.verplichtingskenmerk,
      required this.datumtijdOpgelegd,
      required this.zaakkenmerk,
      required this.primaireVerplichting,
      required this.type,
      required this.categorie,
      required this.bedrag,
      required this.valuta,
      required this.omschrijving,
      required this.juridischeGrondslagOmschrijving,
      required this.juridischeGrondslagBron,
      required this.opgelegdDoor,
      required this.uitgevoerdDoor});

  factory FinancialClaimsInformationDocumentV2FinancieleVerplichtingen.fromJson(
      Map<String, dynamic> json) {
    return FinancialClaimsInformationDocumentV2FinancieleVerplichtingen(
      gebeurtenis: json['gebeurtenis'],
      verplichtingskenmerk: json['verplichtingskenmerk'],
      datumtijdOpgelegd: DateTime.parse(json['datumtijd_opgelegd']),
      zaakkenmerk: json['zaakkenmerk'],
      primaireVerplichting: json['primaire_verplichting'],
      type: json['type'],
      categorie: json['categorie'],
      bedrag: json['bedrag'],
      valuta: json['valuta'],
      omschrijving: json['omschrijving'],
      juridischeGrondslagOmschrijving:
          json['juridische_grondslag_omschrijving'],
      juridischeGrondslagBron: json['juridische_grondslag_bron'],
      opgelegdDoor: json['opgelegd_door'],
      uitgevoerdDoor: json['uitgevoerd_door'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['gebeurtenis'] = gebeurtenis;
    data['verplichtingskenmerk'] = verplichtingskenmerk;
    data['datumtijd_opgelegd'] = datumtijdOpgelegd.toString();
    data['zaakkenmerk'] = zaakkenmerk;
    data['primaire_verplichting'] = primaireVerplichting;
    data['type'] = type;
    data['categorie'] = categorie;
    data['bedrag'] = bedrag;
    data['valuta'] = valuta;
    data['omschrijving'] = omschrijving;
    data['juridische_grondslag_omschrijving'] = juridischeGrondslagOmschrijving;
    data['juridische_grondslag_bron'] = juridischeGrondslagBron;
    data['opgelegd_door'] = opgelegdDoor;
    data['uitgevoerd_door'] = uitgevoerdDoor;
    return data;
  }
}

class FinancialClaimsInformationDocumentV2Betalingsverplichtingen {
  String gebeurtenis;
  String betalingsverplichtingkenmerk;
  DateTime datumtijdOpgelegd;
  String zaakkenmerk;
  int bedrag;
  String valuta;
  String betaalwijze;
  String teBetalenAan;
  String rekeningnummer;
  String rekeningnummerTenaamstelling;
  String betalingskenmerk;
  String vervaldatum;

  FinancialClaimsInformationDocumentV2Betalingsverplichtingen(
      {required this.gebeurtenis,
      required this.betalingsverplichtingkenmerk,
      required this.datumtijdOpgelegd,
      required this.zaakkenmerk,
      required this.bedrag,
      required this.valuta,
      required this.betaalwijze,
      required this.teBetalenAan,
      required this.rekeningnummer,
      required this.rekeningnummerTenaamstelling,
      required this.betalingskenmerk,
      required this.vervaldatum});

  factory FinancialClaimsInformationDocumentV2Betalingsverplichtingen.fromJson(
      Map<String, dynamic> json) {
    return FinancialClaimsInformationDocumentV2Betalingsverplichtingen(
      gebeurtenis: json['gebeurtenis'],
      betalingsverplichtingkenmerk: json['betalingsverplichtingkenmerk'],
      datumtijdOpgelegd: DateTime.parse(json['datumtijd_opgelegd']),
      zaakkenmerk: json['zaakkenmerk'],
      bedrag: json['bedrag'],
      valuta: json['valuta'],
      betaalwijze: json['betaalwijze'],
      teBetalenAan: json['te_betalen_aan'],
      rekeningnummer: json['rekeningnummer'],
      rekeningnummerTenaamstelling: json['rekeningnummer_tenaamstelling'],
      betalingskenmerk: json['betalingskenmerk'],
      vervaldatum: json['vervaldatum'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['gebeurtenis'] = gebeurtenis;
    data['betalingsverplichtingkenmerk'] = betalingsverplichtingkenmerk;
    data['datumtijd_opgelegd'] = datumtijdOpgelegd.toString();
    data['zaakkenmerk'] = zaakkenmerk;
    data['bedrag'] = bedrag;
    data['valuta'] = valuta;
    data['betaalwijze'] = betaalwijze;
    data['te_betalen_aan'] = teBetalenAan;
    data['rekeningnummer'] = rekeningnummer;
    data['rekeningnummer_tenaamstelling'] = rekeningnummerTenaamstelling;
    data['betalingskenmerk'] = betalingskenmerk;
    data['vervaldatum'] = vervaldatum;
    return data;
  }
}

class FinancialClaimsInformationDocumentV2Betalingen {
  String gebeurtenis;
  String verwerktebetalingkenmerk;
  DateTime datumtijdVerwerkt;
  String betalingskenmerk;
  DateTime datumtijdOntvangen;
  String ontvangenDoor;
  String verwerktDoor;
  int bedrag;
  String valuta;

  FinancialClaimsInformationDocumentV2Betalingen(
      {required this.gebeurtenis,
      required this.verwerktebetalingkenmerk,
      required this.datumtijdVerwerkt,
      required this.betalingskenmerk,
      required this.datumtijdOntvangen,
      required this.ontvangenDoor,
      required this.verwerktDoor,
      required this.bedrag,
      required this.valuta});

  factory FinancialClaimsInformationDocumentV2Betalingen.fromJson(
      Map<String, dynamic> json) {
    return FinancialClaimsInformationDocumentV2Betalingen(
      gebeurtenis: json['gebeurtenis'],
      verwerktebetalingkenmerk: json['verwerktebetalingkenmerk'],
      datumtijdVerwerkt: DateTime.parse(json['datumtijd_verwerkt']),
      betalingskenmerk: json['betalingskenmerk'],
      datumtijdOntvangen: DateTime.parse(json['datumtijd_ontvangen']),
      ontvangenDoor: json['ontvangen_door'],
      verwerktDoor: json['verwerkt_door'],
      bedrag: json['bedrag'],
      valuta: json['valuta'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['gebeurtenis'] = gebeurtenis;
    data['verwerktebetalingkenmerk'] = verwerktebetalingkenmerk;
    data['datumtijd_verwerkt'] = datumtijdVerwerkt.toString();
    data['betalingskenmerk'] = betalingskenmerk;
    data['datumtijd_ontvangen'] = datumtijdOntvangen.toString();
    data['ontvangen_door'] = ontvangenDoor;
    data['verwerkt_door'] = verwerktDoor;
    data['bedrag'] = bedrag;
    data['valuta'] = valuta;
    return data;
  }
}
