// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:typed_data';

import 'package:user_ui/entities/certificate_type.dart';
import 'package:user_ui/usecases/request_financial_claims_information/models/document.dart';

class Envelope {
  Document document;
  String documentSignature;
  CertificateType certificateType;
  Uint8List certificate;

  Envelope({
    required this.document,
    required this.documentSignature,
    required this.certificateType,
    required this.certificate,
  });

  factory Envelope.fromJson(Map<String, dynamic> json) {
    return Envelope(
      document: Document.fromJson(json["document"]),
      documentSignature: json["documentSignature"],
      certificate: json["certificate"],
      certificateType: CertificateType.values
          .firstWhere((e) => e.value == json["certificateType"]),
    );
  }

  Map<String, dynamic> toJson() => {
        'document': document,
        'documentSignature': documentSignature,
        'certificate': certificate,
        'certificateType': certificateType.value,
      };
}
