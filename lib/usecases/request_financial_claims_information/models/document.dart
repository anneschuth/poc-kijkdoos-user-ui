// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:user_ui/usecases/request_financial_claims_information/models/document_type.dart';

class Document {
  DocumentType type;

  Document({
    required this.type,
  });

  factory Document.fromJson(Map<String, dynamic> json) {
    return Document(
      type: DocumentType.fromJson(json["type"]),
    );
  }

  Map<String, dynamic> toJson() => {
        'type': type,
      };
}
