// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class DocumentType {
  String name;

  DocumentType({
    required this.name,
  });

  factory DocumentType.fromJson(Map<String, dynamic> json) {
    return DocumentType(
      name: json["name"],
    );
  }

  Map<String, dynamic> toJson() => {
        'name': name,
      };
}
