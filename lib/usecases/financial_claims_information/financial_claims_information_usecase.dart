// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/app_identity/app_key_pair_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_document_type_repository.dart';
import 'package:user_ui/usecases/financial_claims_information/models/financial_claim_detail_view_model.dart';
import 'package:user_ui/usecases/request_financial_claims_information/models/financial_claims_information_document_v3.dart';
import 'package:user_ui/utils/bk_encryption_helper.dart';

abstract class IFinancialClaimsInformationUsecase {
  Future<void> copyFinancialClaimsInformationInboxToStorage();
  Future<FinancialClaimDetailViewModel>
      generateFinancialClaimDetailViewModelFromStorage(
    int financialClaimsInformationStorageId,
    String zaakkenmerk,
  );
}

class FinancialClaimsInformationUsecase
    implements IFinancialClaimsInformationUsecase {
  final FinancialClaimsInformationConfigurationRepository
      financialClaimsInformationConfigurationRepository;
  final FinancialClaimsInformationInboxRepository
      financialClaimsInformationInboxRepository;
  final FinancialClaimsInformationStorageRepository
      financialClaimsInformationStorageRepository;
  final SchemeOrganizationRepository schemeOrganizationRepository;
  final Ref ref;
  final LoggingHelper loggingHelper;

  FinancialClaimsInformationUsecase(
    this.financialClaimsInformationConfigurationRepository,
    this.financialClaimsInformationInboxRepository,
    this.financialClaimsInformationStorageRepository,
    this.schemeOrganizationRepository,
    this.ref,
    this.loggingHelper,
  );

  @override
  Future<void> copyFinancialClaimsInformationInboxToStorage() async {
    var appKeyPair = await ref.read(appKeyPairRepositoryProvider.future);
    if (appKeyPair == null) {
      loggingHelper.addLog(DeviceEvent.uu_37, "App key pair not found.");
      throw Exception("app key pair not found");
    }

    var financialClaimsInformationInbox =
        await financialClaimsInformationInboxRepository
            .latestFinancialClaimsInformationInboxPerOrganization;

    for (var inboxItem
        in financialClaimsInformationInbox.where((e) => !e.copiedToStorage)) {
      var financialClaimsInformationDocument =
          await BKEncryptionHelper().rsaDecrypt(
        inboxItem.encryptedFinancialClaimsInformationDocument,
        "financial-claims-information-document",
        appKeyPair.privateKey,
      );

      financialClaimsInformationStorageRepository.add(
        inboxItem.oin,
        inboxItem.dateTimeRequested,
        inboxItem.dateTimeReceived,
        financialClaimsInformationDocument,
      );

      inboxItem = inboxItem.copyWith(copiedToStorage: true);
      financialClaimsInformationInboxRepository.update(inboxItem);
    }
  }

  @override
  Future<FinancialClaimDetailViewModel>
      generateFinancialClaimDetailViewModelFromStorage(
    int financialClaimsInformationStorageId,
    String zaakkenmerk,
  ) async {
    var financialClaimsInformationStorage =
        await financialClaimsInformationStorageRepository
            .getById(financialClaimsInformationStorageId);

    var financialClaimsInformationDocumentJson = json.decode(
        financialClaimsInformationStorage.financialClaimsInformationDocument);

    var financialClaimsInformationDocument =
        FinancialClaimsInformationDocument.fromJson(
            financialClaimsInformationDocumentJson);

    var financieleZaak = financialClaimsInformationDocument.body.financieleZaken
        .singleWhere((e) => e.zaakkenmerk == zaakkenmerk);

    var primaireFinancieleVerplichting = financieleZaak.gebeurtenissen
            .firstWhere((f) =>
                f.runtimeType == FinancieleVerplichtingOpgelegd &&
                (f as FinancieleVerplichtingOpgelegd).primaireVerplichting)
        as FinancieleVerplichtingOpgelegd;

    var title = getHumanReadableName(primaireFinancieleVerplichting.type);

    var subTitle = "Kenmerk: ${primaireFinancieleVerplichting.zaakkenmerk}";

    var info = primaireFinancieleVerplichting.omschrijving;

    var saldo = financieleZaak.saldo;

    var isResolved = saldo == 0;

    financieleZaak.gebeurtenissen.sort(
      (a, b) => a.datumtijdGebeurtenis.compareTo(b.datumtijdGebeurtenis),
    );

    var betalingGebeurtenissen = <String, DateTime>{};

    DateTime? dueDate;

    var events = <FinancialClaimEvent>[];

    for (var gebeurtenis in financieleZaak.gebeurtenissen) {
      switch (gebeurtenis.runtimeType) {
        case FinancieleVerplichtingOpgelegd:
          gebeurtenis as FinancieleVerplichtingOpgelegd;

          events.add(
            FinancialClaimEvent(
              description: getHumanReadableName(gebeurtenis.type),
              amount: gebeurtenis.bedrag,
              dateTime: gebeurtenis.datumtijdGebeurtenis,
            ),
          );
          break;
        case BetalingsverplichtingOpgelegd:
          gebeurtenis as BetalingsverplichtingOpgelegd;

          betalingGebeurtenissen[gebeurtenis.gebeurtenisKenmerk] =
              gebeurtenis.vervaldatum;

          break;
        case BetalingsverplichtingIngetrokken:
          gebeurtenis as BetalingsverplichtingIngetrokken;

          betalingGebeurtenissen
              .remove(gebeurtenis.ingetrokkenGebeurtenisKenmerk);

          break;
        case BetalingVerwerkt:
          gebeurtenis as BetalingVerwerkt;

          events.add(
            FinancialClaimEvent(
              description: "Betaling",
              amount: -gebeurtenis.bedrag,
              dateTime: gebeurtenis.datumtijdGebeurtenis,
            ),
          );
          break;
        default:
          loggingHelper.addLog(DeviceEvent.uu_38,
              "unknown gebeurtenis.runtimeType: ${gebeurtenis.runtimeType}");
          throw Exception(
              "unknown gebeurtenis.runtimeType: ${gebeurtenis.runtimeType}");
      }

      if (betalingGebeurtenissen.keys.isNotEmpty) {
        dueDate = betalingGebeurtenissen.values.last;
      }
    }

    var isOverdue = financieleZaak.achterstanden.isNotEmpty;

    return FinancialClaimDetailViewModel(
      title: title,
      subTitle: subTitle,
      info: info,
      saldo: saldo,
      isResolved: isResolved,
      isOverdue: isOverdue,
      dueDate: dueDate,
      events: events,
    );
  }

  String getHumanReadableName(String financialClaimType) {
    switch (financialClaimType) {
      case "DUO_LLSF":
        return "Studieschuld";
      case "CJIB_WAHV":
        return "Verkeersboete";
      case "CJIB_ADMINISTRATIEKOSTEN":
        return "Administratiekosten";
      case "CJIB_KOSTEN_EERSTE_AANMANING":
        return "Kosten eerste aanmaning";
      case "BD_Inkomensheffing":
        return "Heffing inkomstenbelasting";
      default:
        return financialClaimType;
    }
  }
}

final financialClaimsInformationUsecaseProvider =
    Provider<IFinancialClaimsInformationUsecase>((ref) {
  final financialClaimsInformationConfigurationRepository = ref.watch(
      financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final financialClaimsInformationInboxRepository =
      ref.watch(financialClaimsInformationInboxRepositoryProvider.notifier);

  final financialClaimsInformationStorageRepository =
      ref.watch(financialClaimsInformationStorageRepositoryProvider.notifier);

  final schemeOrganizationRepository =
      ref.watch(schemeOrganizationRepositoryProvider.notifier);
  final logging = ref.watch(loggingProvider);

  return FinancialClaimsInformationUsecase(
    financialClaimsInformationConfigurationRepository,
    financialClaimsInformationInboxRepository,
    financialClaimsInformationStorageRepository,
    schemeOrganizationRepository,
    ref,
    logging,
  );
});
