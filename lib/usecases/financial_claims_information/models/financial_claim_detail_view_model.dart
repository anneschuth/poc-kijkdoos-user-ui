// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class FinancialClaimEvent {
  String description;
  int amount;
  DateTime dateTime;

  FinancialClaimEvent({
    required this.description,
    required this.amount,
    required this.dateTime,
  });
}

class FinancialClaimDetailViewModel {
  String title;
  String subTitle;
  String info;
  int saldo;
  bool isResolved;
  bool isOverdue;
  DateTime? dueDate;
  List<FinancialClaimEvent> events;

  FinancialClaimDetailViewModel({
    required this.title,
    required this.subTitle,
    required this.info,
    required this.saldo,
    required this.isResolved,
    required this.isOverdue,
    required this.dueDate,
    required this.events,
  });
}
