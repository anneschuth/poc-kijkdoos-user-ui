import 'dart:ui';

import 'package:flutter/material.dart';

/// Enables dragging with mouse, so pull to refresh works on Web.
class CustomScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}
