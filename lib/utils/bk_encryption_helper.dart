// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL
import 'package:fast_rsa/fast_rsa.dart' as fast_rsa;
import 'package:user_ui/utils/rsa_keypair.dart';

abstract class BKEncryption {
  Future<RSAKeypair> generateRSAKeypair();
  Future<String> rsaEncrypt(
    String message,
    String label,
    String publicKeyPem,
  );
  Future<String> rsaDecrypt(
    String cipherText,
    String label,
    String privateKeyPem,
  );
  Future<String> sign(
    String message,
    String privateKeyPem,
  );
  Future<bool> verify(
    String signature,
    String message,
    String publicKeyPem,
  );
}

class BKEncryptionHelper implements BKEncryption {
  @override
  Future<RSAKeypair> generateRSAKeypair() async {
    final keypair = await fast_rsa.RSA.generate(2048);
    return RSAKeypair(keypair.privateKey, keypair.publicKey);
  }

  @override
  Future<String> rsaEncrypt(
    String message,
    String label,
    String publicKeyPem,
  ) async {
    return await fast_rsa.RSA.encryptOAEP(
      message,
      label,
      fast_rsa.Hash.SHA512,
      publicKeyPem,
    );
  }

  @override
  Future<String> rsaDecrypt(
    String cipherText,
    String label,
    String privateKeyPem,
  ) async {
    return await fast_rsa.RSA.decryptOAEP(
      cipherText,
      label,
      fast_rsa.Hash.SHA512,
      privateKeyPem,
    );
  }

  @override
  Future<String> sign(
    String message,
    String privateKeyPem,
  ) async {
    return await fast_rsa.RSA.signPSS(
      message,
      fast_rsa.Hash.SHA512,
      fast_rsa.SaltLength.EQUALS_HASH,
      privateKeyPem,
    );
  }

  @override
  Future<bool> verify(
    String signature,
    String message,
    String publicKeyPem,
  ) async {
    return await fast_rsa.RSA.verifyPSS(
      signature,
      message,
      fast_rsa.Hash.SHA512,
      fast_rsa.SaltLength.EQUALS_HASH,
      publicKeyPem,
    );
  }
}
