import 'package:go_router/go_router.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/app_startup/startup_screen.dart';
import 'package:user_ui/screens/features/error/error_log_screen.dart';
import 'package:user_ui/screens/features/financial_claim_detail/financial_claim_detail_screen.dart';
import 'package:user_ui/screens/features/error/error_message_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/activation/complete_activation_screen.dart';
import 'package:user_ui/screens/features/activation/start_activation_screen.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/screens/features/onboarding/data_use_screen.dart';
import 'package:user_ui/screens/features/onboarding/propositon_screen.dart';
import 'package:user_ui/repositories/app_identity/app_key_pair_repository.dart';
import 'package:user_ui/screens/features/organization_selection/auto_select_all_organizations_screen.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen.dart';
import 'package:user_ui/screens/features/overview/financial_claims_overview_screen.dart';

final goRouterProvider = Provider<GoRouter>((ref) {
  return GoRouter(
    initialLocation: '/',
    redirect: (context, state) async {
      // redirect user to key generation or onboarding if not completed yet
      var appKeyPair = await ref.read(appKeyPairRepositoryProvider.future);

      if (appKeyPair == null) {
        return '/voorbereiden';
      } else {
        return null;
      }
    },
    routes: <RouteBase>[
      GoRoute(
        name: HomeScreen.routeName,
        path: '/',
        builder: (context, state) => const HomeScreen(),
        routes: <RouteBase>[
          GoRoute(
            name: StartupScreen.routeName,
            path: 'voorbereiden',
            builder: (context, state) => const StartupScreen(),
          ),
          GoRoute(
            name: PropositionScreen.routeName,
            path: 'wat-heb-je-aan-de-app',
            builder: (context, state) => const PropositionScreen(),
          ),
          GoRoute(
            name: DataUseScreen.routeName,
            path: 'zo-gaat-de-app-om-met-je-data',
            builder: (context, state) => const DataUseScreen(),
          ),
          GoRoute(
            name: AutoSelectAllOrganizationsScreen.routeName,
            path: 'organisaties-kiezen',
            builder: (context, state) =>
                const AutoSelectAllOrganizationsScreen(),
          ),
          GoRoute(
            name: ActivationInfoScreen.routeName,
            path: 'activation-informatie',
            builder: (context, state) => const ActivationInfoScreen(),
          ),
          GoRoute(
            name: OrganizationSelectionScreen.routeName,
            path: 'organisaties-kiezen-2',
            builder: (context, state) => const OrganizationSelectionScreen(),
          ),
          GoRoute(
            name: StartActivationScreen.routeName,
            path: 'activatie-starten',
            builder: (context, state) => const StartActivationScreen(),
          ),
          GoRoute(
            name: CompleteActivationScreen.routeName,
            path: 'activatie-voltooien',
            builder: (context, state) => const CompleteActivationScreen(),
          ),
          GoRoute(
            name: LoadingScreen.routeName,
            path: 'gegevens-ophalen',
            builder: (context, state) => const LoadingScreen(),
          ),
          GoRoute(
            name: FinancialClaimsOverviewScreen.routeName,
            path: 'overzicht',
            builder: (context, state) => const FinancialClaimsOverviewScreen(),
          ),
          GoRoute(
            name: FinancialClaimDetailScreen.routeName,
            path: 'detail/:financialClaimsInformationStorageId/:zaakkenmerk',
            builder: (context, state) => FinancialClaimDetailScreen(
              financialClaimsInformationStorageId: int.parse(
                  state.pathParameters["financialClaimsInformationStorageId"]!),
              zaakkenmerk: state.pathParameters["zaakkenmerk"]!,
            ),
          ),
          GoRoute(
            name: ErrorMessageScreen.routeName,
            path: 'foutmelding',
            builder: (context, state) =>
                ErrorMessageScreen(state.error.toString()),
          ),
          GoRoute(
            name: ErrorLogScreen.routeName,
            path: 'foutenlogboek',
            builder: (context, state) => const ErrorLogScreen(),
          ),
        ],
      ),
    ],
    errorBuilder: (context, state) =>
        ErrorMessageScreen(state.error.toString()),
  );
});
