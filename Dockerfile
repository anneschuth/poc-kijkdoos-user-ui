FROM ubuntu:20.04 AS build

# Install build-time dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    git curl unzip ca-certificates \
    && rm -rf /var/lib/apt/lists/*

# Install Flutter 3.13.7
RUN git clone --single-branch --depth=1 --branch 3.13.7 https://github.com/flutter/flutter /opt/flutter 2>&1 \
    && ln -sf /opt/flutter/bin/flutter /usr/local/bin/flutter \
    && flutter doctor -v

# Copy and compile app
COPY . /app/
WORKDIR /app/
RUN flutter build web --pwa-strategy=none
# RUN flutter build web --pwa-strategy=none -t lib/main_local.dart

FROM nginx:stable-alpine
COPY --from=build /app/build/web /usr/share/nginx/html

WORKDIR /etc/nginx
COPY ./nginx/env.template.json ./env.template.json
COPY ./nginx/substituteEnv.sh ./substituteEnv.sh

RUN chmod 777 ./substituteEnv.sh
RUN chmod 777 /usr/share/nginx/html/assets/assets/env.json

WORKDIR /etc/nginx
COPY ./nginx/.htpasswd /etc/nginx/.htpasswd
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

# begin Openschift changes
# support running as arbitrary user which belogs to the root group
RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
# users are not allowed to listen on priviliged ports
RUN sed -i.bak 's/listen\(.*\)80;/listen 8008;/' /etc/nginx/conf.d/default.conf
EXPOSE 8008
# comment user directive as master process is run as user in OpenShift anyhow
RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf
# end Openschift changes
